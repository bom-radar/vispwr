# Packaging

This directory contains files needed for packaging Vispwr on various platforms.  Currently we support the generation
of binary packages for Fedora and Ubuntu.  Support for Windows and Mac is desired.

## Notes
Some random notes as we work towards being able to compile and package on Windows:
- Install Qt's development toolkit.  This includes a MinGW environment that can be used via the command line.
- Install CMake's windows distribution.
- Run cmake with the `-G 'MinGW Makefiles'` argument
- Build using `mingw32-make`
- For FFTW
  - Download and unpack the windows DLL package from the FFTW website
  - Run `dlltool.exe -d libfftw3-3.def -l libfftw3-3.a` to generate the `.a` library file needed
  - Pass `-DFFTW_ROOT=<path>` setting the path to the directory you containing the `.a` file

