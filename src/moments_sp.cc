/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "calibration.h"

using namespace vispwr;

namespace vispwr
{
  /// Calculates single polarisation moments
  class moments_sp : public stage
  {
  protected:
    moments_sp();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<autocorrelations const> data, int input) -> void override;

  private:
    int     calib_channel_;
    double  log_threshold_;            // TODO - make thresholding a downstream process??? or one per moment???
    double  sqi_threshold_;            // TODO - make thresholding a downstream process??? or one per moment???
  };
}

static moment_list_ptr moments_h = std::make_shared<moment_list>(std::initializer_list<string>
{
    "DBZH"
  , "VRADH"
  , "WRADH"
  , "SQIH"
  , "SNRH"
});

static moment_list_ptr moments_v = std::make_shared<moment_list>(std::initializer_list<string>
{
    "DBZV"
  , "VRADV"
  , "WRADV"
  , "SQIV"
  , "SNRV"
});

static moment_list_ptr moments_hv = std::make_shared<moment_list>(std::initializer_list<string>
{
    "DBZH"
  , "VRADH"
  , "WRADH"
  , "SQIH"
  , "SNRH"
  , "DBZV"
  , "VRADV"
  , "WRADV"
  , "SQIV"
  , "SNRV"
  , "ZDR"
  , "PHIDP"
  , "RHOHV"
});

static constexpr auto plist = std::array<string_view, 3>{ "Calib", "LOG Threshold", "SQI Threshold" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "acs", data_type::autocorrelations, true, "Input autocorrelations" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
    { "moments", data_type::moments, "Output moment data" }
}};

static const auto desc = R"(
Standard radar moment calculation.  Generates DBZ, VRAD, WRAD, ZDR, RhoHV, PhiDP, SNR, and SQI.  Moments are generated
for both Horizontal and Verical channels where appropriate.

**Input:** Autocorrelations

**Output:** Moments
)";

static auto ireg = stage::enrol<moments_sp>("moments_sp", "Moments", "Moments", desc);

moments_sp::moments_sp()
  : stage{plist, ilist, olist}
  , calib_channel_{0}
  , log_threshold_(-40.0)
  , sqi_threshold_{0.0}
{ }

auto moments_sp::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto moments_sp::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Calib")
    return parameter_integer
    {
      .name = "Calib",
      .description = "Calibration channel",
      .min = 0,
      .max = 10,
      .value = calib_channel_,
    };
  if (name == "LOG Threshold")
    return parameter_real
    {
      .name = "LOG Threshold",
      .description = "",
      .min = -60.0,
      .max = 90.0,
      .value = log_threshold_
    };
  if (name == "SQI Threshold")
    return parameter_real
    {
      .name = "SQI Threshold",
      .description = "",
      .min = 0.0,
      .max = 1.0,
      .step = 0.05,
      .value = sqi_threshold_,
    };
  return {};
}

auto moments_sp::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Calib")
    calib_channel_ = std::get<int>(value);
  else if (name == "LOG Threshold")
    log_threshold_ = std::get<double>(value);
  else if (name == "SQI Threshold")
    sqi_threshold_ = std::get<double>(value);
}

template <bool has_h, bool has_v>
static auto calculate_moments(calibration const& calib, autocorrelations const& acs, moments& m, double th_log, double th_sqi) -> void
{
  m.data.resize(m.gate_count, m.moments->size());
  for (auto igate = 0uz; igate < m.gate_count; ++igate)
  {
    auto range = acs.range_start + igate * acs.range_step;

    double log_h, log_v;
    if constexpr (has_h)
    {
      // reflectivity
      /* TODO - not sure that matched filter losses here are coming in as dB... Leonardo docs seem to suggest
       * that this value is the plain before/after ratio - so in linear units.  they certainly never state that
       * it's a quantity in dB from what I can see.  Also looking at IQR files I never see a value less than 1
       * which suggests maybe this is a linear quantity?  which means we need to convert to dB here first before
       * adding (i think). */
      m.data[igate, 0]
        = 10.0 * std::log10(acs.autocors_h[igate].lag0.real() - calib.noise_power_h)
        //= 10.0 * std::log10(std::abs(acs.autocors_h[igate].lag1) - calib.noise_power_h)
        + calib.sp_calib_h
        + calib.radar_constant_h
        + 20.0 * std::log10(range)
        + calib.atmospheric_attenuation * range
        + acs.matched_filter_losses_h;

      // doppler velocity
      m.data[igate, 1] = (m.nyquist / pi) * std::arg(acs.autocors_h[igate].lag1);

      // spectrum width
      m.data[igate, 2] = (m.nyquist / pi) * std::sqrt((2.0 / 3.0) * std::log(
              std::abs(acs.autocors_h[igate].lag1) / std::abs(acs.autocors_h[igate].lag2)));

      // sqi
      m.data[igate, 3] = std::abs(acs.autocors_h[igate].lag1) / std::abs(acs.autocors_h[igate].lag0);

      // snr
      m.data[igate, 4] = 10.0 * std::log10(acs.autocors_h[igate].lag0.real() / calib.noise_power_h);

      // thresholding (TODO - move elsewhere?)
      log_h = 10.0 * std::log10((acs.autocors_h[igate].lag0.real() - calib.noise_power_h) / calib.noise_power_h);
      if (!(log_h >= th_log) || !(m.data[igate, 3] >= th_sqi))
      {
        m.data[igate, 0] = std::numeric_limits<float>::quiet_NaN();
        m.data[igate, 1] = std::numeric_limits<float>::quiet_NaN();
        m.data[igate, 2] = std::numeric_limits<float>::quiet_NaN();
      }
    }

    if constexpr (has_v)
    {
      auto os = has_h ? 5 : 0;

      // reflectivity
      m.data[igate, os + 0]
        = 10.0 * std::log10(acs.autocors_v[igate].lag0.real() - calib.noise_power_v)
        //= 10.0 * std::log10(std::abs(acs.autocors_v[igate].lag1) - calib.noise_power_v)
        + calib.sp_calib_v
        + calib.radar_constant_v
        + 20.0 * std::log10(range)
        + calib.atmospheric_attenuation * range
        + acs.matched_filter_losses_v;

      // doppler velocity
      m.data[igate, os + 1] = (m.nyquist / pi) * std::arg(acs.autocors_v[igate].lag1);

      // spectrum width
      m.data[igate, os + 2] = (m.nyquist / pi) * std::sqrt((2.0 / 3.0) * std::log(
              std::abs(acs.autocors_v[igate].lag1) / std::abs(acs.autocors_v[igate].lag2)));

      // sqi
      m.data[igate, os + 3] = std::abs(acs.autocors_v[igate].lag1) / std::abs(acs.autocors_v[igate].lag0);

      // snr
      m.data[igate, os + 4] = 10.0 * std::log10(acs.autocors_v[igate].lag0.real() / calib.noise_power_v);

      // thresholding (TODO - move elsewhere?)
      log_v = 10.0 * std::log10((acs.autocors_v[igate].lag0.real() - calib.noise_power_v) / calib.noise_power_v);
      if (!(log_v >= th_log) || !(m.data[igate, os + 3] >= th_sqi))
      {
        m.data[igate, os + 0] = std::numeric_limits<float>::quiet_NaN();
        m.data[igate, os + 1] = std::numeric_limits<float>::quiet_NaN();
        m.data[igate, os + 2] = std::numeric_limits<float>::quiet_NaN();
      }
    }

    if constexpr (has_h && has_v)
    {
      // zdr
      m.data[igate, 10] = calib.zdr_offset + 10.0 * std::log10(
            (acs.autocors_h[igate].lag0.real() - calib.noise_power_h)
          / (acs.autocors_v[igate].lag0.real() - calib.noise_power_v));

      // phidp (TODO - checkthe mod and + 360 here...)
      m.data[igate, 11] = std::fmod(((std::arg(acs.crosscors[igate].lag0) * (180.0 / pi)) + calib.phidp_offset) + 360.0, 360.0);

      // rhohv
      m.data[igate, 12] = std::abs(acs.crosscors[igate].lag0) / std::sqrt(
            std::abs(acs.autocors_h[igate].lag0.real() - calib.noise_power_h)
          * std::abs(acs.autocors_v[igate].lag0.real() - calib.noise_power_v));

      // thresholding (TODO - move elsewhere?)
      if (   !(log_h >= th_log) || !(m.data[igate, 3] >= th_sqi)
          || !(log_v >= th_log) || !(m.data[igate, 8] >= th_sqi))
      {
        m.data[igate, 10] = std::numeric_limits<float>::quiet_NaN();
        m.data[igate, 11] = std::numeric_limits<float>::quiet_NaN();
        m.data[igate, 12] = std::numeric_limits<float>::quiet_NaN();
      }
    }
  }
}

auto moments_sp::process(shared_ptr<autocorrelations const> data, int input) -> void
{
  auto calib = calibration_get(calib_channel_);

  // if we are missing an input it means the stage is terminating
  if (!data || !calib)
    return;

  auto m = make_shared<moments>();

  // since we are 1:1 input/output we can just propogate the autocor ID as our ID
  m->id = data->id;
  m->autocors_id = data->id;
  m->batch_id = data->batch_id;
  m->elevation_first = data->elevation_first;
  m->elevation_last = data->elevation_last;
  m->azimuth_first = data->azimuth_first;
  m->azimuth_last = data->azimuth_last;
  m->range_start = data->range_start;
  m->range_step = data->range_step;
  m->gate_count = data->gate_count;
  m->nyquist = (data->prf * calib->wavelength) / 4.0;

  if (data->autocors_h.size() != 0 && data->autocors_v.size() != 0)
  {
    m->moments = moments_hv;
    calculate_moments<true, true>(*calib, *data, *m, log_threshold_, sqi_threshold_);
  }
  else if (data->autocors_h.size() != 0)
  {
    m->moments = moments_h;
    calculate_moments<true, false>(*calib, *data, *m, log_threshold_, sqi_threshold_);
  }
  else if (data->autocors_v.size() != 0)
  {
    m->moments = moments_v;
    calculate_moments<false, true>(*calib, *data, *m, log_threshold_, sqi_threshold_);
  }

  send(0, std::move(m));
}
