/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"
#include "ui_raster.h"
#include "ui_style.h"

#include <QImage>
#include <QPen>
#include <QRectF>

using namespace vispwr;

namespace vispwr
{
  struct ray_data
  {
    float   elevation_l;  // low elevation edge (not received order)
    float   elevation_h;  // high elevation edge (not received order)
    int     ray;          // index of row containing ray pixels in scan_data::data
  };

  using elevation_store = vector<ray_data>;

  inline auto find_ray(elevation_store const& store, float elevation) -> int
  {
    auto iray = std::ranges::lower_bound(store, elevation, {}, &ray_data::elevation_h);
    if (iray == store.end())
      return -1;
    /* if the previous ray ended 'very close' to the start of this ray then make it continuous to avoid a seam between
     * the rays.  otherwise just comare to our elevation_l.  i'm using a 'very close' of 10% of the ray width. */
    if (iray != store.begin() && iray->elevation_l - std::prev(iray)->elevation_h < 0.1f * (iray->elevation_h - iray->elevation_l))
      return iray->ray;
    return elevation < iray->elevation_l ? -1 : iray->ray;
  }

  struct scan_data
  {
    elevation_store elevations;     // rays sorted by elevation_h
    float           range_start;
    float           range_step;
    array2f         data;         // rays in received order
  };

  class rhi_scene : public ui_raster_scene
  {
    Q_OBJECT
  public:
    rhi_scene(QObject* parent = nullptr);

    auto scene_to_polar(QPointF pos) const -> optional<polar_coordinates> override;
    auto polar_to_scene(polar_coordinates coords) const -> QPointF override;
    auto pick_value(polar_coordinates coords) const -> optional<float> override;

  signals:
    void data_updated(shared_ptr<scan_data> data);

  protected:
    auto drawBackground(QPainter* painter, QRectF const& rect) -> void override;

    auto on_data_updated(shared_ptr<scan_data> data) -> void;

  private:
    QPen      axes_pen_;
    QPen      rings_pen_;
    double    rings_spacing_;

    shared_ptr<scan_data> data_;
  };

  class view_rhi : public stage
  {
  protected:
    view_rhi();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<state const> data, int input) -> void override;
    auto process(shared_ptr<moments const> data, int input) -> void override;

  private:
    ui_raster_window      window_;
    string                moment_;

    moment_list_ptr       moment_list_;     // most recently received moment list
    int                   imoment_;         // index of 'moment_' in most recently received moment list

    shared_ptr<scan_data> data_;
    size_t                rays_;
    size_t                gates_;
  };
}

rhi_scene::rhi_scene(QObject* parent)
  : ui_raster_scene{parent}
  , axes_pen_{ui_style::get().ppi_axes_color, ui_style::get().ppi_axes_width, Qt::SolidLine}
  , rings_pen_{ui_style::get().ppi_rings_color, ui_style::get().ppi_rings_width, Qt::DotLine}
  , rings_spacing_{10.0}
{
  axes_pen_.setCosmetic(true);
  rings_pen_.setCosmetic(true);

  // just to get default range setup
  on_data_updated(nullptr);

  // this connection simply ensures our data update is received on the GUI thread
  connect(this, &rhi_scene::data_updated, this, &rhi_scene::on_data_updated);
}

auto rhi_scene::scene_to_polar(QPointF pos) const -> optional<polar_coordinates>
{
  // TODO - check this
  auto elevation = std::atan2(-pos.y(), pos.x()) * (180.0 / pi);
  if (elevation < 0.0)
    elevation += 360.0;
  auto range = std::sqrt(pos.x() * pos.x() + pos.y() * pos.y());
  return polar_coordinates{float(elevation), 180.0f, float(range)}; // TODO azimuth
}

auto rhi_scene::polar_to_scene(polar_coordinates coords) const -> QPointF
{
  // elevation ignored for a rhi
  auto theta = coords.elevation * (pi / 180.0);
  auto x = coords.range * std::cos(theta);
  auto y = coords.range * -std::sin(theta);
  return QPointF{x, y};
}

auto rhi_scene::pick_value(polar_coordinates coords) const -> optional<float>
{
  // elevation ignored for a rhi
  if (!data_)
    return nullopt;
  auto gate = gate_from_range(data_->range_start, data_->range_step, coords.range);
  if (gate >= data_->data.shape()[1])
    return nullopt;
  auto ray = find_ray(data_->elevations, coords.elevation);
  if (ray < 0)
    return nullopt;
  return data_->data[ray, gate];
}

auto rhi_scene::drawBackground(QPainter* painter, QRectF const& rect) -> void
{
  painter->setClipRect(rect);

  // determine the exposed region size in pixels
  auto size = painter->combinedTransform().mapRect(rect).size();

  // create an image of this size to render out patch
  auto image = QImage{int(std::ceil(size.width())), int(std::ceil(size.height())), QImage::Format::Format_RGB888};

  // handle before first scan received
  if (!data_ || data_->data.size() == 0)
  {
    image.fill(color_missing());
    painter->drawImage(rect, image);
    return;
  }

  // write our pixels
  for (auto y = 0; y < image.height(); ++y)
  {
    for (auto x = 0; x < image.width(); ++x)
    {
      // scale our x,y from image pixel to world coordinates
      auto yy = rect.y() + ((y + 0.5f) / image.height()) * rect.height();
      auto xx = rect.x() + ((x + 0.5f) / image.width()) * rect.width();

      // convert from x,y to range, elevation
      auto coords = rhi_scene::scene_to_polar(QPointF{xx, yy});
      if (!coords)
      {
        image.setPixelColor(x, y, color_missing());
        continue;
      }

      // find the range bin at this range
      auto gate = gate_from_range(data_->range_start, data_->range_step, coords->range);
      if (gate >= data_->data.shape()[1])
      {
        image.setPixelColor(x, y, color_missing());
        continue;
      }

      // find a ray at this elevation
      auto ray = find_ray(data_->elevations, coords->elevation);
      if (ray < 0)
      {
        image.setPixelColor(x, y, color_missing());
        continue;
      }

      // valid location in our dataset, so determine and output the color
      image.setPixelColor(x, y, determine_color(data_->data[ray, gate]));
    }
  }

  // draw the image to the area of interest
  painter->drawImage(rect, image);

  // draw the grid lines and range rings
  {
    auto max_range = width() / 2.0;

    //painter->setRenderHint(QPainter::Antialiasing);
    //painter->setCompositionMode(QPainter::RasterOp_SourceXorDestination);

    painter->setPen(axes_pen_);
    painter->drawLine(-max_range, 0.0, max_range, 0.0);
    painter->drawLine(0.0, -max_range, 0.0, max_range);

    painter->setPen(rings_pen_);
    for (auto radius = rings_spacing_; radius <= max_range; radius += rings_spacing_)
      painter->drawEllipse(QPointF{0.0, 0.0}, radius, radius);
  }
}

auto rhi_scene::on_data_updated(shared_ptr<scan_data> data) -> void
{
  data_ = std::move(data);

  // recalculate the maximum range

  // TODO - determine the range at ray closest to 0, and closest to 180

  auto range = data_ ? data_->range_start + data_->range_step * data_->data.shape()[1] : 100;
  setSceneRect(-range, -range, range * 2, range * 2);

  // update all of our pickers so they display the new value
  update_picks();

  update();
}

static constexpr auto plist = std::array<string_view, 3>{ "Moment", "Display", "geometry" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "moments", data_type::moments, true, "Input moments" }
}};
static constexpr auto olist = std::array<output_meta, 0>
{{
}};

static const auto desc = R"(
View moment data in a side-on RHI style view, where the X axis represents ground range and Y axis represents altitude.

**Input:** Moments

**Output:** None
)";

static auto ireg = stage::enrol<view_rhi>("view_rhi", "RHI", "Display", desc);

view_rhi::view_rhi()
  : stage{plist, ilist, olist}
  , window_{make_unique<rhi_scene>(), "RHI", QSize{600, 300}}
  , moment_{"DBZH"}
  , imoment_{-1}
  , rays_{0}
  , gates_{0}
{
  QObject::connect(&window_, &ui_raster_window::window_hidden, [this]() { notify_parameter_update("Display"); });
}

auto view_rhi::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto view_rhi::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Moment")
    return parameter_enumerate
    {
      .name = "Moment",
      .description = "Moment to display",
      .values = master_moment_list,
      .value = moment_,
    };
  if (name == "Display")
    return parameter_trigger
    {
      .name = "Display",
      .description = "Open or close display window",
      .label = window_.isVisible() ? "Hide" : "Show",
    };
  if (name == "geometry")
    return parameter_string
    {
      .name = "geometry",
      .description = "Window geometry",
      .visible = false,
      .value = fmt::format("{} {} {} {}", window_.x(), window_.y(), window_.width(), window_.height()),
    };
  return {};
}

auto view_rhi::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Moment")
  {
    moment_ = std::get<string>(value);
    imoment_ = -1;

    if (moment_list_)
    {
      auto imom = std::find(moment_list_->begin(), moment_list_->end(), moment_);
      if (imom != moment_list_->end())
        imoment_ = imom - moment_list_->begin();
    }

    // tell the underlying scene to load a new color map and redraw
    window_.scene()->moment_updated(moment_);
  }
  else if (name == "Display")
  {
    if (std::get<bool>(value) == true)
    {
      window_.setVisible(!window_.isVisible());
      notify_parameter_update("Display");
    }
  }
  else if (name == "geometry")
  {
    int x, y, w, h;
    if (sscanf(std::get<string>(value).c_str(), "%d %d %d %d", &x, &y, &w, &h) != 4)
      throw std::runtime_error{"Bad window geometry"};
    window_.setGeometry(x, y, w, h);
  }
}

auto view_rhi::process(shared_ptr<state const> data, int input) -> void
{
  if (data->name == "iq-end" && data_)
  {
    // trim to final array size
    data_->elevations.resize(rays_);
    data_->data.resize_preserve(rays_, gates_, std::numeric_limits<float>::quiet_NaN());

    // eliminate degenerates ray(s) that wrap around from 359 to 0 by splitting it
    for (auto i = 0uz; i < data_->elevations.size(); ++i)
    {
      if (data_->elevations[i].elevation_l < 10.0f && data_->elevations[i].elevation_h > 350.0f)
      {
        data_->elevations.insert(data_->elevations.begin() + i, data_->elevations[i]);
        data_->elevations[i].elevation_l = data_->elevations[i].elevation_h;
        data_->elevations[i].elevation_h = 360.0f;
        data_->elevations[i + 1].elevation_h = data_->elevations[i + 1].elevation_l;
        data_->elevations[i + 1].elevation_l = 0.0f;
        ++i;
      }
    }

    // eliminate any overscanned rays
    /* we keep the more recent rays since we assume the rays at the start are transition rays that were recorded
     * while waiting for the antenna to stabilize. */
    auto overscan_rays = 0uz;
    while (overscan_rays + 1 < data_->elevations.size())
    {
      // see if we have a ray that overlaps the same median elevation (search from end for efficiency)
      auto& osr = data_->elevations[overscan_rays];
      auto median = osr.elevation_l + (osr.elevation_h - osr.elevation_l) * 0.5;
      auto iray = std::find_if(data_->elevations.begin() + overscan_rays + 1, data_->elevations.end(), [&](auto& v)
      {
        return median >= v.elevation_l && median <= v.elevation_h;
      });
      if (iray == data_->elevations.end())
        break;
      ++overscan_rays;
    }
    data_->elevations.erase(data_->elevations.begin(), data_->elevations.begin() + overscan_rays);

    // sort our ray index for efficient use in the display
    // we sort on elevation_h because that's what we use for the std::lower_bound lookup
    std::ranges::sort(data_->elevations, {}, &ray_data::elevation_h);

    // send to the window for display
    static_cast<rhi_scene*>(window_.scene())->data_updated(std::move(data_));
    data_.reset();
  }
}

auto view_rhi::process(shared_ptr<moments const> data, int input) -> void
{
  // start a new sweep?
  if (!data_)
  {
    // initialize a new scan and assume it will be the same size as the last
    data_ = make_shared<scan_data>();
    data_->elevations.resize(rays_, {0.0f, 0.0f, -1});
    data_->range_start = data->range_start;
    data_->range_step = data->range_step;
    data_->data.resize_preserve(rays_, gates_, std::numeric_limits<float>::quiet_NaN());

    // reset the ray and gate counters
    rays_ = 0;
    gates_ = 0;
  }

  // update the moment index if our moment list has changed
  if (moment_list_ != data->moments)
  {
    moment_list_ = data->moments;
    imoment_ = -1;

    auto imom = std::find(moment_list_->begin(), moment_list_->end(), moment_);
    if (imom != moment_list_->end())
      imoment_ = imom - moment_list_->begin();
  }

  auto iray = rays_;

  // update our ray and gate counters
  ++rays_;
  gates_ = std::max(gates_, data->gate_count);

  // expand the data stores as needed to accomodate the ray
  if (data_->elevations.size() < rays_)
    data_->elevations.resize(rays_);
  if (data_->data.shape()[0] < rays_ || data_->data.shape()[1] < gates_)
    data_->data.resize_preserve(rays_, gates_, std::numeric_limits<float>::quiet_NaN());

  // copy in our new points
  data_->elevations[iray].elevation_l = std::min(data->elevation_first, data->elevation_last);
  data_->elevations[iray].elevation_h = std::max(data->elevation_first, data->elevation_last);
  data_->elevations[iray].ray = iray;
  for (auto igate = 0uz; igate < data->gate_count; ++igate)
    data_->data[iray, igate] = imoment_ != -1 ? data->data[igate, imoment_] : std::numeric_limits<float>::quiet_NaN();

  // HACK HACK
  // fill the end of the ray with NaNs to stop visual artefacts for dual-prf in the high-prf only area
  // THIS IS BAD - it will break second trip recovery!!!
  for (auto igate = data->gate_count; igate < gates_; ++igate)
    data_->data[iray, igate] = std::numeric_limits<float>::quiet_NaN();
}

#include "view_rhi.moc"
