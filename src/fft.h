/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "types.h"
#include "array.h"

namespace vispwr
{
  auto fft_fwd(span<complex<float>> in, span<complex<float>> out) -> void;
  auto fft_rev(span<complex<float>> in, span<complex<float>> out) -> void;

  auto fft_fwd(array2cf const& in, array2cf& out) -> void;
  auto fft_rev(array2cf const& in, array2cf& out) -> void;
}
