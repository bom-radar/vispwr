/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "calibration.h"
#include "pick.h"

#include <QLineSeries>
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QPushButton>

using namespace vispwr;

namespace vispwr
{
  class view_spectrum;

  class spectrum_window : public QWidget
  {
    Q_OBJECT
  public:
    spectrum_window(view_spectrum* stage);

  signals:
    void window_hidden();
    void data_updated();

  protected:
    auto hideEvent(QHideEvent* event) -> void override;

  private:
    auto on_data_updated() -> void;
    auto on_save_image() -> void;

  private:
    view_spectrum*  stage_;
    QLineSeries*    series_h_;
    QLineSeries*    series_v_;
    QValueAxis*     x_axis_;
    QValueAxis*     y_axis_;
    QChart*         chart_;
    QChartView*     view_;
  };

  class view_spectrum : public stage
  {
  protected:
    view_spectrum();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<spectra const> data, int input) -> void override;

  private:
    using spectrum_store = vector<pair<float, float>>;

  private:
    auto calculate_spectrum(
          array2cf const& iq
        , int igate
        , float avg_mf_losses
        , float noise_power
        , float adu_to_dbm
        , float prf
        ) -> spectrum_store;

  private:
    int             picker_;
    int             calib_channel_;

    bool            sub_noise_;
    bool            add_mf_loss_;

    mutex           mut_spectrums_;
    spectrum_store  spectrum_h_;
    spectrum_store  spectrum_v_;
    spectrum_window window_;

    QMetaObject::Connection con_pick_updated_;  // handle to signal/slot connection for current picker

    friend class spectrum_window;
  };
}

static constexpr auto plist = std::array<string_view, 6>{ "Picker", "Calib", "Subtract Noise", "Add MF Losses", "Display", "geometry" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "spectra", data_type::spectra, true, "Input spectra" }
}};
static constexpr auto olist = std::array<output_meta, 0>
{{
}};

static const auto desc = R"(
Plot the Power Spectral Density (PSD) of the spectrum at a selected location.

**Input:** Spectra

**Output:** None
)";

static auto ireg = stage::enrol<view_spectrum>("view_spectrum", "Spectrum", "Display", desc);

view_spectrum::view_spectrum()
  : stage{plist, ilist, olist}
  , picker_{0}
  , calib_channel_{0}
  , sub_noise_{false}
  , add_mf_loss_{true}
  , window_{this}
{
  QObject::connect(&window_, &spectrum_window::window_hidden, [this]() { notify_parameter_update("Display"); });
  con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
}

auto view_spectrum::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto view_spectrum::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Picker")
    return parameter_integer
    {
      .name = "Picker",
      .description = "Input pick channel for batch selection",
      .min = 0,
      .max = pick::channels - 1,
      .value = picker_,
    };
  if (name == "Calib")
    return parameter_integer
    {
      .name = "Calib",
      .description = "Calibration channel",
      .min = 0,
      .max = 10,
      .value = calib_channel_,
    };
  if (name == "Subtract Noise")
    return parameter_boolean
    {
      .name = "Subtract Noise",
      .description = "",
      .value = sub_noise_,
    };
  if (name == "Add MF Losses")
    return parameter_boolean
    {
      .name = "Add MF Losses",
      .description = "",
      .value = add_mf_loss_,
    };
  if (name == "Display")
    return parameter_trigger
    {
      .name = "Display",
      .description = "Open or close display window",
      .label = window_.isVisible() ? "Hide" : "Show",
    };
  if (name == "geometry")
    return parameter_string
    {
      .name = "geometry",
      .description = "Window geometry",
      .visible = false,
      .value = fmt::format("{} {} {} {}", window_.x(), window_.y(), window_.width(), window_.height()),
    };
  return {};
}

auto view_spectrum::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Picker")
  {
    picker_ = std::get<int>(value);
    if (con_pick_updated_)
      QObject::disconnect(con_pick_updated_);
    con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
  }
  else if (name == "Calib")
    calib_channel_ = std::get<int>(value);
  else if (name == "Subtract Noise")
    sub_noise_ = std::get<bool>(value);
  else if (name == "Add MF Losses")
    add_mf_loss_ = std::get<bool>(value);
  else if (name == "Display")
  {
    if (std::get<bool>(value) == true)
    {
      window_.setVisible(!window_.isVisible());
      notify_parameter_update("Display");
    }
  }
  else if (name == "geometry")
  {
    int x, y, w, h;
    if (sscanf(std::get<string>(value).c_str(), "%d %d %d %d", &x, &y, &w, &h) != 4)
      throw std::runtime_error{"Bad window geometry"};
    window_.setGeometry(x, y, w, h);
  }
}

auto view_spectrum::process(shared_ptr<spectra const> data, int input) -> void
{
  // get the calibration from our selected channel
  auto calib = calibration_get(calib_channel_);
  if (!calib)
    return;

  // get the currently picked coordiantes from our selected channel
  auto pick_data = pick::channel(picker_).get();
  if (!pick_data.valid)
    return;

  #if 1
  // account for ccw scanning
  auto azimuth_l = data->pulses[0].azimuth;
  auto azimuth_h = data->pulses[data->pulse_count-1].azimuth;
  if (azimuth_l > azimuth_h)
    std::swap(azimuth_l, azimuth_h);

  // account for 360..0 wrap around
  if (azimuth_l < 10.0f && azimuth_h > 350.0f)
    std::swap(azimuth_l, azimuth_h);

  // does this batch overlap with the picked arc?
  if (pick_data.azimuth < azimuth_l || pick_data.azimuth > azimuth_h)
    return;
  #else
  // account for ccw scanning
  auto elevation_l = data->pulses[0].elevation;
  auto elevation_h = data->pulses[data->pulse_count-1].elevation;
  if (elevation_l > elevation_h)
    std::swap(elevation_l, elevation_h);

  // account for 360..0 wrap around
  if (elevation_l < 10.0f && elevation_h > 350.0f)
    std::swap(elevation_l, elevation_h);

  if (pick_data.elevation < elevation_l || pick_data.elevation > elevation_h)
    return;
  #endif

  // do we have the desired gate?
  auto gate = gate_from_range(data->range_start, data->range_step, pick_data.range);
  if (gate >= data->gate_count)
    return;

  // determine average matched filter losses on each channel
  auto avg_mf_losses_h = 0.0, avg_mf_losses_v = 0.0;
  for (auto& v : data->pulses)
  {
    avg_mf_losses_h += v.matched_filter_losses_h;
    avg_mf_losses_v += v.matched_filter_losses_v;
  }
  avg_mf_losses_h /= data->pulse_count;
  avg_mf_losses_v /= data->pulse_count;

  // recalculate our spectrums
  auto spectrum_h = calculate_spectrum(
        data->fiq_rx_h
      , gate
      , avg_mf_losses_h
      , calib->noise_power_h
      , calib->sp_calib_h
      , data->prf);
  auto spectrum_v = calculate_spectrum(
        data->fiq_rx_v
      , gate
      , avg_mf_losses_v
      , calib->noise_power_v
      , calib->sp_calib_v
      , data->prf);

  // store them for access by the gui object
  {
    auto lock = lock_guard<mutex>{mut_spectrums_};
    spectrum_h_ = std::move(spectrum_h);
    spectrum_v_ = std::move(spectrum_v);
  }

  // emit the data updated event (will be transferred to gui thread)
  window_.data_updated();
}

auto view_spectrum::calculate_spectrum(
      array2cf const& fiq
    , int igate
    , float avg_mf_losses
    , float noise_power
    , float adu_to_dbm
    , float prf
    ) -> spectrum_store
{
  // return a null spectrum if there is no IQ data for this channel
  if (fiq.size() == 0)
    return {};

  auto fft_size = fiq.shape()[1];
  auto half_fft_size = fft_size / 2; // deliberate trunction to int

  // normalization to convert from power 'at' frequency to power/hz in region of frequency
  auto to_psd = prf / fft_size;

  // normalize noise power as white noise (spread total power equally over all frequencies)
  noise_power /= fft_size;
  if (!sub_noise_)
    noise_power = 0.0f;

  // append each element to spectrum data
  auto spectrum = spectrum_store{};
  for (auto i = 0uz; i < fft_size; ++i)
  {
    // determine the frequency of this wave
    auto hz = i * (prf / fft_size);
    if (i > half_fft_size)
      hz -= prf;

    // determine the power spectrum in dB of the ADU
    // note either use 10log(abs(mag)), or 20log(mag * conj(mag)).  they are identical but the latter saves a sqrt
    float pwr = ((fiq[igate, i] * conj(fiq[igate, i])).real() * to_psd) - noise_power;

    // convert power to dB (treating 0 as -999 dB)
    auto db = pwr > 0.0f ? 10.0f * std::log10(pwr) : -999.0f;

    // convert from dB(ADU) to dBm (single point cal value)
    db += adu_to_dbm;

    // compensate for matched filter losses
    if (add_mf_loss_)
      db += avg_mf_losses;

    spectrum.emplace_back(hz, db);

    // for spectrums with an even number of points we will get a single frequency exactly at the half prf
    // so duplicate it to the negative side of the graph for the sake of nice visuals
    if (i == half_fft_size && fft_size % 2 == 0uz)
      spectrum.emplace_back(-hz, db);
  }

  // rotate around so 0 is in the middle
  std::rotate(spectrum.rbegin(), spectrum.rbegin() + half_fft_size, spectrum.rend());

  return spectrum;
}

spectrum_window::spectrum_window(view_spectrum* stage)
  : stage_{stage}
  , series_h_{new QLineSeries()}
  , series_v_{new QLineSeries()}
  , x_axis_{new QValueAxis()}
  , y_axis_{new QValueAxis()}
  , chart_{new QChart()}
  , view_{new QChartView(chart_)}
{
  x_axis_->setTitleText("Frequency (Hz)");
  x_axis_->setTickType(QValueAxis::TicksDynamic);
  x_axis_->setTickInterval(100.0);
  x_axis_->setTickAnchor(0.0);
  x_axis_->setMinorTickCount(1);

  y_axis_->setTitleText("Power (dBm)");
  y_axis_->setTickType(QValueAxis::TicksDynamic);
  y_axis_->setTickInterval(20.0);
  y_axis_->setTickAnchor(0.0);
  y_axis_->setMinorTickCount(1);

  chart_->addSeries(series_h_);
  chart_->addSeries(series_v_);
  chart_->addAxis(x_axis_, Qt::AlignBottom);
  chart_->addAxis(y_axis_, Qt::AlignLeft);

  series_h_->setName("Horizontal");
  series_h_->setPointsVisible(true);
  series_h_->attachAxis(x_axis_);
  series_h_->attachAxis(y_axis_);

  series_v_->setName("Vertical");
  series_v_->setPointsVisible(true);
  series_v_->attachAxis(x_axis_);
  series_v_->attachAxis(y_axis_);

  auto toolbar = new QWidget();
  auto layout_tb = new QHBoxLayout(toolbar);
  auto but_save = new QPushButton(QIcon::fromTheme("document-save"), "");
  layout_tb->addWidget(but_save);

  auto layout = new QVBoxLayout{this};
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addWidget(toolbar);
  layout->addWidget(view_);

  resize(800, 400);
  setWindowTitle("View Spectrum");
  show();

  QObject::connect(this, &spectrum_window::data_updated, this, &spectrum_window::on_data_updated);
  QObject::connect(but_save, &QPushButton::clicked, this, &spectrum_window::on_save_image);
}

auto spectrum_window::hideEvent(QHideEvent* event) -> void
{
  window_hidden();
}

auto spectrum_window::on_data_updated() -> void
{
  series_h_->clear();
  series_v_->clear();

  float x_range = 0.0f;

  {
    auto lock = lock_guard<mutex>{stage_->mut_spectrums_};

    if (stage_->spectrum_h_.size() != 0)
    {
      for (auto& point : stage_->spectrum_h_)
        series_h_->append(point.first, point.second);
      x_range = std::max(x_range, std::abs(stage_->spectrum_h_.front().first));
      x_range = std::max(x_range, std::abs(stage_->spectrum_h_.back().first));
    }
    if (stage_->spectrum_v_.size() != 0)
    {
      for (auto& point : stage_->spectrum_v_)
        series_v_->append(point.first, point.second);
      x_range = std::max(x_range, std::abs(stage_->spectrum_v_.front().first));
      x_range = std::max(x_range, std::abs(stage_->spectrum_v_.back().first));
    }
  }

  x_axis_->setRange(-x_range, x_range);
  y_axis_->setRange(-130, 10);
}

auto spectrum_window::on_save_image() -> void
{
  auto path = QFileDialog::getSaveFileName(this, tr("Choose file"), QDir::homePath(), "PNG image files (*.png)");
  if (path.isEmpty())
    return;
  auto pixmap = view_->grab();
  pixmap.save(path, "PNG");
}

#include "view_spectrum.moc"
