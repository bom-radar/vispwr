/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "json.h"
#include "format.h"
//#include "unit_test.h"
#include "util.h"
#include <istream>
#include <ostream>

using namespace vispwr;
using namespace std::string_literals;
using namespace std::string_view_literals;

static constexpr char const* node_type_names[] =
{
    "null"
  , "boolean"
  , "number"
  , "string"
  , "array"
  , "object"
};

// this function assumes that there MUST be something after the whitespace
static inline auto json_skip_whitespace(std::string_view buf, size_t& pos)
{
  pos = buf.find_first_not_of("\t\n\r ", pos, 4);
  if (pos == std::string_view::npos)
  {
    pos = buf.size();
    throw json::parse_error{buf, pos, "Unexpected end of input"};
  }
}

static auto json_parse_string(std::string_view buf, size_t& pos, std::string& out)
{
  // we buffer up chunks of normal characters between escape squences to minimize append() calls
  auto from = pos;
  while (true)
  {
    if (pos == buf.size())
      throw json::parse_error{buf, pos, "Unterminated string constant"};

    if (buf[pos] == '"')
    {
      out.append(&buf[from], &buf[pos]);
      break;
    }

    if (buf[pos] != '\\')
    {
      pos += 1;
      continue;
    }

    out.append(&buf[from], &buf[pos]);
    pos += 1;
    if (pos == buf.size())
      throw json::parse_error{buf, pos, "Unterminated string constant"};

    switch (buf[pos])
    {
    case '"': out.push_back('"'); break;
    case '\\': out.push_back('\\'); break;
    case '/': out.push_back('/'); break;
    case 'b': out.push_back('\b'); break;
    case 'f': out.push_back('\f'); break;
    case 'n': out.push_back('\n'); break;
    case 'r': out.push_back('\r'); break;
    case 't': out.push_back('\t'); break;
    case 'u':
      throw json::parse_error{buf, pos, "Unicode TODO"};
    default:
      throw json::parse_error{buf, pos, fmt::format("Invalid escape sequence '\\{}'", buf[pos])};
    }

    pos += 1;
    from = pos;
  }
}

static auto json_parse_object(std::string_view buf, size_t& pos, json::object_type& out)
{
  json_skip_whitespace(buf, pos);
  if (buf[pos] == '}')
    return;
  while (true)
  {
    std::string key;

    if (buf[pos] != '"')
      throw json::parse_error{buf, pos, "Expected '\"'"};
    pos += 1;
    json_parse_string(buf, pos, key);
    pos += 1;

    json_skip_whitespace(buf, pos);
    if (buf[pos] != ':')
      throw json::parse_error{buf, pos, "Expected ':'"};
    pos += 1;

    json_skip_whitespace(buf, pos);
    try
    {
      out.emplace(std::move(key), json{buf, pos});
    }
    catch (json::parse_error& err)
    {
      /* safe to use key here since parse_error can only come from json constructor, which means we never made it into
       * the emplace() call, which means the key was never actually moved. */
      err.unwind_add_key(key);
      throw;
    }

    json_skip_whitespace(buf, pos);
    if (buf[pos] == '}')
      return;
    if (buf[pos] != ',')
      throw json::parse_error{buf, pos, "Expected '}' or ','"};
    pos += 1;
    json_skip_whitespace(buf, pos);
  }
}

static auto json_parse_array(std::string_view buf, size_t& pos, json::array_type& out)
{
  json_skip_whitespace(buf, pos);
  if (buf[pos] == ']')
    return;
  while (true)
  {
    try
    {
      out.emplace_back(buf, pos);
    }
    catch (json::parse_error& err)
    {
      err.unwind_add_key(fmt::format("{}", out.size()));
      throw;
    }
    json_skip_whitespace(buf, pos);
    if (buf[pos] == ']')
      break;
    if (buf[pos] != ',')
      throw json::parse_error{buf, pos, "Expected ']' or ','"};
    pos += 1;
    json_skip_whitespace(buf, pos);
  }
}

json::json(std::string_view buf, size_t& pos)
{
  // assumes you already called json_skip_whitespace!!!!
  if (buf[pos] == '{')
  {
    pos += 1;
    json_parse_object(buf, pos, value_.emplace<object_type>());
    pos += 1;
  }
  else if (buf[pos] == '[')
  {
    pos += 1;
    json_parse_array(buf, pos, value_.emplace<array_type>());
    pos += 1;
  }
  else if (buf[pos] == '"')
  {
    pos += 1;
    json_parse_string(buf, pos, value_.emplace<std::string>());
    pos += 1;
  }
  else if (buf[pos] == 't')
  {
    if (pos + 4 > buf.size() || buf[pos + 1] != 'r' || buf[pos + 2] != 'u' || buf[pos + 3] != 'e')
      throw json::parse_error{buf, pos, "Invalid value token"};
    value_.emplace<bool>(true);
    pos += 4;
  }
  else if (buf[pos] == 'f')
  {
    if (pos + 5 > buf.size() || buf[pos + 1] != 'a' || buf[pos + 2] != 'l' || buf[pos + 3] != 's' || buf[pos + 4] != 'e')
      throw json::parse_error{buf, pos, "Invalid value token"};
    value_.emplace<bool>(false);
    pos += 5;
  }
  else if (buf[pos] == 'n')
  {
    if (pos + 4 > buf.size() || buf[pos + 1] != 'u' || buf[pos + 2] != 'l' || buf[pos + 3] != 'l')
      throw json::parse_error{buf, pos, "Invalid value token"};
    pos += 4;
  }
  else if (buf[pos] == '-' || (buf[pos] >= '0' && buf[pos] <= '9'))
  {
    auto ret = std::from_chars(&buf[pos], &buf[0] + buf.size(), value_.emplace<double>());
    if (ret.ec != std::errc())
      throw json::parse_error{buf, pos, "Invalid number"};
    pos += ret.ptr - &buf[pos];
  }
  else
    throw json::parse_error{buf, pos, "Invalid value token"};
}

auto json::parse(std::string_view buf) -> json
{
  auto pos = 0uz;
  json_skip_whitespace(buf, pos);
  return {buf, pos};
}

auto json::read(std::istream& is) -> json
{
  auto buf = std::string{std::istreambuf_iterator<char>(is.rdbuf()), std::istreambuf_iterator<char>()};
  return parse(buf);
}

auto json::write(std::ostream& out, int indent) const -> void
{
  std::visit([&](auto&& val)
  {
    using T = std::decay_t<decltype(val)>;
    if constexpr (std::is_same_v<T, std::monostate>)
      out << "null";
    if constexpr (std::is_same_v<T, bool>)
      out << (val ? "true" : "false");
    if constexpr (std::is_same_v<T, double>)
      out << val;
    if constexpr (std::is_same_v<T, std::string>)
      // TODO - string escaping
      out << '\"' << val << '\"';
    if constexpr (std::is_same_v<T, array_type>)
    {
      if (val.empty())
        out << "[]";
      else
      {
        out << '[';
        val.front().write(out, indent);
        for (auto i = val.begin() + 1; i != val.end(); ++i)
        {
          out << ", ";
          i->write(out, indent);
        }
        out << ']';
      }
    }
    if constexpr (std::is_same_v<T, object_type>)
    {
      if (val.empty())
        out << "{}";
      else
      {
        // TODO - string escaping
        out << "{\n";
        for (int i = 0; i < indent + 1; ++i)
          out << ' ' ;
        auto i = val.begin();
        out << "\"" << i->first << "\": ";
        i->second.write(out, indent + 1);
        while (++i != val.end())
        {
          out << ",\n";
          for (int j = 0; j < indent + 1; ++j)
            out << ' ';
          out << '\"' << i->first << "\": ";
          i->second.write(out, indent + 1);
        }
        out << '\n';
        for (int i = 0; i < indent; ++i)
          out << ' ' ;
        out << '}';
      }
    }
  }, value_);
}

auto json::error::what() const noexcept -> char const*
{
  what_ = format_what();
  return what_.c_str();
}

json::parse_error::parse_error(std::string_view buf, size_t pos, std::string cause)
  : where_{1uz, 1uz}
  , cause_{std::move(cause)}
{
  for (auto i = 0uz; i < pos; ++i)
    if (buf[i] == '\n')
      where_.first++, where_.second = 1uz;
    else
      where_.second++;
}

auto json::parse_error::format_what() const -> std::string
{
  auto path = std::string{};
  for (auto i = path_.rbegin(); i != path_.rend(); ++i)
    path.append(1, '/').append(*i);
  if (path_.empty())
    path.push_back('/');
  return fmt::format("JSON parse error at {}, line {}, col {}: {}", path, where_.first, where_.second, cause_);
}

json::api_error::api_error(json const* where)
  : where_{where}
  , path_{"<unknown>"}
{ }

static auto find_node(json const& haystack, json const* needle, std::vector<std::string>& path) -> bool
{
  if (&haystack == needle)
    return true;
  else if (haystack.type() == json::node_type::array)
  {
    auto& array = haystack.array();
    for (auto i = 0uz; i < array.size(); ++i)
    {
      if (find_node(array[i], needle, path))
      {
        path.push_back(fmt::to_string(i));
        return true;
      }
    }
  }
  else if (haystack.type() == json::node_type::object)
  {
    for (auto& val : haystack.object())
    {
      if (find_node(val.second, needle, path))
      {
        path.push_back(val.first);
        return true;
      }
    }
  }
  return false;
}

auto json::api_error::set_root(json const& root) -> bool
{
  // search through the entire structure from root and look for our target node
  std::vector<std::string> path;
  if (!find_node(root, where_, path))
    return false;

  // concatenate up a nice path string to be used in the error message
  path_.clear();
  for (auto i = path.rbegin(); i != path.rend(); ++i)
    path_.append(1, '/').append(*i);
  if (path_.empty())
    path_.push_back('/');
  return true;
}

json::key_error::key_error(json const* where, std::string_view key)
  : api_error{where}
  , key_{key}
{ }

auto json::key_error::format_what() const -> std::string
{
  return fmt::format("JSON path error at {}: no such key '{}'", path(), key_);
}

json::index_error::index_error(json const* where, size_t index)
  : api_error{where}
  , index_{index}
{ }

auto json::index_error::format_what() const -> std::string
{
  return fmt::format("JSON path error at {}: invalid array index {}", path(), index_);
}

json::type_error::type_error(json const* where, node_type expected)
  : api_error{where}
  , expected_{expected}
{ }

auto json::type_error::format_what() const -> std::string
{
  return fmt::format("JSON type error at {}: expected {}", path(), node_type_names[static_cast<int>(expected_)]);
}
#if 0

// GCOV_EXCL_START
TEST_CASE("json_skip_whitespace")
{
  auto pos = 0uz;
  json_skip_whitespace("foo"sv, pos);
  CHECK(pos == 0);
  json_skip_whitespace(" \r\n\tfoo"sv, pos);
  CHECK(pos == 4);
}
TEST_CASE("json_parse_string")
{
  size_t pos = 0;
  std::string str;
  SUBCASE("") { CHECK_NOTHROW(json_parse_string(R"(")", pos, str)); CHECK(str == ""); }
  SUBCASE("") { CHECK_NOTHROW(json_parse_string(R"(foo")", pos, str)); CHECK(str == "foo"); }
  SUBCASE("") { CHECK_NOTHROW(json_parse_string(R"(foo\"bar")", pos, str)); CHECK(str == R"(foo"bar)"); }
  SUBCASE("") { CHECK_NOTHROW(json_parse_string(R"(\"\\\/\b\f\n\r\t")", pos, str)); CHECK(str == "\"\\/\b\f\n\r\t"); }
  SUBCASE("") { CHECK_THROWS(json_parse_string("foo", pos, str)); }   // unterminated string
  SUBCASE("") { CHECK_THROWS(json_parse_string("foo\\", pos, str)); } // unterminated string in escape sequence
  SUBCASE("") { CHECK_THROWS(json_parse_string("\\u\"", pos, str)); } // invalid escape sequence
}
TEST_CASE("json::parse")
{
  auto data = R"STR(
    {
      "foo" : true,
      "bar": [ "hi", "33", "bye" ],
      "numbers": [ 0 , 1, -2, 0.1, -0.22, -3.1e10, 4.0E-3 ],
      "deep":
      {
        "nested": "value",
        "error": true,
        "list": [ 1, false, 3 ]
      }
    }
  )STR";
  auto j = json::parse(data);

  CHECK(int(j["numbers"][0]) == 0);
  CHECK(int(j["numbers"][1]) == 1);
  CHECK(int(j["numbers"][2]) == -2);
  CHECK(float(j["numbers"][3]) == doctest::Approx(0.1));
  CHECK(float(j["numbers"][4]) == doctest::Approx(-0.22));
  CHECK(double(j["numbers"][5]) == doctest::Approx(-3.1e10));
  CHECK(double(j["numbers"][6]) == doctest::Approx(4.0e-3));

  try
  {
    std::string const& str(j["deep"]["list"][3]);
  }
  catch (json::api_error& err)
  {
    err.set_root(j);
    throw;
  }

  /* ideally we would want to be able to make a sensible error out of something like this:
   *   auto bad = int(j["foo"]["bar"][2]);  // err = "type mismatch at /foo/bar/2"
   *
   * that's probably not going to be possible.  but at least if we could report the key used in
   * the final stage that would be helpful:
   *   auto bad = int(j["foo"]["bar"][2]);    // err = "type mismatch at 2"
   *   auto bad = int(j["/foo/bar/2"_jpath]); // err = "type mismatch at /foo/bar/2"
   *
   * perhaps make it so that operator[] returns a special rvalue 'view' type that has the actual casting
   * operators inside it as well as a pointer to the key
   */
}
TEST_CASE("json")
{
  auto jb = json{true};
  CHECK(jb.type() == json::node_type::boolean);
  CHECK(bool(jb) == true);

  auto ji = json{10};
  CHECK(ji.type() == json::node_type::number);
  CHECK(int(ji) == 10);
  CHECK_THROWS(bool(ji));

  auto jf = json{1.1f};
  CHECK(jf.type() == json::node_type::number);

  auto jp = json{"foo"};
  CHECK(jp.type() == json::node_type::string);

  auto js = json{"foo"s};
  CHECK(js.type() == json::node_type::string);

  auto ja = json{json::array_type{}};
  CHECK(ja.type() == json::node_type::array);

  auto jo = json{json::object_type{}};
  CHECK(jo.type() == json::node_type::object);
}
// GCOV_EXCL_STOP
#endif
