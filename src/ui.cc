/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "ui.h"
#include "ui_style.h"
#include "format.h"
#include "stage.h"

#include <QApplication>
#include <QColor>
#include <QComboBox>
#include <QCoreApplication>
#include <QCheckBox>
#include <QFileDialog>
#include <QGraphicsSceneMouseEvent>
#include <QGridLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMenuBar>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPushButton>
#include <QScreen>
#include <QSpinBox>
#include <QStyleFactory>
#include <QStyleOptionGraphicsItem>
#include <QTextEdit>
#include <QTreeWidgetItem>
#include <QWidgetAction>

#include <fstream>

using namespace vispwr;

inline auto to_qstring(string_view str)
{
  return QString{QLatin1String(str.data(), str.size())};
}

static auto show_error_box(string_view message, string_view context, std::exception const* err = nullptr)
{
  QMessageBox mbox;
  mbox.setText(to_qstring(message));
  mbox.setInformativeText(to_qstring(context));
  if (err)
    mbox.setDetailedText(QString::fromStdString(fmt::format("{}", *err)));
  mbox.setIcon(QMessageBox::Critical);
  mbox.exec();
}

ui_stage_picker::ui_stage_picker(QWidget* parent)
  : QDialog{parent}
  , buttons_{QDialogButtonBox::Ok | QDialogButtonBox::Cancel}
{
  setWindowTitle(tr("Add Stage"));

  string_view cur_category;
  QTreeWidgetItem* group = nullptr;
  for (auto& entry : stage::catalog())
  {
    if (entry.category != cur_category)
    {
      group = new QTreeWidgetItem(&tree_);
      group->setText(0, to_qstring(entry.category));
      group->setData(0, Qt::UserRole, QStringLiteral(""));
      cur_category = entry.category;
    }
    auto stage = new QTreeWidgetItem(group);
    stage->setText(0, to_qstring(entry.label));
    stage->setData(0, Qt::UserRole, to_qstring(entry.name));
  }
  tree_.header()->close();

  description_.setReadOnly(true);

  auto layout = new QGridLayout;
  layout->addWidget(&tree_, 0, 0);
  layout->addWidget(&description_, 0, 1);
  layout->addWidget(&buttons_, 1, 0, 1, 2);
  setLayout(layout);

  connect(&tree_, &QTreeWidget::currentItemChanged, this, &ui_stage_picker::current_item_changed);
  connect(&tree_, &QTreeWidget::itemDoubleClicked, this, &ui_stage_picker::item_double_clicked);
  connect(&buttons_, &QDialogButtonBox::accepted, this, &ui_stage_picker::accept);
  connect(&buttons_, &QDialogButtonBox::rejected, this, &ui_stage_picker::reject);
}

auto ui_stage_picker::selected_stage_type() const -> QString
{
  if (auto item = tree_.currentItem())
    return item->data(0, Qt::UserRole).toString();
  return {};
}

auto ui_stage_picker::current_item_changed(QTreeWidgetItem* item, QTreeWidgetItem* previous) -> void
{
  // we only set the userrole data for stages, so we use this to distinguish categories from stages
  auto name = item->data(0, Qt::UserRole).toString().toStdString();
  if (name.size() == 0)
  {
    description_.setMarkdown("");
    buttons_.button(QDialogButtonBox::Ok)->setEnabled(false);
  }
  else
  {
    buttons_.button(QDialogButtonBox::Ok)->setEnabled(true);
    auto markdown = std::ranges::find(stage::catalog(), name, &stage_metadata::name)->description;
    description_.setMarkdown(to_qstring(markdown));
  }
}

auto ui_stage_picker::item_double_clicked(QTreeWidgetItem* item, int column) -> void
{
  if (selected_stage_type().size() != 0)
    accept();
}

auto update_parameter_widget(parameter_trigger const& param, QWidget* widget) -> void
{
  auto& control = dynamic_cast<QPushButton&>(*widget);
  control.setText(to_qstring(param.label));
  control.setToolTip(to_qstring(param.description));
  control.setEnabled(param.editable);
}

auto init_parameter_widget(parameter_trigger const& param, shared_ptr<stage> s) -> QWidget*
{
  auto control = new QPushButton();
  update_parameter_widget(param, control);
  QObject::connect(control, &QPushButton::clicked, [s, name = param.name]()
  {
    s->set_parameter(name, true);
  });
  return control;
}

auto update_parameter_widget(parameter_boolean const& param, QWidget* widget)
{
  auto& control = dynamic_cast<QCheckBox&>(*widget);
  control.setToolTip(to_qstring(param.description));
  control.setEnabled(param.editable);
  control.setCheckState(param.value ? Qt::Checked : Qt::Unchecked);
}

auto init_parameter_widget(parameter_boolean const& param, shared_ptr<stage> s) -> QWidget*
{
  auto control = new QCheckBox();
  update_parameter_widget(param, control);
  QObject::connect(control, &QCheckBox::stateChanged, [s, name = param.name](int state)
  {
    s->set_parameter(name, state != 0);
  });
  return control;
}

auto update_parameter_widget(parameter_integer const& param, QWidget* widget) -> void
{
  auto& control = dynamic_cast<QSpinBox&>(*widget);
  control.setToolTip(to_qstring(param.description));
  control.setEnabled(param.editable);
  control.setMinimum(param.min);
  control.setMaximum(param.max);
  control.setSingleStep(param.step);
  control.setValue(param.value);
}

auto init_parameter_widget(parameter_integer const& param, shared_ptr<stage> s) -> QWidget*
{
  auto control = new QSpinBox();
  update_parameter_widget(param, control);
  QObject::connect(control, qOverload<int>(&QSpinBox::valueChanged), [s, name = param.name](int value)
  {
    s->set_parameter(name, value);
  });
  return control;
}

auto update_parameter_widget(parameter_real const& param, QWidget* widget) -> void
{
  auto& control = dynamic_cast<QDoubleSpinBox&>(*widget);
  control.setToolTip(to_qstring(param.description));
  control.setEnabled(param.editable);
  control.setDecimals(param.decimals);
  control.setMinimum(param.min);
  control.setMaximum(param.max);
  control.setSingleStep(param.step);
  control.setValue(param.value);
}

auto init_parameter_widget(parameter_real const& param, shared_ptr<stage> s) -> QWidget*
{
  auto control = new QDoubleSpinBox();
  update_parameter_widget(param, control);
  QObject::connect(control, qOverload<double>(&QDoubleSpinBox::valueChanged), [s, name = param.name](double value)
  {
    s->set_parameter(name, value);
  });
  return control;
}

auto update_parameter_widget(parameter_string const& param, QWidget* widget) -> void
{
  auto& control = dynamic_cast<QLineEdit&>(*widget);
  control.setToolTip(to_qstring(param.description));
  control.setEnabled(param.editable);
  control.setText(QString::fromStdString(param.value));
}

auto init_parameter_widget(parameter_string const& param, shared_ptr<stage> s) -> QWidget*
{
  auto control = new QLineEdit();
  update_parameter_widget(param, control);
  QObject::connect(control, &QLineEdit::textChanged, [s, name = param.name](QString const& text)
  {
    s->set_parameter(name, text.toStdString());
  });
  return control;
}

auto update_parameter_widget(parameter_enumerate const& param, QWidget* widget) -> void
{
  auto& control = dynamic_cast<QComboBox&>(*widget);
  control.setToolTip(to_qstring(param.description));
  control.setEnabled(param.editable);
  control.setEditable(param.flexible);
  control.setCurrentText(QString::fromStdString(param.value));
  // TODO - allow stage to change items in combo box
}

auto init_parameter_widget(parameter_enumerate const& param, shared_ptr<stage> s) -> QWidget*
{
  auto control = new QComboBox();
  for (auto& value : param.values)
    control->addItem(to_qstring(value));
  update_parameter_widget(param, control);
  QObject::connect(control, &QComboBox::currentTextChanged, [s, name = param.name](QString const& text)
  {
    s->set_parameter(name, text.toStdString());
  });
  return control;
}

auto update_parameter_widget(parameter_path const& param, QWidget* widget) -> void
{
  auto& control = dynamic_cast<QPushButton&>(*widget);
  control.setToolTip(to_qstring(param.description));
  control.setEnabled(param.editable);
  // TODO - update lambda if we change the file filter
}

auto init_parameter_widget(parameter_path const& param, shared_ptr<stage> s) -> QWidget*
{
  auto control = new QPushButton("Choose File");
  update_parameter_widget(param, control);
  QObject::connect(control, &QPushButton::clicked, [s, name = param.name, filter = param.filter]()
  {
    auto path = QFileDialog::getOpenFileName(
          nullptr
        , QObject::tr("Choose file")
        , QDir::homePath()
        , to_qstring(filter)
        , nullptr
        , QFileDialog::ReadOnly
        );
    if (!path.isEmpty())
      s->set_parameter(name, path.toStdString());
  });
  return control;
}

inline auto ui_stage::scene() const -> ui_scene*
{
  return static_cast<ui_scene*>(QGraphicsObject::scene());
}

ui_stage::ui_stage(int id, shared_ptr<stage> s, QPointF position)
  : id_{id}
  , stage_{std::move(s)}
  , proxy_{nullptr}
  , hovered_{false}
  , log_skipped_{0}
{
  // build the parameter widgets
  {
    param_widgets_.resize(stage_->parameters().size(), nullptr);
    auto grid = new QWidget();
    grid->setAttribute(Qt::WA_NoSystemBackground);
    auto layout = new QFormLayout(grid);
    layout_temp_ = layout;
    for (auto name : stage_->parameters())
      on_parameter_updated(name);
    proxy_ = new QGraphicsProxyWidget(this);
    proxy_->setWidget(grid);
    stage_->set_parameter_callback([this](string_view name){parameter_updated(name);});
  }

  // setup logging callback
  stage_->set_logging_callback([this](log_level level, string message){log_output(level, std::move(message));});

  // these connections transfer the asynchronous callbacks onto the main GUI thread
  connect(this, &ui_stage::parameter_updated, this, &ui_stage::on_parameter_updated);
  connect(this, &ui_stage::log_output, this, &ui_stage::on_log_output);

  update_size();

  setFlag(QGraphicsItem::ItemDoesntPropagateOpacityToChildren, true);
  setFlag(QGraphicsItem::ItemIsFocusable, true);
  setFlag(QGraphicsItem::ItemIsMovable, true);
  setFlag(QGraphicsItem::ItemIsSelectable, true);
  setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
  setCacheMode(QGraphicsItem::DeviceCoordinateCache);

  setAcceptHoverEvents(true);
  setZValue(0);
  setPos(position);
  setCursor(QCursor());
}

ui_stage::~ui_stage()
{
  /* This destructor is the reason that items should be removed from the select by simply deleting them rather than calling
   * removeItem() and then deleting.  If you call removeItem() first, then the stage will have already been disassociated
   * from the scene by the time we get here.  As a result it will be unable to loop through the scene items to find any
   * connections.  By simply calling 'delete' on the item you want to remove we can still access the scene here, and the
   * base class destructor will take care of correctly removing us from the scene so there are no dangling references. */
  if (!scene())
  {
    fmt::print("WARNING: A ui_stage was removed from scene before deletion. Its connections may leak.\n");
    return;
  }

  // destroy any connections that this node was participating in
  for (auto item : scene()->items())
    if (auto con = dynamic_cast<ui_connection*>(item))
      if (con->stage_output() == this || con->stage_input() == this)
        delete item;
}

auto ui_stage::boundingRect() const -> QRectF
{
  auto& style = ui_style::get();
  return QRectF(
        -4.0f * style.port_diameter
      , -2.0f * style.port_diameter
      , size_.width() + 8.0f * style.port_diameter
      , size_.height() + 4.0f * style.port_diameter);
}

auto ui_stage::input_position(int i) const -> QPointF
{
  auto& style = ui_style::get();
  auto x = -style.port_diameter;
  auto y = style.stage_label_metrics.height() + (style.port_metrics.height() + style.port_spacing_y) * (i + 0.5);
  return {x, y};
}

auto ui_stage::output_position(int i) const -> QPointF
{
  auto& style = ui_style::get();
  auto x = size_.width() + style.port_diameter;
  auto y = style.stage_label_metrics.height() + (style.port_metrics.height() + style.port_spacing_y) * (i + 0.5);
  return {x, y};
}

auto ui_stage::input_type(int i) const -> data_type
{
  return stage_->inputs()[i].type;
}

auto ui_stage::output_type(int i) const -> data_type
{
  return stage_->outputs()[i].type;
}

auto ui_stage::pick_input(QPointF pos) const -> optional<int>
{
  // we expand the hit box a bit beyond the styled diameter extra to allow for sloppy mouse work
  auto tolerance = ui_style::get().port_diameter * 2;
  for (auto i = 0uz; i < stage_->inputs().size(); ++i)
  {
    auto delta = input_position(i) - pos;
    if (delta.x() * delta.x() + delta.y() * delta.y() < tolerance * tolerance)
      return i;
  }
  return nullopt;
}

auto ui_stage::pick_output(QPointF pos) const -> optional<int>
{
  // we expand the hit box a bit beyond the styled diameter extra to allow for sloppy mouse work
  auto tolerance = ui_style::get().port_diameter * 2;
  for (auto i = 0uz; i < stage_->outputs().size(); ++i)
  {
    auto delta = output_position(i) - pos;
    if (delta.x() * delta.x() + delta.y() * delta.y() < tolerance * tolerance)
      return i;
  }
  return nullopt;
}

auto ui_stage::pick_log_button(QPointF pos) const -> bool
{
  if (log_messages_.empty())
    return false;

  auto& style = ui_style::get();
  auto hit_box = QRectF{
      qreal(size_.width() - style.stage_log_flag_offset - style.stage_log_flag_size)
    , qreal(style.stage_log_flag_offset)
    , qreal(style.stage_log_flag_size)
    , qreal(style.stage_log_flag_size)};
  return hit_box.contains(pos);
}

auto ui_stage::on_log_output(log_level level, string message) -> void
{
  auto& style = ui_style::get();
  if (log_messages_.size() >= style.stage_max_log_messages)
  {
    ++log_skipped_;
    return;
  }
  log_messages_.emplace_back(system_clock::now(), level, std::move(message));
  if (log_messages_.size() == 1)
    update();
}

auto ui_stage::show_log_dialog() -> void
{
  auto& style = ui_style::get();

  string text;
  for (auto& msg : log_messages_)
    text.append(fmt::format(
              "<font color=\"{}\">{} {}</font><br/>"
            , style.stage_log_colors[static_cast<int>(msg.level)]
            , msg.time
            , msg.message));
  if (log_skipped_ > 0)
    text.append(fmt::format("...<br/>{} additional messages discarded", log_skipped_));

  QMessageBox mbox;
  mbox.setText(QString::fromStdString(fmt::format("Log output of '{}' stage:", stage_->metadata().label)));
  mbox.setInformativeText(QString::fromStdString(text));
  mbox.setStandardButtons(QMessageBox::Reset | QMessageBox::Close);
  mbox.setDefaultButton(QMessageBox::Close);
  if (mbox.exec() == QMessageBox::Reset)
  {
    log_messages_.clear();
    log_skipped_ = 0;
    update();
  }
}

auto ui_stage::on_parameter_updated(string_view name) -> void
{
  auto param = stage_->get_parameter(name);
  std::visit([&](auto&& param)
  {
    // determine the parameter index
    size_t iparam = 0;
    while (iparam < stage_->parameters().size() && stage_->parameters()[iparam] != name)
      ++iparam;
    if (iparam == stage_->parameters().size())
      throw std::runtime_error{"Bad parameters"};

    if (param.visible && !param_widgets_[iparam])
    {
      param_widgets_[iparam] = init_parameter_widget(param, stage_);
      layout_temp_->addRow(to_qstring(name), param_widgets_[iparam]);
    }
    else if (param.visible)
    {
      update_parameter_widget(param, param_widgets_[iparam]);
    }
    else if (!param.visible && param_widgets_[iparam])
    {
      layout_temp_->removeRow(param_widgets_[iparam]);
      param_widgets_[iparam] = nullptr;
    }
  }, stage_->get_parameter(name));

  update_size();
}

auto ui_stage::hoverEnterEvent(QGraphicsSceneHoverEvent* event) -> void
{
  hovered_ = true;
  QGraphicsItem::hoverEnterEvent(event);
}

auto ui_stage::hoverMoveEvent(QGraphicsSceneHoverEvent* event) -> void
{
  if (auto port = pick_input(event->pos()))
  {
    auto& metadata = stage_->inputs()[*port];
    auto text = fmt::format("{} - {}\n{}", metadata.name, data_type_names[static_cast<int>(metadata.type)], metadata.description);
    setToolTip(to_qstring(text));
  }
  else if (auto port = pick_output(event->pos()))
  {
    auto& metadata = stage_->outputs()[*port];
    auto text = fmt::format("{} - {}\n{}", metadata.name, data_type_names[static_cast<int>(metadata.type)], metadata.description);
    setToolTip(to_qstring(text));
  }
  else
    // TODO - stage description
    setToolTip("");
}

auto ui_stage::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) -> void
{
  hovered_ = false;
  QGraphicsItem::hoverLeaveEvent(event);
}

auto ui_stage::itemChange(QGraphicsItem::GraphicsItemChange change, QVariant const& value) -> QVariant
{
  if (change == ItemPositionHasChanged && scene())
  {
    for (auto item : scene()->items())
    {
      if (auto con = dynamic_cast<ui_connection*>(item))
        if (con->stage_output() == this || con->stage_input() == this)
          con->update_endpoints();
    }
  }
  return QGraphicsObject::itemChange(change, value);
}

auto ui_stage::mousePressEvent(QGraphicsSceneMouseEvent* event) -> void
{
  if (auto s = scene())
  {
    if (auto port = pick_input(event->pos()))
      s->draw_connection_start(this, false, *port, event->scenePos());
    if (auto port = pick_output(event->pos()))
      s->draw_connection_start(this, true, *port, event->scenePos());
    if (pick_log_button(event->pos()))
      show_log_dialog();
  }
  QGraphicsObject::mousePressEvent(event);
}

auto ui_stage::paint(QPainter* painter, QStyleOptionGraphicsItem const* option, QWidget* widget) -> void
{
  auto& style = ui_style::get();

  painter->setClipRect(option->exposedRect);

  // draw background square
  {
    painter->setPen(QPen(
              isSelected() ? style.selected_color : style.stage_border_color
            , hovered_ ? style.stage_border_width_hover : style.stage_border_width));
    QLinearGradient gradient(QPointF(0.0, 0.0), QPointF(0.0, size_.height()));
    for (auto& node : style.stage_background_colors)
      gradient.setColorAt(node.first, node.second);
    painter->setBrush(gradient);
    auto boundary = QRectF(
          -style.port_diameter
        , -style.port_diameter
        , size_.width() + 2.0 * style.port_diameter
        , size_.height() + 2.0 * style.port_diameter);
    painter->drawRoundedRect(boundary, style.stage_border_radius, style.stage_border_radius);
  }

  // draw input ports
  for (auto i = 0uz; i < stage_->inputs().size(); ++i)
  {
    auto pos = input_position(i);
    auto type = input_type(i);

    // zoom modifier as port hints for when the user is dragging a draft connection near our port
    auto zoom = 1.0f;
    if (auto draft = scene()->draft_connection(); draft && !draft->stage_input())
    {
      auto delta = sceneTransform().map(pos) - draft->pos_input();
      auto dist = std::sqrt(delta.x() * delta.x() + delta.y() * delta.y());

      if (   type == draft->stage_output()->output_type(draft->port_output())
          && (!stage_->input_connected(i) || !stage_->inputs()[i].exclusive))
      {
        if (dist < style.port_diameter * 2)
          zoom = 2.0 - dist / (style.port_diameter * 2);
      }
      else
      {
        if (dist < style.port_diameter * 2)
          zoom = dist / (style.port_diameter * 2);
      }
    }

    painter->setBrush(style.data_type_color[static_cast<int>(type)]);
    painter->drawEllipse(pos, style.port_diameter_free * zoom, style.port_diameter_free * zoom);

    // only draw labels when there are multiple ports
    if (style.stage_show_single_port_labels || stage_->inputs().size() > 1)
    {
      auto label = to_qstring(stage_->inputs()[i].name);
      auto rect = style.port_metrics.boundingRect(label);
      painter->setPen(stage_->input_connected(i) ? style.port_label_color_used : style.port_label_color_free);
      painter->drawText(QPointF(style.port_label_inset, pos.y() + rect.height() / 4.0), label);
    }
  }

  // draw output ports
  for (auto i = 0uz; i < stage_->outputs().size(); ++i)
  {
    auto pos = output_position(i);
    auto type = output_type(i);

    // zoom modifier as port hints for when the user is dragging a draft connection near our port
    auto zoom = 1.0f;
    if (auto draft = scene()->draft_connection(); draft && !draft->stage_output())
    {
      auto delta = sceneTransform().map(pos) - draft->pos_output();
      auto dist = std::sqrt(delta.x() * delta.x() + delta.y() * delta.y());

      if (type == draft->stage_input()->input_type(draft->port_input()))
      {
        if (dist < style.port_diameter * 2)
          zoom = 2.0 - dist / (style.port_diameter * 2);
      }
      else
      {
        if (dist < style.port_diameter * 2)
          zoom = dist / (style.port_diameter * 2);
      }
    }

    painter->setBrush(style.data_type_color[static_cast<int>(type)]);
    painter->drawEllipse(pos, style.port_diameter_free * zoom, style.port_diameter_free * zoom);

    // only draw labels when there are multiple ports
    if (style.stage_show_single_port_labels || stage_->outputs().size() > 1)
    {
      auto label = to_qstring(stage_->outputs()[i].name);
      auto rect = style.port_metrics.boundingRect(label);
      painter->setPen(stage_->output_connected(i) ? style.port_label_color_used : style.port_label_color_free);
      painter->drawText(QPointF(size_.width() - style.port_label_inset - rect.width(), pos.y() + rect.height() / 4.0), label);
    }
  }

  // draw label
  {
    auto label = to_qstring(stage_->metadata().label);
    auto rect = style.stage_label_metrics.boundingRect(label);
    QPointF position((size_.width() - rect.width()) / 2.0, (style.port_spacing_y + style.stage_label_metrics.height()) / 3.0);

    painter->setFont(style.stage_label_font);
    painter->setPen(style.stage_label_color);
    painter->drawText(position, label);
  }

  // draw log message indication
  if (!log_messages_.empty())
  {
    painter->setBrush(style.stage_log_flag_color);
    //painter->drawRect(size_.width() - 2 - 15, 2, 15, 15);
    painter->drawRect(
          size_.width() - style.stage_log_flag_offset - style.stage_log_flag_size
        , style.stage_log_flag_offset
        , style.stage_log_flag_size
        , style.stage_log_flag_size);
  }
}

auto ui_stage::update_size() -> void
{
  auto& style = ui_style::get();

  // determine height needed for label, inputs, outputs, and parameters
  auto height_l = style.stage_label_metrics.height();
  auto height_i = (style.port_metrics.height() + style.port_spacing_y) * int(stage_->inputs().size());
  auto height_o = (style.port_metrics.height() + style.port_spacing_y) * int(stage_->outputs().size());
  auto height_p = proxy_ ? proxy_->widget()->height() : 0;

  // determine total height
  size_.setHeight(height_l + std::max({height_i, height_o, height_p}));

  // determine width needed for label, inputs, outputs, and parameters
  auto width_l = style.stage_label_metrics.boundingRect(to_qstring(stage_->metadata().label)).width();
  auto width_i = [&]
  {
    auto max_width = 0;
    if (style.stage_show_single_port_labels || stage_->inputs().size() > 1)
      for (auto& p : stage_->inputs())
        max_width = std::max(max_width, style.port_metrics.horizontalAdvance(to_qstring(p.name)));
    return max_width;
  }();
  auto width_o = [&]
  {
    auto max_width = 0;
    if (style.stage_show_single_port_labels || stage_->outputs().size() > 1)
      for (auto& p : stage_->outputs())
        max_width = std::max(max_width, style.port_metrics.horizontalAdvance(to_qstring(p.name)));
    return max_width;
  }();
  auto width_p = proxy_ ? proxy_->widget()->width() : 0;

  // determine total width
  size_.setWidth(std::max(width_l, width_i + width_o + width_p + 2 * style.port_spacing_x));

  // position the parameters widget
  if (proxy_)
    proxy_->setPos(QPointF(width_i + style.port_spacing_x, (height_l + size_.height() - height_p) / 2.0f));
}

inline auto ui_connection::scene() const -> ui_scene*
{
  return static_cast<ui_scene*>(QGraphicsObject::scene());
}

ui_connection::ui_connection(ui_stage* out_stage, int out_port, ui_stage* in_stage, int in_port, QPointF pos)
  : stage_output_{out_stage}
  , port_output_{out_port}
  , stage_input_{in_stage}
  , port_input_{in_port}
  , pos_output_{pos}
  , pos_input_{pos}
  , hovered_{false}
{
  setFlag(QGraphicsItem::ItemIsFocusable, true);
  setFlag(QGraphicsItem::ItemIsSelectable, true);

  setAcceptHoverEvents(true);
  setZValue(-1);

  update_endpoints();

  // create the 'real' connection at the signal processor level (if we have both sides already)
  if (stage_output_ && stage_input_)
    stage::connect({stage_output_->get_stage(), port_output_}, {stage_input_->get_stage(), port_input_});
}

ui_connection::~ui_connection()
{
  if (stage_output_ && stage_input_)
  {
    stage::disconnect({stage_output_->get_stage(), port_output_}, {stage_input_->get_stage(), port_input_});
    stage_output_->update();
    stage_input_->update();
  }
}

auto ui_connection::update_endpoints() -> void
{
  prepareGeometryChange();
  if (stage_output_)
    pos_output_ = stage_output_->sceneTransform().map(stage_output_->output_position(port_output_));
  if (stage_input_)
    pos_input_ = stage_input_->sceneTransform().map(stage_input_->input_position(port_input_));
}

auto ui_connection::boundingRect() const -> QRectF
{
  auto& style = ui_style::get();
  auto c1c2 = determine_control_points();
  auto rect1 = QRectF(pos_output_, pos_input_).normalized();
  auto rect2 = QRectF(c1c2.first, c1c2.second).normalized();
  auto bounds = rect1.united(rect2);
  return QRectF(
        bounds.x() - style.port_diameter
      , bounds.y() - style.port_diameter
      , bounds.width() + 2 * style.port_diameter
      , bounds.height() + 2 * style.port_diameter);
}

auto ui_connection::shape() const -> QPainterPath
{
  // determine a more accurate 'hitbox' for our connection
  // without this function the connection will respond to clicks anywhere in its bounding box
  // the path stroker is used to expand slightly beyond the graphical path to allow for sloppy mouse work
  auto c1c2 = determine_control_points();
  auto path = QPainterPath(pos_output_);
  path.cubicTo(c1c2.first, c1c2.second, pos_input_);
  auto stroker = QPainterPathStroker{};
  stroker.setWidth(ui_style::get().con_grab_width);
  return stroker.createStroke(path);
}

auto ui_connection::hoverEnterEvent(QGraphicsSceneHoverEvent* event) -> void
{
  hovered_ = true;
  QGraphicsItem::hoverEnterEvent(event);
}

auto ui_connection::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) -> void
{
  hovered_ = false;
  QGraphicsItem::hoverLeaveEvent(event);
}

auto ui_connection::mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void
{
  if (!stage_output_ || !stage_input_)
  {
    // update the free connection endpoint position to match the mouse
    prepareGeometryChange();
    if (!stage_output_)
      pos_output_ = event->scenePos();
    if (!stage_input_)
      pos_input_ = event->scenePos();
    update();
    scene()->draw_connection_move(event->scenePos());
    event->accept();
  }
  QGraphicsObject::mouseMoveEvent(event);
}

auto ui_connection::mouseReleaseEvent(QGraphicsSceneMouseEvent* event) -> void
{
  QGraphicsObject::mouseReleaseEvent(event);

  if (!stage_output_ || !stage_input_)
  {
    for (auto item : scene()->items(event->scenePos()))
    {
      if (auto stage = dynamic_cast<ui_stage*>(item))
      {
        if (!stage_output_)
        {
          if (auto port = stage->pick_output(stage->sceneTransform().inverted().map(event->scenePos())))
          {
            if (stage->output_type(*port) == stage_input_->input_type(port_input_))
            {
              stage_output_ = stage;
              port_output_ = *port;
            }
          }
        }
        else
        {
          if (auto port = stage->pick_input(stage->sceneTransform().inverted().map(event->scenePos())))
          {
            if (stage->input_type(*port) == stage_output_->output_type(port_output_))
            {
              if (!stage->get_stage()->input_connected(*port) || !stage->get_stage()->inputs()[*port].exclusive)
              {
                stage_input_ = stage;
                port_input_ = *port;
              }
            }
          }
        }
      }
    }

    // create the 'real' connection at the signal processor level (if we now have both sides)
    if (stage_output_ && stage_input_)
      stage::connect({stage_output_->get_stage(), port_output_}, {stage_input_->get_stage(), port_input_});

    // update endpoints to ensure our line exactly terminates on the port (rather than being left on the mouse position)
    update_endpoints();

    update();
    scene()->draw_connection_finish();
  }
}

auto ui_connection::paint(QPainter* painter, QStyleOptionGraphicsItem const* option, QWidget* widget) -> void
{
  auto& style = ui_style::get();

  painter->setClipRect(option->exposedRect);

  // determine data type
  auto type = stage_output_ ? stage_output_->output_type(port_output_) : stage_input_->input_type(port_input_);
  auto type_color = style.data_type_color[static_cast<int>(type)];

  // build our path
  auto c1c2 = determine_control_points();
  auto path = QPainterPath(pos_output_);
  path.cubicTo(c1c2.first, c1c2.second, pos_input_);

  painter->setBrush(Qt::NoBrush);

  QPen p;
  if (!stage_output_ || !stage_input_)
  {
    p.setStyle(Qt::DashLine);
    p.setWidth(style.con_draw_width);
    p.setColor(type_color);
  }
  else if (isSelected())
  {
    // if selected, draw a larger version first that acts as an outline
    p.setWidth(style.con_draw_width * 2);
    p.setColor(style.selected_color);
    painter->setPen(p);
    painter->drawPath(path);

    p.setWidth(style.con_draw_width);
    p.setColor(type_color);
  }
  else if (hovered_)
  {
    p.setWidth(style.con_draw_width * 2);
    p.setColor(type_color);
  }
  else
  {
    p.setWidth(style.con_draw_width);
    p.setColor(type_color);
  }
  painter->setPen(p);
  painter->drawPath(path);

  // start and end points
  if (!stage_output_ || !stage_input_)
  {
    painter->setBrush(type_color);
    painter->drawEllipse(pos_output_, style.port_diameter_free, style.port_diameter_free);
    painter->drawEllipse(pos_input_, style.port_diameter_free, style.port_diameter_free);
  }
}

auto ui_connection::determine_control_points() const -> pair<QPointF, QPointF>
{
  auto offset_def = 200.0f;

  auto delta_x = pos_input_.x() - pos_output_.x();

  auto offset_x = qMin(offset_def, std::abs(delta_x));
  double offset_y = 0;

  double ratioX = 0.5;

  if (delta_x <= 0)
  {
    auto delta_y = pos_input_.y() - pos_output_.y() + 20;
    offset_y = qMin(offset_def, std::abs(delta_y)) * (delta_y < 0.0 ? -1.0 : 1.0);
    ratioX = 1.0;
  }

  offset_x *= ratioX;

  QPointF c1(pos_output_.x() + offset_x, pos_output_.y() + offset_y);
  QPointF c2(pos_input_.x() - offset_x, pos_input_.y() - offset_y);
  return std::make_pair(c1, c2);
}

ui_scene::ui_scene(QObject* parent)
  : QGraphicsScene(parent)
  , next_id_{0}
  , draft_{nullptr}
{
  setItemIndexMethod(QGraphicsScene::NoIndex);
}

auto ui_scene::save() const -> json
{
  auto jstages = json::array_type{};
  for (auto item : items())
  {
    if (auto s = dynamic_cast<ui_stage*>(item))
    {
      auto jstage = json::object_type{};
      jstage["id"] = json{s->id()};
      jstage["type"] = json{s->get_stage()->metadata().name};
      jstage["x"] = json{s->x()};
      jstage["y"] = json{s->y()};
      jstage.emplace("parameters", s->get_stage()->save());
      jstages.emplace_back(std::move(jstage));
    }
  }

  auto jconnections = json::array_type{};
  for (auto item : items())
  {
    if (auto con = dynamic_cast<ui_connection*>(item))
    {
      if (!con->stage_output() || !con->stage_input())
        continue;

      auto jcon = json::object_type{};
      jcon["source"] = json{con->stage_output()->id()};
      jcon["output"] = json{con->stage_output()->get_stage()->outputs()[con->port_output()].name};
      jcon["destination"] = json{con->stage_input()->id()};
      jcon["input"] = json{con->stage_input()->get_stage()->inputs()[con->port_input()].name};
      jconnections.emplace_back(std::move(jcon));
    }
  }

  auto jtop = json::object_type{};
  jtop.emplace("stages", std::move(jstages));
  jtop.emplace("connections", std::move(jconnections));
  return json{std::move(jtop)};
}

auto ui_scene::load(json const& j) -> void
try
{
  clear(); // is this okay? do we need to manually implement clear to ensure nodes are deleted before being removed?
  draft_ = nullptr;

  for (auto& jstage : j["stages"].array())
  {
    /* by calling 'load' on the stage before creating the ui_stage wrapper we will already have all the correct parameter
     * values set when the controls are created.  this ensures they have the correct values without needing to be updated. */
    auto s = stage::create(jstage["type"].string());
    s->load(jstage["parameters"]);

    auto uis = new ui_stage(
          jstage["id"].number<int>()
        , std::move(s)
        , QPointF(jstage["x"].number<double>(), jstage["y"].number<double>()));
    next_id_ = std::max(uis->id() + 1, next_id_);
    addItem(uis);
  }

  for (auto& jcon : j["connections"].array())
  {
    auto source = jcon["source"].number<int>();
    auto destination = jcon["destination"].number<int>();
    ui_stage* stage_output = nullptr;
    ui_stage* stage_input = nullptr;
    for (auto item : items())
    {
      if (auto s = dynamic_cast<ui_stage*>(item))
      {
        if (s->id() == source)
          stage_output = s;
        if (s->id() == destination)
          stage_input = s;
      }
    }

    if (!stage_output)
      throw runtime_error{fmt::format("Connection refers to unknown stage {}", source)};
    if (!stage_input)
      throw runtime_error{fmt::format("Connection refers to unknown stage {}", destination)};
    auto port_output = stage_output->get_stage()->lookup_output(jcon["output"].string());
    auto port_input = stage_input->get_stage()->lookup_input(jcon["input"].string());
    addItem(new ui_connection{stage_output, port_output, stage_input, port_input});
  }
}
catch (...)
{
  // don't leave a partially loaded processing chain in place
  clear();
  throw;
}

auto ui_scene::add_stage(QString const& name, QPointF position) -> void
try
{
  addItem(new ui_stage(next_id_++, stage::create(name.toStdString()), position));
}
catch (std::exception& err)
{
  show_error_box(
        "Failed to add processing stage"
      , fmt::format("An exception occurred while instantiating stage type <b>{}</b>", name.toStdString())
      , &err);
}

auto ui_scene::draw_connection_start(ui_stage* stage, bool output, int port, QPointF pos) -> void
{
  // sanity check
  if (draft_)
  {
    delete draft_;
    draft_ = nullptr;
  }

  // create the new one
  draft_ = output
    ? new ui_connection(stage, port, nullptr, -1, pos)
    : new ui_connection(nullptr, -1, stage, port, pos);
  addItem(draft_);

  // redraw any stages already under the cursor
  draw_connection_move(pos);

  // grab the mouse (must be done after adding to scene - so can't be in constructor)
  draft_->grabMouse();
}

auto ui_scene::draw_connection_move(QPointF pos) -> void
{
  // collect a list of stages under the cursor that might want to provide port hints
  auto hinters = set<ui_stage*>{};
  for (auto item : items(pos))
    if (auto stage = dynamic_cast<ui_stage*>(item))
      hinters.insert(stage);

  // merge this list with our previous list to make a master list of who to update
  // this ensures that stages which are no longer under the cursor get one last update
  for (auto stage : hinters)
    draw_hinters_.insert(stage);

  // update all nodes in our list
  for (auto stage : draw_hinters_)
    stage->update();

  // swap in our new list so that it acts as the previous for next time
  draw_hinters_ = std::move(hinters);
}

auto ui_scene::draw_connection_finish() -> void
{
  // sanity check
  if (!draft_)
    return;

  // give the user back their mouse
  draft_->ungrabMouse();

  // abandon the connection if it didn't full establish
  if (!draft_->stage_output() || !draft_->stage_input())
    delete draft_;

  draft_ = nullptr;

  // one last update to restore visuals on any stages that were providing port hints
  for (auto stage : draw_hinters_)
    stage->update();
  draw_hinters_.clear();
}

ui_view::ui_view(ui_scene* scene, QWidget* parent)
  : QGraphicsView{scene, parent}
  , picker_{this}
  , action_delete_{QStringLiteral("Delete"), this}
{
  setDragMode(QGraphicsView::ScrollHandDrag);
  setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
  setCacheMode(QGraphicsView::CacheBackground);
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setRenderHint(QPainter::Antialiasing);

  setBackgroundBrush(ui_style::get().grid_background);

  // setup keyboard shortcuts
  action_delete_.setShortcut(Qt::Key_Delete);
  addAction(&action_delete_);
  connect(&action_delete_, &QAction::triggered, this, &ui_view::delete_selected);
}

inline auto ui_view::scene() const -> ui_scene*
{
  return static_cast<ui_scene*>(QGraphicsView::scene());
}

auto ui_view::add_stage(QPointF pos) -> void
{
  // move dialog to be centered on mouse
  auto winpos = QCursor::pos();
  winpos.setX(winpos.x() - picker_.width() / 2);
  winpos.setY(winpos.y() - picker_.height() / 2);
  picker_.move(winpos);

  // user cancelled the dialog?
  if (picker_.exec() == QDialog::Rejected)
    return;

  // user had a category selected?
  auto name = picker_.selected_stage_type();
  if (name.size() == 0)
    return;

  scene()->add_stage(name, pos);
}

auto ui_view::contextMenuEvent(QContextMenuEvent* event) -> void
{
  // if user right clicked on a stage or connection defer to those items
  if (itemAt(event->pos()))
  {
    QGraphicsView::contextMenuEvent(event);
    return;
  }

  // otherwise we treat right click in the blank area as an 'add stage' request
  add_stage(mapToScene(event->pos()));
}

auto ui_view::drawBackground(QPainter* painter, QRectF const& rectf) -> void
{
  QGraphicsView::drawBackground(painter, rectf);
  auto draw_grid = [&](QColor color, double width, double step)
  {
    auto wr = rect();
    auto tl = mapToScene(wr.topLeft());
    auto br = mapToScene(wr.bottomRight());

    auto left   = std::floor(tl.x() / step - 0.5);
    auto right  = std::floor(br.x() / step + 1.0);
    auto bottom = std::floor(tl.y() / step - 0.5);
    auto top    = std::floor(br.y() / step + 1.0);

    painter->setPen(QPen(color, width));
    for (auto xi = int(left); xi <= int(right); ++xi)
      painter->drawLine(QLineF(xi * step, bottom * step, xi * step, top * step));
    for (auto yi = int(bottom); yi <= int(top); ++yi)
      painter->drawLine(QLineF(left * step, yi * step, right * step, yi * step));
  };
  auto& style = ui_style::get();
  draw_grid(style.grid_minor_color, style.grid_minor_width, style.grid_minor_spacing);
  draw_grid(style.grid_major_color, style.grid_major_width, style.grid_major_spacing);
}

auto ui_view::mouseMoveEvent(QMouseEvent* event) -> void
{
  QGraphicsView::mouseMoveEvent(event);
  if (!scene()->mouseGrabberItem() && event->buttons() == Qt::LeftButton)
  {
    auto delta = click_pos_ - mapToScene(event->pos());
    setSceneRect(sceneRect().translated(delta));
  }
}

auto ui_view::mousePressEvent(QMouseEvent* event) -> void
{
  QGraphicsView::mousePressEvent(event);
  if (event->button() == Qt::LeftButton)
    click_pos_ = mapToScene(event->pos());
}

auto ui_view::wheelEvent(QWheelEvent* event) -> void
{
  // increment of 120 is a 'standard' single click on a mouse wheel
  auto clicks = event->angleDelta().y() / 120.0;

  // determine our desired scaling factor (a single wheel click increases zoom by 20%)
  auto factor = std::pow(1.20, clicks);

  scale(factor, factor);
}

auto ui_view::delete_selected() -> void
{
  // delete connections first to prevent deletion of stages invalidating our list of selected items
  for (auto item : scene()->selectedItems())
    if (dynamic_cast<ui_connection*>(item))
      delete item;
  for (auto item : scene()->selectedItems())
    if (dynamic_cast<ui_stage*>(item))
      delete item;
}

ui_main_window::ui_main_window()
  : scene_{this}
  , view_{&scene_, this}
{
  auto menu_file = menuBar()->addMenu(tr("&File"));
  menu_file->addAction("Add Stage", this, &ui_main_window::add_stage);
  menu_file->addSeparator();
  menu_file->addAction("Save", this, &ui_main_window::save_file_dialog);
  menu_file->addAction("Load", this, &ui_main_window::load_file_dialog);
  menu_file->addSeparator();
  menu_file->addAction("Exit", [](){ QCoreApplication::quit(); });

  auto menu_help = menuBar()->addMenu(tr("&Help"));
  menu_help->addAction(tr("&About"), this, &ui_main_window::about);

  setCentralWidget(&view_);

  setWindowTitle("Visual Interactive Signal Processor for Weather Radar");
  resize(1024, 768);
}

auto ui_main_window::save_file_dialog() -> void
{
  auto path = QFileDialog::getSaveFileName(this, tr("Choose file"), QDir::homePath(), "Signal Processing Plans (*.vispwr)");
  if (path.isEmpty())
    return;
  try
  {
    auto path_str = path.toStdString();
    if (!path_str.ends_with(".vispwr"))
      path_str.append(".vispwr");
    auto os = std::ofstream{path_str};
    scene_.save().write(os);
  }
  catch (std::exception& err)
  {
    show_error_box(
          "Failed to save signal processing chain"
        , fmt::format("An exception occurred while saving file <b>{}</b>", path.toStdString())
        , &err);
  }
}

auto ui_main_window::load_file_dialog() -> void
{
  auto path = QFileDialog::getOpenFileName(this, tr("Choose file"), QDir::homePath(), "Signal Processing Plans (*.vispwr)");
  if (path.isEmpty())
    return;
  try
  {
    load_file(path);
  }
  catch (std::exception& err)
  {
    show_error_box(
          "Failed to load signal processing chain"
        , fmt::format("An exception occurred while loading file <b>{}</b>", path.toStdString())
        , &err);
  }
}

auto ui_main_window::add_stage() -> void
{
  view_.add_stage(QPointF{0.0, 0.0});
}

auto ui_main_window::about() -> void
{
  auto mbox = QMessageBox{this};
  mbox.setText("Vispwr: Visual Interactive Signal Processor for Weather Radar");
  mbox.setInformativeText("<b>Website:</b> <a href=\"https://gitlab.com/vispwr\">https://gitlab.com/vispwr</a>");
  //mbox.setIcon(QMessageBox::About);
  mbox.exec();
}

auto ui_main_window::load_file(QString const& path) -> void
{
  auto is = std::ifstream{path.toStdString()};
  if (is.fail())
    throw std::runtime_error{fmt::format("Failed to open file")};
  scene_.load(json::read(is));
}

auto ui_main_window::closeEvent(QCloseEvent*) -> void
{
  QCoreApplication::quit();
}

int main(int argc, char* argv[])
try
{
  auto app = QApplication{argc, argv};
  app.setStyle(QStyleFactory::create("fusion"));

  auto palette = QPalette{};
  palette.setColor(QPalette::Window, QColor(53,53,53));
  palette.setColor(QPalette::WindowText, Qt::white);
  palette.setColor(QPalette::Base, QColor(45,45,45));
  palette.setColor(QPalette::AlternateBase, QColor(53,53,53));
  palette.setColor(QPalette::ToolTipBase, QColor(45, 45, 45));
  palette.setColor(QPalette::ToolTipText, Qt::white);
  palette.setColor(QPalette::Text, Qt::white);
  palette.setColor(QPalette::Button, QColor(53,53,53));
  palette.setColor(QPalette::ButtonText, Qt::white);
  palette.setColor(QPalette::BrightText, Qt::red);
  palette.setColor(QPalette::Link, QColor(42, 130, 218));
  palette.setColor(QPalette::Highlight, QColor(42, 130, 218));
  palette.setColor(QPalette::HighlightedText, Qt::black);
  app.setPalette(palette);

  /* Take care if you are thinking about changing this value or calling setOrganisationName().  The values that
   * are set here are used by the QStandardPaths class to build paths for our system and user data directories. */
  QCoreApplication::setApplicationName("vispwr");

  auto win = ui_main_window{};
  win.move(QApplication::primaryScreen()->availableGeometry().center() - win.rect().center());
  win.showNormal();

  // hack load from command line
  if (argc > 1)
    win.load_file(argv[1]);

  return app.exec();
}
catch (std::exception& err)
{
  fmt::print("Fatal exception:\n-> {}\n", err);
}
