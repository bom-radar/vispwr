/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "fft.h"

using namespace vispwr;

namespace vispwr
{
  enum class window
  {
      rectangle
    , hamming
    , blackman
  };

  class build_spectrum : public stage
  {
  protected:
    build_spectrum();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<batch const> data, int input) -> void override;

  private:
    auto calculate_spectrum(array2cf const& iq, size_t fft_size) -> array2cf;

  private:
    int     zero_pad_;
    window  window_;
  };
}

static constexpr auto window_names = std::array<string_view, 3>
{{
    "Rectangle"
  , "Hamming"
  , "Blackman"
}};

static constexpr auto plist = std::array<string_view, 2>{ "Zero Pad", "Window" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "batch", data_type::batch, true, "Input batches" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
    { "spectra", data_type::spectra, "Output spectrums" }
}};

static const auto desc = R"(
Convert batched pulse data from time to frequency domain.  User may select window function used to reduce spectral
contamination due to non-periodicity of pulse sample.  User may also enable zero padding of input data to increase
apparent resolution of spectrum bins.

**Input:** Batches

**Output:** Spectra
)";

static auto ireg = stage::enrol<build_spectrum>("build_spectrum", "Build Spectrum", "Process", desc);

build_spectrum::build_spectrum()
  : stage{plist, ilist, olist}
  , zero_pad_{1}
  , window_{window::rectangle}
{ }

auto build_spectrum::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto build_spectrum::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Zero Pad")
    return parameter_integer
    {
      .name = "Zero Pad",
      .description = "",
      .min = 1,
      .max = 5,
      .value = zero_pad_,
    };
  if (name == "Window")
    return parameter_enumerate
    {
      .name = "Window",
      .description = "Window function for FFT",
      .values = window_names,
      .value = string(window_names[static_cast<int>(window_)]),
    };
  return {};
}

auto build_spectrum::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Zero Pad")
    zero_pad_ = std::get<int>(value);
  else if (name == "Window")
  {
    auto i = std::find(window_names.begin(), window_names.end(), std::get<string>(value));
    if (i == window_names.end())
      throw std::runtime_error{"Unknown moment"};
    window_ = static_cast<window>(i - window_names.begin());
  }
}

auto build_spectrum::process(shared_ptr<batch const> data, int input) -> void
{
  auto s = make_shared<spectra>();

  s->id = data->id;
  s->batch_id = data->id;
  s->range_start = data->range_start;
  s->range_step = data->range_step;
  s->prf = data->prf;
  s->prf_index = data->prf_index;
  s->gate_count = data->gate_count;
  s->pulse_count = data->pulse_count;
  s->fft_size = data->pulse_count * zero_pad_; // simple multiplier (for now)
  s->pulses = data->pulses;

  // build our spectrums
  if (data->iq_rx_h.size() != 0)
    s->fiq_rx_h = calculate_spectrum(data->iq_rx_h, s->fft_size);
  if (data->iq_rx_v.size() != 0)
    s->fiq_rx_v = calculate_spectrum(data->iq_rx_v, s->fft_size);

  send(0, std::move(s));
}

auto build_spectrum::calculate_spectrum(array2cf const& iq, size_t fft_size) -> array2cf
{
  auto gate_count = iq.shape()[0];
  auto pulse_count = iq.shape()[1];

  // precompute window coefficients
  /* division by coefficients RMS corrects for the gain of the window function.  since we care about power quantities
   * we use RMS instead of mean. i.e. we are calculating energy correction factor not amplitude correction factor. */
  auto win = array1f{pulse_count};
  auto win_rms = 0.0;
  switch (window_)
  {
  case window::rectangle:
    for (auto i = 0uz; i < pulse_count; ++i)
      win[i] = 1.0f;
    break;
  case window::hamming:
    for (auto i = 0uz; i < pulse_count; ++i)
    {
      win[i] = 0.54 - 0.46 * cos((2.0 * pi * i) / (pulse_count - 1));
      win_rms += win[i] * win[i];
    }
    win_rms = sqrt(win_rms / pulse_count);
    for (auto i = 0uz; i < pulse_count; ++i)
      win[i] /= win_rms;
    break;
  case window::blackman:
    for (auto i = 0uz; i < pulse_count; ++i)
    {
      win[i] = ((1 - 0.16) / 2) - 0.5 * cos((2 * pi * i) / (pulse_count - 1)) + 0.5 * 0.16 * cos((4 * pi * i) / (pulse_count - 1));
      win_rms += win[i] * win[i];
    }
    win_rms = sqrt(win_rms / pulse_count);
    for (auto i = 0uz; i < pulse_count; ++i)
      win[i] /= win_rms;
    break;
  }

  auto tbuf = array2cf{gate_count, fft_size};
  auto fiq = array2cf{gate_count, fft_size};
  for (auto igate = 0uz; igate < gate_count; ++igate)
  {
    // copy in data while applying the selected window function
    for (auto i = 0uz; i < pulse_count; ++i)
      tbuf[igate, i] = iq[igate, i] * win[i];

    // zero pad out to selected fft size
    for (auto i = pulse_count; i < fft_size; ++i)
      tbuf[igate, i] = complex<float>{0.0f, 0.0f};
  }

  // convert to frequency domain
  fft_fwd(tbuf, fiq);

  for (auto igate = 0uz; igate < gate_count; ++igate)
  {
    for (auto i = 0uz; i < fft_size; ++i)
    {
      // normalize result due to fftw lack of normalization
      fiq[igate, i] /= fft_size;

      // restore amplitude lost due to zero padding
      // HACK - is this needed and/or correct????
      fiq[igate, i] *= sqrt(zero_pad_);
    }
  }

  return fiq;
}
