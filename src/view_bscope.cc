/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"
#include "ui_raster.h"
#include "ui_style.h"

#include <QImage>
#include <QPen>
#include <QRectF>

using namespace vispwr;

namespace vispwr
{
  struct ray_data
  {
    float   azimuth_l;  // low azimuth edge (not received order)
    float   azimuth_h;  // high azimuth edge (not received order)
    int     ray;        // index of row containing ray pixels in scan_data::data
  };

  using azimuth_store = vector<ray_data>;

  inline auto find_ray(azimuth_store const& store, float azimuth) -> int
  {
    auto iray = std::ranges::lower_bound(store, azimuth, {}, &ray_data::azimuth_h);
    if (iray == store.end())
      return -1;
    /* if the previous ray ended 'very close' to the start of this ray then make it continuous to avoid a seam between
     * the rays.  otherwise just comare to our azimuth_l.  i'm using a 'very close' of 10% of the ray width. */
    if (iray != store.begin() && iray->azimuth_l - std::prev(iray)->azimuth_h < 0.1f * (iray->azimuth_h - iray->azimuth_l))
      return iray->ray;
    return azimuth < iray->azimuth_l ? -1 : iray->ray;
  }

  struct scan_data
  {
    azimuth_store azimuths;     // rays sorted by azimuth_h
    float         range_start;
    float         range_step;
    array2f       data;         // rays in received order
  };

  class bscope_scene : public ui_raster_scene
  {
    Q_OBJECT
  public:
    bscope_scene(QObject* parent = nullptr);

    auto scene_to_polar(QPointF pos) const -> optional<polar_coordinates> override;
    auto polar_to_scene(polar_coordinates coords) const -> QPointF override;
    auto pick_value(polar_coordinates coords) const -> optional<float> override;

  signals:
    void aspect_ratio_updated(double ratio);
    void data_updated(shared_ptr<scan_data> data);

  protected:
    auto drawBackground(QPainter* painter, QRectF const& rect) -> void override;

    auto on_aspect_ratio_updated(double ratio) -> void;
    auto on_data_updated(shared_ptr<scan_data> data) -> void;

  private:
    QPen      azimuth_pen_;
    QPen      range_pen_;
    double    range_spacing_;
    double    azimuth_spacing_;
    double    aspect_ratio_;

    shared_ptr<scan_data> data_;
  };

  class view_bscope : public stage
  {
  protected:
    view_bscope();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<state const> data, int input) -> void override;
    auto process(shared_ptr<moments const> data, int input) -> void override;

  private:
    ui_raster_window      window_;
    string                moment_;
    double                aspect_ratio_;

    moment_list_ptr       moment_list_;     // most recently received moment list
    int                   imoment_;         // index of 'moment_' in most recently received moment list

    shared_ptr<scan_data> data_;
    size_t                rays_;
    size_t                gates_;
  };
}

bscope_scene::bscope_scene(QObject* parent)
  : ui_raster_scene{parent}
  , azimuth_pen_{ui_style::get().ppi_axes_color, ui_style::get().ppi_axes_width, Qt::SolidLine}
  , range_pen_{ui_style::get().ppi_rings_color, ui_style::get().ppi_rings_width, Qt::DotLine}
  , range_spacing_{50.0}
  , azimuth_spacing_{90.0}
  , aspect_ratio_{2}
{
  azimuth_pen_.setCosmetic(true);
  range_pen_.setCosmetic(true);

  // just to get default range setup
  on_data_updated(nullptr);

  // these connections ensure updates are received on the GUI thread
  connect(this, &bscope_scene::aspect_ratio_updated, this, &bscope_scene::on_aspect_ratio_updated);
  connect(this, &bscope_scene::data_updated, this, &bscope_scene::on_data_updated);
}

auto bscope_scene::scene_to_polar(QPointF pos) const -> optional<polar_coordinates>
{
  if (pos.x() < -180.0 || pos.x() > 180.0 || pos.y() > 0.0)
    return nullopt;

  auto azimuth = pos.x() < 0.0 ? pos.x() + 360.0 : pos.x();
  auto range = -pos.y() / aspect_ratio_;
  return polar_coordinates{0.5, float(azimuth), float(range)}; // TODO elevation
}

auto bscope_scene::polar_to_scene(polar_coordinates coords) const -> QPointF
{
  return QPointF
  {
      coords.azimuth > 180.0 ? coords.azimuth - 360.0 : coords.azimuth
    , -coords.range * aspect_ratio_
  };
}

auto bscope_scene::pick_value(polar_coordinates coords) const -> optional<float>
{
  // elevation ignored for a ppi
  if (!data_)
    return nullopt;
  auto gate = gate_from_range(data_->range_start, data_->range_step, coords.range);
  if (gate >= data_->data.shape()[1])
    return nullopt;
  auto ray = find_ray(data_->azimuths, coords.azimuth);
  if (ray < 0)
    return nullopt;
  return data_->data[ray, gate];
}

auto bscope_scene::drawBackground(QPainter* painter, QRectF const& rect) -> void
{
  painter->setClipRect(rect);

  // determine the exposed region size in pixels
  auto size = painter->combinedTransform().mapRect(rect).size();

  // create an image of this size to render out patch
  auto image = QImage{int(std::ceil(size.width())), int(std::ceil(size.height())), QImage::Format::Format_RGB888};

  // handle before first scan received
  if (!data_ || data_->data.size() == 0)
  {
    image.fill(color_missing());
    painter->drawImage(rect, image);
    return;
  }

  // write our pixels
  for (auto y = 0; y < image.height(); ++y)
  {
    for (auto x = 0; x < image.width(); ++x)
    {
      // scale our x,y from image pixel to world coordinates
      auto yy = rect.y() + ((y + 0.5f) / image.height()) * rect.height();
      auto xx = rect.x() + ((x + 0.5f) / image.width()) * rect.width();

      // convert from x,y to range, azimuth
      auto coords = bscope_scene::scene_to_polar(QPointF{xx, yy});
      if (!coords)
      {
        image.setPixelColor(x, y, color_missing());
        continue;
      }

      // find the range bin at this range
      auto gate = gate_from_range(data_->range_start, data_->range_step, coords->range);
      if (gate >= data_->data.shape()[1])
      {
        image.setPixelColor(x, y, color_missing());
        continue;
      }

      // find a ray at this azimuth
      auto ray = find_ray(data_->azimuths, coords->azimuth);
      if (ray < 0)
      {
        image.setPixelColor(x, y, color_missing());
        continue;
      }

      // valid location in our dataset, so determine and output the color
      image.setPixelColor(x, y, determine_color(data_->data[ray, gate]));
    }
  }

  // draw the image to the area of interest
  painter->drawImage(rect, image);

  // draw the grid lines and range rings
  {
    auto max_range = height() / aspect_ratio_;

    //painter->setRenderHint(QPainter::Antialiasing);
    //painter->setCompositionMode(QPainter::RasterOp_SourceXorDestination);

    painter->setPen(azimuth_pen_);
    for (auto azimuth = -180.0; azimuth <= 180.0; azimuth += azimuth_spacing_)
      painter->drawLine(azimuth, -max_range * aspect_ratio_, azimuth, 0.0);

    painter->setPen(range_pen_);
    for (auto range = 0.0f; range <= max_range; range += range_spacing_)
      painter->drawLine(-180.0, -range * aspect_ratio_, 180.0, -range * aspect_ratio_);
  }
}

auto bscope_scene::on_aspect_ratio_updated(double ratio) -> void
{
  aspect_ratio_ = ratio;
  update_picks();
  update();
}

auto bscope_scene::on_data_updated(shared_ptr<scan_data> data) -> void
{
  data_ = std::move(data);

  // recalculate the maximum range
  auto range = data_ ? data_->range_start + data_->range_step * data_->data.shape()[1] : 250;
  setSceneRect(-180, -range * aspect_ratio_, 360.0, range * aspect_ratio_);

  // update all of our pickers so they display the new value
  update_picks();

  update();
}

static constexpr auto plist = std::array<string_view, 4>{ "Moment", "Aspect Ratio", "Display", "geometry" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "moments", data_type::moments, true, "Input moments" }
}};
static constexpr auto olist = std::array<output_meta, 0>
{{
}};

static const auto desc = R"(
View moment data in a B-Scope style view, where the X axis represents azimuth angle and the Y axis represents
slant range.

**Input:** Moments

**Output:** None
)";

static auto ireg = stage::enrol<view_bscope>("view_bscope", "B-Scope", "Display", desc);

view_bscope::view_bscope()
  : stage{plist, ilist, olist}
  , window_{make_unique<bscope_scene>(), "B-Scope", QSize{360, 500}}
  , moment_{"DBZH"}
  , aspect_ratio_{2.0}
  , imoment_{-1}
  , rays_{0}
  , gates_{0}
{
  QObject::connect(&window_, &ui_raster_window::window_hidden, [this]() { notify_parameter_update("Display"); });
}

auto view_bscope::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto view_bscope::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Moment")
    return parameter_enumerate
    {
      .name = "Moment",
      .description = "Moment to display",
      .values = master_moment_list,
      .value = moment_,
    };
  if (name == "Aspect Ratio")
    return parameter_real
    {
      .name = "Aspect Ratio",
      .description = "Range (km) to Azimuth (deg) aspect ratio",
      .decimals = 1,
      .min = 0.2,
      .max = 5.0,
      .step = 0.1,
      .value = aspect_ratio_,
    };
  if (name == "Display")
    return parameter_trigger
    {
      .name = "Display",
      .description = "Open or close display window",
      .label = window_.isVisible() ? "Hide" : "Show",
    };
  if (name == "geometry")
    return parameter_string
    {
      .name = "geometry",
      .description = "Window geometry",
      .visible = false,
      .value = fmt::format("{} {} {} {}", window_.x(), window_.y(), window_.width(), window_.height()),
    };
  return {};
}

auto view_bscope::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Moment")
  {
    moment_ = std::get<string>(value);
    imoment_ = -1;

    if (moment_list_)
    {
      auto imom = std::find(moment_list_->begin(), moment_list_->end(), moment_);
      if (imom != moment_list_->end())
        imoment_ = imom - moment_list_->begin();
    }

    // tell the underlying scene to load a new color map and redraw
    window_.scene()->moment_updated(moment_);
  }
  else if (name == "Aspect Ratio")
  {
    aspect_ratio_ = std::get<double>(value);
    static_cast<bscope_scene*>(window_.scene())->aspect_ratio_updated(aspect_ratio_);
  }
  else if (name == "Display")
  {
    if (std::get<bool>(value) == true)
    {
      window_.setVisible(!window_.isVisible());
      notify_parameter_update("Display");
    }
  }
  else if (name == "geometry")
  {
    int x, y, w, h;
    if (sscanf(std::get<string>(value).c_str(), "%d %d %d %d", &x, &y, &w, &h) != 4)
      throw std::runtime_error{"Bad window geometry"};
    window_.setGeometry(x, y, w, h);
  }
}

auto view_bscope::process(shared_ptr<state const> data, int input) -> void
{
  if (data->name == "iq-end" && data_)
  {
    // trim to final array size
    data_->azimuths.resize(rays_);
    data_->data.resize_preserve(rays_, gates_, std::numeric_limits<float>::quiet_NaN());

    // eliminate degenerates ray(s) that wrap around from 359 to 0 by splitting it
    for (auto i = 0uz; i < data_->azimuths.size(); ++i)
    {
      if (data_->azimuths[i].azimuth_l < 10.0f && data_->azimuths[i].azimuth_h > 350.0f)
      {
        data_->azimuths.insert(data_->azimuths.begin() + i, data_->azimuths[i]);
        data_->azimuths[i].azimuth_l = data_->azimuths[i].azimuth_h;
        data_->azimuths[i].azimuth_h = 360.0f;
        data_->azimuths[i + 1].azimuth_h = data_->azimuths[i + 1].azimuth_l;
        data_->azimuths[i + 1].azimuth_l = 0.0f;
        ++i;
      }
    }

    // eliminate any overscanned rays
    /* we keep the more recent rays since we assume the rays at the start are transition rays that were recorded
     * while waiting for the antenna to stabilize. */
    auto overscan_rays = 0uz;
    while (overscan_rays + 1 < data_->azimuths.size())
    {
      // see if we have a ray that overlaps the same median azimuth (search from end for efficiency)
      auto& osr = data_->azimuths[overscan_rays];
      auto median = osr.azimuth_l + (osr.azimuth_h - osr.azimuth_l) * 0.5;
      auto iray = std::find_if(data_->azimuths.begin() + overscan_rays + 1, data_->azimuths.end(), [&](auto& v)
      {
        return median >= v.azimuth_l && median <= v.azimuth_h;
      });
      if (iray == data_->azimuths.end())
        break;
      ++overscan_rays;
    }
    data_->azimuths.erase(data_->azimuths.begin(), data_->azimuths.begin() + overscan_rays);

    // sort our ray index for efficient use in the display
    // we sort on azimuth_h because that's what we use for the std::lower_bound lookup
    std::ranges::sort(data_->azimuths, {}, &ray_data::azimuth_h);

    // send to the window for display
    static_cast<bscope_scene*>(window_.scene())->data_updated(std::move(data_));
    data_.reset();
  }
}

auto view_bscope::process(shared_ptr<moments const> data, int input) -> void
{
  // start a new sweep?
  if (!data_)
  {
    // initialize a new scan and assume it will be the same size as the last
    data_ = make_shared<scan_data>();
    data_->azimuths.resize(rays_, {0.0f, 0.0f, -1});
    data_->range_start = data->range_start;
    data_->range_step = data->range_step;
    data_->data.resize_preserve(rays_, gates_, std::numeric_limits<float>::quiet_NaN());

    // reset the ray and gate counters
    rays_ = 0;
    gates_ = 0;
  }

  // update the moment index if our moment list has changed
  if (moment_list_ != data->moments)
  {
    moment_list_ = data->moments;
    imoment_ = -1;

    auto imom = std::find(moment_list_->begin(), moment_list_->end(), moment_);
    if (imom != moment_list_->end())
      imoment_ = imom - moment_list_->begin();
  }

  auto iray = rays_;

  // update our ray and gate counters
  ++rays_;
  gates_ = std::max(gates_, data->gate_count);

  // expand the data stores as needed to accomodate the ray
  if (data_->azimuths.size() < rays_)
    data_->azimuths.resize(rays_);
  if (data_->data.shape()[0] < rays_ || data_->data.shape()[1] < gates_)
    data_->data.resize_preserve(rays_, gates_, std::numeric_limits<float>::quiet_NaN());

  // copy in our new points
  data_->azimuths[iray].azimuth_l = std::min(data->azimuth_first, data->azimuth_last);
  data_->azimuths[iray].azimuth_h = std::max(data->azimuth_first, data->azimuth_last);
  data_->azimuths[iray].ray = iray;
  for (auto igate = 0uz; igate < data->gate_count; ++igate)
    data_->data[iray, igate] = imoment_ != -1 ? data->data[igate, imoment_] : std::numeric_limits<float>::quiet_NaN();

  // HACK HACK
  // fill the end of the ray with NaNs to stop visual artefacts for dual-prf in the high-prf only area
  // THIS IS BAD - it will break second trip recovery!!!
  for (auto igate = data->gate_count; igate < gates_; ++igate)
    data_->data[iray, igate] = std::numeric_limits<float>::quiet_NaN();
}

#include "view_bscope.moc"
