/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "color_map.h"
#include "pick.h"
#include "types.h"

#include <QComboBox>
#include <QDialog>
#include <QDoubleSpinBox>
#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPushButton>
#include <QWidget>

namespace vispwr
{
  // TEMP HACK - just for now...
  inline constexpr std::array<string_view, 13> master_moment_list
  {
      "DBZH"
    , "VRADH"
    , "WRADH"
    , "SQIH"
    , "SNRH"
    , "DBZV"
    , "VRADV"
    , "WRADV"
    , "SQIV"
    , "SNRV"
    , "ZDR"
    , "PHIDP"
    , "RHOHV"
  };

  inline auto to_qcolor(color const& val) -> QColor
  {
    return {val.r, val.g, val.b, val.a};
  }

  class ui_raster_pick_item : public QGraphicsObject
  {
  public:
    ui_raster_pick_item(int channel);

    auto channel() const { return channel_; }
    auto on_updated() -> void;

    auto boundingRect() const -> QRectF override;

  protected:
    auto mousePressEvent(QGraphicsSceneMouseEvent* event) -> void override;
    auto paint(QPainter* painter, QStyleOptionGraphicsItem const* option, QWidget* widget = nullptr) -> void override;

  private:
    int           channel_;
    QString       label_;
  };

  class ui_raster_scene : public QGraphicsScene
  {
    Q_OBJECT
  public:
    ui_raster_scene(QObject* parent);

    virtual auto scene_to_polar(QPointF pos) const -> optional<polar_coordinates> = 0;
    virtual auto polar_to_scene(polar_coordinates coords) const -> QPointF = 0;
    virtual auto pick_value(polar_coordinates coords) const -> optional<float> = 0;

    auto set_color_scheme(string name) -> void;
    auto set_color_min(float value) -> void;
    auto set_color_max(float value) -> void;

  signals:
    void moment_updated(string moment);

    void color_scheme_changed(string value);
    void color_min_changed(float value);
    void color_max_changed(float value);

  protected:
    virtual auto drawBackground(QPainter* painter, QRectF const& rect) -> void override = 0;

    auto update_picks() -> void;

    auto on_moment_updated(string moment) -> void;

    auto determine_color(float value) -> QColor
    {
      return to_qcolor(col_map_.evaluate((value - val_offset_) * val_scale_));
    }

    auto color_missing() -> QColor { return to_qcolor(col_map_.missing()); }

  private:
    auto update_cmap_offsets() -> void;

  private:
    string    moment_;

    string    col_scheme_;
    float     min_val_;
    float     max_val_;

    color_map col_map_;
    float     val_offset_;
    float     val_scale_;

    vector<unique_ptr<ui_raster_pick_item>> pick_;
  };

  class ui_raster_view : public QGraphicsView
  {
  public:
    ui_raster_view(ui_raster_scene* scene, QWidget* parent);

    auto scene()        { return static_cast<ui_raster_scene*>(QGraphicsView::scene()); }
    auto scene() const  { return static_cast<ui_raster_scene const*>(QGraphicsView::scene()); }

    auto update_cursor_position(optional<QPointF> view_pos) -> void;

  protected:
    auto drawForeground(QPainter* painter, QRectF const& rect) -> void override;
    auto leaveEvent(QEvent* event) -> void override;
    auto mouseMoveEvent(QMouseEvent* event) -> void override;
    auto mousePressEvent(QMouseEvent* event) -> void override;
    auto mouseReleaseEvent(QMouseEvent* event) -> void override;
    auto wheelEvent(QWheelEvent* event) -> void override;

  private:
    QPointF click_pos_;
    QString cursor_label_;
    QRectF  cursor_rect_;
  };

  class ui_raster_window : public QWidget
  {
    Q_OBJECT
  public:
    ui_raster_window(unique_ptr<ui_raster_scene> scene, QString title, QSize size);

    auto scene() { return scene_.get(); }

  signals:
    void window_hidden();

  protected:
    auto hideEvent(QHideEvent* event) -> void override;
    auto enterEvent(QEnterEvent* event) -> void override;
    auto leaveEvent(QEvent* event) -> void override;
    auto resizeEvent(QResizeEvent* event) -> void override;

  private:
    unique_ptr<ui_raster_scene> scene_;
    ui_raster_view  view_;
    QPushButton     but_prefs_;

    QWidget         panel_prefs_;
    QComboBox       combo_scheme_;
    QDoubleSpinBox  spin_min_;
    QDoubleSpinBox  spin_max_;
  };
}
