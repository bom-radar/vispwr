/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"

using namespace vispwr;

namespace vispwr
{
  /// Test stage for user interface elements
  class tester : public stage
  {
  protected:
    tester();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process_idle() -> std::chrono::milliseconds override;

  private:
    std::chrono::system_clock::time_point time_;
    int int_param_;
  };
}

static constexpr auto plist = std::array<string_view, 6>{ "bool", "real", "int", "string", "enum", "path" };
static constexpr auto ilist = std::array<input_meta, 5>
{{
    { "pulse", data_type::pulse, true, "Input pulse" }
  , { "batch", data_type::batch, true, "Input batch" }
  , { "spectra", data_type::spectra, true, "Input spectra" }
  , { "autocorrelations", data_type::autocorrelations, true, "Input autocorrelations" }
  , { "moments", data_type::moments, true, "Input moments" }
}};
static constexpr auto olist = std::array<output_meta, 5>
{{
    { "pulse", data_type::pulse, "Output pulse" }
  , { "batch", data_type::batch, "Output batch" }
  , { "spectra", data_type::spectra, "Output spectra" }
  , { "autocorrelations", data_type::autocorrelations, "Output autocorrelations" }
  , { "moments", data_type::moments, "Output moments" }
}};

static const auto desc = R"(
Dummy stage used to test user interface elements.

**Input:** All

**Output:** All
)";

static auto ireg = stage::enrol<tester>("tester", "GUI tester", "Debug", desc);

tester::tester()
  : stage{plist, ilist, olist}
  , time_{std::chrono::system_clock::now()}
  , int_param_{0}
{ }

auto tester::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto tester::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "bool")
    return parameter_boolean
    {
        .name = "bool"
      , .description = "bool tip"
      , .visible = true
      , .editable = int_param_ % 3 != 0
      , .value = false
    };
  if (name == "int")
    return parameter_integer
    {
        .name = "int"
      , .description = "int tip"
      , .visible = true
      , .editable = int_param_ % 3 != 0
      , .min = 0
      , .max = 100
      , .step = 1
      , .value = int_param_
    };
  if (name == "real")
    return parameter_real
    {
        .name = "real"
      , .description = "real tip"
      , .visible = true
      , .editable = int_param_ % 3 != 0
      , .decimals = 1
      , .min = 0.0
      , .max = 10.0
      , .step = 0.5
      , .value = 1.0
    };
  if (name == "string")
    return parameter_string
    {
        .name = "string"
      , .description = "string tip"
      , .visible = true
      , .editable = int_param_ % 3 != 0
      , .value = "foo"
    };
  if (name == "enum")
    return parameter_enumerate
    {
        .name = "enum"
      , .description = "enum tip"
      , .visible = true
      , .editable = int_param_ % 3 != 0
      , .values = data_type_names
      , .flexible = false
      , .value = "batch"
    };
  if (name == "path")
    return parameter_path
    {
        .name = "path"
      , .description = "path tip"
      , .visible = true
      , .editable = int_param_ % 3 != 0
      , .input_path = true
      , .filter = "IQR files (*.iqr)"
      , .value = ""
    };
  return {};
}

auto tester::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "int")
    int_param_ = std::get<int>(value);
}

auto tester::process_idle() -> std::chrono::milliseconds
{
  // update our int parameter once every 2 seconds as a way to test parameter update callback functionality
  if (std::chrono::system_clock::now() > time_)
  {
    int_param_++;
    notify_parameter_update("bool");
    notify_parameter_update("int");
    notify_parameter_update("real");
    notify_parameter_update("string");
    notify_parameter_update("enum");
    notify_parameter_update("path");
    time_ = std::chrono::system_clock::now() + 2000ms;
  }
  return 100ms;
}
