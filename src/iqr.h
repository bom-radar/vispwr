/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include <filesystem>
#include <fstream>
#include <memory>
#include <unordered_map>

#include "array.h"
#include "format.h"
#include "types.h"
#include "util.h"

namespace vispwr::iqr
{
  enum class archive_data_type : int32_t
  {
      none      = 0x0000  // sentinel used by api to indicate end of file (not part of specification)

    , raw_data  = 0x0100
    , iq_gdrx   = 0x0101
    , tx_gdrx   = 0x0102
    , gdrx5_raw = 0x0110
    , gdrx5_iq  = 0x0112
    , gdrx5_tx  = 0x0113
    , timestamp = 0x0201
    , rcl_state = 0x0301
    , rcl_rt    = 0x0302
    , rcl_file  = 0x0303
    , container = 0x0f00  // cannot be returned by peek() since we automatically recurse into containers
    , zlib      = 0xf000  // cannot be returned by peek() since we automatically recurse into containers
  };

  /// Timestamp item
  struct timestamp
  {
    int64_t time_ms_epoch;  ///< Time in milliseconds since Unix epoch
    int64_t time_ns;        ///< Time in nanoseconds since undefined arbitrary point
  };

  /// Convenience accessors for transmit data flags
  struct tx_data_flags
  {
    static constexpr bool cpi_start(uint32_t flags)                 { return flags & 0b0000000000000001; }
  };

  /// Bitfield for receive data flags
  struct rx_data_flags
  {
    static constexpr bool nafc_frequency_step(uint32_t flags)       { return flags & 0b0000000000000001; }
    static constexpr bool tx_magnitude_corrected(uint32_t flags)    { return flags & 0b0000000000000010; }
    static constexpr bool tx_phase_corrected(uint32_t flags)        { return flags & 0b0000000000000100; }
    static constexpr bool bad_antenna_tag_input(uint32_t flags)     { return flags & 0b0000000000001000; }
    static constexpr bool bad_antenna_tag_software(uint32_t flags)  { return flags & 0b0000000000010000; }
    static constexpr bool gps_timestamp_valid(uint32_t flags)       { return flags & 0b0000000000100000; }
    static constexpr bool sector_blanking_active(uint32_t flags)    { return flags & 0b0000000001000000; }
    static constexpr bool cpi_start(uint32_t flags)                 { return flags & 0b0000000010000000; }
    static constexpr bool antenna_tags_simulated(uint32_t flags)    { return flags & 0b0000000100000000; }
    static constexpr bool trigger_disabled_safety(uint32_t flags)   { return flags & 0b0000001000000000; }
    static constexpr bool signal_processing_reset(uint32_t flags)   { return flags & 0b0000010000000000; }
    static constexpr bool save_spatial_averaging(uint32_t flags)    { return flags & 0b0000100000000000; }
    static constexpr bool safe_sector_protection(uint32_t flags)    { return flags & 0b0001000000000000; }
  };

  /// Bitfield for PRF staggering flags and batch identification
  struct staggering_flags
  {
    static constexpr int  mode(uint32_t flags)                      { return (flags >>  0) & 0b0000000000001111; }
    static constexpr bool cpi_toggle(uint32_t flags)                { return (flags >>  0) & 0b0000000000100000; }
    static constexpr int  prf_batch_count(uint32_t flags)           { return (flags >>  6) & 0b0000111111111111; }
    static constexpr int  prf_batch_index(uint32_t flags)           { return (flags >> 19) & 0b0000111111111111; }
  };

  /// GDRX5 transmit pulse
  struct gdrx5_tx
  {
    uint32_t    tracking_number;
    uint32_t    pulse_counter;
    uint16_t    elevation_angle;
    uint16_t    azimuth_angle;
    float       sample_start;
    float       sample_time;
    float       sample_stop;
    uint32_t    sample_count;
    float       prf;
    float       tx_average_frequency_h;
    float       tx_average_frequency_v;
    float       tx_pulse_frequency_h;
    float       tx_pulse_frequency_v;
    float       tx_pulse_magnitude_h;
    float       tx_pulse_magnitude_v;
    float       tx_pulse_phase_h;
    float       tx_pulse_phase_v;
    float       dds_frequency_h;
    float       dds_frequency_v;
    uint32_t    tx_data_flags;
    uint32_t    staggering_flags;
    uint32_t    timestamp_sec;
    uint32_t    timestamp_nsec;
    uint32_t    timestamp_status;

    array1cf    iq_h;
    array1cf    iq_v;
  };

  /// GDRX5 receive pulse
  struct gdrx5_iq
  {
    uint32_t    tracking_number;
    uint32_t    pulse_counter;
    uint16_t    elevation_angle;
    uint16_t    azimuth_angle;
    float       range_start;
    float       range_step;
    float       range_stop;
    uint32_t    gate_count;
    float       prf;
    float       tx_average_frequency_h;
    float       tx_average_frequency_v;
    float       tx_pulse_frequency_h;
    float       tx_pulse_frequency_v;
    float       tx_pulse_magnitude_h;
    float       tx_pulse_magnitude_v;
    float       tx_pulse_phase_h;
    float       tx_pulse_phase_v;
    float       dds_frequency_h;
    float       dds_frequency_v;
    uint32_t    rx_data_flags;
    uint32_t    staggering_flags;
    uint32_t    timestamp_sec;
    uint32_t    timestamp_nsec;
    uint32_t    timestamp_status;
    uint32_t    reserved_0;
    uint32_t    matched_filter_mode;
    float       matched_filter_losses_h;
    float       matched_filter_losses_v;
    float       equivalent_noise_bandwidth_h;
    float       equivalent_noise_bandwidth_v;

    std::vector<bool> hilo_h;
    std::vector<bool> hilo_v;

    array1cf    iq_h;
    array1cf    iq_v;
  };

  /// File reader for the Leonardo IQR format
  class file
  {
  public:
    /// Open an IQR file
    file(std::filesystem::path path);

    /// Get the path used to open the file
    auto& path() const { return path_; }

    /// Get the file headers
    auto& headers() const { return headers_; }

    /// Lookup a header and throw an exception if it does not exist
    /* NOTE - waiting for heterogenous containers for unordered map to be implemented!
     * until then, passing a string view here still results in a temporary string creation
     * (but no worse than passing a char const* */
    template <typename T>
    auto header(std::string_view name) const -> std::conditional_t<std::is_same_v<T, std::string>, std::string const&, T>;

    /// Lookup a header and return default value if it does not exist
    template <typename T>
    auto header(std::string_view name, T const& def) const -> std::conditional_t<std::is_same_v<T, std::string>, std::string const&, T>;

    /// Rewind the file back to the start
    auto rewind() -> void;

    /// Determine the type of the next item in the file
    auto peek() const { return cur_type_; }

    /// Ignore the next item in the file
    auto ignore() -> void;

    /// Decode the next message as a timestamp
    auto extract(timestamp& msg) -> void;

    /// Decode the next item as a GDRX5 transmit pulse
    auto extract(gdrx5_tx& msg) -> void;

    /// Decode the next item as a GDRX5 receive pulse
    auto extract(gdrx5_iq& msg) -> void;

  private:
    using header_store = std::unordered_map<std::string, std::string, string_hash, std::equal_to<>>;

  private:
    auto read_next_header() -> void;
    auto read_current_and_advance(archive_data_type expected) -> void;

  private:
    std::filesystem::path   path_;        ///< Path used to open file
    std::ifstream           file_;        ///< Handle to file

    header_store            headers_;     ///< Original file headers
    size_t                  body_offset_; ///< Offset of main body data in file

    archive_data_type       cur_type_;    ///< Type of next item (if header already read)
    int64_t                 cur_length_;  ///< Body length of next item

    std::vector<char>       buffer_;      ///< Buffer of preloaded data
  };

  template <typename T>
  auto file::header(std::string_view name) const -> std::conditional_t<std::is_same_v<T, std::string>, std::string const&, T>
  {
    auto i = headers_.find(name);
    if (i == headers_.end())
      throw std::runtime_error{fmt::format("Unknown header {}", name)};
    if constexpr (std::is_same_v<T, std::string>)
      return i->second;
    else
    {
      auto val = T{};
      auto res = std::from_chars(&i->second[0], &i->second[i->second.size()], val);
      if (res.ptr != &i->second[i->second.size()])
        throw std::runtime_error{fmt::format("Failed to parse header {}", name)};
      return val;
    }
  }

  template <typename T>
  auto file::header(std::string_view name, T const& def) const -> std::conditional_t<std::is_same_v<T, std::string>, std::string const&, T>
  {
    auto i = headers_.find(name);
    if (i == headers_.end())
      return def;
    if constexpr (std::is_same_v<T, std::string>)
      return i->second;
    else
    {
      auto val = T{};
      auto res = std::from_chars(&i->second[0], &i->second[i->second.size()], val);
      if (res.ptr != &i->second[i->second.size()])
        throw std::runtime_error{fmt::format("Failed to parse header {}", name)};
      return val;
    }
  }
}

namespace fmt
{
  template <typename Char>
  struct formatter<vispwr::iqr::archive_data_type, Char> : formatter<char const*, Char>
  {
    template <typename FormatContext>
    auto format(vispwr::iqr::archive_data_type const& val, FormatContext& ctx)
    {
      char const* label = "unknown";
      switch (val)
      {
      case vispwr::iqr::archive_data_type::none:      label = "none";       break;
      case vispwr::iqr::archive_data_type::raw_data:  label = "raw_data";   break;
      case vispwr::iqr::archive_data_type::iq_gdrx:   label = "iq_gdrx";    break;
      case vispwr::iqr::archive_data_type::tx_gdrx:   label = "tx_gdrx";    break;
      case vispwr::iqr::archive_data_type::gdrx5_raw: label = "gdrx5_raw";  break;
      case vispwr::iqr::archive_data_type::gdrx5_iq:  label = "gdrx5_iq";   break;
      case vispwr::iqr::archive_data_type::gdrx5_tx:  label = "gdrx5_tx";   break;
      case vispwr::iqr::archive_data_type::timestamp: label = "timestamp";  break;
      case vispwr::iqr::archive_data_type::rcl_state: label = "rcl_state";  break;
      case vispwr::iqr::archive_data_type::rcl_rt:    label = "rcl_rt";     break;
      case vispwr::iqr::archive_data_type::rcl_file:  label = "rcl_file";   break;
      case vispwr::iqr::archive_data_type::container: label = "container";  break;
      case vispwr::iqr::archive_data_type::zlib:      label = "zlib";       break;
      }
      return formatter<char const*, Char>::format(label, ctx);
    }
  };
}
