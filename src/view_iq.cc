/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "calibration.h"
#include "pick.h"

#include <QLineSeries>
#include <QSplineSeries>
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <QToolBar>
#include <QScatterSeries>
#include <QHBoxLayout>
#include <QVBoxLayout>

using namespace vispwr;

namespace vispwr
{
  class iq_window : public QWidget
  {
    Q_OBJECT
  public:
    iq_window();

  signals:
    void window_hidden();
    void set_data(shared_ptr<batch const> data, int igate, shared_ptr<calibration const> c, bool spline);

  protected:
    auto hideEvent(QHideEvent* event) -> void override;

  private:
    auto on_set_data(shared_ptr<batch const> data, int igate, shared_ptr<calibration const> c, bool spline) -> void;
    auto redraw_plot() -> void;

  private:
    QValueAxis* x_axis_;
    QValueAxis* y_axis_;
    QChart*     chart_;
    QChartView* view_;

    // current data to plot
    shared_ptr<batch const> batch_;
    int igate_;
    shared_ptr<calibration const> calib_;
    bool spline_;
  };

  class view_iq : public stage
  {
  protected:
    view_iq();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<batch const> data, int input) -> void override;

  private:
    int       picker_;
    int       calib_channel_;
    bool      spline_;
    iq_window window_;
    QMetaObject::Connection con_pick_updated_;  // handle to signal/slot connection for current picker
  };
}

static constexpr auto plist = std::array<string_view, 5>{ "Picker", "Calib", "Spline", "Display", "geometry" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "batch", data_type::batch, true, "Input batches" }
}};
static constexpr auto olist = std::array<output_meta, 0>
{{
}};

static const auto desc = R"(
Plot the raw received IQ data for the range bin nearest a selected location.

**Input:** Batches

**Output:** None
)";

static auto ireg = stage::enrol<view_iq>("view_iq", "Batch RX (Cartesian)", "Display", desc);

view_iq::view_iq()
  : stage{plist, ilist, olist}
  , picker_{0}
  , calib_channel_{0}
  , spline_{false}
{
  QObject::connect(&window_, &iq_window::window_hidden, [this]() { notify_parameter_update("Display"); });
  con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
}

auto view_iq::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto view_iq::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Picker")
    return parameter_integer
    {
      .name = "Picker",
      .description = "Input pick channel for batch selection",
      .min = 0,
      .max = pick::channels - 1,
      .value = picker_,
    };
  if (name == "Calib")
    return parameter_integer
    {
      .name = "Calib",
      .description = "Calibration channel",
      .min = 0,
      .max = 10,
      .value = calib_channel_,
    };
  if (name == "Spline")
    return parameter_boolean
    {
      .name = "Spline",
      .description = "Draw as splines",
      .value = spline_,
    };
  if (name == "Display")
    return parameter_trigger
    {
      .name = "Display",
      .description = "Open or close display window",
      .label = window_.isVisible() ? "Hide" : "Show",
    };
  if (name == "geometry")
    return parameter_string
    {
      .name = "geometry",
      .description = "Window geometry",
      .visible = false,
      .value = fmt::format("{} {} {} {}", window_.x(), window_.y(), window_.width(), window_.height()),
    };
  return {};
}

auto view_iq::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Picker")
  {
    picker_ = std::get<int>(value);
    if (con_pick_updated_)
      QObject::disconnect(con_pick_updated_);
    con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
  }
  else if (name == "Calib")
    calib_channel_ = std::get<int>(value);
  else if (name == "Spline")
    spline_ = std::get<bool>(value);
  else if (name == "Display")
  {
    if (std::get<bool>(value) == true)
    {
      window_.setVisible(!window_.isVisible());
      notify_parameter_update("Display");
    }
  }
  else if (name == "geometry")
  {
    int x, y, w, h;
    if (sscanf(std::get<string>(value).c_str(), "%d %d %d %d", &x, &y, &w, &h) != 4)
      throw std::runtime_error{"Bad window geometry"};
    window_.setGeometry(x, y, w, h);
  }
}

auto view_iq::process(shared_ptr<batch const> data, int input) -> void
{
  // if we are missing an input it means the stage is terminating
  if (!data)
    return;

  // get the calibration from our selected channel
  auto calib = calibration_get(calib_channel_);
  if (!calib)
    return;

  // get the currently picked coordiantes from our selected channel
  auto pick_data = pick::channel(picker_).get();
  if (!pick_data.valid)
    return;

  // account for ccw scanning
  auto azimuth_l = data->pulses[0].azimuth;
  auto azimuth_h = data->pulses[data->pulse_count-1].azimuth;
  if (azimuth_l > azimuth_h)
    std::swap(azimuth_l, azimuth_h);

  // account for 360..0 wrap around
  if (azimuth_l < 10.0f && azimuth_h > 350.0f)
    std::swap(azimuth_l, azimuth_h);

  // does this batch overlap with the picked arc?
  if (pick_data.azimuth < azimuth_l || pick_data.azimuth > azimuth_h)
    return;

  // do we have the desired gate?
  auto gate = gate_from_range(data->range_start, data->range_step, pick_data.range);
  if (gate >= data->gate_count)
    return;

  // emit the data updated event (will be transferred to the gui thread)
  window_.set_data(std::move(data), gate, std::move(calib), spline_);
}

iq_window::iq_window()
  : x_axis_{new QValueAxis()}
  , y_axis_{new QValueAxis()}
  , chart_{new QChart()}
  , view_{new QChartView(chart_)}
{
  x_axis_->setTitleText("In-Phase (I)");
  #if 0
  x_axis_->setTickType(QValueAxis::TicksDynamic);
  x_axis_->setTickInterval(1.0);
  x_axis_->setTickAnchor(0.0);
  #endif
  x_axis_->setLabelFormat("%g");
  y_axis_->setTitleText("Quadrature (Q)");
  chart_->addAxis(x_axis_, Qt::AlignBottom);
  chart_->addAxis(y_axis_, Qt::AlignLeft);
  chart_->legend()->setAlignment(Qt::AlignRight);

  auto toolbar = new QWidget();

  auto layout = new QVBoxLayout{this};
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addWidget(toolbar);
  layout->addWidget(view_);

  resize(600, 600);
  setWindowTitle("View IQ");
  show();

  QObject::connect(this, &iq_window::set_data, this, &iq_window::on_set_data);
}

auto iq_window::hideEvent(QHideEvent* event) -> void
{
  window_hidden();
}

auto iq_window::on_set_data(shared_ptr<batch const> data, int igate, shared_ptr<calibration const> c, bool spline) -> void
{
  batch_ = std::move(data);
  igate_ = igate;
  calib_ = std::move(c);
  spline_ = spline;
  redraw_plot();
}

auto iq_window::redraw_plot() -> void
{
  chart_->removeAllSeries();

  if (!batch_ || !calib_)
    return;

  auto min_i = std::numeric_limits<float>::max(), min_q = std::numeric_limits<float>::max();
  auto max_i = std::numeric_limits<float>::lowest(), max_q = std::numeric_limits<float>::lowest();
  if (batch_->iq_rx_h.size() != 0)
  {
    auto series = spline_ ? new QSplineSeries() : new QLineSeries();
    series->setName("Horizontal");
    series->setPointsVisible(true);
    for (auto i = 0uz; i < batch_->pulse_count; ++i)
    {
      auto iq = batch_->iq_rx_h[igate_, i];
      min_i = std::min(min_i, iq.real());
      max_i = std::max(max_i, iq.real());
      min_q = std::min(min_q, iq.imag());
      max_q = std::max(max_q, iq.imag());
      series->append(iq.real(), iq.imag());
    }
    chart_->addSeries(series);
    series->attachAxis(x_axis_);
    series->attachAxis(y_axis_);
  }
  if (batch_->iq_rx_v.size() != 0)
  {
    auto series = spline_ ? new QSplineSeries() : new QLineSeries();
    series->setName("Vertical");
    series->setPointsVisible(true);
    for (auto i = 0uz; i < batch_->pulse_count; ++i)
    {
      auto iq = batch_->iq_rx_v[igate_, i];
      min_i = std::min(min_i, iq.real());
      max_i = std::max(max_i, iq.real());
      min_q = std::min(min_q, iq.imag());
      max_q = std::max(max_q, iq.imag());
      series->append(iq.real(), iq.imag());
    }
    chart_->addSeries(series);
    series->attachAxis(x_axis_);
    series->attachAxis(y_axis_);
  }

  x_axis_->setRange(min_i, max_i);
  y_axis_->setRange(min_q, max_q);
}

#include "view_iq.moc"
