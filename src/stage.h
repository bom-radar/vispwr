/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "data_types.h"
#include "format.h"
#include "json.h"

namespace vispwr
{
#ifdef NDEBUG
  inline auto constexpr stage_debug = false;
#else
  inline auto constexpr stage_debug = true;
#endif

  /// Logging severity levels
  enum class log_level { error, warning, information };

  struct resend_request
  {
    int64_t         id;     ///< Request id common to all child requests
  };

  /// Base types for process parameters
  using parameter_value = variant<bool, int, double, string>;

  /* trigger parameter is used to create push button events that the user can trigger manually.  the 'value' of such
   * an event should be set to 'true' when calling set_parameter() to cause the trigger to fire.  however the same
   * value should alwasy be read back as 'false' from a get_parameter() call.  this allows us to serialize trigger
   * parameters to a file and safely read them back in.  if the value was returned as 'true' from a get_paramter()
   * then it would serialize 'true' to the file, and immediately trigger the event when the the stage is loaded
   * back from disk. */
  struct parameter_trigger
  {
    string  name;             ///< Canonical name
    string  description;      ///< Description for tool tip
    bool    visible = true;   ///< Should parameter be displayed in UI?
    bool    editable = true;  ///< Should parameter be user modifiable?
    string  label;            ///< Label for event
    bool    value = false;    ///< Set to true to trigger event, but should always be read by get_parameter as false
  };

  struct parameter_boolean
  {
    string  name;             ///< Canonical name
    string  description;      ///< Description for tool tip
    bool    visible = true;   ///< Should parameter be displayed in UI?
    bool    editable = true;  ///< Should parameter be user modifiable?
    bool    value = false;
  };

  struct parameter_integer
  {
    string  name;             ///< Canonical name
    string  description;      ///< Description for tool tip
    bool    visible = true;   ///< Should parameter be displayed in UI?
    bool    editable = true;  ///< Should parameter be user modifiable?
    int     min = std::numeric_limits<int>::lowest();
    int     max = std::numeric_limits<int>::max();
    int     step = 1;
    int     value = 0;
  };

  struct parameter_real
  {
    string  name;             ///< Canonical name
    string  description;      ///< Description for tool tip
    bool    visible = true;   ///< Should parameter be displayed in UI?
    bool    editable = true;  ///< Should parameter be user modifiable?
    int     decimals = 2;
    double  min = std::numeric_limits<double>::lowest();
    double  max = std::numeric_limits<double>::max();
    double  step = 1.0;
    double  value = 0.0;
  };

  struct parameter_string
  {
    string  name;             ///< Canonical name
    string  description;      ///< Description for tool tip
    bool    visible = true;   ///< Should parameter be displayed in UI?
    bool    editable = true;  ///< Should parameter be user modifiable?
    string  value;
  };

  struct parameter_enumerate
  {
    string  name;             ///< Canonical name
    string  description;      ///< Description for tool tip
    bool    visible = true;   ///< Should parameter be displayed in UI?
    bool    editable = true;  ///< Should parameter be user modifiable?
    span<string_view const> values;
    bool    flexible = false; ///< Allow values from outside the provided list?
    string  value;
  };

  struct parameter_path
  {
    string  name;             ///< Canonical name
    string  description;      ///< Description for tool tip
    bool    visible = true;   ///< Should parameter be displayed in UI?
    bool    editable = true;  ///< Should parameter be user modifiable?
    bool    input_path = true;
    string  filter = "All files (*)";
    string  value;
  };

  /// Variant of all possible parameter descriptor types
  using parameter = variant<
      parameter_trigger
    , parameter_boolean
    , parameter_integer
    , parameter_real
    , parameter_string
    , parameter_enumerate
    , parameter_path
    >;

  /// Structure that describes an input to a stage
  struct input_meta
  {
    string_view name;
    data_type   type;
    bool        exclusive;    ///< Limit to one connection?
    string_view description;
  };

  /// Structure that describes an output of a stage
  struct output_meta
  {
    string_view name;
    data_type   type;
    string_view description;
  };

  class stage;

  /// Factory function for creating a stage
  using stage_factory = auto (*)() -> shared_ptr<stage>;

  /// Structure that describes a stage
  struct stage_metadata
  {
    string_view   name;         ///< Stage class name
    string_view   label;        ///< User friendly name
    string_view   category;     ///< Stage category for grouping in UI
    stage_factory factory;      ///< Factory function
    string_view   description;  ///< Stage description (markdown)
  };
  inline auto operator<(stage_metadata const& lhs, stage_metadata const& rhs) -> bool
  {
    if (lhs.category < rhs.category) return true;
    if (rhs.category < lhs.category) return false;
    return lhs.label < rhs.label;
  }
  using stage_catalog = set<stage_metadata>;

  template <typename T>
  class auto_launch final : public T
  {
  public:
    template <typename... Args>
    auto_launch(Args&&... args) : T{std::forward<Args>(args)...} { this->worker_thread_launch(); }
    ~auto_launch()
    {
      if constexpr (stage_debug)
        fmt::print("Destroying stage {}\n", this->metadata().name);
      this->worker_thread_terminate();
    }
  };

  class stage
  {
  public:
    using peer = pair<shared_ptr<stage>, int>;

    /// Parameter change callback function
    using parameter_callback = function<void(string_view)>;

    /// Logging output callback function
    using logging_callback = function<void(log_level, string)>;

  public:
    /// Register a stage subclass with the catalog
    template <typename T>
    static auto enrol(
          string_view name
        , string_view label
        , string_view category
        , string_view description = {}
        ) -> stage_catalog::iterator;

    /// Access the stage catalog
    static auto catalog() -> stage_catalog const& { return get_catalog(); }

    /// Factory function for stages
    static auto create(string_view name) -> shared_ptr<stage>;

    /// Connect an output of one stage to an input of another stage
    static auto connect(peer source, peer destination) -> void;

    /// Remove a specific connection between an output of one stage and input of another stage
    static auto disconnect(peer source, peer destination) -> void;

  public:
    virtual ~stage();

    /// Get the metadata object for this stage type
    virtual auto metadata() const -> stage_metadata const& = 0;

    /// Get information on parameters exposed by this stage
    auto parameters() const { return plist_; }

    /// Get information on inputs exposed by this stage
    auto inputs() const { return ilist_; }

    /// Get information on outputs exposed by this stage
    auto outputs() const { return olist_; }

    /// Determine the index of an input from its name
    auto lookup_input(string_view name) const -> int;

    /// Determine the index of an output from its name
    auto lookup_output(string_view name) const -> int;

    /// Serialize stage to a JSON object
    auto save() -> json;

    /// Deserialize stage from a JSON object
    auto load(json const& j) -> void;

    /// Get the current value of a parameter
    auto get_parameter(string_view name) -> parameter;

    /// Set the value of a parameter
    auto set_parameter(string_view name, parameter_value value) -> void;

    /// Check if an input is connected to at least one source
    auto input_connected(int input) const -> bool;

    /// Check if an output is connected to at least one destination
    auto output_connected(int output) const -> bool;

    /// Set parameter change callback
    /** This function allows the UI to register a callback for whenever the value or metadata associated with a stage
     *  parameter is changed.  The value passed to the callback is the parameter name.  This function will be called
     *  asynchronously from the worker thread associated with the stage, meaning that processing on the stage will stall
     *  while it is executed.  Care should be taken to not perform long tasks during the callback.
     *
     *  A suggested approach is to have the callback simply register the need to refesh the parameter, which is later
     *  handled by the main or dedicated UI thread. */
    auto set_parameter_callback(parameter_callback callback) -> void;

    /// Set log output callback
    /** This function allows the UI to register a callback for whenever the stage needs to output a message to the
     *  user.  This function will be called asynchronously from the worker thread associated with the stage, meaning
     *  that processing on the stage will stall while it is executed.  Care should be taken to not perform long tasks
     *  during the callback.
     *
     *  The typical use of this function is to report processing errors.  Any exceptions thrown by the stage during
     *  processing will be reported via this callback.  Since a misconfigured processing stage may cause exceptions to
     *  be thrown continuously, it is excpected that the callback should implement some form of rate limiting.  For
     *  example, by showing only the first N messages to the user in a GUI window, and discarding any further messages
     *  until the user clears the window. */
    auto set_logging_callback(logging_callback callback) -> void;

  protected:
    stage(span<string_view const> plist, span<input_meta const> ilist, span<output_meta const> olist);

    /// Get the current value of a parameter
    virtual auto get_parameter_impl(string_view name) const -> parameter = 0;

    /// Set the value of a parameter
    virtual auto set_parameter_impl(string_view name, parameter_value value) -> void = 0;

    /// Perform processing during idle time when the input queue is empty
    /** This function will be called any time there is no data available on the input queue.  This allows stages to
     *  perform short background tasks while waiting for input.  Output only stages such as IQ data sources, will
     *  typically perform most of their data processing in this function.
     *
     *  Just as with the process() functions, it is important that the stage perform only a small amount of work per
     *  call.  This ensures that the stage remains responsive to the master thread for termination and integration
     *  with the UI.  For example, an IQ data source stage may return after output of each pulse.
     *
     *  The return value of this function allows the stage to indicate the maximum amount of idle time that should
     *  pass before it is called again.  Returning a value of 0ms will cause it to be called again as soon as
     *  possible.  Returning a negative value (e.g. -1ms) acts like an infinite timeout.  In all cases, the function
     *  will be called when the stage is 'returning to idle'.  That is, after having processed some input data, after
     *  interactions with the GUI (such as setting or getting a paramter), or after the idle time was otherwise
     *  interrupted (such as by a signal). */
    virtual auto process_idle() -> std::chrono::milliseconds;

    /// Handle a request to resend data typically initiated from a downstream stage
    /** The default implementation of this function will simply forward the request to all upstream stages.  Only
     *  stages that act as data generators (e.g. IQ file readers) are required to implement it, however other stages
     *  could reimplement it if the data needed for resend happens to be cached locally.
     *
     *  The request number passed to this function is part of the mechanism used to detect and ignore duplicate
     *  requests.  Since a processing stage may have multiple inputs and these inputs may be derived from a single
     *  upstream stage, it is possible for a resend request to be duplicated and received multiple times.  Stages
     *  which are reimplementing this function (file readers) should implement their own mechanism to ignore calls
     *  that passed a request number that has already been processed. */
    virtual auto process(shared_ptr<resend_request const> data, int input) -> void;

    /// Process the inputs passed and optionally produce outputs to be sent downstream
    virtual auto process(shared_ptr<state> data, int input) -> void;
    virtual auto process(shared_ptr<state const> data, int input) -> void;
    virtual auto process(shared_ptr<pulse> data, int input) -> void;
    virtual auto process(shared_ptr<pulse const> data, int input) -> void;
    virtual auto process(shared_ptr<batch> data, int input) -> void;
    virtual auto process(shared_ptr<batch const> data, int input) -> void;
    virtual auto process(shared_ptr<spectra> data, int input) -> void;
    virtual auto process(shared_ptr<spectra const> data, int input) -> void;
    virtual auto process(shared_ptr<autocorrelations> data, int input) -> void;
    virtual auto process(shared_ptr<autocorrelations const> data, int input) -> void;
    virtual auto process(shared_ptr<moments> data, int input) -> void;
    virtual auto process(shared_ptr<moments const> data, int input) -> void;

  protected:
    /// Throw an exception if the specified input index is not valid
    auto check_input_index(int input) const -> void;

    /// Throw an exception if the specified output index is not valid
    auto check_output_index(int output) const -> void;

    /// Notify the UI observer that a parameter may have changed
    auto notify_parameter_update(string_view name) -> void;

    /// Output log message to user
    auto output_log(log_level level, string message) -> void;

    /// Send datum to all downstream connections on one of our outputs
    template <typename T>
    auto send(int output, shared_ptr<T> data) -> void;

    /// Broadcast a datum to all downstream connectiosn on all of our outputs
    template <typename T>
    auto broadcast(shared_ptr<T> data) -> void;

    /// Request that the upstream stage resend data
    /** If an input number is given, then the request will be made only to upstream stages connected to this socket.
     *  Otherwise the request will be issued to upstream stages connected to all inputs. */
    auto request_resend(int input = -1) -> void;

    /// Force the worker thread to wake up immediately if waiting for work
    /** This will short circuit the timeout previously returned from a process_idle() call.  This can be useful when a
     *  stage previously had nothing to do and set an infinite idle callback time, but now the situation has changed.
     *  If the situation was changed via a normal set_paramter() call then it's not necessary to call this function
     *  since set_paramter() will wake the worker thread anyway.  It's only when we need to start idle processing but
     *  there are no other "normal" queued events or parameter modifications that can wake it. */
    auto wake_worker() -> void;

  private:
    /// Maximum size of a downstream stage input queue before we start to backoff to avoid overwhelming it
    static constexpr auto backoff_threshold = 32;

    /// Amount of time to backoff for (idle) if we detect that we are overwhelming a downstream stage
    static constexpr auto backoff_timeout = 1ms;

    struct parameter_get
    {
      string              name;
      optional<parameter> param;
      bool                ready;  ///< Set to true by worker thread after processing is complete
    };
    struct parameter_set
    {
      string          name;
      parameter_value value;
      bool            ready;  ///< Set to true by worker thread after processing is complete
    };
    using datum_ptr = variant<
        shared_ptr<parameter_get> // parameter is non-const to allow readback by caller
      , shared_ptr<parameter_set> // parameter is non-const to allow readback by caller
      , shared_ptr<resend_request const>
      , shared_ptr<state>
      , shared_ptr<state const>
      , shared_ptr<pulse>
      , shared_ptr<pulse const>
      , shared_ptr<batch>
      , shared_ptr<batch const>
      , shared_ptr<spectra>
      , shared_ptr<spectra const>
      , shared_ptr<autocorrelations>
      , shared_ptr<autocorrelations const>
      , shared_ptr<moments>
      , shared_ptr<moments const>
      >;
    using data_queue = list<pair<int, datum_ptr>>;
    struct input_state
    {
      vector<peer>  peers;    ///< Details of peers connected to this port
    };
    struct output_state
    {
      vector<peer>  peers;    ///< Details of peers connected to this port
    };

  private:
    static auto get_catalog() -> stage_catalog&;

  private:
    auto enqueue(int input, datum_ptr data) -> size_t;
    auto process(shared_ptr<parameter_get> data, int input) -> void;
    auto process(shared_ptr<parameter_set> data, int input) -> void;

    auto worker_thread_launch() -> void;
    auto worker_thread_terminate() -> void;
    auto worker_thread_main() -> void;
    template <typename T> friend class auto_launch;

  private:
    span<string_view const>     plist_;         ///< Stage parameter names
    span<input_meta const>      ilist_;         ///< Stage input metadata
    span<output_meta const>     olist_;         ///< Stage output metadata

    mutable mutex               mut_io_;        ///< Mutex to protect connection configuration
    parameter_callback          cb_parameter_;  ///< Callback to notify UI of parameter updates
    logging_callback            cb_logging_;    ///< Callback to pass log output to UI
    vector<input_state>         inputs_;        ///< Input connection state
    vector<output_state>        outputs_;       ///< Output connection state

    mutable mutex               mut_queue_;     ///< Mutex to protect work queue state
    data_queue                  queue_;         ///< Input data waiting to be processed
    condition_variable          cv_not_empty_;  ///< Signals when an input queue transitions from empty to not-empty
    condition_variable          cv_params_;     ///< Signals when a parameter get/set request has been fulfilled
    bool                        terminate_;     ///< Flag used to kill off worker thread

    thread                      worker_;        ///< Worker thread
    bool                        backoff_;       ///< Whether to trigger a "backoff" wait this cycle
  };

  template <typename T>
  inline auto stage::enrol(
        string_view name
      , string_view label
      , string_view category
      , string_view description
      ) -> stage_catalog::iterator
  {
    return get_catalog().insert(
    {
        name
      , label
      , category
      , []() -> shared_ptr<stage> { return make_shared<auto_launch<T>>(); }
      , description
    }).first;
  }

  template <typename T>
  auto stage::send(int output, shared_ptr<T> data) -> void
  {
    auto lock = lock_guard<mutex>{mut_io_};
    if (outputs_[output].peers.size() == 1)
    {
      // datum has a single destination so pass it as a non-const
      auto& con = outputs_[output].peers[0];
      if (con.first->enqueue(con.second, std::move(data)) > backoff_threshold)
        backoff_ = true;
    }
    else
    {
      // datum has multiple destinations so pass it as a const
      auto ptr = datum_ptr{std::const_pointer_cast<T const>(std::move(data))};
      for (auto& con : outputs_[output].peers)
      {
        if (con.first->enqueue(con.second, ptr) > backoff_threshold)
          backoff_ = true;
      }
    }
  }

  template <typename T>
  auto stage::broadcast(shared_ptr<T> data) -> void
  {
    auto lock = lock_guard<mutex>{mut_io_};
    if (outputs_.size() == 1 && outputs_[0].peers.size() == 1)
    {
      // datum definitely has a single destination so pass it as a non-const
      auto& con = outputs_[0].peers[0];
      if (con.first->enqueue(con.second, std::move(data)) > backoff_threshold)
        backoff_ = true;
    }
    else
    {
      // datum may have multiple destinations so pass it as a const
      auto ptr = datum_ptr{std::const_pointer_cast<T const>(std::move(data))};
      for (size_t output = 0; output < outputs_.size(); ++output)
      {
        for (auto& con : outputs_[output].peers)
        {
          if (con.first->enqueue(con.second, ptr) > backoff_threshold)
            backoff_ = true;
        }
      }
    }
  }
}
