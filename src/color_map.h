/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array.h"
#include "types.h"
#include "json.h"

namespace vispwr
{
  struct color
  {
    uint8_t r, g, b, a;
  };

  auto hex_to_color(string const& val) -> color;

  constexpr inline auto operator""_rgb(unsigned long long val)
  {
    return color
    {
        uint8_t((val >> 16) & 0xff)
      , uint8_t((val >> 8) & 0xff)
      , uint8_t(val & 0xff)
      , uint8_t(0xff)
    };
  }

  constexpr inline auto operator""_rgba(unsigned long long val)
  {
    return color
    {
        uint8_t((val >> 24) & 0xff)
      , uint8_t((val >> 16) & 0xff)
      , uint8_t((val >> 8) & 0xff)
      , uint8_t(val & 0xff)
    };
  }

  class color_map
  {
  public:
    color_map(json const& j);

    auto missing() const { return missing_; }
    auto underflow() const { return underflow_; }
    auto overflow() const { return overflow_; }
    auto min() const { return colors_[0].val; }
    auto max() const { return colors_[colors_.size()-1].val; }

    auto evaluate(float value) const -> color;

  private:
    struct entry
    {
      float val;
      color col;
    };

  private:
    color         missing_;
    color         underflow_;
    color         overflow_;
    array1<entry> colors_;
  };

  /// Get an index of known color maps
  /** The index is calculated by scanning the standard directories where color maps should be stored on the first
   *  call.  This results is then cached and returned to all subsequent calls of this function.  It is not possible
   *  to recalculate the index, so a user must restart the software to include a new color map file.  This function
   *  is thread safe. */
  auto color_map_index() -> map<string, filesystem::path> const&;
}
