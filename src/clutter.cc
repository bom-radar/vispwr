/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"

using namespace vispwr;

namespace vispwr
{
  /// Simple clutter filter
  class clutter : public stage
  {
  protected:
    clutter();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<spectra> data, int input) -> void override;
    auto process(shared_ptr<spectra const> data, int input) -> void override;

  private:
    int width_;
    bool interpolate_;
  };
}

static constexpr auto plist = std::array<string_view, 2>{ "Width", "Interpolate" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
  { "spectra", data_type::spectra, true, "Input spectra" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
  { "spectra", data_type::spectra, "Output spectra" }
}};

static const auto desc = R"(
Simple frequency domain clutter filter.  Currently this stage implements a hard notch filter, allowing the user to
specify how many FFT bins (waves) are removed.

**Input:** Spectra

**Output:** Spectra
)";

static auto ireg = stage::enrol<clutter>("clutter", "Clutter (freq)", "Process", desc);

clutter::clutter()
  : stage{plist, ilist, olist}
  , width_{1}
  , interpolate_{true}
{ }

auto clutter::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto clutter::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Width")
    return parameter_integer
    {
      .name = "Width",
      .description = "Notch filter width in FFT bins",
      .min = 0,
      .max = 100,
      .value = width_,
    };
  if (name == "Interpolate")
    return parameter_boolean
    {
      .name = "Interpolate",
      .description = "Whether to interpolate over notch filter gap",
      .value = interpolate_,
    };
  return {};
}

auto clutter::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Width")
    width_ = std::get<int>(value);
  else if (name == "Interpolate")
    interpolate_ = std::get<bool>(value);
}

auto clutter::process(shared_ptr<spectra> data, int input) -> void
{
  //printf("%zu\n", data->batch_id);
  auto notch_filter = [&](auto& fiq)
  {
    if (fiq.size() == 0)
      return;

    // width = 1 should just do DC (fiq[0])
    // width = 2 should do DC, plus fiq[1] & fiq[n-1]
    auto n1 = width_;
    auto n2 = width_ < 2 ? fiq.shape()[1] : fiq.shape()[1] - (width_ - 1);
    if (interpolate_)
    {
      /* do we really want to interpolate like this in linear space? */
      auto lerp_points = width_ * 2;
      for (auto igate = 0uz; igate < fiq.shape()[0]; ++igate)
      {
        auto v1 = fiq[igate, n1];
        auto v2 = fiq[igate, n2 - 1];
        for (auto i = 0; i < n1; ++i)
        {
          auto lerp_frac = float(i + width_) / lerp_points;
          fiq[igate, i].real(std::lerp(v2.real(), v1.real(), lerp_frac));
          fiq[igate, i].imag(std::lerp(v2.imag(), v1.imag(), lerp_frac));
        }
        for (auto i = n2; i < fiq.shape()[1]; ++i)
        {
          auto lerp_frac = float(i - n2 + 1) / lerp_points;
          fiq[igate, i].real(std::lerp(v2.real(), v1.real(), lerp_frac));
          fiq[igate, i].imag(std::lerp(v2.imag(), v1.imag(), lerp_frac));
        }
      }
    }
    else
    {
      for (auto igate = 0uz; igate < fiq.shape()[0]; ++igate)
      {
        for (auto i = 0; i < n1; ++i)
          fiq[igate, i] = complex<float>{0.0f, 0.0f};
        for (auto i = n2; i < fiq.shape()[1]; ++i)
          fiq[igate, i] = complex<float>{0.0f, 0.0f};
      }
    }
  };

  notch_filter(data->fiq_rx_h);
  notch_filter(data->fiq_rx_v);

  send(0, std::move(data));
}

auto clutter::process(shared_ptr<spectra const> data, int input) -> void
{
  clutter::process(make_shared<spectra>(*data), input);
}
