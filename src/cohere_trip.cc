/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"

#include <iostream>

using namespace vispwr;

namespace vispwr
{
  /// Cohere batch IQ with transmit phases for trip N
  class cohere_trip : public stage
  {
  public:
    cohere_trip();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<batch const> data, int input) -> void override;

  private:
    size_t trips_;
  };
}

static constexpr auto plist = std::array<string_view, 1>{ "Trips" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "batch", data_type::batch, true, "Input batches" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
    { "batch", data_type::batch, "Trip recovered batches" }
}};

static const auto desc = R"(
Simple multi-trip recovery.  Assumes that input pulse data contains `n` range samples cohered to first trip transmit
phases.  Outputs new batches with up to `3n` range samples, where the samples beyond `n` have been adjusted to cohere
to either second or third trip transmit phases.

**Input:** Batches

**Output:** Batches
)";

static auto ireg = stage::enrol<cohere_trip>("cohere_trip", "Cohere Trip", "Process", desc);

cohere_trip::cohere_trip()
  : stage{plist, ilist, olist}
  , trips_{1}
{ }

auto cohere_trip::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto cohere_trip::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Trips")
    return parameter_integer
    {
      .name = "Trips",
      .description = "Retrieved trips",
      .min = 1,
      .max = 3,
      .value = int(trips_),
    };
  return {};
}

auto cohere_trip::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Trips")
    trips_ = std::get<int>(value);
}

// TODO - utilize mutable input data when possible???

auto cohere_trip::process(shared_ptr<batch const> data, int input) -> void
{
  // TODO - configure trip to retrieve
  #if 0
  auto trip_separation = 0.0; // range gap between trips TODO
  #endif

  auto& trip0 = data;

  if (trip0->pulse_count < trips_)
  {
    // TODO - probaby just throw it away and don't output a batch???
    throw std::runtime_error{"Insufficient pulses in batch to cohere desired trip"};
  }

  auto tripN = make_shared<batch>();
  tripN->id = trip0->id;
  tripN->range_start = trip0->range_start;
  tripN->range_step = trip0->range_step;
  tripN->prf = trip0->prf;
  tripN->prf_index = trip0->prf_index;
  tripN->gate_count = trip0->gate_count * trips_;
  tripN->pulse_count = trip0->pulse_count - (trips_ - 1); // no second trip return possible in first pulse and so on

  tripN->pulses.resize(tripN->pulse_count);
  for (auto ipulse = 0uz; ipulse < tripN->pulse_count; ++ipulse)
    tripN->pulses[ipulse] = trip0->pulses[ipulse + (trips_ - 1)];

  // shift phase of each tx sample to match transmit pulse (i.e. trip 1)
  if (trip0->iq_rx_h.size() != 0)
  {
    tripN->iq_rx_h.resize(tripN->gate_count, tripN->pulse_count);

    // first trip - just copy iq (no phase correction needed)
    for (auto igate = 0uz; igate < trip0->gate_count; ++igate)
      for (auto ipulse = 0uz; ipulse < tripN->pulse_count; ++ipulse)
        tripN->iq_rx_h[igate, ipulse] = trip0->iq_rx_h[igate, ipulse + trips_ - 1];

    // subsequent trips - apply phase correction
    for (auto itrip = 1uz; itrip < trips_; ++itrip)
    {
      for (auto igate = 0uz; igate < trip0->gate_count; ++igate)
      {
        auto ogate = itrip * trip0->gate_count + igate; // ignores gap
        for (auto ipulse = 0uz; ipulse < tripN->pulse_count; ++ipulse)
        {
          auto phasecor = trip0->pulses[ipulse + trips_ - 1].tx_phase_h - trip0->pulses[ipulse + trips_ - 1 - itrip].tx_phase_h;
          auto const& iq0 = trip0->iq_rx_h[igate, ipulse + trips_ - 1];
          auto iq0c = std::complex<double>(iq0);
          auto iqNc = std::polar(std::abs(iq0c), std::arg(iq0c) + phasecor);
          tripN->iq_rx_h[ogate, ipulse] = iqNc;
        }
      }
    }
  }

  if (trip0->iq_rx_v.size() != 0)
  {
    tripN->iq_rx_v.resize(tripN->gate_count, tripN->pulse_count);

    // first trip - store tx phase and copy iq (no phase correction needed)
    for (auto igate = 0uz; igate < trip0->gate_count; ++igate)
      for (auto ipulse = 0uz; ipulse < tripN->pulse_count; ++ipulse)
        tripN->iq_rx_v[igate, ipulse] = trip0->iq_rx_v[igate, ipulse + trips_ - 1];

    // subsequent trips - apply phase correction
    for (auto itrip = 1uz; itrip < trips_; ++itrip)
    {
      for (auto igate = 0uz; igate < trip0->gate_count; ++igate)
      {
        auto ogate = itrip * trip0->gate_count + igate; // ignores gap
        for (auto ipulse = 0uz; ipulse < tripN->pulse_count; ++ipulse)
        {
          auto phasecor = trip0->pulses[ipulse + trips_ - 1].tx_phase_v - trip0->pulses[ipulse + trips_ - 1 - itrip].tx_phase_v;
          auto const& iq0 = trip0->iq_rx_v[igate, ipulse + trips_ - 1];
          auto iq0c = std::complex<double>(iq0);
          auto iqNc = std::polar(std::abs(iq0c), std::arg(iq0c) + phasecor);
          tripN->iq_rx_v[ogate, ipulse] = iqNc;
        }
      }
    }
  }

  send(0, std::move(tripN));
}
