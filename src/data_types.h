/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "array.h"
#include "types.h"

namespace vispwr
{
  /* The 'id' fields used in many of the data types are used to provide semi-unique identifiers to individual
   * instances produced by a stage.  Stages producing types containing an 'id' field MUST implement the following
   * scheme:
   * - The first datum output by a stage must use id 0
   * - Subsequent datum identifiers must be assigned sequentially
   * - Significant break points in the IQ stream must reset the id back to 0
   *
   * For example, a stage which generates pulses from a IQ recording should output the first pulse in the file with an
   * id of 0.  Subsequent pulses must be assigned id 1, 2, 3, etc.  If the stage loops back to the start of the file
   * then the id should be reset so that the same first pulse is given id 0 again.  If the file contains multiple
   * sweeps then the first pulse of each new sweep should also reset to 0.
   *
   * This scheme allows downstream processes to reliably detect and respond to breaks in the IQ stream.  For example,
   * the stage for generating batches will detect a pulse with id 0 and also reset the id sequence its output batches.
   * This resetting of ids should propogate through the entire processing chain.  End stage processes such as moment
   * visualisation or level 2 file output can use the receipt of id 0 to know when to reset the screen or finalize a
   * sweep dataset.
   *
   * Note that this scheme implies that the id fields are semi-unique.  The common situations where two instances of
   * the same data_type share an id value are:
   * 1. After a reset of the id sequence the identifiers are reused.  If looping over a single IQ record, then the
   *    same pulses should be reassigned the same IDs as last time.
   * 2. When multiple stages produce the same data type.  For example, a clutter filter stage will take unfiltered
   *    batches and output clutter filtered batches with identical IDs. */

  /// Enumeration of all inter-process data types
  enum class data_type
  {
      state
    , pulse
    , batch
    , spectra
    , autocorrelations
    , moments
  };

  // TODO - make a traits template for enums and a formatter that uses it
  inline constexpr auto data_type_names = std::array<std::string_view, 6>
  {{
      "state"
    , "pulse"
    , "batch"
    , "spectra"
    , "autocorrelations"
    , "moments"
  }};

  /// Polarization modes
  enum class polarization
  {
      hv  ///< Horizontal and Vertical (e.g. STAR)
    , h   ///< Horizontal only
    , v   ///< Vertical only
  };

  /// Processing chain state
  /** This data type is used to request or report changes to configuration parameters of a stage.  It is unique
   *  in the aspect that this type is not exchanged among 
   *  is a user configurable setting applied to a single stage ('local' parameters).  Less commonly, a parameter
   *  may apply to many stages within the processing chain ('global' parameters).
   *
   *  This data type is used to communicate important state related to the data stream such as the beginning of a new
   *  IQ record, or whether dual-prf is in use.  This data type is unique in that it is sent and received on all
   *  sockets regarless of declared data type.  Generally, a state update is expected to be forwarded to all output
   *  sockets regardless of whether the stage understands or utilizes the state update itself.  In simple cases this
   *  may be achieved using the broadcast() function.
   *
   *  Stage implementations should make an effort to keep state changes synchronized with the rest of the data stream.
   *  For simple stages this requires no effort since process() calls are strictly ordered to match the order of data
   *  reception.  For more complex stages which cache data between process() calls (batching, dual-prf, display
   *  windows), care should be taken to ensure the impact of the state change takes effect as if received in order
   *  despite any data caching.  This may include delayed forwarding of the state change to output sockets so that it
   *  remains well synchronised downstream.
   *
   *  Known states:
   *  - iq-begin    - sent before first pulse of IQ record
   *  - iq-end      - sent after final pulse of IQ record
   *  - prf-mode    - current tx mode single or multi-prf?
   *  - sweep-mode  - sweeping in azimuth or elevation or neither?
   */
  struct state
  {
    string            name;     ///< State name
  };

  /// IQ data for a single pulse
  /** Not currently including tx_frequency_h/v here since from the IQR files they don't seem to be the actual RF
   *  transmit frequency.  Seems rather to line up with the AFC frequency, and for now I'm not sure how to relate
   *  that to RF or what use AFC is by itself. */
  struct pulse
  {
    int64_t           id;             ///< Sequential identifier
    int64_t           source_id;      ///< Identifier provided by the source hardware (if any)

    float             elevation;      ///< Antenna elevation in degrees
    float             azimuth;        ///< Antenna azimuth in degrees
    float             range_start;    ///< Range of first gate (km)
    float             range_step;     ///< Distance between range gates (km)

    float             prf;            ///< Active PRF during this pulse
    int               prf_index;      ///< Index of PRF used for this pulse
    polarization      tx_polarization;   ///< Transmitted polarization(s)
    float             tx_magnitude_h; ///< Transmitter pulse magnitude (H channel)
    float             tx_magnitude_v; ///< Transmitter pulse magnitude (H channel)
    float             tx_phase_h;     ///< Transmitter pulse phase (H channel)
    float             tx_phase_v;     ///< Transmitter pulse phase (V channel)

    /* When using a dynamic matched filter, an estimate of the matched filter loss may be available on a per-pulse
     * basis.  If these values are not supplied per pulse by the receiver then the process should fill them with
     * a uniform value set during calibration. */
    float             matched_filter_losses_h;  ///< Per pulse matched filter losses (H channel)
    float             matched_filter_losses_v;  ///< Per pulse matched filter losses (V channel)

    size_t            sample_count;   ///< Number of transmitter samples
    float             sample_start;   ///< Time of first tx sample relative to (???) (us)
    float             sample_step;    ///< Time step between consecutive tx samples (us)
    array1cf          iq_tx_h;        ///< Transmitter samples (H channel)
    array1cf          iq_tx_v;        ///< Transmitter samples (V channel)

    size_t            gate_count;     ///< Number of range gates
    array1cf          iq_rx_h;        ///< Receiver samples (H channel)
    array1cf          iq_rx_v;        ///< Receiver samples (V channel)
  };

  /// Subset of pulse attributes that we want to keep on a per-pulse basis in batches and spectra
  struct pulse_meta
  {
    float             elevation;      ///< Antenna elevation in degrees
    float             azimuth;        ///< Antenna azimuth in degrees
    float             tx_magnitude_h; ///< Transmitter pulse magnitude (H channel)
    float             tx_magnitude_v; ///< Transmitter pulse magnitude (H channel)
    float             tx_phase_h;     ///< Transmitter pulse phase (H channel)
    float             tx_phase_v;     ///< Transmitter pulse phase (V channel)
    float             matched_filter_losses_h;  ///< Per pulse matched filter losses (H channel)
    float             matched_filter_losses_v;  ///< Per pulse matched filter losses (V channel)
  };

  /// IQ data for a single PRF batch (coherent pulse interval)
  struct batch
  {
    uint64_t          id;           ///< Sequential identifier

    float             range_start;  ///< Range of first gate (km)
    float             range_step;   ///< Distance between range gates (km)

    float             prf;          ///< PRF used for this batch
    int               prf_index;    ///< Index of PRF used for this batch

    size_t            gate_count;   ///< Number of gates in batch
    size_t            pulse_count;  ///< Number of pulses in batch

    array1<pulse_meta> pulses;      ///< Per pulse metadata that is needed

    array2cf          iq_rx_h;      ///< IQ samples [gate, pulse] (H receive co-polar channel)
    array2cf          iq_rx_v;      ///< IQ samples [gate, pulse] (V receive co-polar channel)
  };

  /// Spectrum data for a single PRF batch (coherent pulse interval)
  struct spectra
  {
    uint64_t          id;           ///< Sequential identifier
    uint64_t          batch_id;     ///< Identifier of batch these spectra relate to

    float             range_start;  ///< Range of first gate (km)
    float             range_step;   ///< Distance between range gates (km)

    float             prf;          ///< PRF used for this batch
    int               prf_index;    ///< Index of PRF used for this batch

    size_t            gate_count;   ///< Number of gates in batch
    size_t            pulse_count;  ///< Number of pulses in batch
    size_t            fft_size;     ///< Number of samples in FFT spectrum

    array1<pulse_meta> pulses;      ///< Per pulse metadata that is needed

    array2cf          fiq_rx_h;    ///< Fourier transformed IQ samples [gate, freq] (H channel)
    array2cf          fiq_rx_v;    ///< Fourier transformed IQ samples [gate, freq] (V channel)
  };

  /// Autocorrelations for a single batch
  struct autocorrelations
  {
    struct acset
    {
      complex<double> lag0;   // imag part should always be 0.0 for autocorrelations
      complex<double> lag1;
      complex<double> lag2;
    };

    struct xcset
    {
      complex<double> lag0;
    };

    uint64_t          id;               ///< Sequential identifier
    uint64_t          batch_id;         ///< Identifier of batch these correlations relate to (TODO - multiple?)

    float             prf;              ///< PRF used for batch
    int               prf_index;        ///< Index of PRF used for batch

    float             elevation_first;  ///< Azimuth of first pulse in batch
    float             elevation_last;   ///< Azimuth of last pulse in batch
    float             azimuth_first;    ///< Azimuth of first pulse in batch
    float             azimuth_last;     ///< Azimuth of last pulse in batch

    float             range_start;      ///< Range of first gate (km)
    float             range_step;       ///< Distance between range gates (km)
    size_t            gate_count;       ///< Number of gates in batch

    float             matched_filter_losses_h;  ///< Average matched filter loss over all pulses in batch (H channel)
    float             matched_filter_losses_v;  ///< Average matched filter loss over all pulses in batch (V channel)

    array1<acset>     autocors_h;     ///< Lag 0..2 copolar autocorrelations (H channel)
    array1<acset>     autocors_v;     ///< Lag 0..2 copolar autocorrelations (V channel)
    array1<xcset>     crosscors;      ///< Lag 0 cross correlation
  };

  using moment_list = vector<string>;
  using moment_list_ptr = shared_ptr<moment_list const>;

  /// Moment data for a single batch
  struct moments
  {
    uint64_t          id;               ///< Sequential identifier
    uint64_t          autocors_id;      ///< Identifier of autocorrelations these moments are derived from (TODO - multiple?)
    uint64_t          batch_id;         ///< Identifier of batch used to derive the autocorrelations (TODO - multiple?)

    float             elevation_first;  ///< Azimuth of first pulse in batch
    float             elevation_last;   ///< Azimuth of last pulse in batch
    float             azimuth_first;    ///< Azimuth of first pulse in batch
    float             azimuth_last;     ///< Azimuth of last pulse in batch

    float             range_start;      ///< Range of first gate (km)
    float             range_step;       ///< Distance between range gates (km)
    size_t            gate_count;       ///< Number of gates in batch

    float             nyquist;          ///< Nyquist velocity

    moment_list_ptr   moments;          ///< List of moments stored in 'data'
    array2f           data;             ///< Moment moment value [gate, mom]
  };
}
