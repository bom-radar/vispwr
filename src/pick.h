/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "types.h"
#include <QObject>

namespace vispwr
{
  struct polar_coordinates
  {
    float elevation = std::numeric_limits<float>::quiet_NaN();
    float azimuth = std::numeric_limits<float>::quiet_NaN();
    float range = std::numeric_limits<float>::quiet_NaN();
  };

  /// Determine a range gate index given a range, range start and range step
  /** This function performs no sanity checking on the calculated gate index.  The user must verify that the returned
   *  gate index is sane (>=0 && < gates) before using it to index into an array. */
  inline auto gate_from_range(float range_start, float range_step, float range) -> size_t
  {
    /* yes the cast to int is necessary here. negative float -> unsigned int gives an undefined value, while
     * negative float -> int -> unsigned int is well defined and will yield a very large value due to the modulo
     * arithmetic rules used for the signed -> unsigned conversion. */
    return int(std::floor((range - range_start) / range_step));
  }

  /// Class representing a picked coordinate or datum
  class pick : public QObject
  {
    Q_OBJECT
  public:
    /* Actual data is handled as a single struct to avoid having individual member functions that would each need to
     * lock the mutex and emit the updated() signal.  Better to let the user marshal their changes and only lock and
     * emit once per atomic update. */
    struct data : polar_coordinates
    {
      bool  valid = false; ///< Is this pick value initialized?
    };

    /// Fixed number of channels supported by the picking system
    static constexpr auto channels = 4;

  public:
    static auto channel(int i) -> pick&;

    static auto channel_from_modifiers(Qt::KeyboardModifiers modifiers) -> int;

  public:
    /// Get the current valu (thread safe)
    auto get() const -> data;

    /// Set the current value (thread safe)
    auto set(data const &val) -> void;

    /// Clear all pick channel (thread safe)
    auto clear() -> void;

  signals:
    /// Emitted when a pick data for a channel is modified
    void updated();

  private:
    mutable mutex mut_;
    data          data_;
    static pick   channels_[channels];
  };

  inline auto pick::channel(int i) -> pick&
  {
    return pick::channels_[i];
  }
}
