/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "pick.h"

using namespace vispwr;

pick pick::channels_[channels];

auto pick::channel_from_modifiers(Qt::KeyboardModifiers modifiers) -> int
{
  if (modifiers & Qt::ShiftModifier)
  {
    if (modifiers & Qt::ControlModifier)
      return 3;
    return 1;
  }
  else
  {
    if (modifiers & Qt::ControlModifier)
      return 2;
    return 0;
  }
}

auto pick::get() const -> data
{
  auto lock = lock_guard<mutex>{mut_};
  return data_;
}

auto pick::set(data const& val) -> void
{
  {
    auto lock = lock_guard<mutex>{mut_};
    data_ = val;
  }
  updated();
}

auto pick::clear() -> void
{
  bool send_update = false;
  {
    auto lock = lock_guard<mutex>{mut_};
    send_update = data_.valid;
    data_.valid = false;
  }
  if (send_update)
    updated();
}
