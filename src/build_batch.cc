/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"

using namespace vispwr;

namespace vispwr
{
  enum class batch_mode
  {
      samples
    , angle
    , prf
  };

  static constexpr auto batch_mode_names = std::array<std::string_view, 3>
  {{
      "Samples"
    , "Angle"
    , "Dual-PRF"
  }};

  /// Collect pulses into per-gate batches
  class build_batch : public stage
  {
  protected:
    build_batch();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<pulse const> data, int input) -> void override;

  private:
    auto output_batch() -> void;

  private:
    batch_mode  mode_;
    size_t      sample_count_;
    float       angle_width_;
    float       angle_offset_;

    vector<shared_ptr<pulse const>> pulses_;
    uint64_t  next_id_;
  };
}

static constexpr auto plist = std::array<string_view, 4>{ "Mode", "Samples", "Ray Width", "Ray Offset" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
  { "pulse", data_type::pulse, true, "IQ data for single pulse" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
  { "batch", data_type::batch, "Per-gate and per-PRF IQ batches" }
}};

static const auto desc = R"(
Organise individual pulses into per-ray processing batches.  Pulses may be batched based on pulse count, swept angle,
or by change of PRF.

**Input:** Pulses

**Output:** Batches
)";

static auto ireg = stage::enrol<build_batch>("build_batch", "Build Batch", "Process", desc);

build_batch::build_batch()
  : stage{plist, ilist, olist}
  , mode_{batch_mode::samples}
  , sample_count_{64}
  , angle_width_{1.0f}
  , angle_offset_{0.0f}
  , next_id_{0}
{ }

auto build_batch::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto build_batch::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Mode")
    return parameter_enumerate
    {
      .name = "Mode",
      .description = "Batching mode",
      .values = batch_mode_names,
      .value = string(batch_mode_names[static_cast<int>(mode_)]),
    };
  if (name == "Samples")
    return parameter_integer
    {
      .name = "Samples",
      .description = "Samples per batch",
      .visible = mode_ == batch_mode::samples,
      .min = 4,
      .max = 1024,
      .value = int(sample_count_),
    };
  if (name == "Ray Width")
    return parameter_real
    {
      .name = "Ray Width",
      .description = "Ray width in degrees",
      .visible = mode_ == batch_mode::angle,
      .decimals = 2,
      .min = 0.01,
      .max = 10.0,
      .step = 0.1,
      .value = angle_width_,
    };
  if (name == "Ray Offset")
    return parameter_real
    {
      .name = "Ray Offset",
      .description = "Alignment constraint for leading edge of ray",
      .visible = mode_ == batch_mode::angle,
      .decimals = 2,
      .min = 0.0,
      .max = 10.0,
      .step = 0.1,
      .value = angle_offset_,
    };
  return {};
}

auto build_batch::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Mode")
  {
    auto old_mode = mode_;
    auto i = std::find(batch_mode_names.begin(), batch_mode_names.end(), std::get<string>(value));
    if (i == batch_mode_names.end())
      throw std::runtime_error{"Unknown moment"};
    mode_ = static_cast<batch_mode>(i - batch_mode_names.begin());

    // force the UI to refresh the samples & ray parameters since their visibility may have changed
    if (mode_ != old_mode)
    {
      notify_parameter_update("Samples");
      notify_parameter_update("Ray Width");
      notify_parameter_update("Ray Offset");
    }
  }
  else if (name == "Samples")
    sample_count_ = std::get<int>(value);
  else if (name == "Ray Width")
  {
    angle_width_ = std::get<double>(value);
    if (angle_width_ < 0.01)
      angle_width_ = 0.01;
  }
  else if (name == "Ray Offset")
    angle_offset_ = std::get<double>(value);
}

auto build_batch::process(shared_ptr<pulse const> data, int input) -> void
{
  // do we need to output a completed batch?
  if (!pulses_.empty())
  {
    // we must output the batch if chaning IQ record (id of 0), changing PRF, or changing ray
    if (data->id == 0 || data->prf_index != pulses_[0]->prf_index)
    {
      output_batch();
    }
    else if (mode_ == batch_mode::samples)
    {
      if (pulses_.size() >= sample_count_)
        output_batch();
    }
    else if (mode_ == batch_mode::angle)
    {
      // TODO - check which angle is being swept and use elevation instead of azimuth when appropriate
      if (pulses_.size() > 1)
      {
        auto ray_new = std::lround((data->azimuth + angle_offset_) / angle_width_);
        auto ray_old = std::lround((pulses_.back()->azimuth + angle_offset_) / angle_width_);
        if (ray_new != ray_old)
          output_batch();
      }
    }
  }

  // splice this pulse into the current batch and advance
  pulses_.push_back(std::move(data));
}

auto build_batch::output_batch() -> void
{
  auto b = make_shared<batch>();

  // reset our batch IDs at breaks in the IQ stream
  if (pulses_[0]->id == 0)
    next_id_ = 0;

  // initialize based on information from first header in batch
  b->id = next_id_++;
  b->range_start = pulses_[0]->range_start;
  b->range_step = pulses_[0]->range_step;
  b->prf = pulses_[0]->prf;
  b->prf_index = pulses_[0]->prf_index;
  b->gate_count = pulses_[0]->gate_count;
  b->pulse_count = pulses_.size();

  // copy in the per-pulse metadata that we are interested in downstream
  b->pulses.resize(b->pulse_count);
  for (auto ipulse = 0uz; ipulse < b->pulse_count; ++ipulse)
  {
    b->pulses[ipulse].elevation = pulses_[ipulse]->elevation;
    b->pulses[ipulse].azimuth = pulses_[ipulse]->azimuth;
    b->pulses[ipulse].tx_magnitude_h = pulses_[ipulse]->tx_magnitude_h;
    b->pulses[ipulse].tx_magnitude_v = pulses_[ipulse]->tx_magnitude_v;
    b->pulses[ipulse].tx_phase_h = pulses_[ipulse]->tx_phase_h;
    b->pulses[ipulse].tx_phase_v = pulses_[ipulse]->tx_phase_v;
    b->pulses[ipulse].matched_filter_losses_h = pulses_[ipulse]->matched_filter_losses_h;
    b->pulses[ipulse].matched_filter_losses_v = pulses_[ipulse]->matched_filter_losses_v;
  }

  // copy in the iq data from each pulse
  if (pulses_[0]->iq_rx_h.size() != 0)
  {
    b->iq_rx_h.resize(b->gate_count, b->pulse_count);
    for (auto ipulse = 0uz; ipulse < b->pulse_count; ++ipulse)
    {
      if (pulses_[ipulse]->iq_rx_h.size() != b->gate_count)
        throw std::runtime_error{fmt::format("Pulse {} unexpected gate count for H channel", pulses_[ipulse]->id)};
      for (auto igate = 0uz; igate < b->gate_count; ++igate)
        b->iq_rx_h[igate, ipulse] = pulses_[ipulse]->iq_rx_h[igate];
    }
  }
  if (pulses_[0]->iq_rx_v.size() != 0)
  {
    b->iq_rx_v.resize(b->gate_count, b->pulse_count);
    for (auto ipulse = 0uz; ipulse < b->pulse_count; ++ipulse)
    {
      if (pulses_[ipulse]->iq_rx_v.size() != b->gate_count)
        throw std::runtime_error{fmt::format("Pulse {} unexpected gate count for V channel", pulses_[ipulse]->id)};
      for (auto igate = 0uz; igate < b->gate_count; ++igate)
        b->iq_rx_v[igate, ipulse] = pulses_[ipulse]->iq_rx_v[igate];
    }
  }

  // send the batch to downstream stages
  send(0, std::move(b));

  // clear out our list of pulses
  pulses_.clear();
}
