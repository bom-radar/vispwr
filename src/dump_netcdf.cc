/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"

#include <netcdf.h>

using namespace vispwr;

namespace vispwr
{
  /// Debugging stage used to dump data to a netcdf file
  class dump_netcdf : public stage
  {
  public:
    dump_netcdf();
    ~dump_netcdf();

    auto name() const -> string_view override;
    auto inputs() const -> vector<input> const& override;
    auto outputs() const -> vector<output> const& override;
    auto maximum_concurrency() const -> int override;
    auto startup() -> void override;
    auto shutdown() -> void override;
    auto ready_to_execute(input_flags availability) const -> bool override;
    auto execute(input_vector& inputs, output_store& outputs) -> void override;

  private:
    int file_;

    int var_ac0_h_;
    int var_ac0_v_;

    int var_ac1_abs_h_;
    int var_ac1_arg_h_;
    int var_ac1_abs_v_;
    int var_ac1_arg_v_;

    int var_moment_[moment_list.size() * 2]; // x2 for cor/uncor

    std::vector<float> buffer_;
  };
}

dump_netcdf::dump_netcdf()
{
  auto max_gates = 1200;

  if (nc_create("debug2.nc", NC_NETCDF4, &file_))
    throw runtime_error{"Failed to create netcdf file"};

  int dim_ids[2];
  if (nc_def_dim(file_, "batch", NC_UNLIMITED, &dim_ids[0]))
    throw runtime_error{"Failed to create batch dimension"};
  if (nc_def_dim(file_, "gate", max_gates, &dim_ids[1]))
    throw runtime_error{"Failed to create gates dimension"};

  auto fill_value = -999.0f;

  if (nc_def_var(file_, "ac0_h", NC_FLOAT, 2, dim_ids, &var_ac0_h_))
    throw runtime_error{"Failed to create variable"};
  if (nc_put_att_float(file_, var_ac0_h_, "_FillValue", NC_FLOAT, 1, &fill_value))
    throw runtime_error{"Failed to set fill value"};
  if (nc_def_var(file_, "ac0_v", NC_FLOAT, 2, dim_ids, &var_ac0_v_))
    throw runtime_error{"Failed to create variable"};
  if (nc_put_att_float(file_, var_ac0_v_, "_FillValue", NC_FLOAT, 1, &fill_value))
    throw runtime_error{"Failed to set fill value"};

  if (nc_def_var(file_, "ac1_abs_h", NC_FLOAT, 2, dim_ids, &var_ac1_abs_h_))
    throw runtime_error{"Failed to create variable"};
  if (nc_put_att_float(file_, var_ac1_abs_h_, "_FillValue", NC_FLOAT, 1, &fill_value))
    throw runtime_error{"Failed to set fill value"};
  if (nc_def_var(file_, "ac1_arg_h", NC_FLOAT, 2, dim_ids, &var_ac1_arg_h_))
    throw runtime_error{"Failed to create variable"};
  if (nc_put_att_float(file_, var_ac1_arg_h_, "_FillValue", NC_FLOAT, 1, &fill_value))
    throw runtime_error{"Failed to set fill value"};
  if (nc_def_var(file_, "ac1_abs_v", NC_FLOAT, 2, dim_ids, &var_ac1_abs_v_))
    throw runtime_error{"Failed to create variable"};
  if (nc_put_att_float(file_, var_ac1_abs_v_, "_FillValue", NC_FLOAT, 1, &fill_value))
    throw runtime_error{"Failed to set fill value"};
  if (nc_def_var(file_, "ac1_arg_v", NC_FLOAT, 2, dim_ids, &var_ac1_arg_v_))
    throw runtime_error{"Failed to create variable"};
  if (nc_put_att_float(file_, var_ac1_arg_v_, "_FillValue", NC_FLOAT, 1, &fill_value))
    throw runtime_error{"Failed to set fill value"};

  for (int imoment = 0; imoment < moment_list.size() * 2; ++imoment)
  {
    std::string name = moment_list[imoment % moment_list.size()];
    name.append(imoment / moment_list.size() == 0 ? "_u" : "_c");

    if (nc_def_var(file_, name.c_str(), NC_FLOAT, 2, dim_ids, &var_moment_[imoment]))
      throw runtime_error{"Failed to create variable"};
    if (nc_put_att_float(file_, var_moment_[imoment], "_FillValue", NC_FLOAT, 1, &fill_value))
      throw runtime_error{"Failed to set fill value"};
  }

  buffer_.resize(max_gates);
}

dump_netcdf::~dump_netcdf()
{
  nc_close(file_);
}

auto dump_netcdf::name() const -> string_view
{
  return "NetCDF"sv;
}

auto dump_netcdf::inputs() const -> vector<input> const&
{
  static auto list = []
  {
    auto ret = vector<input>();
    ret.reserve(3);
    ret.emplace_back("autocorrelations"sv, data_type::autocorrelations, "Input autocorrelation data");
    ret.emplace_back("u_moments"sv, data_type::moments, "Uncorrected single pol moments");
    ret.emplace_back("c_moments"sv, data_type::moments, "Corrected single pol moments");
    return ret;
  }();
  return list;
}

auto dump_netcdf::outputs() const -> vector<output> const&
{
  static auto list = []
  {
    auto ret = vector<output>();
    return ret;
  }();
  return list;
}

auto dump_netcdf::maximum_concurrency() const -> int
{
  return 1;
}

auto dump_netcdf::startup() -> void
{ }

auto dump_netcdf::shutdown() -> void
{ }

auto dump_netcdf::ready_to_execute(input_flags availability) const -> bool
{
  return availability.any();
}

auto dump_netcdf::execute(input_vector& inputs, output_store& outputs) -> void
{
  // output pending autocorrelations
  if (inputs[0])
  {
    auto acs = static_cast<autocorrelations const*>(inputs[0].get());

    size_t start[] = { acs->batch_id, 0 };
    size_t count[] = { 1, acs->gate_count };

    if (acs->autocors_h)
    {
      for (int igate = 0; igate < acs->gate_count; ++igate)
        buffer_[igate] = acs->autocors_h[igate].lag0.real();
      if (nc_put_vara_float(file_, var_ac0_h_, start, count, buffer_.data()))
        throw runtime_error{"Failed to write variable"};

      for (int igate = 0; igate < acs->gate_count; ++igate)
        buffer_[igate] = std::abs(acs->autocors_h[igate].lag1);
      if (nc_put_vara_float(file_, var_ac1_abs_h_, start, count, buffer_.data()))
        throw runtime_error{"Failed to write variable"};

      for (int igate = 0; igate < acs->gate_count; ++igate)
        buffer_[igate] = std::arg(acs->autocors_h[igate].lag1);
      if (nc_put_vara_float(file_, var_ac1_arg_h_, start, count, buffer_.data()))
        throw runtime_error{"Failed to write variable"};
    }

    if (acs->autocors_v)
    {
      for (int igate = 0; igate < acs->gate_count; ++igate)
        buffer_[igate] = acs->autocors_v[igate].lag0.real();
      if (nc_put_vara_float(file_, var_ac0_v_, start, count, buffer_.data()))
        throw runtime_error{"Failed to write variable"};

      for (int igate = 0; igate < acs->gate_count; ++igate)
        buffer_[igate] = std::abs(acs->autocors_v[igate].lag1);
      if (nc_put_vara_float(file_, var_ac1_abs_v_, start, count, buffer_.data()))
        throw runtime_error{"Failed to write variable"};

      for (int igate = 0; igate < acs->gate_count; ++igate)
        buffer_[igate] = std::arg(acs->autocors_v[igate].lag1);
      if (nc_put_vara_float(file_, var_ac1_arg_v_, start, count, buffer_.data()))
        throw runtime_error{"Failed to write variable"};
    }
  }

  // output uncorrected moments
  if (inputs[1])
  {
    auto mom = static_cast<moments const*>(inputs[1].get());
    size_t start[] = { mom->batch_id, 0 };
    size_t count[] = { 1, mom->gate_count };
    for (auto imoment = 0; imoment < moment_list.size(); ++imoment)
    {
      // just doing this to filter out nans
      for (auto igate = 0; igate < mom->gate_count; ++igate)
        buffer_[igate] = std::isfinite(mom->data[igate, imoment]) ? mom->data[igate, imoment] : -999.0f;
      if (nc_put_vara_float(file_, var_moment_[imoment], start, count, buffer_.data()))
        throw runtime_error{"Failed to write variable"};
    }
  }

  // output corrected moments
  if (inputs[2])
  {
    auto mom = static_cast<moments const*>(inputs[2].get());
    size_t start[] = { mom->batch_id, 0 };
    size_t count[] = { 1, mom->gate_count };
    for (auto imoment = 0; imoment < moment_list.size(); ++imoment)
    {
      // just doing this to filter out nans
      for (auto igate = 0; igate < mom->gate_count; ++igate)
        buffer_[igate] = std::isfinite(mom->data[igate, imoment]) ? mom->data[igate, imoment] : -999.0f;
      if (nc_put_vara_float(file_, var_moment_[moment_list.size() + imoment], start, count, buffer_.data()))
        throw runtime_error{"Failed to write variable"};
    }
  }
}
