/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "calibration.h"
#include "pick.h"

#include <QLineSeries>
#include <QChart>
#include <QChartView>
#include <QValueAxis>
#include <QToolBar>
#include <QComboBox>
#include <QWidget>
#include <QHBoxLayout>
#include <QScatterSeries>
#include <QVBoxLayout>

using namespace vispwr;

namespace vispwr
{
  class batch_window : public QWidget
  {
    Q_OBJECT

    enum class power_units { dbm, kw, adu };

  public:
    batch_window();

    // called by stage to collect a pulse for our next plot
    void collect_pulse(shared_ptr<pulse const> p);

  signals:
    void window_hidden();
    void set_data(shared_ptr<batch const> b, shared_ptr<calibration const> c);

  protected:
    auto hideEvent(QHideEvent* event) -> void override;

  private:
    auto on_change_units(QString const& text) -> void;
    auto on_set_data(shared_ptr<batch const> b, shared_ptr<calibration const> c) -> void;
    auto redraw_plot() -> void;

  private:
    QComboBox*  combo_units_;
    power_units units_;

    QValueAxis* x_axis_;
    QValueAxis* power_y_axis_;
    QValueAxis* phase_y_axis_;
    QChart*     chart_;
    QChartView* view_;

    // current data to plot
    shared_ptr<batch const> batch_;
    shared_ptr<calibration const> calib_;
  };

  class view_batch : public stage
  {
  protected:
    view_batch();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<batch const> data, int input) -> void override;

  private:
    int             picker_;
    int             calib_channel_;
    batch_window    window_;
    QMetaObject::Connection con_pick_updated_;  // handle to signal/slot connection for current picker
  };
}

static constexpr auto plist = std::array<string_view, 4>{ "Picker", "Calib", "Display", "geometry" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "batch", data_type::batch, true, "Input batches" }
}};
static constexpr auto olist = std::array<output_meta, 0>
{{
}};

static const auto desc = R"(
Plot the transmitted power and phase of each pulse in the batch nearest to a selected location.  Each pulse is
represented as a separate entry on the X axis, with power and phase on the left and right Y axis respectively.

**Input:** Batches

**Output:** None
)";

static auto ireg = stage::enrol<view_batch>("view_batch", "Batch TX (Series)", "Display", desc);

view_batch::view_batch()
  : stage{plist, ilist, olist}
  , picker_{0}
  , calib_channel_{0}
{
  QObject::connect(&window_, &batch_window::window_hidden, [this]() { notify_parameter_update("Display"); });
  con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
}

auto view_batch::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto view_batch::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Picker")
    return parameter_integer
    {
      .name = "Picker",
      .description = "Input pick channel for batch selection",
      .min = 0,
      .max = pick::channels - 1,
      .value = picker_,
    };
  if (name == "Calib")
    return parameter_integer
    {
      .name = "Calib",
      .description = "Calibration channel",
      .min = 0,
      .max = 10,
      .value = calib_channel_,
    };
  if (name == "Display")
    return parameter_trigger
    {
      .name = "Display",
      .description = "Open or close display window",
      .label = window_.isVisible() ? "Hide" : "Show",
    };
  if (name == "geometry")
    return parameter_string
    {
      .name = "geometry",
      .description = "Window geometry",
      .visible = false,
      .value = fmt::format("{} {} {} {}", window_.x(), window_.y(), window_.width(), window_.height()),
    };
  return {};
}

auto view_batch::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Picker")
  {
    picker_ = std::get<int>(value);
    if (con_pick_updated_)
      QObject::disconnect(con_pick_updated_);
    con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
  }
  else if (name == "Calib")
    calib_channel_ = std::get<int>(value);
  else if (name == "Display")
  {
    if (std::get<bool>(value) == true)
    {
      window_.setVisible(!window_.isVisible());
      notify_parameter_update("Display");
    }
  }
  else if (name == "geometry")
  {
    int x, y, w, h;
    if (sscanf(std::get<string>(value).c_str(), "%d %d %d %d", &x, &y, &w, &h) != 4)
      throw std::runtime_error{"Bad window geometry"};
    window_.setGeometry(x, y, w, h);
  }
}

auto view_batch::process(shared_ptr<batch const> data, int input) -> void
{
  // get the calibration from our selected channel
  auto calib = calibration_get(calib_channel_);
  if (!calib)
    return;

  // get the currently picked coordiantes from our selected channel
  auto pick_data = pick::channel(picker_).get();
  if (!pick_data.valid)
    return;

  // account for ccw scanning
  auto azimuth_l = data->pulses[0].azimuth;
  auto azimuth_h = data->pulses[data->pulse_count-1].azimuth;
  if (azimuth_l > azimuth_h)
    std::swap(azimuth_l, azimuth_h);

  // account for 360..0 wrap around
  if (azimuth_l < 10.0f && azimuth_h > 350.0f)
    std::swap(azimuth_l, azimuth_h);

  // does this batch overlap with the picked arc?
  if (pick_data.azimuth < azimuth_l || pick_data.azimuth > azimuth_h)
    return;

  // emit the data updated event (will be transferred to the gui thread)
  window_.set_data(std::move(data), std::move(calib));
}

batch_window::batch_window()
  : combo_units_{new QComboBox()}
  , units_{power_units::dbm}
  , x_axis_{new QValueAxis()}
  , power_y_axis_{new QValueAxis()}
  , phase_y_axis_{new QValueAxis()}
  , chart_{new QChart()}
  , view_{new QChartView(chart_)}
{
  combo_units_->addItem("dBm");
  combo_units_->addItem("kW");
  combo_units_->addItem("adu");

  x_axis_->setTitleText("Pulse No");
  x_axis_->setTickType(QValueAxis::TicksDynamic);
  x_axis_->setTickInterval(1.0);
  x_axis_->setTickAnchor(0.0);
  x_axis_->setLabelFormat("%g");
  power_y_axis_->setTitleText("Power (dBm)");
  //power_y_axis_->setLabelFormat("%.0f");
  phase_y_axis_->setTitleText("Phase (\u00b0)");
  phase_y_axis_->setLabelFormat("%g");
  phase_y_axis_->setRange(-180.0, 180.0);
  chart_->addAxis(x_axis_, Qt::AlignBottom);
  chart_->addAxis(power_y_axis_, Qt::AlignLeft);
  chart_->addAxis(phase_y_axis_, Qt::AlignRight);
  chart_->legend()->setAlignment(Qt::AlignRight);

  auto toolbar = new QWidget();
  auto layout_tb = new QHBoxLayout(toolbar);
  layout_tb->addWidget(combo_units_);

  auto layout = new QVBoxLayout{this};
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addWidget(toolbar);
  layout->addWidget(view_);

  resize(2048, 350);
  setWindowTitle("View Batch");
  show();

  QObject::connect(combo_units_, &QComboBox::currentTextChanged, this, &batch_window::on_change_units);
  QObject::connect(this, &batch_window::set_data, this, &batch_window::on_set_data);
}

auto batch_window::hideEvent(QHideEvent* event) -> void
{
  window_hidden();
}

auto batch_window::on_change_units(QString const& text) -> void
{
  if (text == "dBm")
  {
    units_ = power_units::dbm;
    power_y_axis_->setTitleText("Power (dBm)");
  }
  else if (text == "kW")
  {
    units_ = power_units::kw;
    power_y_axis_->setTitleText("Power (kW)");
  }
  else
  {
    units_ = power_units::adu;
    power_y_axis_->setTitleText("Amplitude (adu)");
  }
  redraw_plot();
}

auto batch_window::on_set_data(shared_ptr<batch const> b, shared_ptr<calibration const> c) -> void
{
  batch_ = std::move(b);
  calib_ = std::move(c);
  redraw_plot();
}

auto batch_window::redraw_plot() -> void
{
  chart_->removeAllSeries();

  if (!batch_ || !calib_)
    return;

  double min_power = std::numeric_limits<double>::max();
  double max_power = 0.0;

  {
    auto series = new QLineSeries();
    series->setName(units_ == power_units::adu ? "Amplitude (H)" : "Power (H)");
    for (auto i = 0uz; i < batch_->pulse_count; ++i)
    {
      auto val = double(batch_->pulses[i].tx_magnitude_h); // adu magnitude (not power)
      if (std::isnan(val))
        continue;
      switch (units_)
      {
      case power_units::dbm:
        val = ((val * val) / calib_->tx_ref_power_adu_h) * calib_->tx_ref_power_kw_h; // adu power to kw
        if (val <= 0.0)
          continue;
        val = 10.0 * std::log10(1000.0 * val) + 30.0; // kw to dbm
        break;
      case power_units::kw:
        val = ((val * val) / calib_->tx_ref_power_adu_h) * calib_->tx_ref_power_kw_h; // adu power to kw
        break;
      case power_units::adu:
        break;
      }
      series->append(i + 1, val);
      min_power = std::min(min_power, val);
      max_power = std::max(max_power, val);
    }
    chart_->addSeries(series);
    series->attachAxis(x_axis_);
    series->attachAxis(power_y_axis_);
  }
  {
    auto series = new QLineSeries();
    series->setName(units_ == power_units::adu ? "Amplitude (V)" : "Power (V)");
    for (auto i = 0uz; i < batch_->pulse_count; ++i)
    {
      auto val = double(batch_->pulses[i].tx_magnitude_v); // adu magnitude (not power)
      if (std::isnan(val))
        continue;
      switch (units_)
      {
      case power_units::dbm:
        val = ((val * val) / calib_->tx_ref_power_adu_v) * calib_->tx_ref_power_kw_v; // adu power to kw
        if (val <= 0.0)
          continue;
        val = 10.0 * std::log10(1000.0 * val) + 30.0; // kw to dbm
        break;
      case power_units::kw:
        val = ((val * val) / calib_->tx_ref_power_adu_v) * calib_->tx_ref_power_kw_v; // adu power to kw
        break;
      case power_units::adu:
        break;
      }
      series->append(i + 1, val);
      min_power = std::min(min_power, val);
      max_power = std::max(max_power, val);
    }
    chart_->addSeries(series);
    series->attachAxis(x_axis_);
    series->attachAxis(power_y_axis_);
  }
  {
    auto series = new QScatterSeries();
    series->setName("Phase (H)");
    for (auto i = 0uz; i < batch_->pulse_count; ++i)
      if (!std::isnan(batch_->pulses[i].tx_phase_h))
        series->append(i + 1, batch_->pulses[i].tx_phase_h * (180.0 / pi));
    chart_->addSeries(series);
    series->attachAxis(x_axis_);
    series->attachAxis(phase_y_axis_);
  }
  {
    auto series = new QScatterSeries();
    series->setName("Phase (V)");
    for (auto i = 0uz; i < batch_->pulse_count; ++i)
      if (!std::isnan(batch_->pulses[i].tx_phase_v))
        series->append(i + 1, batch_->pulses[i].tx_phase_v * (180.0 / pi));
    chart_->addSeries(series);
    series->attachAxis(x_axis_);
    series->attachAxis(phase_y_axis_);
  }

  x_axis_->setRange(0.5f, batch_->pulse_count + 0.5f);
  auto dynamic_range = max_power - min_power;
  power_y_axis_->setRange(min_power - dynamic_range * 0.1, max_power + dynamic_range * 0.1);
}

#include "view_batch.moc"
