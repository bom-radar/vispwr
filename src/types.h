/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include <bitset>
#include <cassert>
#include <cmath>
#include <chrono>
#include <complex>
#include <condition_variable>
#include <exception>
#include <filesystem>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <numbers>
#include <optional>
#include <thread>
#include <set>
#include <span>
#include <stdexcept>
#include <string>
#include <string_view>
#include <tuple>
#include <utility>
#include <variant>
#include <vector>

// Pull frequently used standard library names into our namespace for convenience
namespace vispwr
{
  using std::bitset;

  using std::sqrt;

  using std::chrono::duration;
  using std::chrono::system_clock;
  using namespace std::chrono_literals;

  using std::complex;

  using std::condition_variable;

  using std::exception;

  namespace filesystem = std::filesystem;

  using std::function;

  using std::list;

  using std::map;

  using std::unique_ptr;
  using std::shared_ptr;
  using std::weak_ptr;
  using std::enable_shared_from_this;
  using std::make_unique;
  using std::make_shared;

  using std::mutex;
  using std::lock_guard;
  using std::unique_lock;

  using namespace std::numbers;

  using std::optional;
  using std::nullopt;

  using std::thread;

  using std::set;

  using std::span;

  using std::logic_error;
  using std::runtime_error;

  using std::string;
  using namespace std::string_literals;

  using std::string_view;
  using namespace std::string_view_literals;

  using std::tuple;

  using std::pair;

  using std::variant;

  using std::vector;

  /// Unified hasher for string types
  /** This allows us to use heterogeneous lookup in unordered containers.  I don't understand why this struct hasn't
   *  been added to the standard so we can just use it without the boilerplate. */
  struct string_hash
  {
    using is_transparent = void;
    [[nodiscard]] size_t operator()(char const* val) const { return std::hash<std::string_view>{}(val); }
    [[nodiscard]] size_t operator()(std::string_view val) const { return std::hash<std::string_view>{}(val); }
    [[nodiscard]] size_t operator()(std::string const& val) const { return std::hash<std::string>{}(val); }
  };
}
