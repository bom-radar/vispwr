/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "calibration.h"
#include "pick.h"
#include "util.h"

#include <QLineSeries>
#include <QChart>
#include <QChartView>
#include <QCheckBox>
#include <QValueAxis>
#include <QToolBar>
#include <QComboBox>
#include <QHBoxLayout>
#include <QVBoxLayout>

using namespace vispwr;

namespace vispwr
{
  class pulse_window : public QWidget
  {
    Q_OBJECT

    enum class power_units { dbm, kw, adu };

  public:
    pulse_window();

  signals:
    void window_hidden();
    void update_data(shared_ptr<pulse const> p, double ref_adu_h, double ref_kw_h, double ref_adu_v, double ref_kw_v);

  protected:
    auto hideEvent(QHideEvent* event) -> void override;

  private:
    auto on_toggle_phase(int state) -> void;
    auto on_change_units(QString const& text) -> void;

    void on_update_data(shared_ptr<pulse const> p, double ref_adu_h, double ref_kw_h, double ref_adu_v, double ref_kw_v);

  private:
    QComboBox*            combo_units_;
    QCheckBox*            check_phase_;
    power_units           units_;

    QValueAxis*           power_x_axis_;
    QValueAxis*           power_y_axis_;
    QChart*               power_chart_;
    QChartView*           power_view_;

    QValueAxis*           phase_x_axis_;
    QValueAxis*           phase_y_axis_;
    QChart*               phase_chart_;
    QChartView*           phase_view_;

    float                 xmin_;
    float                 xmax_;
    double                ymax_;
  };

  class view_pulse : public stage
  {
  protected:
    view_pulse();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<pulse const> data, int input) -> void override;

  private:
    int           picker_;
    int           calib_channel_;

    pulse_window  window_;

    shared_ptr<pulse const> prev_pulse_;
    QMetaObject::Connection con_pick_updated_;  // handle to signal/slot connection for current picker
  };
}

static constexpr auto plist = std::array<string_view, 4>{ "Picker", "Calib", "Display", "geometry" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "pulse", data_type::pulse, true, "Input pulses" }
}};
static constexpr auto olist = std::array<output_meta, 0>
{{
}};

static const auto desc = R"(
Plot the transmitted pulse shape (power) and phase of the pulse nearest a selected location.  The X axis represents
time and the Y axis represents power or phase (on separate plots).

**Input:** Pulses

**Output:** None
)";

static auto ireg = stage::enrol<view_pulse>("view_pulse", "Pulse", "Display", desc);

view_pulse::view_pulse()
  : stage{plist, ilist, olist}
  , picker_{0}
  , calib_channel_{0}
{
  QObject::connect(&window_, &pulse_window::window_hidden, [this]() { notify_parameter_update("Display"); });
  con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
}

auto view_pulse::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto view_pulse::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Picker")
    return parameter_integer
    {
      .name = "Picker",
      .description = "Input pick channel for batch selection",
      .min = 0,
      .max = pick::channels - 1,
      .value = picker_,
    };
  if (name == "Calib")
    return parameter_integer
    {
      .name = "Calib",
      .description = "Calibration channel",
      .min = 0,
      .max = 10,
      .value = calib_channel_,
    };
  if (name == "Display")
    return parameter_trigger
    {
      .name = "Display",
      .description = "Open or close display window",
      .label = window_.isVisible() ? "Hide" : "Show",
    };
  if (name == "geometry")
    return parameter_string
    {
      .name = "geometry",
      .description = "Window geometry",
      .visible = false,
      .value = fmt::format("{} {} {} {}", window_.x(), window_.y(), window_.width(), window_.height()),
    };
  return {};
}

auto view_pulse::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Picker")
  {
    picker_ = std::get<int>(value);
    if (con_pick_updated_)
      QObject::disconnect(con_pick_updated_);
    con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
  }
  else if (name == "Calib")
    calib_channel_ = std::get<int>(value);
  else if (name == "Display")
  {
    if (std::get<bool>(value) == true)
    {
      window_.setVisible(!window_.isVisible());
      notify_parameter_update("Display");
    }
  }
  else if (name == "geometry")
  {
    int x, y, w, h;
    if (sscanf(std::get<string>(value).c_str(), "%d %d %d %d", &x, &y, &w, &h) != 4)
      throw std::runtime_error{"Bad window geometry"};
    window_.setGeometry(x, y, w, h);
  }
}

auto view_pulse::process(shared_ptr<pulse const> data, int input) -> void
{
  // get the calibration from our selected channel
  auto calib = calibration_get(calib_channel_);
  if (!calib)
    return;

  // get the currently picked coordiantes from our selected channel
  auto pick_data = pick::channel(picker_).get();
  if (!pick_data.valid)
    return;

  // reset for new scan
  // we set to current so that we can still match an exact pick
  if (data->id == 0 || !prev_pulse_)
    prev_pulse_ = data;

  // have we swept over our target area?
  // TODO cope with RHI / sweep in elevation
  if (is_angle_between(pick_data.azimuth, prev_pulse_->azimuth, data->azimuth))
  {
    // technically we could check whether prev or this pulse is closer, but its
    // easier to use new (so we pick the pulse following the pick instead of closest)
    window_.update_data(data
        , calib->tx_ref_power_adu_h
        , calib->tx_ref_power_kw_h
        , calib->tx_ref_power_adu_v
        , calib->tx_ref_power_kw_v);
  }

  prev_pulse_ = std::move(data);
}

pulse_window::pulse_window()
  : combo_units_{new QComboBox()}
  , check_phase_{new QCheckBox("Show Phase")}
  , units_{power_units::dbm}
  , power_x_axis_{new QValueAxis()}
  , power_y_axis_{new QValueAxis()}
  , power_chart_{new QChart()}
  , power_view_{new QChartView(power_chart_)}
  , phase_x_axis_{new QValueAxis()}
  , phase_y_axis_{new QValueAxis()}
  , phase_chart_{new QChart()}
  , phase_view_{new QChartView(phase_chart_)}
{
  combo_units_->addItem("dBm");
  combo_units_->addItem("kW");
  combo_units_->addItem("adu");

  check_phase_->setChecked(true);

  power_x_axis_->setTitleText("Time (\u00b5s)");
  power_x_axis_->setTickType(QValueAxis::TicksDynamic);
  power_x_axis_->setTickInterval(1.0);
  power_x_axis_->setTickAnchor(3);
  power_y_axis_->setTitleText("Power (dBm)");
  power_y_axis_->setLabelFormat("%.0f");
  power_chart_->addAxis(power_x_axis_, Qt::AlignBottom);
  power_chart_->addAxis(power_y_axis_, Qt::AlignLeft);
  power_chart_->legend()->hide();

  phase_x_axis_->setTitleText("Time (\u00b5s)");
  phase_x_axis_->setTickType(QValueAxis::TicksDynamic);
  phase_x_axis_->setTickInterval(1.0);
  phase_x_axis_->setTickAnchor(3);
  phase_y_axis_->setTitleText("Phase (\u00b0)");
  phase_y_axis_->setLabelFormat("%g");
  phase_y_axis_->setRange(-180.0, 180.0);
  phase_chart_->addAxis(phase_x_axis_, Qt::AlignBottom);
  phase_chart_->addAxis(phase_y_axis_, Qt::AlignLeft);
  phase_chart_->legend()->hide();

  auto toolbar = new QWidget();
  auto layout_tb = new QHBoxLayout(toolbar);
  layout_tb->addWidget(combo_units_);
  layout_tb->addWidget(check_phase_);

  auto layout = new QVBoxLayout{this};
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addWidget(toolbar);
  layout->addWidget(power_view_);
  layout->addWidget(phase_view_);

  resize(500, 600);
  setWindowTitle("View Pulse");
  show();

  QObject::connect(combo_units_, &QComboBox::currentTextChanged, this, &pulse_window::on_change_units);
  QObject::connect(check_phase_, &QCheckBox::stateChanged, this, &pulse_window::on_toggle_phase);
  QObject::connect(this, &pulse_window::update_data, this, &pulse_window::on_update_data);
}

auto pulse_window::hideEvent(QHideEvent* event) -> void
{
  window_hidden();
}

auto pulse_window::on_change_units(QString const& text) -> void
{
  if (text == "dBm")
  {
    units_ = power_units::dbm;
    power_y_axis_->setTitleText("Power (dBm)");
  }
  else if (text == "kW")
  {
    units_ = power_units::kw;
    power_y_axis_->setTitleText("Power (kW)");
  }
  else
  {
    units_ = power_units::adu;
    power_y_axis_->setTitleText("Power (adu)");
  }
}

auto pulse_window::on_toggle_phase(int state) -> void
{
  auto checked = (state != 0);

  if (phase_view_->isVisible() == checked)
    return;

  auto hdelta = power_view_->height() * (checked ? 1 : -1);
  phase_view_->setVisible(checked);
  resize(width(), height() + hdelta);
}

void pulse_window::on_update_data(shared_ptr<pulse const> p, double ref_adu_h, double ref_kw_h, double ref_adu_v, double ref_kw_v)
{
  // clear previous chart
  power_chart_->removeAllSeries();
  phase_chart_->removeAllSeries();

  auto ymax = std::numeric_limits<double>::lowest();

  auto create_series = [&](array1cf const& iq_tx, float sample_start, float sample_step, double ref_adu, double ref_kw)
  {
    auto power_series = new QLineSeries();
    for (auto i = 0uz; i < iq_tx.size(); ++i)
    {
      auto time = sample_start + i * sample_step;
      auto iq = std::complex<double>{iq_tx[i]};

      auto val = (iq * conj(iq)).real(); // adu power
      switch (units_)
      {
      case power_units::dbm:
        val = (val / ref_adu) * ref_kw; // adu power to kw
        if (val <= 0.0)
          continue;
        val = 10.0 * std::log10(1000.0 * val) + 30.0; // kw to dbm
        break;
      case power_units::kw:
        val = (val / ref_adu) * ref_kw; // adu power to kw
        break;
      case power_units::adu:
        val = std::sqrt(val); // adu power to amplitude
        break;
      }

      power_series->append(time, val);
      ymax = std::max(val, ymax);
    }
    power_chart_->addSeries(power_series);
    power_series->attachAxis(power_x_axis_);
    power_series->attachAxis(power_y_axis_);

    auto phase_series = new QLineSeries();
    for (auto i = 0uz; i < iq_tx.size(); ++i)
    {
      auto time = sample_start + i * sample_step;
      auto val = arg(iq_tx[i]);
      val *= 180.0 / pi;
      phase_series->append(time, val);
    }
    phase_chart_->addSeries(phase_series);
    phase_series->attachAxis(phase_x_axis_);
    phase_series->attachAxis(phase_y_axis_);
  };

  // add a line series for each polarization
  if (p->iq_tx_h.size() > 0)
    create_series(p->iq_tx_h, p->sample_start, p->sample_step, ref_adu_h, ref_kw_h);
  if (p->iq_tx_v.size() > 0)
    create_series(p->iq_tx_v, p->sample_start, p->sample_step, ref_adu_v, ref_kw_v);

  // setup axes
  power_x_axis_->setRange(p->sample_start, p->sample_start + p->sample_step * p->sample_count);
  phase_x_axis_->setRange(p->sample_start, p->sample_start + p->sample_step * p->sample_count);
  power_y_axis_->setRange(0, ymax);
}

#include "view_pulse.moc"
