/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "calibration.h"
#include "format.h"

using namespace vispwr;

static mutex                                    mut_calibration_;
static map<int, shared_ptr<calibration const>>  channels_;

auto vispwr::calibration_get(int channel) -> shared_ptr<calibration const>
{
  auto lock = lock_guard<mutex>{mut_calibration_};
  return channels_[channel];
}

auto vispwr::calibration_set(int channel, shared_ptr<calibration const> val) -> void
{
  auto lock = lock_guard<mutex>{mut_calibration_};
  channels_[channel] = std::move(val);

  #if 0
  fmt::print("calibration {} updated:\n", channel);
  fmt::print("    wavelength = {}\n", channels_[channel]->wavelength);
  fmt::print("    tx_ref_power_adu_h = {}\n", channels_[channel]->tx_ref_power_adu_h);
  fmt::print("    tx_ref_power_kw_h = {}\n", channels_[channel]->tx_ref_power_kw_h);
  fmt::print("    noise_power_h = {}\n", channels_[channel]->noise_power_h);
  fmt::print("    sp_calib_h = {}\n", channels_[channel]->sp_calib_h);
  fmt::print("    radar_constant_h = {}\n", channels_[channel]->radar_constant_h);
  fmt::print("    tx_ref_power_adu_v = {}\n", channels_[channel]->tx_ref_power_adu_v);
  fmt::print("    tx_ref_power_kw_v = {}\n", channels_[channel]->tx_ref_power_kw_v);
  fmt::print("    noise_power_v = {}\n", channels_[channel]->noise_power_v);
  fmt::print("    sp_calib_v = {}\n", channels_[channel]->sp_calib_v);
  fmt::print("    radar_constant_v = {}\n", channels_[channel]->radar_constant_v);
  fmt::print("    atmospheric_attenuation = {}\n", channels_[channel]->atmospheric_attenuation);
  fmt::print("    zdr_offset = {}\n", channels_[channel]->zdr_offset);
  fmt::print("    phidp_offset = {}\n", channels_[channel]->phidp_offset);
  #endif
}

auto vispwr::calibration_clear(int channel) -> void
{
  auto lock = lock_guard<mutex>{mut_calibration_};
  channels_.erase(channel);
}

auto vispwr::calibration_clear_all() -> void
{
  auto lock = lock_guard<mutex>{mut_calibration_};
  channels_.clear();
}
