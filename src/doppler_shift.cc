/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"

using namespace vispwr;

namespace vispwr
{
  class doppler_shift : public stage
  {
  protected:
    doppler_shift();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<spectra> data, int input) -> void override;
    auto process(shared_ptr<spectra const> data, int input) -> void override;

  private:
    int shift_;
  };
}

static constexpr auto plist = std::array<string_view, 1>{ "Shift" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
  { "spectra", data_type::spectra, true, "Input spectra" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
  { "spectra", data_type::spectra, "Output spectra" }
}};

static const auto desc = R"(
Rotate the values of a Doppler spectrum.  User selects the number of spectral bins by which to rotate.  No
consideration is given to data streams that utilize different sized spectrums (such as Dual-PRF data).

**Input:** Spectra

**Output:** Spectra
)";

static auto ireg = stage::enrol<doppler_shift>("doppler_shift", "Doppler Shift", "Process", desc);

doppler_shift::doppler_shift()
  : stage{plist, ilist, olist}
  , shift_{1}
{ }

auto doppler_shift::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto doppler_shift::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Shift")
    return parameter_integer
    {
      .name = "Shift",
      .description = "Number of FFT bins to rotate spectrum by",
      .min = -1024,
      .max = 1024,
      .value = shift_,
    };
  return {};
}

auto doppler_shift::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Shift")
    shift_ = std::get<int>(value);
}

auto doppler_shift::process(shared_ptr<spectra> data, int input) -> void
{
  if (data->fiq_rx_h.size() != 0)
  {
    for (auto igate = 0uz; igate < data->gate_count; ++igate)
      std::rotate(
            &data->fiq_rx_h[igate, 0]
          , &data->fiq_rx_h[igate, shift_ % data->fft_size]
          , &data->fiq_rx_h[igate, data->fft_size]);
  }

  if (data->fiq_rx_v.size() != 0)
  {
    for (auto igate = 0uz; igate < data->gate_count; ++igate)
      std::rotate(
            &data->fiq_rx_v[igate, 0]
          , &data->fiq_rx_v[igate, shift_ % data->fft_size]
          , &data->fiq_rx_v[igate, data->fft_size]);
  }

  send(0, std::move(data));
}

auto doppler_shift::process(shared_ptr<spectra const> data, int input) -> void
{
  doppler_shift::process(make_shared<spectra>(*data), input);
}
