# Vispwr: The Visual Interactive Signal Processor for Weather Radar

**Vispwr** is an open-source application for experimenting with signal processing for weather radars.  It allows
users to graphically assemble complex custom signal processor from a library of filters and then apply these
to raw IQ datasets.  Integrated visualisation tools allow the user to inspect the output of each stage in the
signal processing graph, and with a single mouse click, the moments, spectra, IQ, Tx power and phase, and even
pulse shape associated with the point of interest can be displayed.  This functionality makes Vispwr a powerful
tool for improving the understanding of how various signal processing techniques interact.

