/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "iqr.h"
#include "format.h"
#include "calibration.h"
#include "util.h"

#include <QHBoxLayout>
#include <QIcon>
#include <QFileDialog>
#include <QListWidget>
#include <QPushButton>
#include <QVBoxLayout>

using namespace vispwr;

namespace vispwr
{
  class playlist_window : public QWidget
  {
    Q_OBJECT
  public:
    playlist_window();

  signals:
    void window_hidden();
    void file_selected(string const& path);

  protected:
    auto hideEvent(QHideEvent* event) -> void override;

    auto on_add_file() -> void;
    auto on_remove_file() -> void;
    auto on_promote_file() -> void;
    auto on_demote_file() -> void;
    auto on_select_file(QString const& path) -> void;

  private:
    QListWidget list_;
  };

  /// IQ replayer for Leonardo IQR format
  class replay_iqr : public stage
  {
  protected:
    replay_iqr();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process_idle() -> std::chrono::milliseconds override;
    auto process(shared_ptr<resend_request const> data, int input) -> void override;

    auto send_calibration() -> void;

  private:
    using time_point = std::chrono::steady_clock::time_point;

  private:
    optional<iqr::file> file_;            ///< File being replayed
    double              filter_from_ms_;  ///< Time offset for start of replay (ms since unix epoch)
    double              filter_till_ms_;  ///< Time offset for end of replay (ms since unix epoch)

    double              file_from_ms_;    ///< Start time in IQR headers (ms since unix epoch)
    double              file_till_ms_;    ///< Stop time in IQR headers (ms since unix epoch)

    bool                filtering_;       ///< True while outside from/till filter bounds
    int                 calib_channel_;

    shared_ptr<pulse>   pulse_;           ///< Pulse currently under construction
    uint64_t            next_id_;         ///< Sequential ID for next pulse to output

    playlist_window     window_;
  };
}

static constexpr auto plist = std::array<string_view, 4>{ "Playlist", "Calib", "From", "Till" };
static constexpr auto ilist = std::array<input_meta, 0>
{{
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
   { "pulse"sv, data_type::pulse, "IQ data for a single pulse" }
}};

static const auto desc = R"(
Read and replay IQ data from the Leonardo IQR format output by GDRX series signal processors.

**Input:** None

**Output:** Pulses
)";

static auto ireg = stage::enrol<replay_iqr>("replay_iqr", "Replay IQR", "Input", desc);

replay_iqr::replay_iqr()
  : stage{plist, ilist, olist}
  , filter_from_ms_{0.0}
  , filter_till_ms_{999000.0}
  , filtering_{false}
  , calib_channel_{0}
  , next_id_{0}
{
  QObject::connect(&window_, &playlist_window::window_hidden, [this]() { notify_parameter_update("Playlist"); });
  QObject::connect(&window_, &playlist_window::file_selected, [this](string const& path)
  {
    // TODO - this doesn't do anything!!!!!!!
    // there are no mutexes locked during a normal process() call so we can't possibly protect access like this
    // we would need our own mutex in this stage to protect the file object
    //auto lock = lock_mutex();

    file_.reset();
    pulse_.reset();
    next_id_ = 0;

    if (!path.empty())
    {
      file_.emplace(path);
      file_from_ms_ = file_->header<long>("data.time.ms.from");
      file_till_ms_ = file_->header<long>("data.time.ms.to");
      broadcast(make_shared<state>("iq-begin"));
      send_calibration();
    }

    wake_worker();
  });
}

auto replay_iqr::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto replay_iqr::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Playlist")
    return parameter_trigger
    {
      .name = "Playlist",
      .description = "Open or close playlist window",
      .label = window_.isVisible() ? "Hide" : "Show",
    };
  if (name == "Calib")
    return parameter_integer
    {
      .name = "Calib",
      .description = "Output calibration channel",
      .min = 0,
      .max = 10,
      .value = calib_channel_,
    };
  if (name == "From")
    return parameter_real
    {
      .name = "From",
      .description = "Offset time to play from in seconds",
      .decimals = 2,
      .min = 0.0,
      .max = 1800.0,
      .step = 1.0,
      .value = filter_from_ms_ * 0.001,
    };
  if (name == "Till")
    return parameter_real
    {
      .name = "Till",
      .description = "Offset time to play until in seconds",
      .decimals = 2,
      .min = 0.0,
      .max = 999.0,
      .step = 1.0,
      .value = filter_till_ms_ * 0.001,
    };
  return {};
}

auto replay_iqr::set_parameter_impl(string_view name, parameter_value value) -> void
{
  // TODO - protect parameters from changing during execution
  if (name == "Playlist")
  {
    if (std::get<bool>(value) == true)
    {
      window_.setVisible(!window_.isVisible());
      notify_parameter_update("Playlist");
    }
  }
  else if (name == "Calib")
    calib_channel_ = std::get<int>(value);
  else if (name == "From")
    filter_from_ms_ = std::get<double>(value) * 1000.0;
  else if (name == "Till")
    filter_till_ms_ = std::get<double>(value) * 1000.0;
}

auto replay_iqr::process_idle() -> std::chrono::milliseconds
{
  // if we have no file, tell the system we want to block forever by returning a negative timeout
  // the act of selecting a file via set_parameter will unblock the worker thread
  if (!file_)
    return -1ms;

  // process the file one archive item at a time
  switch (file_->peek())
  {
  case iqr::archive_data_type::none:
    // tell downstream processes we have reached the end of the file
    broadcast(make_shared<state>("iq-end"));

    // block forever until the user requests a resend or changes file
    return -1ms;

  case iqr::archive_data_type::timestamp:
    {
      auto data = iqr::timestamp{};
      file_->extract(data);
      filtering_ = data.time_ms_epoch < file_from_ms_ + filter_from_ms_ || data.time_ms_epoch >= file_from_ms_ + filter_till_ms_;
      #if 0
      fmt::print(
            "file {}, from {}, till {}, file+from {}, file+till {}, ts {}\n"
          , file_from_ms_
          , filter_from_ms_
          , filter_till_ms_
          , file_from_ms_ + filter_from_ms_
          , file_from_ms_ + filter_till_ms_
          , data.time_ms_epoch);
      #endif
#if 0
      auto t = std::chrono::system_clock::from_time_t(data.time_ms_epoch / 1000);
      fmt::print("ts = {}\n", t);
#endif
    }
    break;

  case iqr::archive_data_type::gdrx5_tx:
    {
      auto data = iqr::gdrx5_tx{};
      file_->extract(data);

      if (!filtering_)
      {
        // transmit samples always come first - so initialize a new pulse for output
        pulse_ = std::make_shared<pulse>();

        // we ignore headers that are in common with the rx payload since they will be seen again later
        pulse_->source_id = data.pulse_counter;
        pulse_->sample_count = data.sample_count;
        pulse_->sample_start = data.sample_start;
        pulse_->sample_step = data.sample_time;
        pulse_->iq_tx_h = std::move(data.iq_h);
        pulse_->iq_tx_v = std::move(data.iq_v);
      }
    }
    break;

  case iqr::archive_data_type::gdrx5_iq:
    {
      auto data = iqr::gdrx5_iq{};
      file_->extract(data);

      if (!filtering_)
      {
        // only initialize a new pulse if we didn't already receive the corresponding TX message
        if (!pulse_ || data.pulse_counter != pulse_->source_id)
        {
          pulse_ = std::make_shared<pulse>();

          // zero out the tx only fields
          pulse_->sample_count = 0;
          pulse_->sample_start = 0.0f;
          pulse_->sample_step = 0.0f;
        }

        pulse_->id = next_id_++;
        pulse_->source_id = data.pulse_counter;
        pulse_->elevation = data.elevation_angle * (360.0 / 65536.0);
        pulse_->azimuth = data.azimuth_angle * (360.0 / 65536.0);
        pulse_->range_start = data.range_start;
        pulse_->range_step = data.range_step;
        pulse_->prf = data.prf;
        pulse_->prf_index = iqr::staggering_flags::prf_batch_index(data.staggering_flags);
        pulse_->tx_polarization = polarization::hv; // TODO - detect H-only, V-only, and alternating modes (spbpolmode?)
        pulse_->tx_magnitude_h = data.tx_pulse_magnitude_h;
        pulse_->tx_magnitude_v = data.tx_pulse_magnitude_v;
        pulse_->tx_phase_h = data.tx_pulse_phase_h;
        pulse_->tx_phase_v = data.tx_pulse_phase_v;
        pulse_->matched_filter_losses_h = data.matched_filter_losses_h;
        pulse_->matched_filter_losses_v = data.matched_filter_losses_v;

        pulse_->gate_count = data.gate_count;
        pulse_->iq_rx_h = std::move(data.iq_h);
        pulse_->iq_rx_v = std::move(data.iq_v);

        // output the completed pulse datum
        send(0, std::move(pulse_));
      }
    }
    break;

  default:
    file_->ignore();
  }

  // tell the system we want to process more data immediately (0s timeout)
  return 0ms;
}

auto replay_iqr::process(shared_ptr<resend_request const> data, int input) -> void
{
  if (file_)
  {
    file_->rewind();
    next_id_ = 0;
    broadcast(make_shared<state>("iq-begin"));
    send_calibration();
  }
}

static auto nth_token(string const& str, int n) -> string_view
{
  auto index = 0;
  size_t pos = 0;
  while ((pos = str.find_first_not_of(" ", pos)) != string::npos)
  {
    size_t end = str.find_first_of(" ", pos + 1);
    size_t len = (end == string::npos ? str.size() : end) - pos;
    if (index++ == n)
      return string_view{&str[pos], len};
    pos += len;
  }
  throw std::runtime_error{fmt::format("Failed to extract token {} from string '{}'", n, str)};
}

auto replay_iqr::send_calibration() -> void
{
  if (!file_)
    return;

  auto c = make_shared<calibration>();

  // which pulse width index is in use?
  /* a pulse index of -1 is valid, and indicates dynamic pulse width is in use.  in that case we should expect
   * a single entry in each header instead of a list (so treat it as if asking for index 0) */
  auto ipulse = std::max(file_->header<int>("states.spbpwidth"), 0);

  #if 0
  fmt::print("selected pw index {}\n", ipulse);
  #endif

  // lambda for looking up the value in a header that corresponds the active pulse width
  auto pulse_header = [&](string const& name)
  {
    double val;
    auto token = nth_token(file_->header<string>(name), ipulse);
    if (auto res = std::from_chars(&token[0], &token[token.size()], val); res.ptr != &token[token.size()])
      throw std::runtime_error{fmt::format("Failed to parse header {}", name)};
    return val;
  };

  c->wavelength = file_->header<double>("states.rsplambda") * 0.01;
  c->tx_ref_power_adu_h = pulse_header("states.gdrx5caltxpowpwh");
  c->tx_ref_power_kw_h = pulse_header("states.gdrx5caltxpowkwpwh");
  c->noise_power_h = pulse_header("states.rspnoisepwr");
  c->sp_calib_h = pulse_header("states.spbdbmtologoffset");
  c->radar_constant_h = pulse_header("states.rspdphradconst");
  c->tx_ref_power_adu_v = pulse_header("states.gdrx5caltxpowpwv");
  c->tx_ref_power_kw_v = pulse_header("states.gdrx5caltxpowkwpwv");
  c->noise_power_v = pulse_header("states.rspdpvnoisepwr");
  c->sp_calib_v = pulse_header("states.spbdpvdbmtologoffset");
  c->radar_constant_v = pulse_header("states.rspdpvradconst");
  c->atmospheric_attenuation = file_->header<double>("states.rspathatt");
  c->zdr_offset = pulse_header("states.spbzdroffpw"); //maybe?
  c->phidp_offset = pulse_header("states.spbphidpoffsyspw"); //maybe?

  #if 0
  fmt::print("wavelength {}\n", file_->header<string>("states.rsplambda"));
  fmt::print("freqh {}\n", file_->header<string>("states.gdrx5txfreqh"));
  fmt::print("freqv {}\n", file_->header<string>("states.gdrx5txfreqv"));

  fmt::print("spbmfloss {}\n", file_->header<string>("states.spbmfloss"));
  #endif

  calibration_set(calib_channel_, std::move(c));
}

playlist_window::playlist_window()
{
  auto toolbar = new QWidget();
  auto layout_tb = new QHBoxLayout(toolbar);

  auto but_add = new QPushButton("+");
  layout_tb->addWidget(but_add);
  auto but_remove = new QPushButton("-");
  layout_tb->addWidget(but_remove);
  auto but_up = new QPushButton("^");
  layout_tb->addWidget(but_up);
  auto but_down = new QPushButton("v");
  layout_tb->addWidget(but_down);

  auto layout = new QVBoxLayout(this);
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addWidget(toolbar);
  layout->addWidget(&list_);

  resize(300, 500);
  setWindowTitle("IQR Playlist");
  show();

  QObject::connect(but_add, &QPushButton::clicked, this, &playlist_window::on_add_file);
  QObject::connect(but_remove, &QPushButton::clicked, this, &playlist_window::on_remove_file);
  QObject::connect(but_up, &QPushButton::clicked, this, &playlist_window::on_promote_file);
  QObject::connect(but_down, &QPushButton::clicked, this, &playlist_window::on_demote_file);
  QObject::connect(&list_, &QListWidget::currentTextChanged, this, &playlist_window::on_select_file);
}

auto playlist_window::hideEvent(QHideEvent* event) -> void
{
  window_hidden();
}

auto playlist_window::on_add_file() -> void
{
  auto path_list = QFileDialog::getOpenFileNames(
        nullptr
      , QObject::tr("Choose file")
      , "" //QDir::homePath()
      , "IQR files (*.iqr)"
      , nullptr
      , QFileDialog::ReadOnly
      );
  for (auto& path : path_list)
    list_.addItem(path);
}

auto playlist_window::on_remove_file() -> void
{
  auto selected = list_.selectedItems();
  for (auto& item : selected)
    delete list_.takeItem(list_.row(item));
}

auto playlist_window::on_promote_file() -> void
{
  auto row = list_.currentRow();
  if (list_.count() > 1 && row > 0)
  {
    auto item = list_.takeItem(row);
    list_.insertItem(row - 1, item);
    list_.setCurrentRow(row - 1);
  }
}

auto playlist_window::on_demote_file() -> void
{
  auto row = list_.currentRow();
  if (list_.count() > 1 && row < list_.count() - 1)
  {
    auto item = list_.takeItem(row);
    list_.insertItem(row + 1, item);
    list_.setCurrentRow(row + 1);
  }
}

auto playlist_window::on_select_file(QString const& path) -> void
{
  file_selected(path.toStdString());
}

#include "replay_iqr.moc"
