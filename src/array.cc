/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "array.h"
#include "format.h"
#include <fftw3.h>

using namespace vispwr;

auto vispwr::aligned_malloc(size_t size) -> void*
{
  if (auto ptr = fftwf_malloc(size))
    return ptr;
  throw std::runtime_error{fmt::format("Failed to allocate aligned memory of size {}", size)};
}

auto vispwr::aligned_free(void* ptr) -> void
{
  fftwf_free(ptr);
}
