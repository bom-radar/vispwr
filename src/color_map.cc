/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "color_map.h"
#include "util.h"
#include <algorithm>
#include <filesystem>
#include <numeric>
#include <ranges>

using namespace vispwr;

auto vispwr::hex_to_color(string const& val) -> color
{
  color col;
  if (val.size() == 8)
  {
    if (sscanf(val.c_str(), "%2hhx%2hhx%2hhx%2hhx", &col.r, &col.g, &col.b, &col.a) == 4)
      return col;
  }
  else if (val.size() == 6)
  {
    if (sscanf(val.c_str(), "%2hhx%2hhx%2hhx", &col.r, &col.g, &col.b) == 3)
    {
      col.a = 0xff;
      return col;
    }
  }
  throw runtime_error{fmt::format("Failed to parse color string '{}'", val)};
}

color_map::color_map(json const& j)
  : missing_{hex_to_color(j["missing"].string())}
  , underflow_{hex_to_color(j["underflow"].string())}
  , overflow_{hex_to_color(j["overflow"].string())}
{
  auto& colors = j["colors"].array();
  if (colors.size() < 2)
    throw runtime_error{"Insufficient entries in color map"};
  colors_.resize(colors.size());
  for (auto i = 0uz; i < colors_.size(); ++i)
    colors_[i].col = hex_to_color(colors[i].string());

  if (auto ivalues = j.object().find("values"); ivalues != j.object().end())
  {
    auto& values = ivalues->second.array();
    if (values.size() != colors_.size())
      throw runtime_error{"Mismatch between values and colors array size in color map"};
    colors_[0].val = values[0].number<float>();
    for (auto i = 1uz; i < colors_.size(); ++i)
    {
      colors_[i].val = values[i].number<float>();
      if (!(colors_[i].val > colors_[i - 1].val))
        throw runtime_error{"Explicit color map values are not strictly increasing"};
    }
  }
  else
  {
    auto delta = 1.0f / (colors_.size() - 1);
    for (auto i = 0uz; i < colors_.size(); ++i)
      colors_[i].val = i * delta;
  }
}

auto color_map::evaluate(float value) const -> color
{
  if (std::isnan(value))
    return missing_;

  auto i = std::ranges::upper_bound(colors_, value, {}, &entry::val);
  if (i == colors_.begin())
    return underflow_;
  if (i == colors_.end())
      return value == (i - 1)->val ? (i - 1)->col : overflow_;

  auto frac = (value - (i - 1)->val) / (i->val - (i - 1)->val);
  return
  {
      uint8_t(std::lerp((i-1)->col.r, i->col.r, frac))
    , uint8_t(std::lerp((i-1)->col.g, i->col.g, frac))
    , uint8_t(std::lerp((i-1)->col.b, i->col.b, frac))
    , uint8_t(std::lerp((i-1)->col.a, i->col.a, frac))
  };
}

auto vispwr::color_map_index() -> map<string, filesystem::path> const&
{
  static auto index_ = []()
  {
    auto index = map<string, filesystem::path>{};
    for (auto& base_path : data_paths())
      if (auto path = base_path / "cmaps"; filesystem::is_directory(path))
        for (auto& entry : filesystem::directory_iterator(path))
          index.try_emplace(entry.path().stem().string(), entry.path());
    return index;
  }();
  return index_;
}
