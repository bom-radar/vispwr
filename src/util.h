/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "types.h"
#include <charconv>
#include <concepts>

#if defined(__GLIBCXX__) && defined(__MINGW32__) && !_GLIBCXX_HAVE_USELOCALE
namespace std
{
  auto from_chars(char const* first, char const* last, float& value, chars_format fmt = chars_format::general) noexcept -> from_chars_result;
  auto from_chars(char const* first, char const* last, double& value, chars_format fmt = chars_format::general) noexcept -> from_chars_result;
  auto from_chars(char const* first, char const* last, long double& value, chars_format fmt = chars_format::general) noexcept -> from_chars_result;
}
#endif

namespace vispwr
{
  /// Less verbose way of getting a NaN
  template <std::floating_point T> inline constexpr T nan = std::numeric_limits<T>::quiet_NaN();

  /// Get a list of paths to search for application data
  /** Despite returning a reference, this function is thread safe. */
  auto data_paths() -> vector<filesystem::path> const&;

  // TODO - optimize this
  template <std::floating_point T>
  inline auto is_angle_between(T target, T angle1, T angle2)
  {
    target = std::fmod(target + 360.0, 360.0);
    angle1 = std::fmod(angle1 + 360.0, 360.0);
    angle2 = std::fmod(angle2 + 360.0, 360.0);
    if (angle2 < angle1)
      std::swap(angle1, angle2);
    if (angle2 - angle1 <= 180.0)
      return target >= angle1 && target <= angle2;
    else
      return target <= angle1 || target >= angle2;
  }
}
