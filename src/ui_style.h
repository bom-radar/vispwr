/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "pick.h"
#include "types.h"

#include <QColor>
#include <QFont>
#include <QFontMetrics>

namespace vispwr
{
  struct ui_style
  {
    // styles for the main graph
    QColor        selected_color = "#88ffff";

    QColor        grid_background = "#353535";
    QColor        grid_major_color = "#191919";
    double        grid_major_width = 1.0;
    double        grid_major_spacing = 200;
    QColor        grid_minor_color = "#3c3c3c";
    double        grid_minor_width = 1.0;
    double        grid_minor_spacing = 20;

    float         stage_border_radius = 3.0f;
    double        stage_border_width = 1.0;
    double        stage_border_width_hover = 3.0;
    QColor        stage_border_color = "#ffffff";
    vector<pair<double, QColor>> stage_background_colors =
    {
        { 0.00, "#cc808080" }
      , { 0.03, "#cc505050" }
      , { 0.97, "#cc404040" }
      , { 1.00, "#cc3a3a3a" }
    };
    QColor        stage_label_color = "#ffffff";
    QFont         stage_label_font = []{ QFont font; font.setBold(true); return font; }();
    QFontMetrics  stage_label_metrics;
    bool          stage_show_single_port_labels = false;

    int           stage_log_flag_offset = 2;
    int           stage_log_flag_size = 14;
    QColor        stage_log_flag_color = "#e64c4c";

    size_t        stage_max_log_messages = 15;
    std::array<string_view, 3> stage_log_colors =
    {
        "#ff0000"
      , "#ffff00"
      , "#ffffff"
    };

    QFont         port_font;
    QFontMetrics  port_metrics;
    int           port_spacing_x = 10;                // extra space between ports and parameters
    int           port_spacing_y = 20;                // extra space between ports (vertically)
    float         port_diameter = 8.0f;               // max port diameter for sizing, spacing, hitbox
    float         port_diameter_free = 5.5;           // drawn size for disconnected sockets
    int           port_label_inset = 5;               // text inset from square edge
    QColor        port_label_color_used = "#ffffff";
    QColor        port_label_color_free = "#808080";

    double        con_draw_width = 3.0;
    double        con_grab_width = 10.0;

    std::array<QColor, 6>  data_type_color =
    {
        "#ff6666"
      , "#ffff66"
      , "#66ff66"
      , "#66ffff"
      , "#6666ff"
      , "#ff66ff"
    };

    // styles for picker flags in data viewer windows
    double        pick_radius = 6.0;
    QFont         pick_font = []{ QFont f; f.setStyleHint(QFont::TypeWriter); f.setFamily("Monospace"); f.setPointSize(10.0); return f; }();
    QFontMetrics  pick_metrics;
    QColor        pick_background_color = "#aa000000";
    QColor        pick_border_color = "#ffffff";
    double        pick_border_width = 2.0;
    QColor        pick_text_color = "#ffffff";
    std::array<QColor, pick::channels> pick_channel_color = { "#e7298a", "#7570b3", "#1b9e77", "#d95f02" };

    // styles for PPI window
    QColor        ppi_axes_color = "#ffffff";
    double        ppi_axes_width = 1.0;
    QColor        ppi_rings_color = "#ffffff";
    double        ppi_rings_width = 1.0;

    ui_style()
      : stage_label_metrics(stage_label_font)
      , port_metrics(port_font)
      , pick_metrics(pick_font)
    { }

    /* this is 'initialized on demand' because if we create our fonts as static variables something goes wrong and the font
     * metrics give us incorrect sizes.  my guess is that the metrics can't determine mapping to device coordinate / pixels
     * so early since the rest of Qt won't have been initialized yet. */
    static auto get() -> ui_style const&;
  };

  inline auto ui_style::get() -> ui_style const&
  {
    static ui_style s;
    return s;
  }
}
