/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

// hacks to pull in IWRF codebase from LROSE
#include "/usr/local/lrose/include/radar/IwrfTsReader.hh"
