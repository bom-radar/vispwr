/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"

using namespace vispwr;

namespace vispwr
{
  class recover_trips : public stage
  {
  public:
    recover_trips();
    recover_trips(bool cx_filter);
    ~recover_trips();

    auto name() const -> string_view override;
    auto inputs() const -> vector<input> const& override;
    auto outputs() const -> vector<output> const& override;
    auto maximum_concurrency() const -> int override;
    auto startup() -> void override;
    auto shutdown() -> void override;
    auto ready_to_execute(input_flags availability) const -> bool override;
    auto execute(input_vector& inputs, output_store& outputs) -> void override;

  private:
    auto cohere_trip(
          multi_array<iq, 2> const& trip0
        , int N
        , multi_array<iq, 2>& tripN
        ) -> void;

  private:
    vector<complex<float>> tbuf_;
    vector<complex<float>> fbuf_;
    fftwf_plan plan_fwd_;
    fftwf_plan plan_rev_;
    bool cx_filter_;
  };
}

constexpr auto fft_size = 160;
constexpr auto notch_clutter = 11;
constexpr auto notch_trip = 11;

recover_trips::recover_trips()
  : recover_trips(false)
{ }

recover_trips::recover_trips(bool cx_filter)
{
  tbuf_.resize(fft_size);
  fbuf_.resize(fft_size);
  plan_fwd_ = fftwf_plan_dft_1d(fft_size, (fftwf_complex*) tbuf_.data(), (fftwf_complex*) fbuf_.data(), FFTW_FORWARD, FFTW_ESTIMATE);
  plan_rev_ = fftwf_plan_dft_1d(fft_size, (fftwf_complex*) fbuf_.data(), (fftwf_complex*) tbuf_.data(), FFTW_BACKWARD, FFTW_ESTIMATE);
  cx_filter_ = cx_filter;
}

recover_trips::~recover_trips()
{
  fftwf_destroy_plan(plan_fwd_);
  fftwf_destroy_plan(plan_rev_);
}

auto recover_trips::name() const -> string_view
{
  return "Recover Trips"sv;
}

auto recover_trips::inputs() const -> vector<input> const&
{
  static auto list = []
  {
    auto ret = vector<input>();
    ret.reserve(1);
    ret.emplace_back("batch"sv, data_type::batch, "Batch to recover trips for");
    return ret;
  }();
  return list;
}

auto recover_trips::outputs() const -> vector<output> const&
{
  static auto list = []
  {
    auto ret = vector<output>();
    ret.reserve(1);
    ret.emplace_back("batch"sv, data_type::batch, "Trip recovered batches");
    return ret;
  }();
  return list;
}

auto recover_trips::maximum_concurrency() const -> int
{
  return 1;
}

auto recover_trips::startup() -> void
{ }

auto recover_trips::shutdown() -> void
{ }

auto recover_trips::ready_to_execute(input_flags availability) const -> bool
{
  return availability.any();
}

static auto calculate_sqi(iq const* data, int pulse_count) -> double
{
  std::complex<double> lag0, lag1;
  lag0.real(lag0.real() + (data[0].i * data[0].i + data[0].q * data[0].q));
  for (auto i = 1; i < pulse_count; ++i)
  {
    lag0.real(lag0.real() + (data[i].i * data[ i ].i + data[i].q * data[ i ].q));
    lag1.real(lag1.real() + (data[i].i * data[i-1].i + data[i].q * data[i-1].q));
    lag1.imag(lag1.imag() + (data[i].q * data[i-1].i - data[i].i * data[i-1].q));
  }
  lag0 /= pulse_count;
  lag1 /= (pulse_count - 1);
  return std::abs(lag1) / std::abs(lag0);
}

auto recover_trips::execute(input_vector& inputs, output_store& outputs) -> void
{
  // TODO - configure trip to retrieve
  auto trips = 2;
  auto trip_separation = 0.0; // range gap between trips TODO

  auto batch_i = static_cast<batch const*>(inputs[0].get());
  if (batch_i->pulse_count < trips)
  {
    // TODO - probaby just throw it away and don't output a batch???
    throw std::runtime_error{"Insufficient pulses in batch to cohere desired trip"};
  }

  auto batch_o = make_shared<batch>();
  batch_o->id = batch_i->id;
  batch_o->range_start = batch_i->range_start;
  batch_o->range_step = batch_i->range_step;
  batch_o->prf_index = batch_i->prf_index;
  batch_o->gate_count = batch_i->gate_count * 2;
  batch_o->pulse_count = batch_i->pulse_count - 1; // no second trip return possible in first pulse and so on
  batch_o->azimuth = std::make_unique<float[]>(batch_o->pulse_count);
  for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
    batch_o->azimuth[ipulse] = batch_i->azimuth[ipulse + 1];
  batch_o->elevation = std::make_unique<float[]>(batch_o->pulse_count);
  for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
    batch_o->elevation[ipulse] = batch_i->elevation[ipulse + 1];

  // shift phase of each tx sample to match transmit pulse (i.e. trip 1)
  if (batch_i->iq_rx_h.size() != 0)
  {
    batch_o->tx_phase_h = std::make_unique<float[]>(batch_o->pulse_count);
    batch_o->iq_rx_h.resize(boost::extents[batch_o->gate_count, batch_o->pulse_count]);

    for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
      batch_o->tx_phase_h[ipulse] = batch_i->tx_phase_h[ipulse + 1];

    for (auto igate = 0; igate < batch_i->gate_count; ++igate)
    {
      // determine sqi as first trip
      auto sqi_trip_0 = calculate_sqi(&batch_i->iq_rx_h[igate, 1], batch_o->pulse_count);

      // phase correct for second trip, store in output batch (just a buffer)
      for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
      {
        auto phasecor = batch_i->tx_phase_h[ipulse + 1] - batch_i->tx_phase_h[ipulse];
        auto const& iq0 = batch_i->iq_rx_h[igate, ipulse + 1];
        auto iq0c = std::complex<double>(iq0.i, iq0.q);
        auto iqNc = std::polar(std::abs(iq0c), std::arg(iq0c) + phasecor);
        batch_o->iq_rx_h[igate, ipulse].i = iqNc.real();
        batch_o->iq_rx_h[igate, ipulse].q = iqNc.imag();
      }

      // determine sqi as second trip
      auto sqi_trip_1 = calculate_sqi(&batch_o->iq_rx_h[igate, 0], batch_o->pulse_count);

      // should we process trip 0 or trip 1 first?
      if (sqi_trip_0 > sqi_trip_1)
      {
        // first trip is strongest

        // convert to frequency domain
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          tbuf_[i].real(batch_i->iq_rx_h[igate, i + 1].i);
          tbuf_[i].imag(batch_i->iq_rx_h[igate, i + 1].q);
        }
        for (auto i = batch_o->pulse_count; i < fft_size; ++i)
        {
          tbuf_[i].real(0.0);
          tbuf_[i].imag(0.0);
        }
        fftwf_execute(plan_fwd_);
        for (auto i = 0; i < fft_size; ++i)
          fbuf_[i] /= fft_size;

        // apply a clutter filter
        if (cx_filter_)
        {
          for (auto i = 0; i < notch_clutter; ++i)
          {
            fbuf_[i].real(0.0f);
            fbuf_[i].imag(0.0f);
            fbuf_[fft_size - 1 - i].real(0.0f);
            fbuf_[fft_size - 1 - i].imag(0.0f);
          }
        }

        // convert back to time domain
        fftwf_execute(plan_rev_);

        // copy into trip 1 position
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          batch_o->iq_rx_h[igate, i].i = tbuf_[i].real();
          batch_o->iq_rx_h[igate, i].q = tbuf_[i].imag();
        }

        // notch out the the first trip data entirely
        for (auto i = 0; i < notch_trip; ++i)
        {
          fbuf_[i].real(0.0f);
          fbuf_[i].imag(0.0f);
          fbuf_[fft_size - 1 - i].real(0.0f);
          fbuf_[fft_size - 1 - i].imag(0.0f);
        }

        // convert back to time domain again
        fftwf_execute(plan_rev_);

        // phase correct for second trip
        for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
        {
          auto phasecor = batch_i->tx_phase_h[ipulse + 1] - batch_i->tx_phase_h[ipulse];
          tbuf_[ipulse] = std::polar(std::abs(tbuf_[ipulse]), std::arg(tbuf_[ipulse]) + phasecor);
        }

        // apply clutter filter
        if (cx_filter_)
        {
          for (auto i = batch_o->pulse_count; i < fft_size; ++i)
          {
            tbuf_[i].real(0.0);
            tbuf_[i].imag(0.0);
          }
          fftwf_execute(plan_fwd_);
          for (auto i = 0; i < fft_size; ++i)
            fbuf_[i] /= fft_size;
          for (auto i = 0; i < notch_clutter; ++i)
          {
            fbuf_[i].real(0.0f);
            fbuf_[i].imag(0.0f);
            fbuf_[fft_size - 1 - i].real(0.0f);
            fbuf_[fft_size - 1 - i].imag(0.0f);
            fftwf_execute(plan_rev_);
          }
        }

        // copy into trip 2 position
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          batch_o->iq_rx_h[batch_i->gate_count + igate, i].i = tbuf_[i].real();
          batch_o->iq_rx_h[batch_i->gate_count + igate, i].q = tbuf_[i].imag();
        }
      }
      else
      {
        // second trip is strongest

        // convert to frequency domain (cohered data already sitting in batch_o)
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          tbuf_[i].real(batch_o->iq_rx_h[igate, i].i);
          tbuf_[i].imag(batch_o->iq_rx_h[igate, i].q);
        }
        for (auto i = batch_o->pulse_count; i < fft_size; ++i)
        {
          tbuf_[i].real(0.0);
          tbuf_[i].imag(0.0);
        }
        fftwf_execute(plan_fwd_);
        for (auto i = 0; i < fft_size; ++i)
          fbuf_[i] /= fft_size;

        // apply a clutter filter
        if (cx_filter_)
        {
          for (auto i = 0; i < notch_clutter; ++i)
          {
            fbuf_[i].real(0.0f);
            fbuf_[i].imag(0.0f);
            fbuf_[fft_size - 1 - i].real(0.0f);
            fbuf_[fft_size - 1 - i].imag(0.0f);
          }
        }

        // convert back to time domain
        fftwf_execute(plan_rev_);

        // copy into second trip position
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          batch_o->iq_rx_h[batch_i->gate_count + igate, i].i = tbuf_[i].real();
          batch_o->iq_rx_h[batch_i->gate_count + igate, i].q = tbuf_[i].imag();
        }

        // notch out the the second trip data entirely
        for (auto i = 0; i < notch_trip; ++i)
        {
          fbuf_[i].real(0.0f);
          fbuf_[i].imag(0.0f);
          fbuf_[fft_size - 1 - i].real(0.0f);
          fbuf_[fft_size - 1 - i].imag(0.0f);
        }

        // convert back to time domain again
        fftwf_execute(plan_rev_);

        // phase correct back to first trip
        for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
        {
          auto phasecor = batch_i->tx_phase_h[ipulse + 1] - batch_i->tx_phase_h[ipulse];
          tbuf_[ipulse] = std::polar(std::abs(tbuf_[ipulse]), std::arg(tbuf_[ipulse]) - phasecor);
        }

        // apply clutter filter
        if (cx_filter_)
        {
          for (auto i = batch_o->pulse_count; i < fft_size; ++i)
          {
            tbuf_[i].real(0.0);
            tbuf_[i].imag(0.0);
          }
          fftwf_execute(plan_fwd_);
          for (auto i = 0; i < fft_size; ++i)
            fbuf_[i] /= fft_size;
          for (auto i = 0; i < notch_clutter; ++i)
          {
            fbuf_[i].real(0.0f);
            fbuf_[i].imag(0.0f);
            fbuf_[fft_size - 1 - i].real(0.0f);
            fbuf_[fft_size - 1 - i].imag(0.0f);
            fftwf_execute(plan_rev_);
          }
        }

        // copy into first position
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          batch_o->iq_rx_h[igate, i].i = tbuf_[i].real();
          batch_o->iq_rx_h[igate, i].q = tbuf_[i].imag();
        }
      }
    }
  }

  if (batch_i->iq_rx_v.size() != 0)
  {
    batch_o->tx_phase_v = std::make_unique<float[]>(batch_o->pulse_count);
    batch_o->iq_rx_v.resize(boost::extents[batch_o->gate_count, batch_o->pulse_count]);

    for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
      batch_o->tx_phase_v[ipulse] = batch_i->tx_phase_v[ipulse + 1];

    for (auto igate = 0; igate < batch_i->gate_count; ++igate)
    {
      // determine sqi as first trip
      auto sqi_trip_0 = calculate_sqi(&batch_i->iq_rx_v[igate, 1], batch_o->pulse_count);

      // phase correct for second trip, store in output batch (just a buffer)
      for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
      {
        auto phasecor = batch_i->tx_phase_v[ipulse + 1] - batch_i->tx_phase_v[ipulse];
        auto const& iq0 = batch_i->iq_rx_v[igate, ipulse + 1];
        auto iq0c = std::complex<double>(iq0.i, iq0.q);
        auto iqNc = std::polar(std::abs(iq0c), std::arg(iq0c) + phasecor);
        batch_o->iq_rx_v[igate, ipulse].i = iqNc.real();
        batch_o->iq_rx_v[igate, ipulse].q = iqNc.imag();
      }

      // determine sqi as second trip
      auto sqi_trip_1 = calculate_sqi(&batch_o->iq_rx_v[igate, 0], batch_o->pulse_count);

      // should we process trip 0 or trip 1 first?
      if (sqi_trip_0 > sqi_trip_1)
      {
        // first trip is strongest

        // convert to frequency domain
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          tbuf_[i].real(batch_i->iq_rx_v[igate, 1 + i].i);
          tbuf_[i].imag(batch_i->iq_rx_v[igate, 1 + i].q);
        }
        for (auto i = batch_o->pulse_count; i < fft_size; ++i)
        {
          tbuf_[i].real(0.0);
          tbuf_[i].imag(0.0);
        }
        fftwf_execute(plan_fwd_);
        for (auto i = 0; i < fft_size; ++i)
          fbuf_[i] /= fft_size;

        // apply a clutter filter
        if (cx_filter_)
        {
          for (auto i = 0; i < notch_clutter; ++i)
          {
            fbuf_[i].real(0.0f);
            fbuf_[i].imag(0.0f);
            fbuf_[fft_size - 1 - i].real(0.0f);
            fbuf_[fft_size - 1 - i].imag(0.0f);
          }
        }

        // convert back to time domain
        fftwf_execute(plan_rev_);

        // copy into trip 1 position
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          batch_o->iq_rx_v[igate, i].i = tbuf_[i].real();
          batch_o->iq_rx_v[igate, i].q = tbuf_[i].imag();
        }

        // notch out the the first trip data entirely
        for (auto i = 0; i < notch_trip; ++i)
        {
          fbuf_[i].real(0.0f);
          fbuf_[i].imag(0.0f);
          fbuf_[fft_size - 1 - i].real(0.0f);
          fbuf_[fft_size - 1 - i].imag(0.0f);
        }

        // convert back to time domain again
        fftwf_execute(plan_rev_);

        // phase correct for second trip
        for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
        {
          auto phasecor = batch_i->tx_phase_v[ipulse + 1] - batch_i->tx_phase_v[ipulse];
          tbuf_[ipulse] = std::polar(std::abs(tbuf_[ipulse]), std::arg(tbuf_[ipulse]) + phasecor);
        }

        // apply clutter filter
        if (cx_filter_)
        {
          fftwf_execute(plan_fwd_);
          for (auto i = 0; i < fft_size; ++i)
            fbuf_[i] /= fft_size;
          for (auto i = 0; i < notch_clutter; ++i)
          {
            fbuf_[i].real(0.0f);
            fbuf_[i].imag(0.0f);
            fbuf_[fft_size - 1 - i].real(0.0f);
            fbuf_[fft_size - 1 - i].imag(0.0f);
            fftwf_execute(plan_rev_);
          }
        }

        // copy into trip 2 position
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          batch_o->iq_rx_v[batch_i->gate_count + igate, i].i = tbuf_[i].real();
          batch_o->iq_rx_v[batch_i->gate_count + igate, i].q = tbuf_[i].imag();
        }
      }
      else
      {
        // second trip is strongest

        // convert to frequency domain (cohered data already sitting in batch_o)
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          tbuf_[i].real(batch_o->iq_rx_v[igate, i].i);
          tbuf_[i].imag(batch_o->iq_rx_v[igate, i].q);
        }
        for (auto i = batch_o->pulse_count; i < fft_size; ++i)
        {
          tbuf_[i].real(0.0);
          tbuf_[i].imag(0.0);
        }
        fftwf_execute(plan_fwd_);
        for (auto i = 0; i < fft_size; ++i)
          fbuf_[i] /= fft_size;

        // apply a clutter filter
        if (cx_filter_)
        {
          for (auto i = 0; i < notch_clutter; ++i)
          {
            fbuf_[i].real(0.0f);
            fbuf_[i].imag(0.0f);
            fbuf_[fft_size - 1 - i].real(0.0f);
            fbuf_[fft_size - 1 - i].imag(0.0f);
          }
        }

        // convert back to time domain
        fftwf_execute(plan_rev_);

        // copy into second trip position
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          batch_o->iq_rx_v[batch_i->gate_count + igate, i].i = tbuf_[i].real();
          batch_o->iq_rx_v[batch_i->gate_count + igate, i].q = tbuf_[i].imag();
        }

        // notch out the the second trip data entirely
        for (auto i = 0; i < notch_trip; ++i)
        {
          fbuf_[i].real(0.0f);
          fbuf_[i].imag(0.0f);
          fbuf_[fft_size - 1 - i].real(0.0f);
          fbuf_[fft_size - 1 - i].imag(0.0f);
        }

        // convert back to time domain again
        fftwf_execute(plan_rev_);

        // phase correct back to first trip
        for (auto ipulse = 0; ipulse < batch_o->pulse_count; ++ipulse)
        {
          auto phasecor = batch_i->tx_phase_v[ipulse + 1] - batch_i->tx_phase_v[ipulse];
          tbuf_[ipulse] = std::polar(std::abs(tbuf_[ipulse]), std::arg(tbuf_[ipulse]) - phasecor);
        }

        // apply clutter filter
        if (cx_filter_)
        {
          for (auto i = batch_o->pulse_count; i < fft_size; ++i)
          {
            tbuf_[i].real(0.0);
            tbuf_[i].imag(0.0);
          }
          fftwf_execute(plan_fwd_);
          for (auto i = 0; i < fft_size; ++i)
            fbuf_[i] /= fft_size;
          for (auto i = 0; i < notch_clutter; ++i)
          {
            fbuf_[i].real(0.0f);
            fbuf_[i].imag(0.0f);
            fbuf_[fft_size - 1 - i].real(0.0f);
            fbuf_[fft_size - 1 - i].imag(0.0f);
            fftwf_execute(plan_rev_);
          }
        }

        // copy into first position
        for (auto i = 0; i < batch_o->pulse_count; ++i)
        {
          batch_o->iq_rx_v[igate, i].i = tbuf_[i].real();
          batch_o->iq_rx_v[igate, i].q = tbuf_[i].imag();
        }
      }
    }
  }

  outputs.emplace_back(0, std::move(batch_o));
}
