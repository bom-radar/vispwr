/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "data_types.h"
#include "json.h"

#include <QDialog>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QGraphicsObject>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMainWindow>
#include <QMenu>
#include <QTextEdit>
#include <QTreeWidget>
#include <QWidget>

namespace vispwr
{
  enum class log_level;
  class stage;
  class ui_scene;

  class ui_stage_picker : public QDialog
  {
  public:
    ui_stage_picker(QWidget* parent);

    auto selected_stage_type() const -> QString;

    auto current_item_changed(QTreeWidgetItem* item, QTreeWidgetItem* previous) -> void;
    auto item_double_clicked(QTreeWidgetItem* item, int column) -> void;

  private:
    QTreeWidget       tree_;
    QTextEdit         description_;
    QDialogButtonBox  buttons_;
  };

  class ui_stage : public QGraphicsObject
  {
    Q_OBJECT
  public:
    ui_stage(int id, shared_ptr<stage> s, QPointF position);
    ~ui_stage();

    auto id() const { return id_; }
    auto boundingRect() const -> QRectF override;

    auto get_stage() const { return stage_; }
    auto input_position(int i) const -> QPointF;
    auto output_position(int i) const -> QPointF;
    auto input_type(int i) const -> data_type;
    auto output_type(int i) const -> data_type;

    auto pick_log_button(QPointF pos) const -> bool;
    auto pick_input(QPointF pos) const -> optional<int>;
    auto pick_output(QPointF pos) const -> optional<int>;

  signals:
    auto parameter_updated(string_view name) -> void;
    auto log_output(log_level level, string message) -> void;

  protected:
    auto on_parameter_updated(string_view name) -> void;
    auto on_log_output(log_level level, string message) -> void;
    auto show_log_dialog() -> void;
    auto hoverEnterEvent(QGraphicsSceneHoverEvent* event) -> void override;
    auto hoverMoveEvent(QGraphicsSceneHoverEvent* event) -> void override;
    auto hoverLeaveEvent(QGraphicsSceneHoverEvent* event) -> void override;
    auto itemChange(QGraphicsItem::GraphicsItemChange change, QVariant const& value) -> QVariant override;
    auto mousePressEvent(QGraphicsSceneMouseEvent* event) -> void override;
    auto paint(QPainter* painter, QStyleOptionGraphicsItem const* option, QWidget* widget = nullptr) -> void override;

  private:
    struct log_message
    {
      system_clock::time_point  time;
      log_level                 level;
      string                    message;
    };

  private:
    auto scene() const -> ui_scene*;
    auto update_size() -> void;

  private:
    int                   id_;
    shared_ptr<stage>     stage_;
    vector<QWidget*>      param_widgets_;
    QGraphicsProxyWidget* proxy_;

    bool                  hovered_;
    QSize                 size_;

    vector<log_message>   log_messages_;
    size_t                log_skipped_;

    QFormLayout* layout_temp_;
  };

  class ui_connection : public QGraphicsObject
  {
  public:
    ui_connection(ui_stage* out_stage, int out_port, ui_stage* in_stage, int in_port, QPointF pos = {0.0f, 0.0f});
    ~ui_connection();

    auto stage_output() const { return stage_output_; }
    auto port_output() const { return port_output_; }
    auto pos_output() const { return pos_output_; }
    auto stage_input() const { return stage_input_; }
    auto port_input() const { return port_input_; }
    auto pos_input() const { return pos_input_; }

    auto update_endpoints() -> void;

    auto boundingRect() const -> QRectF override;
    auto shape() const -> QPainterPath override;

  protected:
    auto hoverEnterEvent(QGraphicsSceneHoverEvent* event) -> void override;
    auto hoverLeaveEvent(QGraphicsSceneHoverEvent* event) -> void override;
    auto mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void override;
    auto mouseReleaseEvent(QGraphicsSceneMouseEvent* event) -> void override;
    auto paint(QPainter* painter, QStyleOptionGraphicsItem const* option, QWidget* widget = nullptr) -> void override;

  private:
    auto scene() const -> ui_scene*;
    auto determine_control_points() const -> pair<QPointF, QPointF>;

  private:
    ui_stage*       stage_output_;
    int             port_output_;
    ui_stage*       stage_input_;
    int             port_input_;

    QPointF         pos_output_;
    QPointF         pos_input_;

    bool            hovered_;
  };

  class ui_scene : public QGraphicsScene
  {
  public:
    ui_scene(QObject* parent);

    auto save() const -> json;
    auto load(json const& j) -> void;

    auto add_stage(QString const& name, QPointF position) -> void;

    auto draft_connection() const { return draft_; }
    auto draw_connection_start(ui_stage* stage, bool output, int port, QPointF pos) -> void;
    auto draw_connection_move(QPointF pos) -> void;
    auto draw_connection_finish() -> void;

  private:
    int             next_id_;

    ui_connection*  draft_;
    set<ui_stage*>  draw_hinters_;
  };

  class ui_view : public QGraphicsView
  {
  public:
    ui_view(ui_scene* scene, QWidget* parent);

    auto add_stage(QPointF pos) -> void;

  protected:
    auto scene() const -> ui_scene*;

    auto contextMenuEvent(QContextMenuEvent* event) -> void override;
    auto drawBackground(QPainter* painter, QRectF const& rect) -> void override;
    auto mouseMoveEvent(QMouseEvent* event) -> void override;
    auto mousePressEvent(QMouseEvent* event) -> void override;
    auto wheelEvent(QWheelEvent* event) -> void override;

    auto delete_selected() -> void;

  private:
    ui_stage_picker   picker_;
    QAction           action_delete_;
    QPointF           click_pos_;
  };

  class ui_main_window : public QMainWindow
  {
  public:
    ui_main_window();

    auto save_file_dialog() -> void;
    auto load_file_dialog() -> void;
    auto add_stage() -> void;
    auto about() -> void;

    auto load_file(QString const& path) -> void;

  protected:
    auto closeEvent(QCloseEvent* event) -> void override;

  private:
    ui_scene  scene_;
    ui_view   view_;
  };
}
