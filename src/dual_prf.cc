/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"

using namespace vispwr;

namespace vispwr
{
  class dual_prf : public stage
  {
  protected:
    dual_prf();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<moments const> data, int input) -> void override;

  private:
    shared_ptr<moments const> m1_;
    uint64_t next_id_;
  };
}

static constexpr auto plist = std::array<string_view, 0>{};
static constexpr auto ilist = std::array<input_meta, 1>
{{
  { "moments", data_type::moments, true, "Input moment data" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
  { "moments", data_type::moments, "Output moment data" }
}};

static const auto desc = R"(
Simple Dual-PRF processing for Doppler Velocity.

**Input:** Moments

**Output:** Moments
)";

static auto ireg = stage::enrol<dual_prf>("dual_prf", "Dual PRF", "Moments", desc);

dual_prf::dual_prf()
  : stage{plist, ilist, olist}
  , next_id_{0}
{ }

auto dual_prf::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto dual_prf::get_parameter_impl(string_view name) const -> parameter
{
  return {};
}

auto dual_prf::set_parameter_impl(string_view name, parameter_value value) -> void
{ }

auto dual_prf::process(shared_ptr<moments const> data, int input) -> void
{
  auto& m0 = data;

  // reset our identifier sequence if upstream has
  if (m0->id == 0)
  {
    m1_.reset();
    next_id_ = 0;
  }

  // if we don't have a previous ray then we need to wait
  // TODO - deal with multi trips (store trip no in moments, keep separate list per trip)
  if (!m1_)
  {
    m1_ = std::move(m0);
    return;
  }

  // sanity check we get alternating PRFs
  if (std::abs(m0->nyquist - m1_->nyquist) < 0.1)
  {
    //printf("same PRF!\n");
    m1_ = std::move(m0);
    return;
  }

  // is our current pulse the high or low PRF?
  auto high = m0->nyquist > m1_->nyquist;

  // determine the combined nyquist
  auto combined_nyquist = high
    ? (m0->nyquist / ((m0->nyquist / m1_->nyquist) - 1))
    : (m1_->nyquist / ((m1_->nyquist / m0->nyquist) - 1));

  #if 0
  printf(
        "azi %f - %f = %f  prf %f %f -> %f\n"
      , m0->azimuth_first
      , m0->azimuth_last
      , m0->azimuth_last - m0->azimuth_first
      , m1_->nyquist
      , m0->nyquist
      , combined_nyquist);
  #endif

  auto m = make_shared<moments>();

  // since we may output less moments than we receive, we must run our own id sequence
  m->id = next_id_++;
  m->autocors_id = m0->autocors_id;   // TODO - we kind of now depend on two autocors??
  m->batch_id = m0->batch_id;         // TODO - we kind of now depend on two batches??
  m->elevation_first = m0->elevation_first;
  m->elevation_last = m0->elevation_last;
  m->azimuth_first = m0->azimuth_first;
  m->azimuth_last = m0->azimuth_last;
  m->range_start = m0->range_start;
  m->range_step = m0->range_step;
  m->gate_count = m0->gate_count;
  m->nyquist = combined_nyquist;
  m->moments = m0->moments;

  // hack - just copy all of the moments initially, then we overwrite VRADx and WRADx
  m->data = m0->data;

  // TODO - determine correct moment indexes from incoming data!!!

  for (auto igate = 0uz; igate < std::min(m0->gate_count, m1_->gate_count); ++igate)
  {
    // determine the normalized delta v
    auto dv = high
      ? ((m1_->data[igate, 1] / m1_->nyquist) - (m0->data[igate, 1] / m0->nyquist))
      : ((m0->data[igate, 1] / m0->nyquist) - (m1_->data[igate, 1] / m1_->nyquist));

    if (dv < -1.0f)
      dv += 2.0f;
    else if (dv > 1.0f)
      dv -= 2.0f;

    dv *= combined_nyquist;

    #if 1
    auto vmin = dv - m0->nyquist;
    auto vmax = dv + m0->nyquist;

    auto vel = m0->data[igate, 1];
    while (vel < vmin)
      vel += m0->nyquist;
    while (vel > vmax)
      vel -= m0->nyquist;

    m->data[igate, 1] = vel;
    #else
    m->data[igate, 1] = dv;
    #endif
  }
  for (auto igate = std::min(m0->gate_count, m1_->gate_count); igate < m->gate_count; ++igate)
  {
    m->data[igate, 1] = std::numeric_limits<float>::quiet_NaN();
  }

  send(0, std::move(m));
  m1_ = std::move(m0);
}
