/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "fft.h"
#include "format.h"
#include <fftw3.h>

using namespace vispwr;

/* FFTW planning is NOT thread safe, so all calls to the planning functions must be protected by this mutex.  On the
 * other hand, FFTW execution IS thread safe, so calls to the execution functions require no protection.  With the
 * obvious caveat that the target arrays are not used at once (either by using different plans from different threads
 * or by using the same plan from multiple with the "array new" style execute functions to redirect I/O. */
static mutex mut_fftw_planning;

struct plan_hnd
{
  fftwf_plan plan;

  plan_hnd(span<complex<float>> in, span<complex<float>> out, bool forward);
  plan_hnd(array2cf const& in, array2cf& out, bool forward);

  plan_hnd(plan_hnd&) noexcept = delete;
  plan_hnd(plan_hnd const&) = delete;
  auto operator=(plan_hnd&&) noexcept ->plan_hnd& = delete;
  auto operator=(plan_hnd const&) -> plan_hnd& = delete;

  ~plan_hnd();
};

// 1d complex transform
plan_hnd::plan_hnd(span<complex<float>> in, span<complex<float>> out, bool forward)
{
  auto lock = lock_guard<mutex>{mut_fftw_planning};
  plan = fftwf_plan_dft_1d(
        in.size()
      , reinterpret_cast<fftwf_complex*>(in.data())
      , reinterpret_cast<fftwf_complex*>(out.data())
      , forward ? FFTW_FORWARD : FFTW_BACKWARD
      , FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
}

// multiple 1d complex transforms (1 per row)
plan_hnd::plan_hnd(array2cf const& in, array2cf& out, bool forward)
{
  auto lock = lock_guard<mutex>{mut_fftw_planning};
  auto iy = int(in.shape()[0]), ix = int(in.shape()[1]), ox = int(out.shape()[1]);
  plan = fftwf_plan_many_dft(
        1, &ix, iy
      , reinterpret_cast<fftwf_complex*>(const_cast<complex<float>*>(in.data()))
      , &ix, 1, ix
      , reinterpret_cast<fftwf_complex*>(out.data())
      , &ox, 1, ox
      , forward ? FFTW_FORWARD : FFTW_BACKWARD
      , FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
}

plan_hnd::~plan_hnd()
{
  auto lock = lock_guard<mutex>{mut_fftw_planning};
  fftwf_destroy_plan(plan);
}

auto vispwr::fft_fwd(span<complex<float>> in, span<complex<float>> out) -> void
{
  thread_local map<size_t, plan_hnd> plans_;
  auto& plan = plans_.try_emplace(in.size(), in, out, true).first->second;
  fftwf_execute_dft(plan.plan, reinterpret_cast<fftwf_complex*>(in.data()), reinterpret_cast<fftwf_complex*>(out.data()));
}

auto vispwr::fft_rev(span<complex<float>> in, span<complex<float>> out) -> void
{
  thread_local map<size_t, plan_hnd> plans_;
  auto& plan = plans_.try_emplace(in.size(), in, out, false).first->second;
  fftwf_execute_dft(plan.plan, reinterpret_cast<fftwf_complex*>(in.data()), reinterpret_cast<fftwf_complex*>(out.data()));
}

auto vispwr::fft_fwd(array2cf const& in, array2cf& out) -> void
{
  thread_local map<array2cf::shape_type, plan_hnd> plans_;
  auto& plan = plans_.try_emplace(in.shape(), in, out, true).first->second;
  fftwf_execute_dft(
        plan.plan
      , reinterpret_cast<fftwf_complex*>(const_cast<complex<float>*>(in.data()))
      , reinterpret_cast<fftwf_complex*>(out.data()));
}

auto vispwr::fft_rev(array2cf const& in, array2cf& out) -> void
{
  thread_local map<array2cf::shape_type, plan_hnd> plans_;
  auto& plan = plans_.try_emplace(in.shape(), in, out, false).first->second;
  fftwf_execute_dft(
        plan.plan
      , reinterpret_cast<fftwf_complex*>(const_cast<complex<float>*>(in.data()))
      , reinterpret_cast<fftwf_complex*>(out.data()));
}
