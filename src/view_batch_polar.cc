/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "calibration.h"
#include "pick.h"

#include <QLineSeries>
#include <QPolarChart>
#include <QChartView>
#include <QValueAxis>
#include <QToolBar>
#include <QComboBox>
#include <QHBoxLayout>
#include <QIcon>
#include <QFileDialog>
#include <QPushButton>
#include <QScatterSeries>
#include <QVBoxLayout>

using namespace vispwr;

namespace vispwr
{
  class batch_polar_window : public QWidget
  {
    Q_OBJECT

    enum class power_units { dbm, kw, adu };

  public:
    batch_polar_window();

    // called by stage to collect a pulse for our next plot
    void collect_pulse(shared_ptr<pulse const> p);

  signals:
    void window_hidden();
    void set_data(shared_ptr<batch const> b, shared_ptr<calibration const> c);

  protected:
    auto hideEvent(QHideEvent* event) -> void override;

  private:
    auto on_change_units(QString const& text) -> void;
    auto on_set_data(shared_ptr<batch const> b, shared_ptr<calibration const> c) -> void;
    auto on_save_image() -> void;
    auto redraw_plot() -> void;

  private:
    QComboBox*    combo_units_;
    power_units   units_;

    QValueAxis*   power_axis_;
    QValueAxis*   phase_axis_;
    QPolarChart*  chart_;
    QChartView*   view_;

    // current data to plot
    shared_ptr<batch const> batch_;
    shared_ptr<calibration const> calib_;
  };

  class view_batch_polar : public stage
  {
  protected:
    view_batch_polar();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<batch const> data, int input) -> void override;

  private:
    int                 picker_;
    int                 calib_channel_;
    batch_polar_window  window_;
    QMetaObject::Connection con_pick_updated_;  // handle to signal/slot connection for current picker
  };
}

static constexpr auto plist = std::array<string_view, 4>{ "Picker", "Calib", "Display", "geometry" };
static constexpr auto ilist = std::array<input_meta, 1>
{{
    { "batch", data_type::batch, true, "Input batches" }
}};
static constexpr auto olist = std::array<output_meta, 0>
{{
}};

static const auto desc = R"(
Plot the transmitted power and phase of each pulse in the batch nearest to a selected location.  Pulses are plotted
in polar coordinates with an angle correponding to the phase, and magnitude corresponding to the power.

**Input:** Batches

**Output:** None
)";

static auto ireg = stage::enrol<view_batch_polar>("view_batch_polar", "Batch TX (Polar)", "Display", desc);

view_batch_polar::view_batch_polar()
  : stage{plist, ilist, olist}
  , picker_{0}
  , calib_channel_{0}
{
  QObject::connect(&window_, &batch_polar_window::window_hidden, [this]() { notify_parameter_update("Display"); });
  con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
}

auto view_batch_polar::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto view_batch_polar::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "Picker")
    return parameter_integer
    {
      .name = "Picker",
      .description = "Input pick channel for batch selection",
      .min = 0,
      .max = pick::channels - 1,
      .value = picker_,
    };
  if (name == "Calib")
    return parameter_integer
    {
      .name = "Calib",
      .description = "Calibration channel",
      .min = 0,
      .max = 10,
      .value = calib_channel_,
    };
  if (name == "Display")
    return parameter_trigger
    {
      .name = "Display",
      .description = "Open or close display window",
      .label = window_.isVisible() ? "Hide" : "Show",
    };
  if (name == "geometry")
    return parameter_string
    {
      .name = "geometry",
      .description = "Window geometry",
      .visible = false,
      .value = fmt::format("{} {} {} {}", window_.x(), window_.y(), window_.width(), window_.height()),
    };
  return {};
}

auto view_batch_polar::set_parameter_impl(string_view name, parameter_value value) -> void
{
  if (name == "Picker")
  {
    picker_ = std::get<int>(value);
    if (con_pick_updated_)
      QObject::disconnect(con_pick_updated_);
    con_pick_updated_ = QObject::connect(&pick::channel(picker_), &pick::updated, [this]{ request_resend(); });
  }
  else if (name == "Calib")
    calib_channel_ = std::get<int>(value);
  else if (name == "Display")
  {
    if (std::get<bool>(value) == true)
    {
      window_.setVisible(!window_.isVisible());
      notify_parameter_update("Display");
    }
  }
  else if (name == "geometry")
  {
    int x, y, w, h;
    if (sscanf(std::get<string>(value).c_str(), "%d %d %d %d", &x, &y, &w, &h) != 4)
      throw std::runtime_error{"Bad window geometry"};
    window_.setGeometry(x, y, w, h);
  }
}

auto view_batch_polar::process(shared_ptr<batch const> data, int input) -> void
{
  // get the calibration from our selected channel
  auto calib = calibration_get(calib_channel_);
  if (!calib)
    return;

  // get the currently picked coordiantes from our selected channel
  auto pick_data = pick::channel(picker_).get();
  if (!pick_data.valid)
    return;

  // account for ccw scanning
  auto azimuth_l = data->pulses[0].azimuth;
  auto azimuth_h = data->pulses[data->pulse_count-1].azimuth;
  if (azimuth_l > azimuth_h)
    std::swap(azimuth_l, azimuth_h);

  // account for 360..0 wrap around
  if (azimuth_l < 10.0f && azimuth_h > 350.0f)
    std::swap(azimuth_l, azimuth_h);

  // does this batch overlap with the picked arc?
  if (pick_data.azimuth < azimuth_l || pick_data.azimuth > azimuth_h)
    return;

  // emit the data updated event (will be transferred to the gui thread)
  window_.set_data(std::move(data), std::move(calib));
}

batch_polar_window::batch_polar_window()
  : combo_units_{new QComboBox()}
  , units_{power_units::dbm}
  , power_axis_{new QValueAxis()}
  , phase_axis_{new QValueAxis()}
  , chart_{new QPolarChart()}
  , view_{new QChartView(chart_)}
{
  combo_units_->addItem("dBm");
  combo_units_->addItem("kW");
  combo_units_->addItem("adu");

  power_axis_->setTitleText("Power (dBm)");
  power_axis_->setTickCount(5);
  power_axis_->setMinorTickCount(2);

  //phase_axis_->setTitleText("Phase (\u00b0)");
  phase_axis_->setLabelFormat("%g");
  phase_axis_->setTickCount(9);
  phase_axis_->setRange(0.0, 360.0);

  chart_->addAxis(power_axis_, QPolarChart::PolarOrientationRadial);
  chart_->addAxis(phase_axis_, QPolarChart::PolarOrientationAngular);
  chart_->legend()->setAlignment(Qt::AlignRight);

  auto toolbar = new QWidget();
  auto layout_tb = new QHBoxLayout(toolbar);
  layout_tb->addWidget(combo_units_);
  auto but_save = new QPushButton(QIcon::fromTheme("document-save"), "");
  layout_tb->addWidget(but_save);

  auto layout = new QVBoxLayout{this};
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addWidget(toolbar);
  layout->addWidget(view_);

  resize(600, 600);
  setWindowTitle("View Batch");
  show();

  QObject::connect(combo_units_, &QComboBox::currentTextChanged, this, &batch_polar_window::on_change_units);
  QObject::connect(this, &batch_polar_window::set_data, this, &batch_polar_window::on_set_data);
  QObject::connect(but_save, &QPushButton::clicked, this, &batch_polar_window::on_save_image);
}

auto batch_polar_window::hideEvent(QHideEvent* event) -> void
{
  window_hidden();
}

auto batch_polar_window::on_change_units(QString const& text) -> void
{
  if (text == "dBm")
  {
    units_ = power_units::dbm;
    power_axis_->setTitleText("Power (dBm)");
  }
  else if (text == "kW")
  {
    units_ = power_units::kw;
    power_axis_->setTitleText("Power (kW)");
  }
  else
  {
    units_ = power_units::adu;
    power_axis_->setTitleText("Amplitude (adu)");
  }
  redraw_plot();
}

auto batch_polar_window::on_set_data(shared_ptr<batch const> b, shared_ptr<calibration const> c) -> void
{
  batch_ = std::move(b);
  calib_ = std::move(c);
  redraw_plot();
}

auto batch_polar_window::on_save_image() -> void
{
  auto path = QFileDialog::getSaveFileName(this, tr("Choose file"), QDir::homePath(), "PNG image files (*.png)");
  if (path.isEmpty())
    return;
  auto pixmap = view_->grab();
  pixmap.save(path, "PNG");
}

static auto increment_below(double val, double increment)
{
  return increment * std::floor(val / increment);
}

static auto increment_above(double val, double increment)
{
  return increment * std::ceil(val / increment);
}

auto batch_polar_window::redraw_plot() -> void
{
  chart_->removeAllSeries();

  if (!batch_ || !calib_)
    return;

  double min_power = std::numeric_limits<double>::max();
  double max_power = 0.0f;

  {
    auto series = new QScatterSeries();
    series->setName("Horizontal");
    for (auto i = 0uz; i < batch_->pulse_count; ++i)
    {
      if (std::isnan(batch_->pulses[i].tx_magnitude_h) || std::isnan(batch_->pulses[i].tx_phase_h))
        continue;
      auto val = double(batch_->pulses[i].tx_magnitude_h); // adu magnitude (not power)
      switch (units_)
      {
      case power_units::dbm:
        val = ((val * val) / calib_->tx_ref_power_adu_h) * calib_->tx_ref_power_kw_h; // adu power to kw
        if (val <= 0.0)
          continue;
        val = 10.0 * std::log10(1000.0 * val) + 30.0; // kw to dbm
        break;
      case power_units::kw:
        val = ((val * val) / calib_->tx_ref_power_adu_h) * calib_->tx_ref_power_kw_h; // adu power to kw
        break;
      case power_units::adu:
        break;
      }
      auto phase = batch_->pulses[i].tx_phase_h * (180.0 / pi);
      if (phase < 0.0)
        phase += 360.0;
      series->append(phase, val);
      min_power = std::min(min_power, val);
      max_power = std::max(max_power, val);
    }
    chart_->addSeries(series);
    series->attachAxis(phase_axis_);
    series->attachAxis(power_axis_);
  }
  {
    auto series = new QScatterSeries();
    series->setName("Vertical");
    for (auto i = 0uz; i < batch_->pulse_count; ++i)
    {
      if (std::isnan(batch_->pulses[i].tx_magnitude_v) || std::isnan(batch_->pulses[i].tx_phase_v))
        continue;
      auto val = double(batch_->pulses[i].tx_magnitude_v); // adu magnitude (not power)
      switch (units_)
      {
      case power_units::dbm:
        val = ((val * val) / calib_->tx_ref_power_adu_v) * calib_->tx_ref_power_kw_v; // adu power to kw
        if (val <= 0.0)
          continue;
        val = 10.0 * std::log10(1000.0 * val) + 30.0; // kw to dbm
        break;
      case power_units::kw:
        val = ((val * val) / calib_->tx_ref_power_adu_v) * calib_->tx_ref_power_kw_v; // adu power to kw
        break;
      case power_units::adu:
        break;
      }
      auto phase = batch_->pulses[i].tx_phase_v * (180.0 / pi);
      if (phase < 0.0)
        phase += 360.0;
      series->append(phase, val);
      min_power = std::min(min_power, val);
      max_power = std::max(max_power, val);
    }
    chart_->addSeries(series);
    series->attachAxis(phase_axis_);
    series->attachAxis(power_axis_);
  }

  auto dynamic_range = max_power - min_power;
  switch (units_)
  {
  case power_units::dbm:
    power_axis_->setRange(increment_below(min_power - dynamic_range, 0.1), increment_above(max_power, 0.1));
    power_axis_->setLabelFormat("%.2f");
    break;
  case power_units::kw:
    power_axis_->setRange(increment_below(min_power - dynamic_range, 1.0), increment_above(max_power, 1.0));
    power_axis_->setLabelFormat("%.1f");
    break;
  case power_units::adu:
    power_axis_->setRange(min_power - dynamic_range, max_power);
    power_axis_->setLabelFormat("%.0f");
    break;
  }
}

#include "view_batch_polar.moc"
