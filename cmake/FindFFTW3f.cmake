#----------------------------------------------------------------------------------------------------------------------
# Visual Interactive Signal Processor for Weather Radar
#
# Copyright 2020 Mark Curtis
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------

if (FFTW_ROOT)
  find_library(FFTW3f_LIBRARY
    NAMES "fftw3f" "fftw3f-3"
    PATHS ${FFTW_ROOT}
    PATH_SUFFIXES "lib64" "lib"
    NO_DEFAULT_PATH
  )
  find_path(FFTW3f_INCLUDE_DIR
    NAMES "fftw3.h"
    PATHS ${FFTW_ROOT}
    PATH_SUFFIXES "include"
    NO_DEFAULT_PATH
  )
else()
  find_package(PkgConfig)
  if (PKG_CONFIG_FOUND)
    pkg_check_modules(PKG_FFTW3f QUIET fftw3f)
  endif()
  find_library(FFTW3f_LIBRARY
    NAMES "fftw3f" "fftw3f-3"
    PATHS ${PKG_FFTW3f_LIBRARY_DIRS}
  )
  find_path(FFTW3f_INCLUDE_DIR
    NAMES "fftw3.h"
    PATHS ${PKG_FFTW3f_INCLUDE_DIRS}
  )
  set(FFTW3f_VERSION ${PKG_FFTW3f_VERSION})
endif()

# on windows we need to locate the .dll as well as the .lib/.dll.a
if (FFTW3f_LIBRARY AND CMAKE_SYSTEM_NAME STREQUAL "Windows")
  # find dlltool
  if (NOT DEFINED DLLTOOL_EXECUTABLE)
    find_program(DLLTOOL_EXECUTABLE NAMES dlltool dlltool.exe)
    if (NOT DLLTOOL_EXECUTABLE)
      message(FATAL_ERROR "Unable to locate dlltool executable")
    endif()
  endif()

  # use dlltool to map from .lib/.a to .dll
  execute_process(
    COMMAND "${DLLTOOL_EXECUTABLE}" -I "${FFTW3f_LIBRARY}"
    OUTPUT_VARIABLE dll_name
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )

  # get the directory of the import library to use as a search path
  get_filename_component(import_dir "${FFTW3f_LIBRARY}" DIRECTORY)

  # locate the dll
  find_program(FFTW3f_DLL_PATH
    NAMES "${dll_name}"
    PATHS
      "${import_dir}"
      ENV PATH
  )
endif()

# standard processing
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FFTW3f
  REQUIRED_VARS
    FFTW3f_LIBRARY
    FFTW3f_INCLUDE_DIR
  VERSION_VAR FFTW3f_VERSION
  FAIL_MESSAGE "Failed to find FFTW library.  Try setting FFTW_ROOT."
)

if (FFTW3f_FOUND)
  set(FFTW3f_LIBRARIES ${FFTW3f_LIBRARY})
  set(FFTW3f_INCLUDE_DIRS ${FFTW3f_INCLUDE_DIR})
  set(FFTW3f_DEFINITIONS ${PKG_FFTW3f_CFLAGS_OTHER})

  # make an imported library
  if (FFTW3f_FOUND AND NOT TARGET FFTW3f::FFTW3f)
    add_library(FFTW3f::FFTW3f SHARED IMPORTED)
    set_target_properties(FFTW3f::FFTW3f PROPERTIES
      INTERFACE_COMPILE_OPTIONS "${PKG_FFTW3f_CFLAGS_OHTER}"
      INTERFACE_INCLUDE_DIRECTORIES "${FFTW3f_INCLUDE_DIR}"
    )
    if (CMAKE_SYSTEM_NAME STREQUAL "Windows")
      set_target_properties(FFTW3f::FFTW3f PROPERTIES
        IMPORTED_LOCATION "${FFTW3f_DLL_PATH}"
        IMPORTED_IMPLIB "${FFTW3f_LIBRARY}"
      )
    else()
      set_target_properties(FFTW3f::FFTW3f PROPERTIES
        IMPORTED_LOCATION "${FFTW3f_LIBRARY}"
      )
    endif()
  endif()
endif()

mark_as_advanced(
  FFTW3f_INCLUDE_DIR
  FFTW3f_LIBRARY
)
