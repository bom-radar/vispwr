/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "format.h"
#include <concepts>
#include <map>
#include <string>
#include <utility>
#include <variant>
#include <vector>

namespace vispwr
{
  class json
  {
  public:
    enum class node_type
    {
        null
      , boolean
      , number
      , string
      , array
      , object
    };
    using array_type = std::vector<json>;
    using object_type = std::map<std::string, json, std::less<>>;
    using value_type = std::variant<std::monostate, bool, double, std::string, array_type, object_type>;

    class error : public std::exception
    {
    public:
      auto what() const noexcept -> char const* override;
    protected:
      virtual auto format_what() const -> std::string = 0;
    private:
      mutable std::string what_;
    };

    class parse_error : public error
    {
    public:
      parse_error(std::string_view buf, size_t pos, std::string cause);
      auto unwind_add_key(std::string key) { path_.push_back(std::move(key)); }
    protected:
      auto format_what() const -> std::string override;
    private:
      std::pair<size_t, size_t> where_;
      std::string               cause_;
      std::vector<std::string>  path_;
    };

    class api_error : public error
    {
    public:
      api_error(json const* where);
      auto set_root(json const& root) -> bool;
    protected:
      auto& path() const { return path_; }
    private:
      json const* where_;
      std::string path_;
    };

    class key_error : public api_error
    {
    public:
      key_error(json const* where, std::string_view key);
    protected:
      auto format_what() const -> std::string override;
    private:
      std::string key_;
    };

    class index_error : public api_error
    {
    public:
      index_error(json const* where, size_t index);
    protected:
      auto format_what() const -> std::string override;
    private:
      size_t index_;
    };

    class type_error : public api_error
    {
    public:
      type_error(json const* where, node_type expected);
    protected:
      auto format_what() const -> std::string override;
    private:
      node_type expected_;
    };

  public:
    static auto parse(std::string_view buf) -> json;
    static auto read(std::istream& is) -> json;
    static auto read(std::istream&& is) -> json { return read(is); }

  public:
    json() { }
    explicit json(bool val) : value_{val} { }
    explicit json(std::integral auto val) : value_{static_cast<double>(val)} { }
    explicit json(std::floating_point auto val) : value_{static_cast<double>(val)} { }
    explicit json(char const* val) : value_{std::string{val}} { }
    explicit json(std::string val) : value_{std::move(val)} { }
    explicit json(std::string_view val) : value_{std::string(val)} { }
    explicit json(array_type val) : value_{std::move(val)} { }
    explicit json(object_type val) : value_{std::move(val)} { }

    json(std::string_view buf, size_t& pos);

    auto write(std::ostream& out, int indent = 2) const -> void;

    /// Get the type of this node
    auto type() const { return static_cast<node_type>(value_.index()); }

    auto boolean() const -> bool
    {
      if (!std::holds_alternative<bool>(value_))
        throw type_error{this, node_type::boolean};
      return std::get<bool>(value_);
    }

    template <typename T>
    auto number() const -> T requires std::integral<T> || std::floating_point<T>
    {
      // TODO - for integral types we may want to check the user acutally supplied an integer
      if (!std::holds_alternative<double>(value_))
        throw type_error{this, node_type::number};
      return std::get<double>(value_);
    }

    auto string() const -> std::string const&
    {
      if (!std::holds_alternative<std::string>(value_))
        throw type_error{this, node_type::string};
      return std::get<std::string>(value_);
    }

    auto string() -> std::string&
    {
      if (!std::holds_alternative<std::string>(value_))
        throw type_error{this, node_type::string};
      return std::get<std::string>(value_);
    }

    auto array() const -> array_type const&
    {
      if (!std::holds_alternative<array_type>(value_))
        throw type_error{this, node_type::array};
      return std::get<array_type>(value_);
    }

    auto array() -> array_type&
    {
      if (!std::holds_alternative<array_type>(value_))
        throw type_error{this, node_type::array};
      return std::get<array_type>(value_);
    }

    auto object() const -> object_type const&
    {
      if (!std::holds_alternative<object_type>(value_))
        throw type_error{this, node_type::object};
      return std::get<object_type>(value_);
    }

    auto object() -> object_type&
    {
      if (!std::holds_alternative<object_type>(value_))
        throw type_error{this, node_type::object};
      return std::get<object_type>(value_);
    }

    /// Access as a boolean
    explicit operator bool() const
    {
      if (!std::holds_alternative<bool>(value_))
        throw type_error{this, node_type::boolean};
      return std::get<bool>(value_);
    }

    /// Access as a number
    template <typename T>
    explicit operator T() const requires std::integral<T> || std::floating_point<T>
    {
      if (!std::holds_alternative<double>(value_))
        throw type_error{this, node_type::number};
      return std::get<double>(value_);
    }

    /// Access as a string
    explicit operator std::string const&() const
    {
      if (!std::holds_alternative<std::string>(value_))
        throw type_error{this, node_type::string};
      return std::get<std::string>(value_);
    }

    auto operator[](size_t i) const -> json const&
    {
      if (!std::holds_alternative<array_type>(value_))
        throw type_error{this, node_type::array};
      auto& val = std::get<array_type>(value_);
      if (i >= val.size())
        throw index_error{this, i};
      return val[i];
    }

    auto operator[](size_t i) -> json&
    {
      if (!std::holds_alternative<array_type>(value_))
        throw type_error{this, node_type::array};
      auto& val = std::get<array_type>(value_);
      if (i >= val.size())
        throw index_error{this, i};
      return val[i];
    }

    auto operator[](std::string_view key) const -> json const&
    {
      if (!std::holds_alternative<object_type>(value_))
        throw type_error{this, node_type::object};
      auto& val = std::get<object_type>(value_);
      auto i = val.find(key);
      if (i == val.end())
        throw key_error{this, key};
      return i->second;
    }

    auto operator[](std::string_view key) -> json&
    {
      if (!std::holds_alternative<object_type>(value_))
        throw type_error{this, node_type::object};
      auto& val = std::get<object_type>(value_);
      auto i = val.find(key);
      if (i == val.end())
        throw key_error{this, key};
      return i->second;
    }

    auto optional(std::string_view key, std::string_view def) const -> std::string_view
    {
      if (!std::holds_alternative<object_type>(value_))
        throw type_error{this, node_type::object};
      auto& val = std::get<object_type>(value_);
      auto i = val.find(key);
      if (i == val.end())
        return def;
      return i->second.string();
    }

  private:
    value_type value_;
  };
}
