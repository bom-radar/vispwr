# Icon production

The application icons here were created using guidance from th GNOME Human Interface Guidelines document which is
available at https://teams.pages.gitlab.gnome.org/Design/hig-www/guidelines/app-icons.html.

The process used for creation is as follows:
1. Use the App Icon Preview software to create a new (blank) icon.  This will generate a template SVG file that
   contains some sample icons as well as a special blank template area where the new icon should be drawn.  This
   file becomes the 'source' document for the icon and should be named with the '.Source' suffix.
2. Use Inkscape to modify the source document.  The visible icon elements should be added to the 'icons' layer
   in the template.  Simply add graphical elemetns to the blank 'Hicolor' template space available.  You can
   also add a mono-color 'Symbolic' icon if desired.  It is safe to delete the sample icons to save space and
   reduce visual noise.
3. View the source icon file in App Icon Preview software.  This will show the icon scaled to different sizes.
4. Once happy, click the "Export" button in App Icon Preview.  Save the final icon to this directory without the
   '.Source' suffix.  This exported version will have the template guides and examples stripped out and be optimised
   for size.  This is the icon file that should actually be used in the software.
5. Manually open the SVG in Gimp and save as an .ico file
