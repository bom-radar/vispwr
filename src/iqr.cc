/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "iqr.h"

#include <iostream>
#include <QtEndian>

using namespace vispwr::iqr;

template <typename T>
class big_endian
{
  T val;
public:
  auto& operator=(T v) { val = QBigEndianStorageType<T>::toSpecial(v); return *this; }
  operator T() const { return QBigEndianStorageType<T>::fromSpecial(val); }
};
using big_int16_t = big_endian<int16_t>;
using big_int32_t = big_endian<int32_t>;
using big_int64_t = big_endian<int64_t>;
using big_uint16_t = big_endian<uint16_t>;
using big_uint32_t = big_endian<uint32_t>;
using big_uint64_t = big_endian<uint64_t>;
using big_float32_t = big_endian<float>;
using big_float64_t = big_endian<double>;

#pragma pack(push, 1)
struct archive_item_header
{
  big_int32_t   data_type;
  big_int64_t   data_length;
};
static_assert(sizeof(archive_item_header) == 3 * 4);

struct timestamp_hdr
{
  big_int64_t   time_ms_epoch;
  big_int64_t   time_ns;
};
static_assert(sizeof(timestamp_hdr) == 16);

enum class gdrx5_data_type : uint32_t
{
    iq16_tx_h  = 6
  , iq16_tx_v  = 7
  , iq16_tx_hv = 8
  , iq32_rx_h  = 14
  , iq32_rx_v  = 15
  , iq32_rx_hv = 16
};

struct gdrx_base_header
{
  big_uint32_t  magic;          // must equal 0xdd0ddd0d
  big_uint32_t  data_type;
  big_uint32_t  header_rev;
  big_uint32_t  header_rev_ext;
  big_uint32_t  type_length;    // in words not bytes
  big_uint32_t  data_length;    // in words not bytes
};
static_assert(sizeof(gdrx_base_header) == 6 * 4);

struct gdrx_base_footer
{
  big_uint32_t  magic;          // must equal 0xdd0ddd0d
};
static_assert(sizeof(gdrx_base_footer) == 4);

struct iq16_tx_h_hdr
{
  big_uint32_t    tracking_number;
  big_uint32_t    pulse_counter;
  big_uint16_t    elevation_angle;
  big_uint16_t    azimuth_angle;
  big_float32_t   sample_start;
  big_float32_t   sample_time;
  big_float32_t   sample_stop;
  big_uint32_t    sample_count;
  big_float32_t   prf;
  big_float32_t   tx_average_frequency_h;
  big_float32_t   tx_pulse_frequency_h;
  big_float32_t   tx_pulse_magnitude_h;
  big_float32_t   tx_pulse_phase_h;
  big_float32_t   dds_frequency_h;
  big_uint32_t    tx_data_flags;
  big_uint32_t    staggering_flags;
  big_uint32_t    timestamp_sec;
  big_uint32_t    timestamp_nsec;
  big_uint32_t    timestamp_status;
};
static_assert(sizeof(iq16_tx_h_hdr) == 18 * 4);

struct iq16_tx_v_hdr
{
  big_uint32_t    tracking_number;
  big_uint32_t    pulse_counter;
  big_uint16_t    elevation_angle;
  big_uint16_t    azimuth_angle;
  big_float32_t   sample_start;
  big_float32_t   sample_time;
  big_float32_t   sample_stop;
  big_uint32_t    sample_count;
  big_float32_t   prf;
  big_float32_t   tx_average_frequency_v;
  big_float32_t   tx_pulse_frequency_v;
  big_float32_t   tx_pulse_magnitude_v;
  big_float32_t   tx_pulse_phase_v;
  big_float32_t   dds_frequency_v;
  big_uint32_t    tx_data_flags;
  big_uint32_t    staggering_flags;
  big_uint32_t    timestamp_sec;
  big_uint32_t    timestamp_nsec;
  big_uint32_t    timestamp_status;
};
static_assert(sizeof(iq16_tx_v_hdr) == 18 * 4);

struct iq16_tx_hv_hdr
{
  big_uint32_t    tracking_number;
  big_uint32_t    pulse_counter;
  big_uint16_t    elevation_angle;
  big_uint16_t    azimuth_angle;
  big_float32_t   sample_start;
  big_float32_t   sample_time;
  big_float32_t   sample_stop;
  big_uint32_t    sample_count;
  big_float32_t   prf;
  big_float32_t   tx_average_frequency_h;
  big_float32_t   tx_average_frequency_v;
  big_float32_t   tx_pulse_frequency_h;
  big_float32_t   tx_pulse_frequency_v;
  big_float32_t   tx_pulse_magnitude_h;
  big_float32_t   tx_pulse_magnitude_v;
  big_float32_t   tx_pulse_phase_h;
  big_float32_t   tx_pulse_phase_v;
  big_float32_t   dds_frequency_h;
  big_float32_t   dds_frequency_v;
  big_uint32_t    tx_data_flags;
  big_uint32_t    staggering_flags;
  big_uint32_t    timestamp_sec;
  big_uint32_t    timestamp_nsec;
  big_uint32_t    timestamp_status;
};
static_assert(sizeof(iq16_tx_hv_hdr) == 23 * 4);

struct iq32_rx_h_hdr
{
  big_uint32_t    tracking_number;
  big_uint32_t    pulse_counter;
  big_uint16_t    elevation_angle;
  big_uint16_t    azimuth_angle;
  big_float32_t   range_start;
  big_float32_t   range_step;
  big_float32_t   range_stop;
  big_uint32_t    gate_count;
  big_float32_t   prf;
  big_float32_t   tx_average_frequency_h;
  big_float32_t   tx_pulse_frequency_h;
  big_float32_t   tx_pulse_magnitude_h;
  big_float32_t   tx_pulse_phase_h;
  big_float32_t   dds_frequency_h;
  big_uint32_t    rx_data_flags;
  big_uint32_t    staggering_flags;
  big_uint32_t    timestamp_sec;
  big_uint32_t    timestamp_nsec;
  big_uint32_t    timestamp_status;
  big_uint32_t    reserved_0;
  big_uint32_t    matched_filter_mode;
  big_float32_t   matched_filter_losses_h;
  big_float32_t   equivalent_noise_bandwidth_h;
};
static_assert(sizeof(iq32_rx_h_hdr) == 22 * 4);

struct iq32_rx_v_hdr
{
  big_uint32_t    tracking_number;
  big_uint32_t    pulse_counter;
  big_uint16_t    elevation_angle;
  big_uint16_t    azimuth_angle;
  big_float32_t   range_start;
  big_float32_t   range_step;
  big_float32_t   range_stop;
  big_uint32_t    gate_count;
  big_float32_t   prf;
  big_float32_t   tx_average_frequency_v;
  big_float32_t   tx_pulse_frequency_v;
  big_float32_t   tx_pulse_magnitude_v;
  big_float32_t   tx_pulse_phase_v;
  big_float32_t   dds_frequency_v;
  big_uint32_t    rx_data_flags;
  big_uint32_t    staggering_flags;
  big_uint32_t    timestamp_sec;
  big_uint32_t    timestamp_nsec;
  big_uint32_t    timestamp_status;
  big_uint32_t    reserved_0;
  big_uint32_t    matched_filter_mode;
  big_float32_t   matched_filter_losses_v;
  big_float32_t   equivalent_noise_bandwidth_v;
};
static_assert(sizeof(iq32_rx_v_hdr) == 22 * 4);

struct iq32_rx_hv_hdr
{
  big_uint32_t    tracking_number;
  big_uint32_t    pulse_counter;
  big_uint16_t    elevation_angle;
  big_uint16_t    azimuth_angle;
  big_float32_t   range_start;
  big_float32_t   range_step;
  big_float32_t   range_stop;
  big_uint32_t    gate_count;
  big_float32_t   prf;
  big_float32_t   tx_average_frequency_h;
  big_float32_t   tx_average_frequency_v;
  big_float32_t   tx_pulse_frequency_h;
  big_float32_t   tx_pulse_frequency_v;
  big_float32_t   tx_pulse_magnitude_h;
  big_float32_t   tx_pulse_magnitude_v;
  big_float32_t   tx_pulse_phase_h;
  big_float32_t   tx_pulse_phase_v;
  big_float32_t   dds_frequency_h;
  big_float32_t   dds_frequency_v;
  big_uint32_t    rx_data_flags;
  big_uint32_t    staggering_flags;
  big_uint32_t    timestamp_sec;
  big_uint32_t    timestamp_nsec;
  big_uint32_t    timestamp_status;
  big_uint32_t    reserved_0;
  big_uint32_t    matched_filter_mode;
  big_float32_t   matched_filter_losses_h;
  big_float32_t   matched_filter_losses_v;
  big_float32_t   equivalent_noise_bandwidth_h;
  big_float32_t   equivalent_noise_bandwidth_v;
};
static_assert(sizeof(iq32_rx_hv_hdr) == 29 * 4);

struct tx_iq
{
  big_int16_t     i;
  big_int16_t     q;
};
static_assert(sizeof(tx_iq) == 2 * 2);

struct tx_iq_hv
{
  tx_iq           h;
  tx_iq           v;
};
static_assert(sizeof(tx_iq_hv) == 2 * 4);

struct rx_iq
{
  big_float32_t   i;
  big_float32_t   q;
};
static_assert(sizeof(rx_iq) == 4 * 2);

struct rx_iq_hv
{
  rx_iq           h;
  rx_iq           v;
};
static_assert(sizeof(rx_iq_hv) == 4 * 4);

struct iq32_rx_h_data_block
{
  big_uint32_t    channel_mask;
  big_uint32_t    reserved_0;
  big_uint32_t    reserved_1;
  rx_iq           iq_h[32];
};
static_assert(sizeof(iq32_rx_h_data_block) == 67 * 4);

struct iq32_rx_v_data_block
{
  big_uint32_t    channel_mask;
  big_uint32_t    reserved_0;
  big_uint32_t    reserved_1;
  rx_iq           iq_v[32];
};
static_assert(sizeof(iq32_rx_v_data_block) == 67 * 4);

struct iq32_rx_hv_data_block
{
  big_uint32_t    channel_mask;
  big_uint32_t    reserved_0;
  big_uint32_t    reserved_1;
  rx_iq_hv        iq_hv[16];
};
static_assert(sizeof(iq32_rx_hv_data_block) == 67 * 4);
#pragma pack(pop)

file::file(std::filesystem::path path)
try
  : path_{path}
  , cur_type_{archive_data_type::none}
  , cur_length_{0}
{
  // open the file
  file_.exceptions(std::ifstream::badbit | std::ifstream::failbit);
  file_.open(path_, std::ios::in | std::ios::binary);

  // read the ascii headers
  auto line = std::string();
  std::getline(file_, line);
  if (file_.eof())
    throw std::runtime_error{"Unexpected end of file"};
  if (line != "[ARCHIVE_HEADER_START]")
    throw std::runtime_error{"Missing [ARCHIVE_HEADER_START] tag"};
  while (true)
  {
    std::getline(file_, line);
    if (file_.eof())
      throw std::runtime_error{"Unexpected end of file"};
    if (line == "[ARCHIVE_HEADER_END]")
      break;
    auto pos = line.find('=');
    if (pos == std::string::npos)
      throw std::runtime_error{"Invalid header, missing '='"};
    if (!headers_.emplace(line.substr(0, pos), line.substr(pos + 1)).second)
      throw std::runtime_error{"Duplicate header encountered"};
  }

  // move to start of data
  rewind();
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("Failed to read IQR file {}", path.string())});
}

auto file::rewind() -> void
{
  // seek to the start of the body data
  body_offset_ = header<size_t>("body.offset");
  file_.seekg(body_offset_);

  // preload the type of first item
  read_next_header();
}

auto file::ignore() -> void
{
  file_.seekg(cur_length_, std::ios::cur);
  read_next_header();
}

inline auto file::read_current_and_advance(archive_data_type expected) -> void
{
  if (cur_type_ != expected)
    throw std::runtime_error{fmt::format("Attempt to extract {} archive type as {}", cur_type_, archive_data_type::timestamp)};

  buffer_.resize(cur_length_);
  file_.read(buffer_.data(), cur_length_);
  read_next_header();
}

auto file::extract(timestamp& msg) -> void
{
  read_current_and_advance(archive_data_type::timestamp);

  if (buffer_.size() < sizeof(timestamp_hdr))
    throw std::runtime_error{"Archive item too small for timestamp"};

  auto& hdr = *reinterpret_cast<timestamp_hdr const*>(buffer_.data());
  msg.time_ms_epoch = hdr.time_ms_epoch;
  msg.time_ns = hdr.time_ns;
}

auto file::extract(gdrx5_tx& msg) -> void
{
  read_current_and_advance(archive_data_type::gdrx5_tx);

  // pointers to current read position and end of buffer
  auto cur = buffer_.data();
  auto end = buffer_.data() + buffer_.size();

  // read the gdrx base dataframe header
  if (cur + sizeof(gdrx_base_header) > end)
    throw std::runtime_error{"Archive item too small for GDRX-5 base header"};
  auto& hdr_base = *reinterpret_cast<gdrx_base_header const*>(cur);
  if (hdr_base.magic != 0xdd0ddd0d)
    throw std::runtime_error{"Missing magic token at start of GDRX-5 pulse data frame"};
  cur += sizeof(gdrx_base_header);

  auto type = static_cast<gdrx5_data_type>(uint32_t(hdr_base.data_type));
  if (type == gdrx5_data_type::iq16_tx_hv)
  {
    // read the type header
    if (cur + sizeof(iq16_tx_hv_hdr) > end)
      throw std::runtime_error{"Archive item too small for iq16_tx_hv type header"};
    auto& hdr = *reinterpret_cast<iq16_tx_hv_hdr const*>(cur);
    cur += hdr_base.type_length * 4;

    msg.tracking_number = hdr.tracking_number;
    msg.pulse_counter = hdr.pulse_counter;
    msg.elevation_angle = hdr.elevation_angle;
    msg.azimuth_angle = hdr.azimuth_angle;
    msg.sample_start = hdr.sample_start;
    msg.sample_time = hdr.sample_time;
    msg.sample_stop = hdr.sample_stop;
    msg.sample_count = hdr.sample_count;
    msg.prf = hdr.prf;
    msg.tx_average_frequency_h = hdr.tx_average_frequency_h;
    msg.tx_average_frequency_v = hdr.tx_average_frequency_v;
    msg.tx_pulse_frequency_h = hdr.tx_pulse_frequency_h;
    msg.tx_pulse_frequency_v = hdr.tx_pulse_frequency_v;
    msg.tx_pulse_magnitude_h = hdr.tx_pulse_magnitude_h;
    msg.tx_pulse_magnitude_v = hdr.tx_pulse_magnitude_v;
    msg.tx_pulse_phase_h = hdr.tx_pulse_phase_h;
    msg.tx_pulse_phase_v = hdr.tx_pulse_phase_v;
    msg.dds_frequency_h = hdr.dds_frequency_h;
    msg.dds_frequency_v = hdr.dds_frequency_v;
    msg.tx_data_flags = hdr.tx_data_flags;
    msg.staggering_flags = hdr.staggering_flags;
    msg.timestamp_sec = hdr.timestamp_sec;
    msg.timestamp_nsec = hdr.timestamp_nsec;
    msg.timestamp_status = hdr.timestamp_status;

    msg.iq_h.resize(msg.sample_count);
    msg.iq_v.resize(msg.sample_count);

    // read the sample data
    if (cur + msg.sample_count * sizeof(tx_iq_hv) > end)
      throw std::runtime_error{"Archive item too small for iq16_tx_hv data payload"};
    auto data = reinterpret_cast<tx_iq_hv const*>(cur);
    for (uint32_t i = 0; i < msg.sample_count; ++i)
    {
      msg.iq_h[i].real(data[i].h.i);
      msg.iq_h[i].imag(data[i].h.q);
      msg.iq_v[i].real(data[i].v.i);
      msg.iq_v[i].imag(data[i].v.q);
    }
    cur += msg.sample_count * sizeof(tx_iq_hv);
  }
  else if (type == gdrx5_data_type::iq16_tx_h)
  {
    // read the type header
    if (cur + sizeof(iq16_tx_h_hdr) > end)
      throw std::runtime_error{"Archive item too small for iq16_tx_h type header"};
    auto& hdr = *reinterpret_cast<iq16_tx_h_hdr const*>(cur);
    cur += hdr_base.type_length * 4;

    msg.tracking_number = hdr.tracking_number;
    msg.pulse_counter = hdr.pulse_counter;
    msg.elevation_angle = hdr.elevation_angle;
    msg.azimuth_angle = hdr.azimuth_angle;
    msg.sample_start = hdr.sample_start;
    msg.sample_time = hdr.sample_time;
    msg.sample_stop = hdr.sample_stop;
    msg.sample_count = hdr.sample_count;
    msg.prf = hdr.prf;
    msg.tx_average_frequency_h = hdr.tx_average_frequency_h;
    msg.tx_average_frequency_v = nan<float>;
    msg.tx_pulse_frequency_h = hdr.tx_pulse_frequency_h;
    msg.tx_pulse_frequency_v = nan<float>;
    msg.tx_pulse_magnitude_h = hdr.tx_pulse_magnitude_h;
    msg.tx_pulse_magnitude_v = nan<float>;
    msg.tx_pulse_phase_h = hdr.tx_pulse_phase_h;
    msg.tx_pulse_phase_v = nan<float>;
    msg.dds_frequency_h = hdr.dds_frequency_h;
    msg.dds_frequency_v = nan<float>;
    msg.tx_data_flags = hdr.tx_data_flags;
    msg.staggering_flags = hdr.staggering_flags;
    msg.timestamp_sec = hdr.timestamp_sec;
    msg.timestamp_nsec = hdr.timestamp_nsec;
    msg.timestamp_status = hdr.timestamp_status;

    msg.iq_h.resize(msg.sample_count);
    msg.iq_v = array1cf{};

    // read the sample data
    if (cur + msg.sample_count * sizeof(tx_iq) > end)
      throw std::runtime_error{"Archive item too small for iq16_tx_h data payload"};
    auto data = reinterpret_cast<tx_iq const*>(cur);
    for (uint32_t i = 0; i < msg.sample_count; ++i)
    {
      msg.iq_h[i].real(data[i].i);
      msg.iq_h[i].imag(data[i].q);
    }
    cur += msg.sample_count * sizeof(tx_iq);
  }
  else if (type == gdrx5_data_type::iq16_tx_v)
  {
    // read the type header
    if (cur + sizeof(iq16_tx_v_hdr) > end)
      throw std::runtime_error{"Archive item too small for iq16_tx_v type header"};
    auto& hdr = *reinterpret_cast<iq16_tx_v_hdr const*>(cur);
    cur += hdr_base.type_length * 4;

    msg.tracking_number = hdr.tracking_number;
    msg.pulse_counter = hdr.pulse_counter;
    msg.elevation_angle = hdr.elevation_angle;
    msg.azimuth_angle = hdr.azimuth_angle;
    msg.sample_start = hdr.sample_start;
    msg.sample_time = hdr.sample_time;
    msg.sample_stop = hdr.sample_stop;
    msg.sample_count = hdr.sample_count;
    msg.prf = hdr.prf;
    msg.tx_average_frequency_h = nan<float>;
    msg.tx_average_frequency_v = hdr.tx_average_frequency_v;
    msg.tx_pulse_frequency_h = nan<float>;
    msg.tx_pulse_frequency_v = hdr.tx_pulse_frequency_v;
    msg.tx_pulse_magnitude_h = nan<float>;
    msg.tx_pulse_magnitude_v = hdr.tx_pulse_magnitude_v;
    msg.tx_pulse_phase_h = nan<float>;
    msg.tx_pulse_phase_v = hdr.tx_pulse_phase_v;
    msg.dds_frequency_h = nan<float>;
    msg.dds_frequency_v = hdr.dds_frequency_v;
    msg.tx_data_flags = hdr.tx_data_flags;
    msg.staggering_flags = hdr.staggering_flags;
    msg.timestamp_sec = hdr.timestamp_sec;
    msg.timestamp_nsec = hdr.timestamp_nsec;
    msg.timestamp_status = hdr.timestamp_status;

    msg.iq_h = array1cf{};
    msg.iq_v.resize(msg.sample_count);

    // read the sample data
    if (cur + msg.sample_count * sizeof(tx_iq) > end)
      throw std::runtime_error{"Archive item too small for iq16_tx_v data payload"};
    auto data = reinterpret_cast<tx_iq const*>(cur);
    for (uint32_t i = 0; i < msg.sample_count; ++i)
    {
      msg.iq_v[i].real(data[i].i);
      msg.iq_v[i].imag(data[i].q);
    }
    cur += msg.sample_count * sizeof(tx_iq);
  }
  else
    throw std::runtime_error{"Unexpected gdrx5 data type encountered in gdrx5_tx archive data item"};

  // sanity check the magic token at the end of the data frame
  if (cur + sizeof(gdrx_base_footer) > end)
    throw std::runtime_error{"Archive item too small for GDRX-base footer"};
  if (reinterpret_cast<gdrx_base_footer const*>(cur)->magic != 0xcc0ccc0c)
    throw std::runtime_error{"Missing magic token at end of GDRX-5 pulse data frame"};
  cur += sizeof(gdrx_base_footer);
}

auto file::extract(gdrx5_iq& msg) -> void
{
  read_current_and_advance(archive_data_type::gdrx5_iq);

  // pointers to current read position and end of buffer
  auto cur = buffer_.data();
  auto end = buffer_.data() + buffer_.size();

  // read the gdrx base dataframe header
  if (cur + sizeof(gdrx_base_header) > end)
    throw std::runtime_error{"Archive item too small for GDRX-5 base header"};
  auto& hdr_base = *reinterpret_cast<gdrx_base_header const*>(cur);
  if (hdr_base.magic != 0xdd0ddd0d)
    throw std::runtime_error{"Missing magic token at start of GDRX-5 pulse data frame"};
  cur += sizeof(gdrx_base_header);

  auto type = static_cast<gdrx5_data_type>(uint32_t(hdr_base.data_type));
  if (type == gdrx5_data_type::iq32_rx_hv)
  {
    // read the type header
    if (cur + sizeof(iq32_rx_hv_hdr) > end)
      throw std::runtime_error{"Archive item too small for iq32_rx_hv type header"};
    auto& hdr = *reinterpret_cast<iq32_rx_hv_hdr const*>(cur);
    cur += hdr_base.type_length * 4;

    msg.tracking_number = hdr.tracking_number;
    msg.pulse_counter = hdr.pulse_counter;
    msg.elevation_angle = hdr.elevation_angle;
    msg.azimuth_angle = hdr.azimuth_angle;
    msg.range_start = hdr.range_start;
    msg.range_step = hdr.range_step;
    msg.range_stop = hdr.range_stop;
    msg.gate_count = hdr.gate_count;
    msg.prf = hdr.prf;
    msg.tx_average_frequency_h = hdr.tx_average_frequency_h;
    msg.tx_average_frequency_v = hdr.tx_average_frequency_v;
    msg.tx_pulse_frequency_h = hdr.tx_pulse_frequency_h;
    msg.tx_pulse_frequency_v = hdr.tx_pulse_frequency_v;
    msg.tx_pulse_magnitude_h = hdr.tx_pulse_magnitude_h;
    msg.tx_pulse_magnitude_v = hdr.tx_pulse_magnitude_v;
    msg.tx_pulse_phase_h = hdr.tx_pulse_phase_h;
    msg.tx_pulse_phase_v = hdr.tx_pulse_phase_v;
    msg.dds_frequency_h = hdr.dds_frequency_h;
    msg.dds_frequency_v = hdr.dds_frequency_v;
    msg.rx_data_flags = hdr.rx_data_flags;
    msg.staggering_flags = hdr.staggering_flags;
    msg.timestamp_sec = hdr.timestamp_sec;
    msg.timestamp_nsec = hdr.timestamp_nsec;
    msg.timestamp_status = hdr.timestamp_status;
    msg.reserved_0 = hdr.reserved_0;
    msg.matched_filter_mode = hdr.matched_filter_mode;
    msg.matched_filter_losses_h = hdr.matched_filter_losses_h;
    msg.matched_filter_losses_v = hdr.matched_filter_losses_v;
    msg.equivalent_noise_bandwidth_h = hdr.equivalent_noise_bandwidth_h;
    msg.equivalent_noise_bandwidth_v = hdr.equivalent_noise_bandwidth_v;

    msg.hilo_h.resize(msg.gate_count);
    msg.hilo_v.resize(msg.gate_count);
    msg.iq_h.resize(msg.gate_count);
    msg.iq_v.resize(msg.gate_count);

    // read the iq data
    auto full_blocks = msg.gate_count / 16;
    auto remainder = msg.gate_count % 16;
    #if 0 // TODO - partially filled blocks are NOT written as full blocks after all. need to put this fix in properly!
    auto total_blocks = full_blocks + (remainder > 0 ? 1 : 0);
    if (cur + total_blocks * sizeof(iq32_rx_hv_data_block) > end)
    {
      printf("gate count %zu    full %zu   remainder %zu    total %zu\n"
             , msg.gate_count, full_blocks, remainder, total_blocks);
      printf("end - cur = %zu    total * sizeeof = %zu\n"
             , end - cur
             , total_blocks * sizeof(iq32_rx_hv_data_block));
      printf("cur %zu total_bocks %zu sizeof %zu = %zu > end %zu\n",
             cur, total_blocks, sizeof(iq32_rx_hv_data_block),
             cur + total_blocks * sizeof(iq32_rx_hv_data_block),
             end);
      throw std::runtime_error{"Archive item too small for iq32_rx_hv data payload"};
    }
    #else
    auto total_size = full_blocks * sizeof(iq32_rx_hv_data_block);
    if (remainder > 0)
      total_size += (4 * 3) + remainder * sizeof(rx_iq_hv);
    if (cur + total_size > end)
      throw std::runtime_error{"Archive item too small for iq32_rx_hv data payload"};
    #endif
    auto blocks = reinterpret_cast<iq32_rx_hv_data_block const*>(cur);
    auto igate = 0;
    for (uint32_t iblock = 0; iblock < full_blocks; ++iblock)
    {
      for (uint32_t i = 0; i < 16; ++i)
      {
        auto channel_mask = uint32_t(blocks[iblock].channel_mask);
        msg.hilo_h[igate] = channel_mask & (1 << (i * 2));
        msg.hilo_v[igate] = channel_mask & (1 << (i * 2 + 1));
        msg.iq_h[igate].real(blocks[iblock].iq_hv[i].h.i);
        msg.iq_h[igate].imag(blocks[iblock].iq_hv[i].h.q);
        msg.iq_v[igate].real(blocks[iblock].iq_hv[i].v.i);
        msg.iq_v[igate].imag(blocks[iblock].iq_hv[i].v.q);
        ++igate;
      }
    }
    for (uint32_t i = 0; i < remainder; ++i)
    {
      auto channel_mask = uint32_t(blocks[full_blocks].channel_mask);
      msg.hilo_h[igate] = channel_mask & (1 << (i * 2));
      msg.hilo_v[igate] = channel_mask & (1 << (i * 2 + 1));
      msg.iq_h[igate].real(blocks[full_blocks].iq_hv[i].h.i);
      msg.iq_h[igate].imag(blocks[full_blocks].iq_hv[i].h.q);
      msg.iq_v[igate].real(blocks[full_blocks].iq_hv[i].v.i);
      msg.iq_v[igate].imag(blocks[full_blocks].iq_hv[i].v.q);
      ++igate;
    }
    #if 0
    cur += total_blocks * sizeof(iq32_rx_hv_data_block);
    #else
    cur += total_size;
    #endif
  }
  else if (type == gdrx5_data_type::iq32_rx_h)
  {
    // read the type header
    if (cur + sizeof(iq32_rx_h_hdr) > end)
      throw std::runtime_error{"Archive item too small for iq32_rx_h type header"};
    auto& hdr = *reinterpret_cast<iq32_rx_h_hdr const*>(cur);
    cur += hdr_base.type_length * 4;

    msg.tracking_number = hdr.tracking_number;
    msg.pulse_counter = hdr.pulse_counter;
    msg.elevation_angle = hdr.elevation_angle;
    msg.azimuth_angle = hdr.azimuth_angle;
    msg.range_start = hdr.range_start;
    msg.range_step = hdr.range_step;
    msg.range_stop = hdr.range_stop;
    msg.gate_count = hdr.gate_count;
    msg.prf = hdr.prf;
    msg.tx_average_frequency_h = hdr.tx_average_frequency_h;
    msg.tx_average_frequency_v = nan<float>;
    msg.tx_pulse_frequency_h = hdr.tx_pulse_frequency_h;
    msg.tx_pulse_frequency_v = nan<float>;
    msg.tx_pulse_magnitude_h = hdr.tx_pulse_magnitude_h;
    msg.tx_pulse_magnitude_v = nan<float>;
    msg.tx_pulse_phase_h = hdr.tx_pulse_phase_h;
    msg.tx_pulse_phase_v = nan<float>;
    msg.dds_frequency_h = hdr.dds_frequency_h;
    msg.dds_frequency_v = nan<float>;
    msg.rx_data_flags = hdr.rx_data_flags;
    msg.staggering_flags = hdr.staggering_flags;
    msg.timestamp_sec = hdr.timestamp_sec;
    msg.timestamp_nsec = hdr.timestamp_nsec;
    msg.timestamp_status = hdr.timestamp_status;
    msg.reserved_0 = hdr.reserved_0;
    msg.matched_filter_mode = hdr.matched_filter_mode;
    msg.matched_filter_losses_h = hdr.matched_filter_losses_h;
    msg.matched_filter_losses_v = nan<float>;
    msg.equivalent_noise_bandwidth_h = hdr.equivalent_noise_bandwidth_h;
    msg.equivalent_noise_bandwidth_v = nan<float>;

    msg.hilo_h.resize(msg.gate_count);
    msg.hilo_v.clear();

    msg.iq_h.resize(msg.gate_count);
    msg.iq_v = array1cf{};

    // read the iq data
    auto full_blocks = msg.gate_count / 32;
    auto remainder = msg.gate_count % 32;
    auto total_blocks = full_blocks + (remainder > 0 ? 1 : 0);
    if (cur + total_blocks * sizeof(iq32_rx_h_data_block) > end)
      throw std::runtime_error{"Archive item too small for iq32_rx_h data payload"};
    auto blocks = reinterpret_cast<iq32_rx_h_data_block const*>(cur);
    auto igate = 0;
    for (uint32_t iblock = 0; iblock < full_blocks; ++iblock)
    {
      for (uint32_t i = 0; i < 32; ++i)
      {
        auto channel_mask = uint32_t(blocks[iblock].channel_mask);
        msg.hilo_h[igate] = channel_mask & (1 << i);
        msg.iq_h[igate].real(blocks[iblock].iq_h[i].i);
        msg.iq_h[igate].imag(blocks[iblock].iq_h[i].q);
        ++igate;
      }
    }
    for (uint32_t i = 0; i < remainder; ++i)
    {
      auto channel_mask = uint32_t(blocks[full_blocks].channel_mask);
      msg.hilo_h[igate] = channel_mask & (1 << i);
      msg.iq_h[igate].real(blocks[full_blocks].iq_h[i].i);
      msg.iq_h[igate].imag(blocks[full_blocks].iq_h[i].q);
      ++igate;
    }
    cur += total_blocks * sizeof(iq32_rx_h_data_block);
  }
  else if (type == gdrx5_data_type::iq32_rx_v)
  {
    // read the type header
    if (cur + sizeof(iq32_rx_v_hdr) > end)
      throw std::runtime_error{"Archive item too small for iq32_rx_v type header"};
    auto& hdr = *reinterpret_cast<iq32_rx_v_hdr const*>(cur);
    cur += hdr_base.type_length * 4;

    msg.tracking_number = hdr.tracking_number;
    msg.pulse_counter = hdr.pulse_counter;
    msg.elevation_angle = hdr.elevation_angle;
    msg.azimuth_angle = hdr.azimuth_angle;
    msg.range_start = hdr.range_start;
    msg.range_step = hdr.range_step;
    msg.range_stop = hdr.range_stop;
    msg.gate_count = hdr.gate_count;
    msg.prf = hdr.prf;
    msg.tx_average_frequency_h = nan<float>;
    msg.tx_average_frequency_v = hdr.tx_average_frequency_v;
    msg.tx_pulse_frequency_h = nan<float>;
    msg.tx_pulse_frequency_v = hdr.tx_pulse_frequency_v;
    msg.tx_pulse_magnitude_h = nan<float>;
    msg.tx_pulse_magnitude_v = hdr.tx_pulse_magnitude_v;
    msg.tx_pulse_phase_h = nan<float>;
    msg.tx_pulse_phase_v = hdr.tx_pulse_phase_v;
    msg.dds_frequency_h = nan<float>;
    msg.dds_frequency_v = hdr.dds_frequency_v;
    msg.rx_data_flags = hdr.rx_data_flags;
    msg.staggering_flags = hdr.staggering_flags;
    msg.timestamp_sec = hdr.timestamp_sec;
    msg.timestamp_nsec = hdr.timestamp_nsec;
    msg.timestamp_status = hdr.timestamp_status;
    msg.reserved_0 = hdr.reserved_0;
    msg.matched_filter_mode = hdr.matched_filter_mode;
    msg.matched_filter_losses_h = nan<float>;
    msg.matched_filter_losses_v = hdr.matched_filter_losses_v;
    msg.equivalent_noise_bandwidth_h = nan<float>;
    msg.equivalent_noise_bandwidth_v = hdr.equivalent_noise_bandwidth_v;

    msg.hilo_h.clear();
    msg.hilo_v.resize(msg.gate_count);

    msg.iq_h = array1cf{};
    msg.iq_v.resize(msg.gate_count);

    // read the iq data
    auto full_blocks = msg.gate_count / 32;
    auto remainder = msg.gate_count % 32;
    auto total_blocks = full_blocks + (remainder > 0 ? 1 : 0);
    if (cur + total_blocks * sizeof(iq32_rx_v_data_block) > end)
      throw std::runtime_error{"Archive item too small for iq32_rx_v data payload"};
    auto blocks = reinterpret_cast<iq32_rx_v_data_block const*>(cur);
    auto igate = 0;
    for (uint32_t iblock = 0; iblock < full_blocks; ++iblock)
    {
      for (uint32_t i = 0; i < 32; ++i)
      {
        auto channel_mask = uint32_t(blocks[iblock].channel_mask);
        msg.hilo_v[igate] = channel_mask & (1 << i);
        msg.iq_v[igate].real(blocks[iblock].iq_v[i].i);
        msg.iq_v[igate].imag(blocks[iblock].iq_v[i].q);
        ++igate;
      }
    }
    for (uint32_t i = 0; i < remainder; ++i)
    {
      auto channel_mask = uint32_t(blocks[full_blocks].channel_mask);
      msg.hilo_v[igate] = channel_mask & (1 << i);
      msg.iq_v[igate].real(blocks[full_blocks].iq_v[i].i);
      msg.iq_v[igate].imag(blocks[full_blocks].iq_v[i].q);
      ++igate;
    }
    cur += total_blocks * sizeof(iq32_rx_v_data_block);
  }
  else
    throw std::runtime_error{"Unexpected gdrx5 data type encountered in gdrx5_tx archive data item"};

  // sanity check the magic token at the end of the data frame
  if (cur + sizeof(gdrx_base_footer) > end)
    throw std::runtime_error{"Archive item too small for GDRX-base footer"};
  if (reinterpret_cast<gdrx_base_footer const*>(cur)->magic != 0xcc0ccc0c)
    throw std::runtime_error{"Missing magic token at end of GDRX-5 pulse data frame"};
  cur += sizeof(gdrx_base_footer);
}

auto file::read_next_header() -> void
{
  // check if there are any more items
  if (file_.peek() == EOF)
  {
    cur_type_ = archive_data_type::none;
    cur_length_ = 0;
    return;
  }

  // read the next header
  auto hdr = archive_item_header{};
  file_.read(reinterpret_cast<char*>(&hdr), sizeof(hdr));
  cur_type_ = archive_data_type(int32_t{hdr.data_type});
  cur_length_ = int64_t{hdr.data_length};

  // recurse into containers and skip unknown data types
  switch (cur_type_)
  {
  case archive_data_type::raw_data:
  case archive_data_type::iq_gdrx:
  case archive_data_type::tx_gdrx:
  case archive_data_type::gdrx5_raw:
  case archive_data_type::gdrx5_iq:
  case archive_data_type::gdrx5_tx:
  case archive_data_type::timestamp:
  case archive_data_type::rcl_state:
  case archive_data_type::rcl_rt:
  case archive_data_type::rcl_file:
    break;

  case archive_data_type::container:
    cur_length_ = 0;
    read_next_header();
    break;

  case archive_data_type::zlib:
    throw std::runtime_error{"Unimplemented - zlib extraction"};
    break;

  default:
    #if 0 // TODO - rate limit this output!!!! (Greenvale has heaps for 0x0304)
    std::cerr << fmt::format("Unknown archive item type 0x{:04x} skipped at offset {}\n", int32_t(cur_type_), file_.tellg());
    #endif
    ignore();
    break;
  }
}
