/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "util.h"
#include "format.h"

#include <QStandardPaths>

using namespace vispwr;

#if defined(__GLIBCXX__) && defined(__MINGW32__) && !_GLIBCXX_HAVE_USELOCALE
#warning "HACK to supply std::from_chars<float/double> missing on libstdc++ with MinGW"
#include <malloc.h>
auto std::from_chars(char const* first, char const* last, float& value, chars_format fmt) noexcept -> from_chars_result
{
  auto size = last - first;
  if (size == 0)
    return { first, std::errc::invalid_argument };

  auto buf = static_cast<char*>(alloca(size + 1));
  for (auto i = 0; i < size; ++i)
    buf[i] = first[i];
  buf[size] = '\0';

  char* end = nullptr;
  errno = 0;
  value = strtof(buf, &end);
  if (errno != 0)
  {
    if (errno == ERANGE)
      return { end, std::errc::result_out_of_range };
    else
      return { first, std::errc::invalid_argument };
  }
  return { end, std::errc{} };
}
auto std::from_chars(char const* first, char const* last, double& value, chars_format fmt) noexcept -> from_chars_result
{
  auto size = last - first;
  if (size == 0)
    return { first, std::errc::invalid_argument };

  auto buf = static_cast<char*>(alloca(size + 1));
  for (auto i = 0; i < size; ++i)
    buf[i] = first[i];
  buf[size] = '\0';

  char* end = nullptr;
  errno = 0;
  value = strtod(buf, &end);
  if (errno != 0)
  {
    if (errno == ERANGE)
      return { end, std::errc::result_out_of_range };
    else
      return { first, std::errc::invalid_argument };
  }
  return { end, std::errc{} };
}
auto std::from_chars(char const* first, char const* last, long double& value, chars_format fmt) noexcept -> from_chars_result
{
  auto size = last - first;
  if (size == 0)
    return { first, std::errc::invalid_argument };

  auto buf = static_cast<char*>(alloca(size + 1));
  for (auto i = 0; i < size; ++i)
    buf[i] = first[i];
  buf[size] = '\0';

  char* end = nullptr;
  errno = 0;
  value = strtold(buf, &end);
  if (errno != 0)
  {
    if (errno == ERANGE)
      return { end, std::errc::result_out_of_range };
    else
      return { first, std::errc::invalid_argument };
  }
  return { end, std::errc{} };
}
#endif

auto vispwr::data_paths() -> vector<filesystem::path> const&
{
  static auto path_list_ = []
  {
    auto path_list = vector<filesystem::path>{};

    // enum value - just to reduce line lengths
    auto aldl = QStandardPaths::StandardLocation::AppLocalDataLocation;

    // collect the standard locations that Qt knows about, which includes the users home directory for overwrites
    for (auto& dir : QStandardPaths::standardLocations(aldl))
      if (auto path = filesystem::path{dir.toStdString()}; filesystem::is_directory(path))
        path_list.emplace_back(path);

    /* for linux try to detect the share directory based on the executable path to permit the use of a custom install
     * prefix.  we attempt to detect the presence of a local data overwrite directory in the user's home directory
     * and put this location immediately beneath it.  this ensures that user ovewrites are highest priority, then
     * our custom prefix (if present), then finally the remaining standard data dirs. the writeable location returend
     * by QStandardPaths is assumed to be the home directory path for overwrites (e.g. ~/.local/share/vispwr). */
#ifdef __linux__
    auto exe_path = filesystem::canonical("/proc/self/exe").parent_path();
    if (auto path = filesystem::weakly_canonical(exe_path / "../share/vispwr"); filesystem::is_directory(path))
    {
      if (path_list.empty() || path_list[0] != filesystem::path{QStandardPaths::writableLocation(aldl).toStdString()})
        path_list.insert(path_list.begin(), std::move(path));
      else
        path_list.insert(std::next(path_list.begin()), std::move(path));
    }
#endif

    return path_list;
  }();
  return path_list_;
}
