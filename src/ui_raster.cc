/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "ui_raster.h"
#include "ui_style.h"
#include "format.h"

#include <QFormLayout>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QVBoxLayout>

#include <fstream>

using namespace vispwr;

ui_raster_pick_item::ui_raster_pick_item(int channel)
  : channel_{channel}
{
  setFlag(QGraphicsItem::ItemIgnoresTransformations, true);

  QObject::connect(&pick::channel(channel_), &pick::updated, this, &ui_raster_pick_item::on_updated);
}

auto ui_raster_pick_item::on_updated() -> void
{
  if (!scene())
    return;
  auto scn = static_cast<ui_raster_scene*>(scene());

  auto pdat = pick::channel(channel_).get();
  if (!pdat.valid)
  {
    setVisible(false);
    return;
  }

  string label;
  if (auto val = scn->pick_value(pdat))
    label = fmt::format("{:.3}", *val);
  else
    label = "---";
  label_ = QString::fromStdString(label);

  setPos(scn->polar_to_scene(pdat));

  setVisible(true);
}

auto ui_raster_pick_item::boundingRect() const -> QRectF
{
  auto& style = ui_style::get();
  auto dot_rect = QRectF{-style.pick_radius, -style.pick_radius, style.pick_radius * 2, style.pick_radius * 2};
  auto text_rect = QRectF{style.pick_metrics.boundingRect(QRect{}, Qt::AlignVCenter, label_)};
  text_rect.moveLeft(style.pick_radius * 1.5);
  auto box_rect = QRectF{
      0.0
    , text_rect.y() - style.pick_radius
    , text_rect.width() + style.pick_radius * 2.5
    , text_rect.height() + style.pick_radius * 2};
  return dot_rect.united(box_rect);
}

auto ui_raster_pick_item::mousePressEvent(QGraphicsSceneMouseEvent* event) -> void
{
  if (event->button() == Qt::MiddleButton)
    pick::channel(channel_).clear();
}

auto ui_raster_pick_item::paint(QPainter* painter, QStyleOptionGraphicsItem const* option, QWidget* widget) -> void
{
  painter->setRenderHint(QPainter::Antialiasing);

  auto& style = ui_style::get();

  auto text_rect = QRectF{style.pick_metrics.boundingRect(QRect{}, Qt::AlignVCenter, label_)};
  text_rect.moveLeft(style.pick_radius * 1.5);
  auto box_rect = QRectF{
      0.0
    , text_rect.y() - style.pick_radius
    , text_rect.width() + style.pick_radius * 2.5
    , text_rect.height() + style.pick_radius * 2};

  auto border_pen = QPen(style.pick_border_color, style.pick_border_width);

  // background for label
  painter->setPen(border_pen);
  painter->setBrush(style.pick_background_color);
  painter->drawRect(box_rect);

  // label
  painter->setFont(style.pick_font);
  painter->setBrush(Qt::NoBrush);
  painter->setPen(style.pick_text_color);
  painter->drawText(text_rect, label_);

  // target dot
  painter->setBrush(style.pick_channel_color[channel_]);
  painter->setPen(border_pen);
  painter->drawEllipse(QPointF{0.0, 0.0}, style.pick_radius, style.pick_radius);
}

ui_raster_scene::ui_raster_scene(QObject* parent)
  : QGraphicsScene{parent}
  , moment_{"DBZH"}
  , col_scheme_{"reflectivity"}
  , min_val_{-32.0f}
  , max_val_{60.0f}
  , col_map_{json::read(std::ifstream{color_map_index().at(col_scheme_)})}
{
  update_cmap_offsets();

  /* it is important that we do NOT call the pick's on_update() function here because it will in turn call back to
   * our pick_value() function.  but pick_value() is pure virtual during our constructor since it is to be provided
   * by the child class.  so if we called on_update() here we would crash immediately due to call of pure virtual. */
  for (auto i = 0; i < pick::channels; ++i)
  {
    pick_.push_back(make_unique<ui_raster_pick_item>(i));
    addItem(pick_[i].get());
  }

  connect(this, &ui_raster_scene::moment_updated, this, &ui_raster_scene::on_moment_updated);
}

auto ui_raster_scene::set_color_scheme(string name) -> void
{
  auto& path = color_map_index().at(name);

  bool changed = name != col_scheme_;
  col_scheme_ = name;
  col_map_ = color_map{json::read(std::ifstream{path})};
  update_cmap_offsets();
  if (changed)
    color_scheme_changed(col_scheme_);
}

auto ui_raster_scene::set_color_min(float value) -> void
{
  auto changed = abs(value - min_val_) > 0.00001f;
  min_val_ = value;
  update_cmap_offsets();
  if (changed)
    color_min_changed(min_val_);
}

auto ui_raster_scene::set_color_max(float value) -> void
{
  auto changed = abs(value - max_val_) > 0.00001f;
  max_val_ = value;
  update_cmap_offsets();
  if (changed)
    color_max_changed(max_val_);
}

auto ui_raster_scene::update_cmap_offsets() -> void
{
  val_offset_ = min_val_ - col_map_.min();
  val_scale_ = (col_map_.max() - col_map_.min()) / (max_val_ - min_val_);
}

auto ui_raster_scene::update_picks() -> void
{
  for (auto& p : pick_)
    p->on_updated();
}

auto ui_raster_scene::on_moment_updated(string moment) -> void
{
  if (moment_ == moment)
    return;

  moment_ = std::move(moment);

  // HACK - we should not hard code these, and the user should be able to modify
  if (moment_ == "DBZH" || moment_ == "DBZV")
  {
    set_color_scheme("reflectivity");
    set_color_min(-32.0f);
    set_color_max(60.0f);
  }
  else if (moment_ == "VRADH" || moment_ == "VRADV")
  {
    set_color_scheme("velocity");
    set_color_min(-27.77f);
    set_color_max(27.77f);
  }
  else if (moment_ == "WRADH" || moment_ == "WRADV")
  {
    set_color_scheme("spectw");
    set_color_min(0.0f);
    set_color_max(27.7f);
  }
  else if (moment_ == "SQIH" || moment_ == "SQIV")
  {
    set_color_scheme("jet");
    set_color_min(0.0f);
    set_color_max(1.0f);
  }
  else if (moment_ == "SNRH" || moment_ == "SNRV")
  {
    set_color_scheme("jet");
    set_color_min(0.0f);
    set_color_max(30.0f);
  }
  else if (moment_ == "ZDR")
  {
    set_color_scheme("zdr");
    set_color_min(-6.0f);
    set_color_max(6.0f);
  }
  else if (moment_ == "PHIDP")
  {
    set_color_scheme("phidp");
    set_color_min(0.0f);
    set_color_max(360.0f);
  }
  else if (moment_ == "RHOHV")
  {
    set_color_scheme("rhohv");
    set_color_min(0.0f);
    set_color_max(1.0f);
  }
  else if (moment == "KDP")
  {
    set_color_scheme("kdp");
    set_color_min(-2.0f);
    set_color_max(7.0f);
  }
  else
  {
    set_color_scheme("jet");
    set_color_min(1.0f);
    set_color_max(0.0f);
  }

  update();
}

ui_raster_view::ui_raster_view(ui_raster_scene* scene, QWidget* parent)
  : QGraphicsView{scene, parent}
  , click_pos_{0.0, 0.0}
{
  setDragMode(QGraphicsView::ScrollHandDrag);
  viewport()->setCursor(Qt::ArrowCursor);

  setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

auto ui_raster_view::update_cursor_position(optional<QPointF> view_pos) -> void
{
  auto& style = ui_style::get();

  // keep a record of the region we previously drew to so we can update it
  auto old_rect = cursor_rect_;

  // update the label text and our new bounding box
  cursor_label_.clear();
  cursor_rect_ = QRectF{style.pick_radius, style.pick_radius, 0, 0};
  if (view_pos)
  {
    auto scn = scene();
    auto scene_pos = mapToScene(view_pos->toPoint());

    if (auto coords = scn->scene_to_polar(scene_pos))
    {
      auto label = fmt::format("{:0.2f}\u00b0 {:0.2f}\u00b0 {:0.2f}km", coords->elevation, coords->azimuth, coords->range);
      if (auto val = scn->pick_value(*coords))
        label.append(fmt::format(" ({:.3})", *val));

      cursor_label_ = QString::fromStdString(label);

      auto text_rect = QRectF{style.pick_metrics.boundingRect(QRect{}, Qt::AlignTop, cursor_label_)};
      text_rect.moveTopLeft(QPointF{style.pick_radius * 2, style.pick_radius * 2});
      auto box_rect = QRectF{
          style.pick_radius
        , style.pick_radius
        , text_rect.width() + style.pick_radius * 2
        , text_rect.height() + style.pick_radius * 2};
      cursor_rect_ = box_rect;
    }
  }

  // force a repaint of the union of the old and new bounding box
  viewport()->update(old_rect.united(cursor_rect_).toAlignedRect());
}

auto ui_raster_view::drawForeground(QPainter* painter, QRectF const& rect) -> void
{
  if (cursor_label_.size() == 0)
    return;

  auto& style = ui_style::get();

  painter->setWorldMatrixEnabled(false);

  auto text_rect = QRectF{style.pick_metrics.boundingRect(QRect{}, Qt::AlignTop, cursor_label_)};
  text_rect.moveTopLeft(QPointF{style.pick_radius * 2, style.pick_radius * 2});
  auto box_rect = QRectF{
      style.pick_radius
    , style.pick_radius
    , text_rect.width() + style.pick_radius * 2
    , text_rect.height() + style.pick_radius * 2};

  // background for label
  painter->setPen(Qt::NoPen);
  painter->setBrush(style.pick_background_color);
  painter->drawRect(box_rect);

  // label
  painter->setFont(style.pick_font);
  painter->setBrush(Qt::NoBrush);
  painter->setPen(style.pick_text_color);
  painter->drawText(text_rect, cursor_label_);
}

auto ui_raster_view::leaveEvent(QEvent* event) -> void
{
  QGraphicsView::leaveEvent(event);
  update_cursor_position(nullopt);
}

auto ui_raster_view::mouseMoveEvent(QMouseEvent* event) -> void
{
  QGraphicsView::mouseMoveEvent(event);
  if (!scene()->mouseGrabberItem() && event->buttons() == Qt::LeftButton)
    setSceneRect(sceneRect().translated(click_pos_ - mapToScene(event->pos())));
  else
    update_cursor_position(event->pos());
}

auto ui_raster_view::mousePressEvent(QMouseEvent* event) -> void
{
  QGraphicsView::mousePressEvent(event);

  auto scn = scene();
  auto scene_pos = mapToScene(event->pos());

  if (event->button() == Qt::LeftButton)
  {
    click_pos_ = scene_pos;

    // disable the cursor tracker during dragging
    // this is mostly so we can be lazy about figuring out exactly where to repaint as the user drags the window
    // without which our tracker leaves trails on the scene as it pans
    update_cursor_position(nullopt);
  }
  else if (event->button() == Qt::RightButton)
  {
    auto coords = scn->scene_to_polar(scene_pos);
    auto p = pick::data{};
    if (coords)
    {
      static_cast<polar_coordinates&>(p) = *coords;
      p.valid = true;
    }
    else
      p.valid = false;
    pick::channel(pick::channel_from_modifiers(event->modifiers())).set(std::move(p));
  }
}

auto ui_raster_view::mouseReleaseEvent(QMouseEvent* event) -> void
{
  QGraphicsView::mouseReleaseEvent(event);

  /* setDragMode(ScrollHandDrag) causese the cursor to be permanently changed to a hand, which is horrible for
   * precision picking.  we force it back to an arrow after each mouse release (i.e. at the end of each drag) so
   * that the user can see what they are doing.  next time they start dragging the setDragMode functionality
   * will automatically give them the hand cursor again.  there doesn't seem to be any way to achieve this
   * behaviour explicitly so we are forced to hack it into a manual override of the release event. */
  viewport()->setCursor(Qt::ArrowCursor);
}

auto ui_raster_view::wheelEvent(QWheelEvent* event) -> void
{
  // increment of 120 is a 'standard' single click on a mouse wheel
  auto clicks = event->angleDelta().y() / 120.0;

  // determine our desired scaling factor (a single wheel click increases zoom by 20%)
  auto factor = std::pow(1.20, clicks);

  // rescale the view
  scale(factor, factor);

  // update our picked coordinate
  update_cursor_position(event->position());
}

ui_raster_window::ui_raster_window(unique_ptr<ui_raster_scene> scene, QString title, QSize size)
  : scene_{std::move(scene)}
  , view_{scene_.get(), this}
  , but_prefs_{QIcon::fromTheme("document-properties"), "", this}
  , panel_prefs_{this}
{
  scene_->setParent(this);

  auto layout = new QVBoxLayout(this);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->setSpacing(0);
  layout->addWidget(&view_);

  but_prefs_.setCheckable(true);
  but_prefs_.resize(30, 30);

  auto& cmap_index = color_map_index();
  for (auto& entry : cmap_index)
    combo_scheme_.addItem(QString::fromStdString(entry.first));
  spin_min_.setMinimum(-9999.0);
  spin_min_.setMaximum(9999.0);
  spin_max_.setMinimum(-9999.0);
  spin_max_.setMaximum(9999.0);

  auto layout_prefs = new QFormLayout(&panel_prefs_);
  layout_prefs->addRow("Scheme", &combo_scheme_);
  layout_prefs->addRow("Min", &spin_min_);
  layout_prefs->addRow("Max", &spin_max_);
  panel_prefs_.setAutoFillBackground(true);
  panel_prefs_.resize(panel_prefs_.sizeHint());
  panel_prefs_.setVisible(but_prefs_.isChecked());

  resize(size);
  setWindowTitle(title);
  setVisible(true);
  show();

  connect(&but_prefs_, &QPushButton::clicked, [this]() { panel_prefs_.setVisible(but_prefs_.isChecked()); resizeEvent(nullptr); });
  connect(&combo_scheme_, &QComboBox::currentTextChanged, [this](QString const& value) { scene_->set_color_scheme(value.toStdString()); });
  connect(&spin_min_, qOverload<double>(&QDoubleSpinBox::valueChanged), [this](double val) { return scene_->set_color_min(val); });
  connect(&spin_max_, qOverload<double>(&QDoubleSpinBox::valueChanged), [this](double val) { return scene_->set_color_max(val); });
  connect(scene_.get(), &ui_raster_scene::color_scheme_changed, [this](string val) { combo_scheme_.setCurrentText(QString::fromStdString(val)); });
  connect(scene_.get(), &ui_raster_scene::color_min_changed, [this](float val) { spin_min_.setValue(val); });
  connect(scene_.get(), &ui_raster_scene::color_max_changed, [this](float val) { spin_max_.setValue(val); });
}

auto ui_raster_window::hideEvent(QHideEvent* event) -> void
{
  window_hidden();
}

auto ui_raster_window::enterEvent(QEnterEvent* event) -> void
{
  but_prefs_.setVisible(true);
  resizeEvent(nullptr);
}

auto ui_raster_window::leaveEvent(QEvent* event) -> void
{
  if (!panel_prefs_.isVisible())
    but_prefs_.setVisible(false);
  resizeEvent(nullptr);
}

auto ui_raster_window::resizeEvent(QResizeEvent* event) -> void
{
  if (panel_prefs_.isVisible())
  {
    but_prefs_.move(10 + panel_prefs_.size().width() + 10, size().height() - 10 - but_prefs_.size().height());
    panel_prefs_.move(10, size().height() - 10 - panel_prefs_.size().height());
  }
  else
  {
    but_prefs_.move(10, size().height() - but_prefs_.size().height() - 10);
  }
}
