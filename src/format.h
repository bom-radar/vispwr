/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <chrono>
#include <ctime>

namespace vispwr
{
  using namespace fmt::literals;
}

namespace fmt
{
  /// Formatter for std::exception that prints nested exceptions
  template <typename T, typename Char>
  struct formatter<T, Char, std::enable_if_t<std::is_base_of<std::exception, T>::value>>
  {
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)
    {
      return ctx.begin();
    }

    template <typename FormatContext>
    auto format(std::exception const& err, FormatContext& ctx) -> decltype(ctx.out())
    {
      format_to(ctx.out(), "{}", err.what());
      try
      {
        std::rethrow_if_nested(err);
      }
      catch (std::exception const& nested)
      {
        format_to(ctx.out(), "\n-> ");
        format(nested, ctx);
      }
      return ctx.out();
    }
  };

  #if 0
  /// Formatter for std::chrono::system_clock::time_point
  template <typename Char>
  struct formatter<std::chrono::system_clock::time_point, Char> : formatter<std::tm, Char>
  {
    using fmt::formatter<std::tm, Char>::specs;

    // if the user provided no strftime format then overwrite the default ('') with iso8601 ("%Y-%m-%dT%H:%M:%SZ")
    template <typename ParseContext>
    constexpr auto parse(ParseContext &ctx)
    {
      auto ret = formatter<std::tm, Char>::parse(ctx);
      if (tm_format.size() <= 1)
      {
        constexpr char iso8601[] = "%Y-%m-%dT%H:%M:%SZ";
        tm_format.resize(sizeof(iso8601));
        for (size_t i = 0; i < sizeof(iso8601); ++i)
          tm_format[i] = iso8601[i];
      }
      return ret;
    }

    template <typename FormatContext>
    auto format(std::chrono::system_clock::time_point const& v, FormatContext& ctx)
    {
      auto t = std::chrono::system_clock::to_time_t(v);
      struct tm tt;
      return formatter<std::tm, Char>::format(*gmtime_r(&t, &tt), ctx);
    }
  };
  #endif
}
