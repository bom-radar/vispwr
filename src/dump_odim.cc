/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"

#include <bom/io/odim.h>

using namespace vispwr;

namespace vispwr
{
  /// Debugging stage used to dump data to a netcdf file
  class dump_odim : public stage
  {
  public:
    dump_odim();
    ~dump_odim();

    auto name() const -> string_view override;
    auto inputs() const -> vector<input> const& override;
    auto outputs() const -> vector<output> const& override;
    auto maximum_concurrency() const -> int override;
    auto startup() -> void override;
    auto shutdown() -> void override;
    auto ready_to_execute(input_flags availability) const -> bool override;
    auto execute(input_vector& inputs, output_store& outputs) -> void override;

  private:
    // hack
    std::vector<float> buf_[moment_list.size() * 2]; // x2 for cor/uncor
  };
}

dump_odim::dump_odim()
{
  auto max_gates = 1200;

  for (int imoment = 0; imoment < moment_list.size() * 2; ++imoment)
  {
    buf_[imoment].resize(360 * 1200);
    std::fill(buf_[imoment].begin(), buf_[imoment].end(), std::numeric_limits<float>::quiet_NaN());
  }
}

dump_odim::~dump_odim()
{
}

auto dump_odim::name() const -> string_view
{
  return "ODIM"sv;
}

auto dump_odim::inputs() const -> vector<input> const&
{
  static auto list = []
  {
    auto ret = vector<input>();
    ret.reserve(3);
    ret.emplace_back("autocorrelations"sv, data_type::autocorrelations, "Input autocorrelation data");
    ret.emplace_back("u_moments"sv, data_type::moments, "Uncorrected single pol moments");
    ret.emplace_back("c_moments"sv, data_type::moments, "Corrected single pol moments");
    return ret;
  }();
  return list;
}

auto dump_odim::outputs() const -> vector<output> const&
{
  static auto list = []
  {
    auto ret = vector<output>();
    return ret;
  }();
  return list;
}

auto dump_odim::maximum_concurrency() const -> int
{
  return 1;
}

auto dump_odim::startup() -> void
{ }

auto dump_odim::shutdown() -> void
{
  auto vol = bom::io::odim::polar_volume{"debug.pvol.h5", bom::io_mode::create};
  vol.set_longitude(132.0);
  vol.set_latitude(-23.0);
  vol.set_height(0.0);

  auto scan = vol.scan_append();
  scan.set_elevation_angle(0.5);
  scan.set_bin_count(1200);
  scan.set_range_start(0.0);
  scan.set_range_scale(250.0);
  scan.set_ray_count(360);
  scan.set_ray_start(-0.5);
  scan.set_first_ray_radiated(0);

  for (int imoment = 0; imoment < moment_list.size() * 2; ++imoment)
  {
    std::string name = moment_list[imoment % moment_list.size()];
    if (imoment / moment_list.size() == 0)
      name.append("_U");

    size_t dims[] = { 360, 1200 };
    auto dat = scan.data_append(bom::io::odim::data::data_type::f32, 2, dims);
    dat.set_quantity(name);
    dat.set_gain(1.0);
    dat.set_offset(0.0);
    dat.set_nodata(-999.0);
    dat.set_undetect(-998.0);
    dat.write_pack(buf_[imoment].data(), [](auto v){return false;}, [](auto v){return std::isnan(v);});
  }
}

auto dump_odim::ready_to_execute(input_flags availability) const -> bool
{
  return availability.any();
}

auto dump_odim::execute(input_vector& inputs, output_store& outputs) -> void
{
  // hacky way to decide the ray NOT CORRECT
  auto determine_ray = [](auto az_first, auto az_last)
  {
    auto df = az_last - az_first;
    if (df < -180.0f)
      df += 360.0f;
    if (df > 180.0f)
      df -= 360.0f;
    auto az = az_first + 0.5 * df;
    return int(std::round(az)) % 360;
  };

  // output uncorrected moments
  if (inputs[1])
  {
    auto mom = static_cast<moments const*>(inputs[1].get());
    auto iray = determine_ray(mom->azimuth_first, mom->azimuth_last);
    for (auto igate = 0; igate < std::min(1200, mom->gate_count); ++igate)
    {
      for (auto imoment = 0; imoment < moment_list.size(); ++imoment)
        buf_[imoment, iray * 1200 + igate] = mom->data[igate, imoment];
    }
  }

  // output corrected moments
  if (inputs[2])
  {
    auto mom = static_cast<moments const*>(inputs[2].get());
    auto iray = determine_ray(mom->azimuth_first, mom->azimuth_last);
    for (auto igate = 0; igate < std::min(1200, mom->gate_count); ++igate)
    {
      for (auto imoment = 0; imoment < moment_list.size(); ++imoment)
        buf_[imoment + moment_list.size(), iray * 1200 + igate] = mom->data[igate, imoment];
    }
  }
}
