/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"
#include <iostream>

using namespace vispwr;

auto stage::get_catalog() -> stage_catalog&
{
  static stage_catalog catalog_;
  return catalog_;
}

/* This function ensures our stages are always wrapped in the auto_launch template which starts and stops the
 * worker thread appropriately.  It's a good idea to make the constructor for all derived stages protected to
 * force all creation to be performed via this function. */
auto stage::create(string_view name) -> shared_ptr<stage>
try
{
  auto& catalog = get_catalog();
  auto ientry = std::ranges::find(catalog, name, &stage_metadata::name);
  if (ientry == catalog.end())
    throw runtime_error{"Unknown stage type requested"};
  return ientry->factory();
}
catch (...)
{
  std::throw_with_nested(std::runtime_error{fmt::format("Failed to create stage type {}", name)});
}

auto stage::connect(peer source, peer destination) -> void
{
  source.first->check_output_index(source.second);
  destination.first->check_input_index(destination.second);
  if (source.first->olist_[source.second].type != destination.first->ilist_[destination.second].type)
    throw runtime_error{"Attempt to connect output to input of different type"};

  if constexpr (stage_debug)
    fmt::print(
          "Connecting stage {}.{} to {}.{}\n"
        , source.first->metadata().name
        , source.second
        , destination.first->metadata().name
        , destination.second);

  {
    auto lock_src = lock_guard<mutex>{source.first->mut_io_};
    auto lock_dst = lock_guard<mutex>{destination.first->mut_io_};

    auto& peers = source.first->outputs_[source.second].peers;
    if (std::ranges::find(peers, destination) != peers.end())
      throw runtime_error{"Attempt to create duplicate connection"};
    if (destination.first->ilist_[destination.second].exclusive && !destination.first->inputs_[destination.second].peers.empty())
      throw runtime_error{"Attempt to create multiple connections to exclusive input"};

    source.first->outputs_[source.second].peers.push_back(destination);
    destination.first->inputs_[destination.second].peers.push_back(source);
  }

  // have the destination stage immediately request a resend of data to this socket
  destination.first->request_resend(destination.second);
}

auto stage::disconnect(peer source, peer destination) -> void
{
  source.first->check_output_index(source.second);
  destination.first->check_input_index(destination.second);

  if constexpr (stage_debug)
    fmt::print(
          "Disconnecting stage {}.{} to {}.{}\n"
        , source.first->metadata().name
        , source.second
        , destination.first->metadata().name
        , destination.second);

  auto lock_src = lock_guard<mutex>{source.first->mut_io_};
  auto lock_dst = lock_guard<mutex>{destination.first->mut_io_};

  if (std::erase(source.first->outputs_[source.second].peers, destination) != 1)
    throw runtime_error{"Attempt to delete unknown connection"};
  if (std::erase(destination.first->inputs_[destination.second].peers, source) != 1)
    throw logic_error{"Corrupted connection lists"};
}

stage::stage(
      span<string_view const> plist
    , span<input_meta const> ilist
    , span<output_meta const> olist)
  : plist_{plist}
  , ilist_{ilist}
  , olist_{olist}
  , inputs_{ilist_.size()}
  , outputs_{olist_.size()}
  , terminate_{false}
  , backoff_{false}
{ }

/* If we are being destroyed, then it is impossible for another stage to be currently sending us data via receive().
 * This is because the upstream connections to us are made via shared_ptr.  So we don't need to worry about notifying
 * the free space condition variable or dealing with other threads being blocked on our queue. */
stage::~stage()
{
  /* if the worker thread is still running there's no way to safely recover since memory corruption has likely already
   * happened (we could crash before we even get here).  this is because the worker thread may be in the middle of the
   * process() function in the derived class, but we are now in the destructor of the base class.  that means we have
   * already destroyed the members of the derived class even though it was in use.  the only safe thing to do now is to
   * terminate.  in reality this isn't a big issue since only the auto_launch template has access to the worker thread
   * launch function and it guarantees that the thread is terminated before destruction arrives at the derived class. */
  if (worker_.joinable())
  {
    std::cerr << "Stage destroyed before worker thread terminated" << std::endl;
    std::terminate();
  }
}

auto stage::lookup_input(string_view name) const -> int
{
  for (auto i = 0uz; i < ilist_.size(); ++i)
    if (ilist_[i].name == name)
      return i;
  throw runtime_error{fmt::format("Unknown input {} for stage {}", name, metadata().name)};
}

auto stage::lookup_output(string_view name) const -> int
{
  for (auto i = 0uz; i < olist_.size(); ++i)
    if (olist_[i].name == name)
      return i;
  throw runtime_error{fmt::format("Unknown output {} for stage {}", name, metadata().name)};
}

auto stage::save() -> json
{
  auto jparams = json::object_type{};
  for (auto name : plist_)
  {
    std::visit([&](auto&& param)
    {
      jparams.emplace(name, param.value);
    }, get_parameter(name));
  }
  return json{std::move(jparams)};
}

auto stage::load(json const& j) -> void
{
  for (auto name : plist_)
  {
    auto ijparam = j.object().find(name);
    if (ijparam == j.object().end())
      continue;

    std::visit([&](auto&& param)
    {
      using value_type = std::decay_t<decltype(param.value)>;
      if constexpr (std::is_same_v<value_type, bool>)
        set_parameter(name, ijparam->second.boolean());
      if constexpr (std::is_same_v<value_type, int>)
        set_parameter(name, ijparam->second.template number<int>());
      if constexpr (std::is_same_v<value_type, double>)
        set_parameter(name, ijparam->second.template number<double>());
      if constexpr (std::is_same_v<value_type, string>)
        set_parameter(name, ijparam->second.string());
    }, get_parameter(name));
  }
}

auto stage::get_parameter(string_view name) -> parameter
{
  /* it is possible for this function to be called from the worker thread if the parameter change callback
   * registered by the UI calls it.  ideally it won't, and will just offload the notification to be handled
   * in the main thread or a dedicated UI thread.  but if we are called from the worker thread then we need
   * to directly retrieve and return the parameter.  enqueuing and waiting would immediately deadlock because
   * we will be blocking the same thread that needs to process the request. */
  if (std::this_thread::get_id() == worker_.get_id())
    return get_parameter_impl(name);

  // enqueue a request to read the parameter
  auto p = make_shared<parameter_get>();
  p->name = name;
  p->ready = false;
  enqueue(-1, p);

  // wait for the parameter to be read by a process() call on the worker thread
  auto lock = unique_lock<mutex>{mut_queue_};
  while (!terminate_ && !p->ready)
    cv_params_.wait(lock);

  // TODO - propogate any errors to this thread

  return std::move(*p->param);
}

auto stage::set_parameter(string_view name, parameter_value value) -> void
{
  /* it is possible for this function to be called from the worker thread if the parameter change callback
   * registered by the UI calls it.  ideally it won't, and will just offload the notification to be handled
   * in the main thread or a dedicated UI thread.  but if we are called from the worker thread then we need
   * to directly modify the parameter.  enqueuing and waiting would immediately deadlock because we will be
   * blocking the same thread that needs to process the request. */
  if (std::this_thread::get_id() == worker_.get_id())
  {
    set_parameter_impl(name, std::move(value));
    return;
  }

  // enqueue a request to write the parameter
  auto p = make_shared<parameter_set>();
  p->name = name;
  p->value = std::move(value);
  p->ready = false;
  enqueue(-1, p);

  // wait for the parameter to be updated by a process() call on the worker thread
  {
    auto lock = unique_lock<mutex>{mut_queue_};
    while (!terminate_ && !p->ready)
      cv_params_.wait(lock);
  }

  // TODO - propogate any errors to this thread

  // request any upstream data be resent so that our new parameter value is accounted for
  // TODO - allow stages to decide when to do this.  easiest way is by return code of set_parameter_impl()
  request_resend();
}

auto stage::input_connected(int input) const -> bool
{
  check_input_index(input);
  auto lock = lock_guard<mutex>{mut_io_};
  return !inputs_[input].peers.empty();
}

auto stage::output_connected(int output) const -> bool
{
  check_output_index(output);
  auto lock = lock_guard<mutex>{mut_io_};
  return !outputs_[output].peers.empty();
}

auto stage::set_parameter_callback(parameter_callback callback) -> void
{
  auto lock = lock_guard<mutex>{mut_io_};
  cb_parameter_ = std::move(callback);
}

auto stage::set_logging_callback(logging_callback callback) -> void
{
  auto lock = lock_guard<mutex>{mut_io_};
  cb_logging_ = std::move(callback);
}

auto stage::check_input_index(int input) const -> void
{
  if (size_t(input) >= ilist_.size())
    throw std::runtime_error{fmt::format("Invalid input index {} for stage {}", input, metadata().name)};
}

auto stage::check_output_index(int output) const -> void
{
  if (size_t(output) >= olist_.size())
    throw std::runtime_error{fmt::format("Invalid output index {} for stage {}", output, metadata().name)};
}

/* we don't want to invoke the callbacks while our mutex is locked, because that's an invitation to a deadlock if the
 * client calls any other member functions from the callback - which is very likely.  but we need to hold the mutex lock
 * to access the callback function since it is set in the main/ui thread and read in the worker thread.  this means we
 * are forced to copy the std::function here before invoking it.  given the relative rareity of these callback events,
 * the performance hit is is likely to be negligible. */
auto stage::notify_parameter_update(string_view name) -> void
{
  auto lock = unique_lock<mutex>{mut_io_};
  auto callback = cb_parameter_;
  lock.unlock();
  if (callback)
    callback(name);
}

auto stage::output_log(log_level level, string message) -> void
{
  auto lock = unique_lock<mutex>{mut_io_};
  auto callback = cb_logging_;
  lock.unlock();
  if (callback)
    callback(level, std::move(message));
}

auto stage::request_resend(int input) -> void
{
  static auto req_id = std::atomic_int64_t{0};
  auto msg = make_shared<resend_request const>(req_id++);

  /* TODO - should we refactor this and process(resend_req) below to have a dedicated send_upstream
   * and broadcast_upstream() function?  would also be relevant if stages need to mutate the request
   * as they are passed up the chain (e.g. to filter on requested area) */
  if (input == -1)
  {
    auto lock = lock_guard<mutex>{mut_io_};
    for (auto& inp : inputs_)
      for (auto& con : inp.peers)
        con.first->enqueue(con.second, msg);
  }
  else
  {
    check_input_index(input);
    auto lock = lock_guard<mutex>{mut_io_};
    for (auto& con : inputs_[input].peers)
      con.first->enqueue(con.second, msg);
  }
}

auto stage::wake_worker() -> void
{
  cv_not_empty_.notify_all();
}

auto stage::process_idle() -> std::chrono::milliseconds
{
  return -1ms;
}

auto stage::process(shared_ptr<resend_request const> data, int input) -> void
{
  // forward the message to all upstream processes
  // note that we supply the _output_ socket here instead of the usual input socket number!
  auto lock = lock_guard<mutex>{mut_io_};
  for (auto& inp : inputs_)
    for (auto& con : inp.peers)
      con.first->enqueue(con.second, data);
}

auto stage::process(shared_ptr<state> data, int input) -> void
{
  process(std::const_pointer_cast<state const>(std::move(data)), input);
}

auto stage::process(shared_ptr<state const> data, int input) -> void
{
  // default behaviour for a state object is to forward it to all downstream stages
  broadcast(std::move(data));
}

auto stage::process(shared_ptr<pulse> data, int input) -> void
{
  process(std::const_pointer_cast<pulse const>(std::move(data)), input);
}

auto stage::process(shared_ptr<pulse const> data, int input) -> void
{
  output_log(log_level::error, "Unimplemented process() variant called");
}

auto stage::process(shared_ptr<batch> data, int input) -> void
{
  process(std::const_pointer_cast<batch const>(std::move(data)), input);
}

auto stage::process(shared_ptr<batch const> data, int input) -> void
{
  output_log(log_level::error, "Unimplemented process() variant called");
}

auto stage::process(shared_ptr<spectra> data, int input) -> void
{
  process(std::const_pointer_cast<spectra const>(std::move(data)), input);
}

auto stage::process(shared_ptr<spectra const> data, int input) -> void
{
  output_log(log_level::error, "Unimplemented process() variant called");
}

auto stage::process(shared_ptr<autocorrelations> data, int input) -> void
{
  process(std::const_pointer_cast<autocorrelations const>(std::move(data)), input);
}

auto stage::process(shared_ptr<autocorrelations const> data, int input) -> void
{
  output_log(log_level::error, "Unimplemented process() variant called");
}

auto stage::process(shared_ptr<moments> data, int input) -> void
{
  process(std::const_pointer_cast<moments const>(std::move(data)), input);
}

auto stage::process(shared_ptr<moments const> data, int input) -> void
{
  output_log(log_level::error, "Unimplemented process() variant called");
}

auto stage::process(shared_ptr<parameter_get> data, int input) -> void
{
  // perform the desired get or set action
  data->param = get_parameter_impl(data->name);

  // set the flag to indicate that request has been fulfilled and is safe for caller to access
  {
    auto lock = lock_guard<mutex>{mut_queue_};
    data->ready = true;
  }

  // wake up the caller
  cv_params_.notify_all();
}

auto stage::process(shared_ptr<parameter_set> data, int input) -> void
{
  // perform the desired get or set action
  set_parameter_impl(data->name, data->value);

  // set the flag to indicate that request has been fulfilled and is safe for caller to access
  {
    auto lock = lock_guard<mutex>{mut_queue_};
    data->ready = true;
  }

  // wake up the caller
  cv_params_.notify_all();
}

auto stage::enqueue(int input, datum_ptr data) -> size_t
{
  auto lock = unique_lock<mutex>{mut_queue_};

  // enqueue the value
  queue_.emplace_back(input, std::move(data));

  // if the queue transitions from empty to non-empty then notify receivers that are waiting
  if (queue_.size() == 1)
  {
    lock.unlock();
    cv_not_empty_.notify_all();
  }

  // return the current queue size to allow upstream processes to detect when they are overwhelming us
  return queue_.size();
}

auto stage::worker_thread_launch() -> void
{
  worker_ = std::thread{&stage::worker_thread_main, this};
}

auto stage::worker_thread_terminate() -> void
{
  {
    auto lock = lock_guard<mutex>{mut_queue_};
    terminate_ = true;
  }
  cv_not_empty_.notify_all();
  worker_.join();
}

auto stage::worker_thread_main() -> void
{
  auto lock = unique_lock<mutex>{mut_queue_};
  while (!terminate_)
  {
    // take a short break if we have been sending data too fast for our downstream processes to keep up with
    if (backoff_)
    {
      std::this_thread::sleep_for(backoff_timeout);
      backoff_ = false;
    }

    // if there is data available dequeue and process it
    if (!queue_.empty())
    {
      auto entry = std::move(queue_.front());
      queue_.pop_front();
      lock.unlock();
      try
      {
        std::visit([&](auto&& data) { process(std::move(data), entry.first); }, std::move(entry.second));
      }
      catch (std::exception& err)
      {
        output_log(log_level::error, fmt::format("Exception during processing: {}", err));
      }
      lock.lock();
    }
    // queue was empty, process anyway with no input if desired by policy
    else
    {
      // no data queued, so do some idle time processing
      lock.unlock();
      std::chrono::milliseconds wait_time;
      try
      {
        wait_time = process_idle();
      }
      catch (std::exception& err)
      {
        // force a 100ms wait to prevent a constantly failing process from consuming 100% of a CPU
        wait_time = 100ms;
        output_log(log_level::error, fmt::format("Exception during processing: {}", err));
      }
      lock.lock();

      // if the queue is still empty, wait for new data to arrive (or our idle timeout)
      if (!terminate_ && queue_.empty() && wait_time != 0ms)
      {
        // a negative wait time means no time limit
        if (wait_time < 0ms)
          cv_not_empty_.wait(lock);
        else
          cv_not_empty_.wait_for(lock, wait_time);
      }
    }
  }
}
