/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "iwrf.h"
#include "format.h"
#include "calibration.h"

using namespace vispwr;

namespace vispwr
{
  /// IQ replayer for NCAR IWRF format
  class replay_iwrf : public stage
  {
  protected:
    replay_iwrf();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process_idle() -> std::chrono::milliseconds override;
    auto process(shared_ptr<resend_request const> data, int input) -> void override;

  private:
    using time_point = std::chrono::steady_clock::time_point;

  private:
    auto send_calibration() -> void;

  private:
    string              path_;        ///< Path of file being replayed
    bool                loop_;        ///< Whether to loop or pause when reaching the end of file

    int                 calib_channel_;

    uint64_t            next_id_;     ///< Sequential ID for next pulse to output

    unique_ptr<IwrfTsReader>  file_;
  };
}

static constexpr auto plist = std::array<string_view, 2>{ "File", "Calib" };
static constexpr auto ilist = std::array<input_meta, 0>
{{
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
   { "pulse"sv, data_type::pulse, "IQ data for a single pulse" }
}};

static const auto desc = R"(
Read and replay IQ data from the NCAR IWRF time series format.

**Input:** None

**Output:** Pulses
)";

static auto ireg = stage::enrol<replay_iwrf>("replay_iwrf", "Replay IWRF", "Input", desc);

static constexpr auto iwrf_debug = IWRF_DEBUG_OFF; // IWRF_DEBUG_EXTRA;
static auto open_file(string path) -> unique_ptr<IwrfTsReader>
{
  auto file = make_unique<IwrfTsReaderFile>(vector<string>{path}, iwrf_debug);
  //file->setSigmetLegacyUnpacking(true);
  return file;
}

replay_iwrf::replay_iwrf()
  : stage{plist, ilist, olist}
  , calib_channel_{0}
  , next_id_{0}
{ }

auto replay_iwrf::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto replay_iwrf::get_parameter_impl(string_view name) const -> parameter
{
  if (name == "File")
    return parameter_path
    {
      .name = "File",
      .description = "Path of IWRF file",
      .filter = "IWRF files (*.iwrf*)",
      .value = path_,
    };
  if (name == "Calib")
    return parameter_integer
    {
      .name = "Calib",
      .description = "Output calibration channel",
      .min = 0,
      .max = 10,
      .value = calib_channel_,
    };
  return {};
}

auto replay_iwrf::set_parameter_impl(string_view name, parameter_value value) -> void
{
  // TODO - protect parameters from changing during execution
  if (name == "File")
  {
    path_ = std::get<string>(value);
    if (path_.empty())
      file_.reset();
    else
      file_ = open_file(path_);
    next_id_ = 0;
    // TODO - signal the cv to wake a thread
  }
  else if (name == "Calib")
    calib_channel_ = std::get<int>(value);
}

auto replay_iwrf::process_idle() -> std::chrono::milliseconds
{
  // if we have no file, tell the system we want to block forever by returning a negative timeout
  // the act of selecting a file via set_parameter will unblock the worker thread
  if (!file_)
    return -1ms;

  // process the file one item at a time
  if (auto iwrf_pulse = unique_ptr<IwrfTsPulse>{file_->getNextPulse()})
  {
    // calibration data is only loaded when reading first pulse, so update it on the first pulse of each pass through
    if (next_id_ == 0)
      send_calibration();

    #if 1
    static uint64_t foo = 0;
    //if (foo++ % 300 == 0)
    if (foo > 3000 && foo < 3400)
      fmt::print(
            "got a pulse: gates={} spacing={} channels={} time={:0.4f} azi={:0.3f} prf={} pw={} hpwr={} hpol={}\n"
          , iwrf_pulse->getNGates()
          , iwrf_pulse->get_gate_spacing_km()
          , iwrf_pulse->get_n_channels()
          , iwrf_pulse->getFTime()
          , iwrf_pulse->getAz()
          , iwrf_pulse->getPrf()
          , iwrf_pulse->getPulseWidthUs()
          , iwrf_pulse->getMeasXmitPowerDbmH()
          , iwrf_pulse->isHoriz()
          );
    ++foo;
    #endif

    //iwrf_pulse->cohereIqToBurstPhase();
    iwrf_pulse->convertToFL32();

    auto p = make_shared<pulse>();

    p->id = next_id_++;
    p->source_id = iwrf_pulse->getSeqNum();
    p->elevation = iwrf_pulse->getEl();
    p->azimuth = iwrf_pulse->getAz();
    p->range_start = iwrf_pulse->get_start_range_km();
    p->range_step = iwrf_pulse->get_gate_spacing_km();
    #if 0
    p->prf = iwrf_pulse->getPrf() * 1000000.0f; // Mhz to hz
    #else
    p->prf = 1000.0;
    #endif
    p->prf_index = int(p->prf); // TODO

    // determine the polarization mode
    switch (file_->getOpsInfo().getTsProcessing().xmit_rcv_mode)
    {
    case IWRF_SINGLE_POL: // single polarization - H
      p->tx_polarization = polarization::h;
      break;
    case IWRF_ALT_HV_CO_ONLY: // Dual pol, alternating transmission, copolar receiver only
    case IWRF_ALT_HV_CO_CROSS: // Dual pol, alternating transmission, co-polar and cross-polar receivers
    case IWRF_ALT_HV_FIXED_HV: // Dual pol, alternating transmission, fixed H and V receivers
      p->tx_polarization = iwrf_pulse->isHoriz() ? polarization::h : polarization::v;
      break;
    case IWRF_SIM_HV_FIXED_HV: // Dual pol, simultaneous transmission, fixed H and V receivers
    case IWRF_SIM_HV_SWITCHED_HV: // Dual pol, simultaneous transmission, switching H and V receivers
      p->tx_polarization = polarization::hv;
      break;
    case IWRF_H_ONLY_FIXED_HV: // Dual pol, H transmission, fixed H and V receivers
      p->tx_polarization = polarization::h;
      break;
    case IWRF_V_ONLY_FIXED_HV: // Dual pol, V transmission, fixed H and V receivers
      p->tx_polarization = polarization::v;
      break;
    case IWRF_ALT_HHVV_FIXED_HV: // Dual pol, alternating transmission, pulsing HHVV sequence, fixed receiver chain
      p->tx_polarization = iwrf_pulse->isHoriz() ? polarization::h : polarization::v;
      break;
    case IWRF_SINGLE_POL_V: // single polarization - V
      p->tx_polarization = polarization::v;
      break;
    };

    #if 1
    p->samples = iwrf_pulse->get_n_gates_burst();
    p->sample_start = 0.0; // TODO
    p->sample_step = 0.1; // TODO
    #else
    p->samples = 0;
    p->sample_start = 0.0;
    p->sample_step = 0.1;
    #endif

    p->gate_count = iwrf_pulse->get_n_gates();

    if (iwrf_pulse->get_n_channels() > 0)
    {
      p->tx_magnitude_h = iwrf_pulse->get_burst_mag(0);
      p->tx_phase_h = iwrf_pulse->get_burst_arg(0);
      //fmt::print("mag {} phase {}\n", p->tx_magnitude_h, p->tx_phase_h);
      p->matched_filter_losses_h = 0.0;//std::numeric_limits<float>::quiet_NaN(); // TODO

      #if 0
      p->iq_tx_h.resize(p->samples);
      for (auto i = 0; i < p->samples; ++i)
      {
        p->iq_tx_h[i].real(iwrf_pulse->getBurstIq0()[i * 2]);
        p->iq_tx_h[i].imag(iwrf_pulse->getBurstIq0()[i * 2 + 1]);
      }
      #endif

      p->iq_rx_h.resize(p->gate_count);
      for (auto i = 0; i < p->gate_count; ++i)
      {
        p->iq_rx_h[i].real(iwrf_pulse->getIq0()[i * 2]);
        p->iq_rx_h[i].imag(iwrf_pulse->getIq0()[i * 2 + 1]);
      }
    }
    else
    {
      p->tx_magnitude_h = std::numeric_limits<float>::quiet_NaN();
      p->tx_phase_h = std::numeric_limits<float>::quiet_NaN();
      p->matched_filter_losses_h = std::numeric_limits<float>::quiet_NaN();
    }

    if (iwrf_pulse->get_n_channels() > 1)
    {
      p->tx_magnitude_v = iwrf_pulse->get_burst_mag(1);
      p->tx_phase_v = iwrf_pulse->get_burst_arg(1);
      p->matched_filter_losses_v = 0.0;//std::numeric_limits<float>::quiet_NaN(); // TODO

      #if 0
      p->iq_tx_v.resize(p->samples);
      for (auto i = 0; i < p->samples; ++i)
      {
        p->iq_tx_v[i].real(iwrf_pulse->getBurstIq1()[i * 2]);
        p->iq_tx_v[i].imag(iwrf_pulse->getBurstIq1()[i * 2 + 1]);
      }
      #endif

      p->iq_rx_v.resize(p->gate_count);
      for (auto i = 0; i < p->gate_count; ++i)
      {
        p->iq_rx_v[i].real(iwrf_pulse->getIq0()[i * 2]);
        p->iq_rx_v[i].imag(iwrf_pulse->getIq0()[i * 2 + 1]);
      }
    }
    else
    {
      p->tx_magnitude_v = std::numeric_limits<float>::quiet_NaN();
      p->tx_phase_v = std::numeric_limits<float>::quiet_NaN();
      p->matched_filter_losses_v = std::numeric_limits<float>::quiet_NaN();
    }

    send(0, std::move(p));
  }
  else
  {
    // tell downstream processes we have reached the end of the file
    broadcast(make_shared<state>("iq-end"));

    // block forever until the user requests a resend or changes file
    return -1ms;
  }

  // tell the system we want to process more data immediately (0s timeout)
  return 0ms;
}

auto replay_iwrf::process(shared_ptr<resend_request const> data, int input) -> void
{
  if (!path_.empty())
  {
    file_ = open_file(path_);
    next_id_ = 0;
    broadcast(make_shared<state>("iq-begin"));
  }
}

auto replay_iwrf::send_calibration() -> void
{
  if (!file_)
    return;

  auto c = make_shared<calibration>();

  // HACK HACK
  auto& info = file_->getOpsInfo();

  c->wavelength = info.get_radar_wavelength_cm() / 100.0;

  #if 0
  c->tx_ref_power_adu_h = 0;
  c->tx_ref_power_kw_h = info.get_calib_base_dbz_1km_hc();//216;// hack resuing for other stuff
  c->noise_power_h = info.get_calib_noise_dbm_hc();//9.4;
  c->sp_calib_h = info.get_calib_receiver_gain_db_hc();
  c->radar_constant_h = info.get_calib_radar_constant_h();
  #elif 0
  c->tx_ref_power_adu_h = 0;
  c->tx_ref_power_kw_h = info.get_rvp8_f_dbz_calib();
  c->noise_power_h = info.get_rvp8_f_noise_dbm(0);
  c->sp_calib_h = 0;
  c->radar_constant_h = 0;
  #else
  c->tx_ref_power_adu_h = 0;
  c->tx_ref_power_kw_h = -48.019;
  c->noise_power_h = std::pow(10.0, 0.1 * -76.577); // convert from db to linear
  c->sp_calib_h = 39.5974 * -1; // receiver gain
  c->radar_constant_h = 68.1549;
  #endif

  c->tx_ref_power_adu_v = 0;
  c->tx_ref_power_kw_v = 0;
  c->noise_power_v = std::pow(10.0, 0.1 * -76.2979); // convert from db to linear
  c->sp_calib_v = 39.5766 * -1;
  c->radar_constant_v = 68.3249;

  c->atmospheric_attenuation = 0.016;
  c->zdr_offset = -1.28;
  c->phidp_offset = 0.0;

  calibration_set(calib_channel_, std::move(c));
}
