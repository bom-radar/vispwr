/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "types.h"

/* There are pros and cons to using int rather than size_t.  I'm still not 100% sure which is the better choice:
 *
 * pros:
 * - supposed to be the fastest integer for the machine, and is nice to use by default
 * - signed types make reverse iteration easier since you can use "i >= 0" as the loop condition
 * - signed type means overflow is undefined (as opposed to modulo) which gives the compiler more freedom to optimise
 * - usually 32-bit which means it takes less storage and memory/cache bandwidth
 *
 * cons:
 * - different to size type of standard library which introduces inconsistencies
 * - 32-bit and signed means the value must be sign-extended to 64-bit. sign extension is slower than a zero extension
 *   or 64-bit load (which would be roughly equivalent) since sign-extension requires the low 32-bits to be loaded
 *   first in the hardware before sign extension can be done.
 *
 * the sign-extension issue seems like a show-stopper since in theory it impacts every call to operator[].  in practice
 * it probably not as big a deal as it seems though. after checking generated assembly for some toy examples on x86_64,
 * simple loops over the whole array only have a single sign-extension (when loading the size) not one per iteration.
 * the 'i' iterator is implemented with a 64-bit register even if the iterator variable is declared as an int, and the
 * size is similarly placed in a register and is only extended once.  however it is uncertain if this would be true in
 * more complex loops where the registers must spill, or the optimiser cannot determine that the size of the container
 * is constant through the whole loop. */

namespace vispwr
{
  template <typename T>
  concept TriviallyCopyable = std::is_trivially_copyable_v<T>;

  /// Allocate aligned memory
  /** This function is guaranteed to produce memory which is nicely aligned to support SIMD instruction sets and other
   *  optimizations.  It is currently implemented using the aligned memory allocator from the FFTW library.  Unlike
   *  regular malloc() this function will throw an exception if the requested memory cannot be allocated. */
  auto aligned_malloc(size_t size) -> void*;

  /// Free aligned memory
  auto aligned_free(void* ptr) -> void;

  /// Deleter type for use with smart pointers and containers that simply calls aligned_free
  template <TriviallyCopyable T>
  struct aligned_deleter { auto operator()(T* ptr) -> void { aligned_free(ptr); } };

  /// 1D array
  /** This class guarantees that memory alignment is suitable for use with the fourier transform library. */
  template <TriviallyCopyable T>
  class array1
  {
  public:
    using size_type = std::size_t;
    using shape_type = std::array<size_type, 1>;
    using value_type = T;

  public:
    /// Default constructor
    array1() : size_{0} { }

    /// Sized constructor
    explicit array1(size_type size)
      : size_{size}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    { }

    /// Copy constructor
    array1(array1 const& rhs)
      : size_{rhs.size_}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    {
      for (size_type i = 0; i < size_; ++i)
        data_[i] = rhs.data_[i];
    }

    /// Move constructor
    array1(array1&& rhs) noexcept = default;

    /// Copy assignment
    auto operator=(array1 const& rhs) -> array1&
    {
      if (size_ != rhs.size_)
      {
        data_ = storage_ptr{static_cast<T*>(aligned_malloc(sizeof(T) * rhs.size_))};
        size_ = rhs.size_;
      }
      for (size_type i = 0; i < size_; ++i)
        data_[i] = rhs.data_[i];
      return *this;
    }

    /// Move assignment
    auto operator=(array1&& rhs) noexcept -> array1& = default;

    /// Resize the array without preserving prior contents
    auto resize(size_type size)
    {
      if (size_ == size)
        return;
      data_.reset(static_cast<T*>(aligned_malloc(sizeof(T) * size)));
      size_ = size;
    }

    /// Resize the array and preserve original contents
    auto resize_preserve(size_type size, value_type fill)
    {
      if (size_ == size)
        return;
      auto data = storage_ptr{static_cast<T*>(aligned_malloc(sizeof(T) * size))};

      for (size_type i = 0; i < std::min(size_, size); ++i)
        data[i] = data_[i];
      for (size_type i = std::min(size_, size); i < size; ++i)
        data[i] = fill;

      size_ = size;
      data_ = std::move(data);
    }

    /// Get the number of elements in the array
    auto size() const { return size_; }

    /// Get size of data in bytes (useful for I/O)
    auto size_bytes() const { return size_ * size_type(sizeof(value_type)); }

    /// Get the number of elements in each dimension
    auto shape() const { return shape_type{size_}; }

    /// Get the raw data pointer
    auto data() { return data_.get(); }

    /// Get the raw data pointer
    auto data() const { return (T const*) data_.get(); }

    /// Get a reference to an element of the array
    auto& operator[](size_type i) { return data_[i]; }

    /// Get a reference to an element of the array
    auto& operator[](size_type i) const { return data_[i]; }

    /// Get begin iterator
    auto begin() { return data_.get(); }

    /// Get begin iterator
    auto begin() const { return (T const*) data_.get(); }

    /// Get begin iterator
    auto cbegin() const { return (T const*) data_.get(); }

    /// Get end iterator
    auto end() { return data_.get() + size_; }

    /// Get end iterator
    auto end() const { return (T const*) data_.get() + size_; }

    /// Get end iterator
    auto cend() const { return (T const*) data_.get() + size_; }

  private:
    using storage_ptr = std::unique_ptr<T[], aligned_deleter<T>>;

  private:
    size_type   size_;
    storage_ptr data_;
  };

  using array1i = array1<int>;
  using array1l = array1<long>;
  using array1f = array1<float>;
  using array1d = array1<double>;
  using array1cf = array1<complex<float>>;
  using array1cd = array1<complex<float>>;

  /// 2D array
  /** This class guarantees that memory alignment is suitable for use with the fourier transform library. */
  template <TriviallyCopyable T>
  class array2
  {
  public:
    using size_type = std::size_t;
    using shape_type = std::array<size_type, 2>;
    using value_type = T;

  public:
    /// Default constructor
    array2() : shape_{0, 0}, size_{0} { }

    /// Sized constructor
    explicit array2(shape_type shape)
      : shape_{shape}
      , size_{shape_[0] * shape_[1]}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    { }

    /// Sized constructor
    array2(size_type y, size_type x)
      : shape_{y, x}
      , size_{shape_[0] * shape_[1]}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    { }

    /// Copy constructor
    array2(array2 const& rhs)
      : shape_{rhs.shape_}
      , size_{rhs.size_}
      , data_{static_cast<T*>(aligned_malloc(sizeof(T) * size_))}
    {
      for (size_type i = 0; i < size_; ++i)
        data_[i] = rhs.data_[i];
    }

    /// Move constructor
    array2(array2&& rhs) noexcept = default;

    /// Copy assignment
    auto operator=(array2 const& rhs) -> array2&
    {
      if (size_ != rhs.size_)
      {
        data_ = storage_ptr{static_cast<T*>(aligned_malloc(sizeof(T) * rhs.size_))};
        shape_ = rhs.shape_;
        size_ = rhs.size_;
      }
      for (size_type i = 0; i < size_; ++i)
        data_[i] = rhs.data_[i];
      return *this;
    }

    /// Move assignment
    auto operator=(array2&& rhs) noexcept -> array2& = default;

    /// Resize the array without preserving prior contents
    auto resize(shape_type shape)
    {
      if (shape_ == shape)
        return;
      auto size = shape[0] * shape[1];
      if (size_ != size)
        data_.reset(static_cast<T*>(aligned_malloc(sizeof(T) * size)));
      shape_ = shape;
      size_ = size;
    }

    /// Resize the array without preserving prior contents
    auto resize(size_type y, size_type x)
    {
      resize({y, x});
    }

    /// Resize the array and preserve original contents
    auto resize_preserve(shape_type shape, value_type fill)
    {
      if (shape_ == shape)
        return;
      auto size = shape[0] * shape[1];
      auto data = storage_ptr{static_cast<T*>(aligned_malloc(sizeof(T) * size))};

      for (size_type y = 0; y < std::min(shape_[0], shape[0]); ++y)
      {
        for (size_type x = 0; x < std::min(shape_[1], shape[1]); ++x)
          data[y * shape[1] + x] = data_[y * shape_[1] + x];
        for (size_type x = std::min(shape_[1], shape[1]); x < shape[1]; ++x)
          data[y * shape[1] + x] = fill;
      }
      for (size_type y = std::min(shape_[0], shape[0]); y < shape[0]; ++y)
        for (size_type x = 0; x < shape[1]; ++x)
          data[y * shape[1] + x] = fill;

      shape_ = shape;
      size_ = size;
      data_ = std::move(data);
    }

    /// Resize the array and preserve original contents
    auto resize_preserve(size_type y, size_type x, value_type fill)
    {
      resize_preserve({y, x}, fill);
    }

    /// Get the number of elements in the array
    auto size() const { return size_; }

    /// Get size of data in bytes (useful for I/O)
    auto size_bytes() const { return size_ * size_type(sizeof(value_type)); }

    /// Get the number of elements in each dimension
    auto shape() const { return shape_; }

    /// Get the raw data pointer
    auto data() { return data_.get(); }

    /// Get the raw data pointer
    auto data() const { return (T const*) data_.get(); }

    /// Get a span representing an entire row
    auto operator[](size_type y) { return std::span<value_type>{&data_[y * shape_[1]], shape_[0]}; }

    /// Get a span representing an entire row
    auto operator[](size_type y) const { return std::span<value_type const>{&data_[y * shape_[1]], shape_[0]}; }

    /// Get a reference to an element of the array
    auto& operator[](shape_type yx) { return data_[yx[0] * shape_[1] + yx[1]]; }

    /// Get a reference to an element of the array
    auto& operator[](shape_type yx) const { return data_[yx[0] * shape_[1] + yx[1]]; }

    /// Get a reference to an element of the array
    auto& operator[](size_type y, size_type x) { return data_[y * shape_[1] + x]; }

    /// Get a reference to an element of the array
    auto& operator[](size_type y, size_type x) const { return data_[y * shape_[1] + x]; }

    /// Get begin iterator
    auto begin() { return data_.get(); }

    /// Get begin iterator
    auto begin() const { return (T const*) data_.get(); }

    /// Get begin iterator
    auto cbegin() const { return (T const*) data_.get(); }

    /// Get end iterator
    auto end() { return data_.get() + size_; }

    /// Get end iterator
    auto end() const { return (T const*) data_.get() + size_; }

    /// Get end iterator
    auto cend() const { return (T const*) data_.get() + size_; }

  private:
    using storage_ptr = std::unique_ptr<T[], aligned_deleter<T>>;

  private:
    shape_type  shape_;
    size_type   size_;
    storage_ptr data_;
  };

  using array2i = array2<int>;
  using array2l = array2<long>;
  using array2f = array2<float>;
  using array2d = array2<double>;
  using array2cf = array2<complex<float>>;
  using array2cd = array2<complex<float>>;
}
