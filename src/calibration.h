/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2022 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#pragma once

#include "types.h"

namespace vispwr
{
  /// Calibration constants
  struct calibration
  {
    double            wavelength;

    double            tx_ref_power_adu_h;
    double            tx_ref_power_kw_h;
    double            noise_power_h;
    double            sp_calib_h;
    double            radar_constant_h;

    double            tx_ref_power_adu_v;
    double            tx_ref_power_kw_v;
    double            noise_power_v;
    double            sp_calib_v;
    double            radar_constant_v;

    double            atmospheric_attenuation;
    double            zdr_offset;
    double            phidp_offset;
  };

  /// Get the current value of a calibration channel
  auto calibration_get(int channel) -> shared_ptr<calibration const>;

  /// Set the current value of a calibration channel
  auto calibration_set(int channel, shared_ptr<calibration const> val) -> void;

  /// Clear all calibration channels
  auto calibration_clear(int channel) -> void;

  /// Clear all calibration channels
  auto calibration_clear_all() -> void;
}
