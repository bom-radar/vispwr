/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "format.h"

#include <iostream>

using namespace vispwr;

namespace vispwr
{
  /// Calculated autocorrelations using time series algorithm
  class autocor_time : public stage
  {
  protected:
    autocor_time();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<batch const> data, int input) -> void override;

  private:
    auto calculate_autocorrelations(array2cf const& iq) const -> array1<autocorrelations::acset>;
    auto calculate_crosscorrelations(array2cf const& iq_h, array2cf const& iq_v) const -> array1<autocorrelations::xcset>;
  };
}

static constexpr auto plist = std::array<string_view, 0>{};
static constexpr auto ilist = std::array<input_meta, 1>
{{
  { "batch", data_type::batch, true, "Batch to determine autocorrelations for" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
  { "acs", data_type::autocorrelations, "Lag 0,1,2 autocorrelations for every range gate in batch" }
}};

static const auto desc = R"(
Time domain autocorrelation processing.

**Input:** Batches

**Output:** Autocorrelations
)";

static auto ireg = stage::enrol<autocor_time>("autocor_time", "Autocors (time)", "Process", desc);

autocor_time::autocor_time()
  : stage{plist, ilist, olist}
{ }

auto autocor_time::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto autocor_time::get_parameter_impl(string_view name) const -> parameter
{
  return {};
}

auto autocor_time::set_parameter_impl(string_view name, parameter_value value) -> void
{ }

auto autocor_time::process(shared_ptr<batch const> data, int input) -> void
{
  if (data->pulse_count < 3)
  {
    output_log(log_level::warning, fmt::format("Ignoring batch {} with insufficient pulse count of {}", data->id, data->pulse_count));
    return;
  }

  auto a = make_shared<autocorrelations>();

  // since we are 1:1 input/output we can just propogate the batch ID as our ID
  a->id = data->id;
  a->batch_id = data->id;
  a->prf = data->prf;
  a->prf_index = data->prf_index;
  a->elevation_first = data->pulses[0].elevation;
  a->elevation_last = data->pulses[data->pulse_count - 1].elevation;
  a->azimuth_first = data->pulses[0].azimuth;
  a->azimuth_last = data->pulses[data->pulse_count - 1].azimuth;
  a->range_start = data->range_start;
  a->range_step = data->range_step;
  a->gate_count = data->gate_count;

  if (data->iq_rx_h.size() != 0)
  {
    auto avg = 0.0;
    for (auto i = 0uz; i < data->pulses.size(); ++i)
      avg += data->pulses[i].matched_filter_losses_h;
    a->matched_filter_losses_h = avg / data->pulse_count;

    a->autocors_h = calculate_autocorrelations(data->iq_rx_h);
  }
  else
    a->matched_filter_losses_h = std::numeric_limits<float>::quiet_NaN();

  if (data->iq_rx_v.size() != 0)
  {
    auto avg = 0.0;
    for (auto i = 0uz; i < data->pulses.size(); ++i)
      avg += data->pulses[i].matched_filter_losses_v;
    a->matched_filter_losses_v = avg / data->pulse_count;

    a->autocors_v = calculate_autocorrelations(data->iq_rx_v);
  }
  else
    a->matched_filter_losses_v = std::numeric_limits<float>::quiet_NaN();

  if (data->iq_rx_h.size() != 0 && data->iq_rx_v.size() != 0)
    a->crosscors = calculate_crosscorrelations(data->iq_rx_h, data->iq_rx_v);

  send(0, std::move(a));
}

auto autocor_time::calculate_autocorrelations(array2cf const& iq) const -> array1<autocorrelations::acset>
{
  auto gate_count = iq.shape()[0];
  auto pulse_count = iq.shape()[1];

  auto acs = array1<autocorrelations::acset>{gate_count};
  for (auto igate = 0uz; igate < gate_count; ++igate)
  {
    auto iqg = iq[igate];
    auto& acg = acs[igate];

#if 0
    // first pulse
    acg.lag0.real(iqg[0].i * iqg[0].i + iqg[0].q * iqg[0].q);

    // second pulse
    acg.lag0.real(acg.lag0.real() + (iqg[1].i * iqg[1].i + iqg[1].q * iqg[1].q));
    acg.lag1.real(iqg[1].i * iqg[0].i + iqg[1].q * iqg[0].q);
    acg.lag1.imag(iqg[1].q * iqg[0].i - iqg[1].i * iqg[0].q);

    // third pulse
    acg.lag0.real(acg.lag0.real() + (iqg[2].i * iqg[2].i + iqg[2].q * iqg[2].q));
    acg.lag1.real(acg.lag1.real() + (iqg[2].i * iqg[1].i + iqg[2].q * iqg[1].q));
    acg.lag1.imag(acg.lag1.imag() + (iqg[2].q * iqg[1].i - iqg[2].i * iqg[1].q));
    acg.lag2.real(iqg[2].i * iqg[0].i + iqg[2].q * iqg[0].q);
    acg.lag2.imag(iqg[2].q * iqg[0].i - iqg[2].i * iqg[0].q);

    // fourth to N pulses
    for (auto i = 3uz; i < pulse_count; ++i)
    {
      acg.lag0.real(acg.lag0.real() + (iqg[i].i * iqg[ i ].i + iqg[i].q * iqg[ i ].q));
      acg.lag1.real(acg.lag1.real() + (iqg[i].i * iqg[i-1].i + iqg[i].q * iqg[i-1].q));
      acg.lag1.imag(acg.lag1.imag() + (iqg[i].q * iqg[i-1].i - iqg[i].i * iqg[i-1].q));
      acg.lag2.real(acg.lag2.real() + (iqg[i].i * iqg[i-2].i + iqg[i].q * iqg[i-2].q));
      acg.lag2.imag(acg.lag2.imag() + (iqg[i].q * iqg[i-2].i - iqg[i].i * iqg[i-2].q));
    }
#else // assumes iq is a std::complex
    // first pulse
    acg.lag0  = iqg[0] * conj(iqg[0]);

    // second pulse
    acg.lag0 += iqg[1] * conj(iqg[1]);
    acg.lag1  = iqg[1] * conj(iqg[0]);

    // third pulse
    acg.lag0 += iqg[2] * conj(iqg[2]);
    acg.lag1 += iqg[2] * conj(iqg[1]);
    acg.lag2  = iqg[2] * conj(iqg[0]);

    // fourth to N pulses
    for (auto i = 3uz; i < pulse_count; ++i)
    {
      acg.lag0 += iqg[i] * conj(iqg[i]);
      acg.lag1 += iqg[i] * conj(iqg[i-1]);
      acg.lag2 += iqg[i] * conj(iqg[i-2]);
    }
#endif

    // finalize
    acg.lag0 /= pulse_count;
    acg.lag1 /= (pulse_count - 1);
    acg.lag2 /= (pulse_count - 2);
  }
  return acs;
}

auto autocor_time::calculate_crosscorrelations(array2cf const& iq_h, array2cf const& iq_v) const -> array1<autocorrelations::xcset>
{
  auto gate_count = iq_h.shape()[0];
  auto pulse_count = iq_v.shape()[1];

  auto xcs = array1<autocorrelations::xcset>{gate_count};
  for (auto igate = 0uz; igate < gate_count; ++igate)
  {
    auto iqg_h = iq_h[igate];
    auto iqg_v = iq_v[igate];
    auto& xcg = xcs[igate];

    #if 0
    xcg.lag0.real(iqg_h[0].i * iqg_v[0].i + iqg_h[0].q * iqg_v[0].q);
    xcg.lag0.imag(iqg_h[0].q * iqg_v[0].i - iqg_h[0].i * iqg_v[0].q);

    for (auto i = 1; i < pulse_count; ++i)
    {
      xcg.lag0.real(xcg.lag0.real() + (iqg_h[i].i * iqg_v[i].i + iqg_h[i].q * iqg_v[i].q));
      xcg.lag0.imag(xcg.lag0.imag() + (iqg_h[i].q * iqg_v[i].i - iqg_h[i].i * iqg_v[i].q));
    }
    #else
    xcg.lag0 = iqg_h[0] * conj(iqg_v[0]);
    for (auto i = 1uz; i < pulse_count; ++i)
      xcg.lag0 += iqg_h[i] * conj(iqg_v[i]);
    xcg.lag0 /= pulse_count;
    #endif
  }
  return xcs;
}
