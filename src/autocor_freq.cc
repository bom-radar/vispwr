/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"
#include "fft.h"
#include "format.h"

#include <iostream>

using namespace vispwr;

namespace vispwr
{
  class autocor_freq : public stage
  {
  protected:
    autocor_freq();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<spectra const> data, int input) -> void override;

  private:
    auto calculate_autocorrelations(array2cf const& fiq) -> array1<autocorrelations::acset>;
    auto calculate_crosscorrelations(array2cf const& fiq_h, array2cf const& fiq_v) -> array1<autocorrelations::xcset>;
  };
}

static constexpr auto plist = std::array<string_view, 0>{};
static constexpr auto ilist = std::array<input_meta, 1>
{{
  { "spectra", data_type::spectra, true, "Spectra to determine autocorrelations from" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
  { "acs", data_type::autocorrelations, "Lag 0,1,2 autocorrelations for every range gate in batch" }
}};

static const auto desc = R"(
Frequency domain autocorrelation processing.

**Input:** Spectra

**Output:** Autocorrelations
)";

static auto ireg = stage::enrol<autocor_freq>("autocor_freq", "Autocors (freq)", "Process", desc);

autocor_freq::autocor_freq()
  : stage{plist, ilist, olist}
{ }

auto autocor_freq::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto autocor_freq::get_parameter_impl(string_view name) const -> parameter
{
  return {};
}

auto autocor_freq::set_parameter_impl(string_view name, parameter_value value) -> void
{ }

auto autocor_freq::process(shared_ptr<spectra const> data, int input) -> void
{
  auto a = make_shared<autocorrelations>();

  // since we are 1:1 input/output we can just propogate the batch ID as our ID
  a->id = data->id;
  a->batch_id = data->id;
  a->prf = data->prf;
  a->prf_index = data->prf_index;
  a->elevation_first = data->pulses[0].elevation;
  a->elevation_last = data->pulses[data->pulse_count - 1].elevation;
  a->azimuth_first = data->pulses[0].azimuth;
  a->azimuth_last = data->pulses[data->pulse_count - 1].azimuth;
  a->range_start = data->range_start;
  a->range_step = data->range_step;
  a->gate_count = data->gate_count;

  if (data->fiq_rx_h.size() != 0)
  {
    auto avg = 0.0;
    for (auto i = 0uz; i < data->pulses.size(); ++i)
      avg += data->pulses[i].matched_filter_losses_h;
    a->matched_filter_losses_h = avg / data->pulse_count;

    a->autocors_h = calculate_autocorrelations(data->fiq_rx_h);
  }

  if (data->fiq_rx_v.size() != 0)
  {
    auto avg = 0.0;
    for (auto i = 0uz; i < data->pulses.size(); ++i)
      avg += data->pulses[i].matched_filter_losses_v;
    a->matched_filter_losses_v = avg / data->pulse_count;

    a->autocors_v = calculate_autocorrelations(data->fiq_rx_v);
  }

  if (data->fiq_rx_h.size() != 0 && data->fiq_rx_v.size() != 0)
    a->crosscors = calculate_crosscorrelations(data->fiq_rx_h, data->fiq_rx_v);

  send(0, std::move(a));
}

auto autocor_freq::calculate_autocorrelations(array2cf const& fiq) -> array1<autocorrelations::acset>
{
  auto gate_count = fiq.shape()[0];
  auto fft_size = fiq.shape()[1];

  auto fbuf = array2cf{gate_count, fft_size};
  auto tbuf = array2cf{gate_count, fft_size};

  // copy in data while multipling each element by it's conjugate
  for (auto igate = 0uz; igate < gate_count; ++igate)
    for (auto i = 0uz; i < fft_size; ++i)
      fbuf[igate, i] = fiq[igate, i] * std::conj(fiq[igate, i]);

  // invert the DFTs
  fft_rev(fbuf, tbuf);

  // copy out the lags
  auto acs = array1<autocorrelations::acset>{gate_count};
  for (auto igate = 0uz; igate < gate_count; ++igate)
  {
    acs[igate].lag0 = tbuf[igate, 0];
    acs[igate].lag1 = tbuf[igate, 1];
    acs[igate].lag2 = tbuf[igate, 2];
  }
  return acs;
}

auto autocor_freq::calculate_crosscorrelations(array2cf const& fiq_h, array2cf const& fiq_v) -> array1<autocorrelations::xcset>
{
  auto gate_count = fiq_h.shape()[0];
  auto fft_size = fiq_h.shape()[1];

  auto fbuf = array2cf{gate_count, fft_size};
  auto tbuf = array2cf{gate_count, fft_size};

  // copy in data while multipling each element by it's conjugate
  for (auto igate = 0uz; igate < gate_count; ++igate)
    for (auto i = 0uz; i < fft_size; ++i)
      fbuf[igate, i] = fiq_h[igate, i] * std::conj(fiq_v[igate, i]);

  // invert the DFTs
  fft_rev(fbuf, tbuf);

  // copy out the lags
  auto xcs = array1<autocorrelations::xcset>{gate_count};
  for (auto igate = 0uz; igate < gate_count; ++igate)
    xcs[igate].lag0 = tbuf[igate, 0];

  return xcs;
}
