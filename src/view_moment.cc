/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2021 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#if 0
#include "stage.h"
#include "color_map.h"
#include "format.h"
#include "pick.h"

#include <QPainter>
#include <QVBoxLayout>
#include <QLabel>
#include <QMouseEvent>
#include <QWidget>
#include <QPixmap>
#include <QScrollArea>
#include <QScrollBar>

using namespace vispwr;

namespace vispwr
{
  class moment_canvas : public QWidget
  {
    Q_OBJECT
  public:
    moment_canvas(QWidget* parent = nullptr)
      : QWidget{parent}
    {
      setMouseTracking(true);
    }

  signals:
    void mouse_moved(QMouseEvent* event);
    void mouse_pressed(QMouseEvent* event);
    void mouse_wheeled(QWheelEvent* event);
    void painted(QPaintEvent* event);

  protected:
    auto mouseMoveEvent(QMouseEvent* event) -> void override { mouse_moved(event); }
    auto mousePressEvent(QMouseEvent* event) -> void override { mouse_pressed(event); }
    auto wheelEvent(QWheelEvent* event) -> void override { mouse_wheeled(event); }
    auto paintEvent(QPaintEvent* event) -> void override { painted(event); }
  };

  class draggable_scroll_area : public QScrollArea
  {
    Q_OBJECT
  public:
    draggable_scroll_area(QWidget* parent = nullptr)
      : QScrollArea{parent}
    {
      installEventFilter(this);
    }

  protected:
    auto eventFilter(QObject* obj, QEvent* event) -> bool
    {
      if (event->type() == QEvent::MouseMove)
      {
        auto e = static_cast<QMouseEvent*>(event);
        if (e->buttons() & Qt::LeftButton)
        {
          if (spos_)
          {
            horizontalScrollBar()->setValue(horizontalScrollBar()->value() + spos_->x() - e->globalPos().x());
            verticalScrollBar()->setValue(verticalScrollBar()->value() + spos_->y() - e->globalPos().y());
          }
          spos_ = e->globalPos();
          return true;
        }
      }
      else if (event->type() == QEvent::MouseButtonRelease)
        spos_.reset();

      return QObject::eventFilter(obj, event);
    }

  private:
    optional<QPoint> spos_; // drag start position
  };

  class view_moment;

  class ascope_window : public QWidget
  {
    Q_OBJECT
  public:
    ascope_window(view_moment* stage);

    auto hack_update_scheme(string_view name, double min, double max) -> void;

  signals:
    void data_updated(QRect dirty);

  private:
    auto on_data_updated(QRect dirty) -> void;
    auto on_motion(QMouseEvent* event) -> void;
    auto on_click(QMouseEvent* event) -> void;
    auto on_wheel(QWheelEvent* event) -> void;
    auto on_paint(QPaintEvent* event) -> void;

    auto update_zoom() -> void;

  private:
    view_moment*          stage_;
    QLabel                label_cursor_;
    QLabel                label_pick_;
    draggable_scroll_area scroll_;
    moment_canvas         canvas_;
    float                 zoom_;
    float                 scale_;
  };

  class view_moment : public stage
  {
  protected:
    view_moment();
    auto metadata() const -> stage_metadata const& override;
    auto save() const -> json override;
    auto load(json const& j) -> void override;
    auto get_parameter(string_view name) const -> parameter_value override;
    auto set_parameter(string_view name, parameter_value value) -> void override;
    auto execute() -> void override;

  protected:
    struct cursor_info
    {
      int   ray;      // index of ray
      int   gate;     // index of gate
      float azimuth;  // central azimuth for ray
      float range;    // central azimuth for gate
      float value;    // value of moment at ray/gate
    };

    // get information about what is under the cursor
    // x/y here are normalized to (0..1) so that we don't need to worry about zoom level
    auto pick_cursor(float y, float x) -> optional<cursor_info>;

    // send a pick datum to downstream processes
    auto send_pick(int ray, int gate) -> void;

  private:
    string            moment_;
    string            cmap_name_;
    color_map         cmap_;
    int               picker_;

    moment_list_ptr   moment_list_;     // most recently received moment list
    int               imoment_;         // index of 'moment_' in most recently received moment list

    vector<pair<float, float>>         azimuths_;  // temp - central azimuths for picking
    float                 range_start_; // tmep - most recent range start
    float                 range_step_; // temp - most recent range step
    array2f               data_;

    int                   max_rays_;    // maximum ray ID we have written since last seeing ray 0
    int                   max_gates_;   // maximum number of gates we have written since last ray 0

    ascope_window     window_;

    friend class ascope_window;
  };
}

// HACK
static constexpr auto cmap_names = std::array<string_view, 8>
{{
    "Reflectivity"
  , "Velocity"
  , "SpWidth"
  , "RhoHV"
  , "ZDR"
  , "PhiDP"
  , "KDP"
  , "Jet"
}};
static auto open_color_map(string_view name, float min, float max)
{
  // lookup the standard map name
  auto iname = std::ranges::find(cmap_names, name);
  if (iname == cmap_names.end())
    throw std::runtime_error{fmt::format("Unknown color map {}", name)};

  // convert to our standard map enum
  auto id = static_cast<standard_color_map>(iname - cmap_names.begin());

  // reinitialize the color map and remap min/max if desired
  return color_map{id, min, max};
}
inline constexpr std::array<string_view, 13> master_moment_list
{
    "DBZH"
  , "VRADH"
  , "WRADH"
  , "SQIH"
  , "SNRH"
  , "DBZV"
  , "VRADV"
  , "WRADV"
  , "SQIV"
  , "SNRV"
  , "ZDR"
  , "PHIDP"
  , "RHOHV"
};
struct moment_pref
{
  string_view cmap_name;
  double min;
  double max;
};
static auto const moment_prefs = std::map<string_view, moment_pref>
{
    { "DBZH",   { "Reflectivity", -32.0, 60.0 } }
  , { "VRADH",  { "Velocity", -20.0, 20.0 } }
  , { "WRADH",  { "SpWidth", 0.0, 10.0 } }
  , { "SQIH",   { "Jet", 0.0, 1.0 } }
  , { "SNRH",   { "Jet", 0.0, 30.0 } }
  , { "DBZV",   { "Reflectivity", -32.0, 60.0 } }
  , { "VRADV",  { "Velocity", -20.0, 20.0 } }
  , { "WRADV",  { "SpWidth", 0.0, 10.0 } }
  , { "SQIV",   { "Jet", 0.0, 1.0 } }
  , { "SNRV",   { "Jet", 0.0, 30.0 } }
  , { "ZDR",    { "ZDR", -6.0, 6.0 } }
  , { "PHIDP",  { "PhiDP", 0.0, 360.0 } }
  , { "RHOHV",  { "RhoHV", 0.0, 1.0 } }
};
// END HACK

static constexpr auto plist = std::array<parameter, 5>
{{
    parameter_enumerate{ "Moment", "Moment to display", master_moment_list, false } // for now force to known list
  , parameter_enumerate{ "Scheme", "Color scheme", cmap_names, false }
  , parameter_real{ "Min Val", "Minimum value", 2, -999.0, 999.0, 1.0 }
  , parameter_real{ "Max Val", "Maximum value", 2, -999.0, 999.0, 1.0 }
  , parameter_integer{ "Picker", "Output pick channel for right clicks", 0, pick::channels - 1, 1 }
}};
static constexpr auto ilist = std::array<input, 1>
{{
    { "moments", data_type::moments, true, "Input moments" }
}};
static constexpr auto olist = std::array<output, 0>
{{
}};

static auto ireg = stage::enrol<view_moment>("view_moment", "A-Scope", "Display");

view_moment::view_moment()
  : stage{plist, ilist, olist}
  , moment_{"DBZH"}
  , cmap_name_{"Reflectivity"}
  , cmap_{open_color_map(cmap_name_, -32.0f, 60.0f)}
  , picker_{0}
  , imoment_{-1}
  , max_rays_{0}
  , max_gates_{0}
  , window_{this}
{ }

auto view_moment::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto view_moment::save() const -> json
{
  auto ret = stage::save();
  auto& jobj = ret.object();
  jobj.emplace("win_x", json{window_.x()});
  jobj.emplace("win_y", json{window_.y()});
  jobj.emplace("win_width", json{window_.width()});
  jobj.emplace("win_height", json{window_.height()});
  return ret;
}

auto view_moment::load(json const& j) -> void
{
  stage::load(j);
  window_.setGeometry(
        j["win_x"].number<int>()
      , j["win_y"].number<int>()
      , j["win_width"].number<int>()
      , j["win_height"].number<int>());
}

auto view_moment::get_parameter(string_view name) const -> parameter_value
{
  if (name == "Moment")
    return moment_;
  if (name == "Scheme")
    return cmap_name_;
  if (name == "Min Val")
    return cmap_.min();
  if (name == "Max Val")
    return cmap_.max();
  if (name == "Picker")
    return picker_;
  return stage::get_parameter(name);
}

auto view_moment::set_parameter(string_view name, parameter_value value) -> void
{
  if (name == "Moment")
  {
    moment_ = std::get<string>(value);
    imoment_ = -1;

    if (moment_list_)
    {
      auto imom = std::find(moment_list_->begin(), moment_list_->end(), moment_);
      if (imom != moment_list_->end())
        imoment_ = imom - moment_list_->begin();
    }

    // HACK HACK - set default scheme when changing moment
    // this should be saved somewhere properly
    if (auto ipref = moment_prefs.find(moment_); ipref != moment_prefs.end())
      window_.hack_update_scheme(ipref->second.cmap_name, ipref->second.min, ipref->second.max);
  }
  else if (name == "Scheme")
  {
    cmap_name_ = std::get<string>(value);
    cmap_ = open_color_map(cmap_name_, cmap_.min(), cmap_.max());
  }
  else if (name == "Min Val")
    cmap_ = open_color_map(cmap_name_, std::get<double>(value), cmap_.max());
  else if (name == "Max Val")
    cmap_ = open_color_map(cmap_name_, cmap_.min(), std::get<double>(value));
  else if (name == "Picker")
    picker_ = std::get<int>(value);
  else
    stage::set_parameter(name, std::move(value));
}

auto view_moment::send_pick(int ray, int gate) -> void
{
  auto lock = acquire_lock();
  if (ray < 0 || ray >= azimuths_.size())
    return;
  auto p = pick::data{};
  p.valid = true;
  p.azimuth = std::midpoint(azimuths_[ray].first, azimuths_[ray].second);
  p.range = range_start_ + (gate + 0.5) * range_step_;
  pick::channel(picker_).set(std::move(p));
}

auto view_moment::execute() -> void
{
  auto mom = receive<moments>(0);

  // if we are missing an input it means the stage is terminating
  if (!mom)
    return;

  // the moment ID resets to 0 when our IQ replay loops or changes sweep, so it is safe to use as the ray number
  // note that this is NOT related to azimuth angle - just rays in acquisition order.  due to dual prf the rays may not
  // even have the same angular width as each other!
  auto iray = mom->id;

  // update the moment index if our moment list has changed
  if (moment_list_ != mom->moments)
  {
    moment_list_ = mom->moments;
    imoment_ = -1;

    auto imom = std::find(moment_list_->begin(), moment_list_->end(), moment_);
    if (imom != moment_list_->end())
      imoment_ = imom - moment_list_->begin();
  }

  // if this datum doesn't contain the selected moment then just skip it
  if (imoment_ == -1)
    return;

  // update the data
  {
    auto lock = acquire_lock();

    // keep note of current range start and step
    // TODO - this is a hack - what happens if different rays have different range binning???
    range_start_ = mom->range_start;
    range_step_ = mom->range_step;

    // if this is ray 0, shrink our data array to fit the previous scan
    /* this is needed when the user changes IQ file and the new stream has a different geometry.  on the first loop through
     * we will be 'overwriting' the old data line by line.  once we reach the end of the record we shrink to fit so that we
     * eliminate any excess space in the array which will only contain old data from the previous IQ record and look ugly. */
    if (mom->id == 0)
    {
      data_.resize_preserve(max_rays_, max_gates_, std::numeric_limits<float>::quiet_NaN());
      max_rays_ = iray + 1;
      max_gates_ = mom->gate_count;
    }
    else
    {
      max_rays_ = std::max(max_rays_, int(iray + 1));
      max_gates_ = std::max(max_gates_, mom->gate_count);
    }

    // expand the data stores as needed to accomodate the ray
    if (azimuths_.size() <= iray)
      azimuths_.resize(iray + 1);
    if (data_.shape()[0] <= iray || data_.shape()[1] < mom->gate_count)
      data_.resize_preserve(
            std::max(data_.shape()[0], int(iray) + 1)
          , std::max(data_.shape()[1], mom->gate_count)
          , std::numeric_limits<float>::quiet_NaN());

    // update the new points
    azimuths_[iray].first = mom->azimuth_first;
    azimuths_[iray].second = mom->azimuth_last;
    for (auto igate = 0; igate < mom->gate_count; ++igate)
      data_[iray, igate] = mom->data[igate, imoment_];

    // HACK HACK
    // fill the end of the ray with NaNs to stop visual artefacts for dual-prf in the high-prf only area
    // THIS IS BAD - it will break second trip recovery!!!
    for (auto igate = mom->gate_count; igate < data_.shape()[1]; ++igate)
      data_[iray, igate] = std::numeric_limits<float>::quiet_NaN();
  }

  // HACK - should only output to mom->gate_count (part of above dual-prf hack)
  window_.data_updated(QRect{0, iray, data_.shape()[1], 1});
  //window_->data_updated(QRect{0, iray, mom->gate_count, 1});
}

auto view_moment::pick_cursor(float y, float x) -> optional<cursor_info>
{
  auto lock = acquire_lock();

  auto dy = int(y * data_.shape()[0]);
  auto dx = int(x * data_.shape()[1]);

  if (dy < 0 || dy >= data_.shape()[0] || dx < 0 || dx >= data_.shape()[1])
    return std::nullopt;

  auto azi = std::midpoint(azimuths_[dy].first, azimuths_[dy].second);
  auto rng = range_start_ + (dx + 0.5f) * range_step_;
  auto val = data_[dy, dx];

  return cursor_info{ dy, dx, azi, rng, val };
}

ascope_window::ascope_window(view_moment* stage)
  : stage_{stage}
  , zoom_{0.0f}
  , scale_{1.0f}
{
  auto layout = new QVBoxLayout(this);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->setSpacing(0);

  auto toolbar = new QWidget();
  auto tb_layout = new QHBoxLayout(toolbar);
  tb_layout->addWidget(&label_cursor_);
  label_pick_.setAlignment(Qt::AlignRight);
  tb_layout->addWidget(&label_pick_);
  layout->addWidget(toolbar);

  canvas_.setBackgroundRole(QPalette::Base);
  canvas_.setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
  scroll_.setWidget(&canvas_);
  layout->addWidget(&scroll_);

  resize(600, 360);
  setWindowTitle("View Moment");
  setVisible(true);
  show();

  QObject::connect(this, &ascope_window::data_updated, this, &ascope_window::on_data_updated);
  QObject::connect(&canvas_, &moment_canvas::mouse_moved, this, &ascope_window::on_motion);
  QObject::connect(&canvas_, &moment_canvas::mouse_pressed, this, &ascope_window::on_click);
  QObject::connect(&canvas_, &moment_canvas::mouse_wheeled, this, &ascope_window::on_wheel);
  QObject::connect(&canvas_, &moment_canvas::painted, this, &ascope_window::on_paint);
}

auto ascope_window::hack_update_scheme(string_view name, double min, double max) -> void
{
  #if 0
  dynamic_cast<QComboBox&>(*param_widgets_[1]).setCurrentText(to_qstring(name));
  dynamic_cast<QDoubleSpinBox&>(*param_widgets_[2]).setValue(min);
  dynamic_cast<QDoubleSpinBox&>(*param_widgets_[3]).setValue(max);
  #endif
}

auto ascope_window::on_data_updated(QRect dirty) -> void
{
  // get the size of the data array
  int d_width, d_height;
  {
    auto lock = stage_->acquire_lock();
    d_height = stage_->data_.shape()[0];
    d_width = stage_->data_.shape()[1];
  }

  // determine the desired size of our canvas for the current zoom
  auto c_height = int(d_height * scale_);
  auto c_width = int(d_width * scale_);

  // resize the canvas if needed (will trigger a full redraw)
  if (canvas_.height() != c_height || canvas_.width() != c_width)
  {
    canvas_.setMinimumSize(c_width, c_height);
    canvas_.resize(c_width, c_height);
    return;
  }

  // translate the dirty data rect into zoom scaled coordinates
  auto x0 = int((float(dirty.x()) / d_width) * c_width);
  auto x1 = int((float(dirty.x() + dirty.width()) / d_width) * c_width) + 1;
  auto y0 = int((float(dirty.y()) / d_height) * c_height);
  auto y1 = int((float(dirty.y() + dirty.height()) / d_height) * c_height) + 1;

  canvas_.update(QRect(x0, y0, x1 - x0, y1 - y0));
}

auto ascope_window::on_motion(QMouseEvent* event) -> void
{
  auto cinfo = stage_->pick_cursor((event->y() + 0.5f) / canvas_.height(), (event->x() + 0.5f) / canvas_.width());
  if (!cinfo)
  {
    label_cursor_.setText("");
    return;
  }

  auto txt = fmt::format("Azi: {:.2f} Rng: {:.2f} Val: {:.3f}", cinfo->azimuth, cinfo->range, cinfo->value);
  label_cursor_.setText(QString::fromStdString(std::move(txt)));
}

auto ascope_window::on_click(QMouseEvent* event) -> void
{
  if (!(event->buttons() & Qt::RightButton))
    return;

  auto cinfo = stage_->pick_cursor((event->y() + 0.5f) / canvas_.height(), (event->x() + 0.5f) / canvas_.width());
  if (!cinfo)
  {
    label_pick_.setText("");
    return;
  }

  auto txt = fmt::format("Azi: {:.2f} Rng: {:.2f} Val: {:.3f}", cinfo->azimuth, cinfo->range, cinfo->value);
  label_pick_.setText(QString::fromStdString(std::move(txt)));

  stage_->send_pick(cinfo->ray, cinfo->gate);
}

auto ascope_window::on_wheel(QWheelEvent* e) -> void
{
  // accept the event here to stop it propogating to our scroll area
  e->accept();

  // increment of 120 is a 'standard' single click on a mouse wheel
  zoom_ += (e->angleDelta().y() / 120.0f);

  auto old_scale = scale_;

  update_zoom();

  // update scroll bars to maintain current position
  // this relies on knowing the delta from the previous zoom factor
  auto factor = scale_ / old_scale;
  {
    auto bar = scroll_.horizontalScrollBar();
    bar->setValue(int(factor * bar->value() + ((factor - 1) * bar->pageStep()/2)));
  }
  {
    auto bar = scroll_.verticalScrollBar();
    bar->setValue(int(factor * bar->value() + ((factor - 1) * bar->pageStep()/2)));
  }
}

auto ascope_window::update_zoom() -> void
{
  // adjust this to change the distance between zoom levels
  constexpr auto zoom_speed = 0.2f;

  // determine our scale factor based on zoom level
  scale_ = std::pow(2.0, zoom_ * zoom_speed);

  // redraw at new zoom level
  on_data_updated({});
}

static inline auto to_qcolor(color const& val) -> QColor
{
  return {val.r, val.g, val.b, val.a};
}

auto ascope_window::on_paint(QPaintEvent* event) -> void
{
  auto c_height = canvas_.height();
  auto c_width = canvas_.width();

  auto painter = QPainter(&canvas_);
  for (auto& rect : event->region())
  {
    auto image = QImage{rect.width(), rect.height(), QImage::Format::Format_RGB888};
    {
      auto lock = stage_->acquire_lock();

      auto nodata = to_qcolor(stage_->cmap_.missing());

      // hacky cope with color map
      #if 0
      auto range_scale = (stage_->cmap_.colours()[stage_->cmap_.colours().size()-1].first - stage_->cmap_.colours()[0].first) / (stage_->scale_max_ - stage_->scale_min_);
      auto range_offset = stage_->cmap_.colours()[0].first - (stage_->scale_min_ * range_scale);
      #endif

      auto d_height = stage_->data_.shape()[0];
      auto d_width = stage_->data_.shape()[1];

      for (auto y = 0; y < rect.height(); ++y)
      {
        auto cy = y + rect.y(); // canvas coordinate
        auto dy = int((((cy + 0.5f) / c_height) * d_height)); // data coordinate

        if (dy < 0 || dy >= d_height)
        {
          for (auto x = 0; x < rect.width(); ++x)
            image.setPixelColor(x, y, nodata);
          continue;
        }

        for (auto x = 0; x < rect.width(); ++x)
        {
          auto cx = x + rect.x(); // canvas coordinate
          auto dx = int((((cx + 0.5f) / c_width) * d_width)); // data coordinate

          if (dx < 0 || dx >= d_width)
          {
            image.setPixelColor(x, y, nodata);
            continue;
          }

          auto val = stage_->data_[dy, dx];
          image.setPixelColor(x, y, to_qcolor(stage_->cmap_.evaluate(val)));
          #if 0
            auto col = stage_->cmap_.evaluate((val * range_scale) + range_offset);
          #endif
        }
      }
    }
    painter.drawImage(rect, image);
  }
}

#include "view_moment.moc"
#endif
