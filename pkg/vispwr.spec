#-----------------------------------------------------------------------------------------------------------------------
# Visual Interactive Signal Processor for Weather Radar
# Copyright 2022 Mark Curtis
#--------------------------------------------------------------------------------------------------------------------*/

# the user MUST supply --define="src_path mypath" to tell us where the main source directory is
%{!?src_path: %define src_path %{nil}}

# default to version 0.0.0 (indicates a local developer build)
# our CI job supplies an official version number for tagged releases
%{!?version: %define version 0.0.0}

Name:          vispwr
Version:       %{version}
Release:       1%{?dist}
Summary:       Visual Interactive Signal Processor for Weather Radar
License:       Apache-2.0
URL:           https://gitlab.com/mcurtis/vispwr
Source0:       https://gitlab.com/mcurtis/vispwr/-/archive/master/vispwr-master.tar.gz
BuildRequires: cmake make gcc-c++
BuildRequires: fmt-devel fftw-devel qt6-qtbase-devel qt6-qtcharts-devel
BuildRequires: desktop-file-utils
Requires:      fmt fftw qt6-qtbase qt6-qtcharts

%description
TODO

%prep
%setup -c -T

%build
%cmake %{src_path} -DRELEASE_VERSION=%{version}
%cmake_build

%install
%cmake_install

%check

%files
%{_bindir}/vispwr
%{_datadir}/vispwr
%{_datadir}/icons/hicolor/scalable/apps/vispwr.svg
%{_datadir}/icons/hicolor/scalable/mimetypes/application-x-vispwr.svg
%{_datadir}/icons/Adwaita/scalable/mimetypes/application-x-vispwr.svg
%{_datadir}/mime/packages/vispwr.xml
%{_datadir}/applications/vispwr.desktop

%post
gtk-update-icon-cache %{_datadir}/icons/hicolor
gtk-update-icon-cache %{_datadir}/icons/Adwaita
update-mime-database %{_datadir}/mime
update-desktop-database %{_datadir}/applications
exit 0

%postun
gtk-update-icon-cache %{_datadir}/icons/hicolor
gtk-update-icon-cache %{_datadir}/icons/Adwaita
update-mime-database %{_datadir}/mime
update-desktop-database %{_datadir}/applications
exit 0

%changelog

