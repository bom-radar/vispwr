/*----------------------------------------------------------------------------------------------------------------------
 * Visual Interactive Signal Processor for Weather Radar
 * Copyright 2020 Mark Curtis
 *--------------------------------------------------------------------------------------------------------------------*/
#include "stage.h"

using namespace vispwr;

namespace vispwr
{
  class despeckle : public stage
  {
  protected:
    despeckle();
    auto metadata() const -> stage_metadata const& override;
    auto get_parameter_impl(string_view name) const -> parameter override;
    auto set_parameter_impl(string_view name, parameter_value value) -> void override;
    auto process(shared_ptr<moments> data, int input) -> void override;
    auto process(shared_ptr<moments const> data, int input) -> void override;

  private:
    shared_ptr<moments const> m1_;
    uint64_t next_id_;
  };
}

static constexpr auto plist = std::array<string_view, 0>{};
static constexpr auto ilist = std::array<input_meta, 1>
{{
  { "moments", data_type::moments, true, "Input moment data" }
}};
static constexpr auto olist = std::array<output_meta, 1>
{{
  { "moments", data_type::moments, "Output moment data" }
}};

static const auto desc = R"(
Simple despeckle filter.  Removes individual samples that have no valid neighbors in range only (1D kernel).

**Input:** Moments

**Output:** Moments
)";

static auto ireg = stage::enrol<despeckle>("despeckle", "Despeckle", "Moments", desc);

despeckle::despeckle()
  : stage{plist, ilist, olist}
  , next_id_{0}
{ }

auto despeckle::metadata() const -> stage_metadata const&
{
  return *ireg;
}

auto despeckle::get_parameter_impl(string_view name) const -> parameter
{
  return {};
}

auto despeckle::set_parameter_impl(string_view name, parameter_value value) -> void
{ }

auto despeckle::process(shared_ptr<moments> data, int input) -> void
{
  // sanity check
  if (data->data.shape()[0] > 1)
  {
    for (auto imom = 0uz; imom < data->data.shape()[1]; ++imom)
      if (!std::isnan(data->data[0, imom]) && std::isnan(data->data[1, imom]))
        data->data[0, imom] = std::numeric_limits<float>::quiet_NaN();
    for (auto igate = 1uz; igate < data->data.shape()[0] - 1; ++igate)
    {
      for (auto imom = 0uz; imom < data->data.shape()[1]; ++imom)
        if (!std::isnan(data->data[igate, imom]) && std::isnan(data->data[igate-1, imom]) && std::isnan(data->data[igate+1, imom]))
          data->data[igate, imom] = std::numeric_limits<float>::quiet_NaN();
    }
    for (auto imom = 0uz; imom < data->data.shape()[1]; ++imom)
      if (!std::isnan(data->data[data->data.shape()[0] - 1, imom]) && std::isnan(data->data[data->data.shape()[0] - 2, imom]))
        data->data[data->data.shape()[0] - 1, imom] = std::numeric_limits<float>::quiet_NaN();
  }

  send(0, std::move(data));
}

auto despeckle::process(shared_ptr<moments const> data, int input) -> void
{
  despeckle::process(make_shared<moments>(*data), input);
}
